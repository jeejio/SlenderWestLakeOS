#!/bin/bash
#本脚本用于更新静态库，将在intra分支文件夹编译得到的.a静态库文件复制到release分支文件夹。
#
##【情况1】.a静态库在本脚本中已存在
#1、分别下载intra和release分支代码，然后放置在在同一级目录下。例如：
#       ~/project
#         ├── repo_slenderwestlake_manhattan_release
#         └── repo_slenderwestlake_manhattan_intra
#2、编译repo_slenderwestlake_manhattan_intra代码
#3、修改repo_slenderwestlake_manhattan_intra/build/libexport.sh，将release_dir_name改为release文件夹的名称。例如本例为：
#       release_dir_name=repo_slenderwestlake_manhattan_release
#4、打开ubuntu终端，在repo_slenderwestlake_manhattan_intra/路径下执行libexport.sh脚本，脚本中的静态库将复制到release目录中的对应文件夹。
#       source build/libexport.sh
#5、在release文件夹下提交静态库文件到gerrit（仅需提交改动过代码的静态库）。
#
##【情况2】.a静态库在本脚本中不存在
#1、与【情况1】中第1、2、3步相同。
#2、修改新静态库所在目录的CmakeList.txt
#（1）删除idf_component_register(SRCS后面的.c文件列表
#（2）添加如下内容
#       set(LIBS libcloud)
#       add_library(${LIBS} STATIC IMPORTED)
#       set_property(TARGET ${LIBS} PROPERTY IMPORTED_LOCATION ${CMAKE_CURRENT_SOURCE_DIR}/${LIBS}.a)
#       target_link_libraries(${COMPONENT_LIB} INTERFACE ${LIBS})
#       set_property(TARGET ${LIBS} APPEND PROPERTY INTERFACE_LINK_LIBRARIES ${COMPONENT_LIB})
#（3）以framework/cloud/CMakeLists.txt为例：
#                 修改后                                                                                                                  修改前
#       idf_component_register(SRCS                                                                                            idf_component_register(SRCS
#                                                                                                                                  "maopao_cloud_http.c"
#           INCLUDE_DIRS "."                                                                                                   
#               "${jeejio_path}/external/cJSON"                                                                                    INCLUDE_DIRS "." 
#               "${jeejio_path}/external/webclient"                                                                                    "${jeejio_path}/external/cJSON"
#               "${jeejio_path}/framework/maopao"                                                                                      "${jeejio_path}/external/webclient"
#           REQUIRES external                                                                                                          "${jeejio_path}/framework/maopao"
#       )                                                                                                                          REQUIRES external
#                                                                                                                              )
#       set(LIBS libcloud)                                                                                                 
#       add_library(${LIBS} STATIC IMPORTED)                                                         
#       set_property(TARGET ${LIBS} PROPERTY IMPORTED_LOCATION ${CMAKE_CURRENT_SOURCE_DIR}/${LIBS}.a)
#       target_link_libraries(${COMPONENT_LIB} INTERFACE ${LIBS})                                    
#       set_property(TARGET ${LIBS} APPEND PROPERTY INTERFACE_LINK_LIBRARIES ${COMPONENT_LIB})      
#    
#   tips: 如果发现静态库中的自动初始化没有启动，则在CMakeLists.txt加上 target_link_libraries(${COMPONENT_LIB} INTERFACE "-u ${...}")，省略号处应填写静态库的自动初始化函数；如ota功能的自动初始化函数为vMaopaoOtaInit，则 在CMakeLists.txt加上 target_link_libraries(${COMPONENT_LIB} INTERFACE "-u vMaopaoOtaInit")
#3、参考下方脚本指令添加新静态库的cp指令到本脚本中，并检查头文件是否需要更新，如果需要更新则将头文件复制过来；release更新.a文件后编译不通过的问题大部分是头文件与.a文件不匹配。以framework/ota/为例：
#       cp $INTRA_PATH/out/target/esp32c3/jeejio/cloud/libcloud.a $RELEASE_PATH/framework/cloud/libcloud.a
#       check_header_file_diff framework/ota/
#4、删除release目录下对应文件夹中的.c文件。
#5、与【情况1】中第4、5步相同。


#更新release文件夹的名称
release_dir_name=repo_slenderwestlake_manhattan_release

export INTRA_PATH=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && cd ..; pwd )
export RELEASE_PATH=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && cd ../../$release_dir_name; pwd )

function append_all_header_file() {

    for file in `ls -1 $INTRA_PATH/$1`
    do
        if [ "${file##*.}" = "h" ]; then
            RELATIVE_PATH+=($1/$file)
        fi;
        if [ -d "$INTRA_PATH/$1/$file" ]; then
            append_all_header_file $1/$file
        fi;
    done
}

function update_header_file() {
    if [ ! "$1" ]; then
        echo "[failed] no relative path input into function<check_header_file_diff>"
        return 1
    fi
    local RELATIVE_PATH=()

    # 判断是否是.h结尾，如果不是.h结尾则认为传入参数是目录，获取目录下的.h文件
    if [[ "$1" =~ \.h$ ]]; then
        RELEASE_PATH+=($1)
    elif [ -d "$INTRA_PATH/$1" ]; then
        append_all_header_file $1
    fi
    
    # 检查每个头文件，是否intra分支和release分支是否没有同步内容，是则同步
    for file in ${RELATIVE_PATH[@]}
    do
        if [ ! -f $RELEASE_PATH/$file ]; then
            if [ ! -f $INTRA_PATH/$file ]; then
                echo "[failed] file $INTRA_PATH/$file in branch<intra> is not exist"
                return 1
            fi
            cp $INTRA_PATH/$file $RELEASE_PATH/$file
            continue
        fi
        local RESULT=$(diff -p $INTRA_PATH/$file $RELEASE_PATH/$file)
        if [ "$RESULT" ];then 
            echo "$file between branch<intra> and branch<release> is inconsistent"
            
            if [ ! -f $INTRA_PATH/$file ]; then
                echo "[failed] file $INTRA_PATH/$file in branch<intra> is not exist"
                return 1
            fi
            cp $INTRA_PATH/$file $RELEASE_PATH/$file
        fi
    done

    return 0
}

#将在intra分支文件夹编译得到的.a静态库文件复制到release分支文件夹
cp $INTRA_PATH/out/target/esp32c3/jeejio/hal/libhal.a $RELEASE_PATH/hal/libhal.a 
update_header_file /hal/include
cp $INTRA_PATH/out/target/esp32c3/jeejio/cloud/libcloud.a $RELEASE_PATH/framework/cloud/libcloud.a 
update_header_file /framework/cloud/
cp $INTRA_PATH/out/target/esp32c3/jeejio/factory_test/libfactory_test.a $RELEASE_PATH/framework/factory_test/libfactory_test.a 
update_header_file /framework/factory_test/
cp $INTRA_PATH/out/target/esp32c3/jeejio/maopao/libmaopao.a $RELEASE_PATH/framework/maopao/libmaopao.a 
update_header_file /framework/maopao/
cp $INTRA_PATH/out/target/esp32c3/jeejio/ota/libota.a $RELEASE_PATH/framework/ota/libota.a 
update_header_file /framework/ota/
cp $INTRA_PATH/out/target/esp32c3/jeejio/authentication/libauthentication.a $RELEASE_PATH/framework/authentication/libauthentication.a
update_header_file /framework/authentication
cp $INTRA_PATH/out/target/esp32c3/jeejio/connectivity/libconnectivity.a $RELEASE_PATH/framework/connectivity/libconnectivity.a
update_header_file /framework/connectivity/
cp $INTRA_PATH/out/target/esp32c3/jeejio/kernel/libkernel.a $RELEASE_PATH/kernel/libkernel.a 
cp $INTRA_PATH/out/target/esp32c3/jeejio/pthread/libpthread.a $RELEASE_PATH/kernel/lib/pthread/libpthread.a 
cp $INTRA_PATH/out/target/esp32c3/jeejio/ringbuf/libringbuf.a $RELEASE_PATH/kernel/lib/ringbuf/libringbuf.a
cp $INTRA_PATH/out/target/esp32c3/jeejio/mbedtls/libmbedtls.a $RELEASE_PATH/kernel/lib/mbedtls/libmbedtls.a
update_header_file /kernel/

# 将代码生成工具同样进行迁移
cp -r $INTRA_PATH/tools/mprpcgen/example $RELEASE_PATH/tools/mprpcgen/example
cp -r $INTRA_PATH/tools/mprpcgen/release $RELEASE_PATH/tools/mprpcgen/release

echo "libexport to $release_dir_name success"

