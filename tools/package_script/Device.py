from typing import List


class Device(object):
    def __init__(self, category, deviceName, deviceNameCN,
                 manufacturer=None, apiVersion="v0.0.1",
                 developer=None, committer=None,
                 driverPath=None, talPath=None, appPath=None,
                 bugFixed=None, newFunction=None, remark=None, tags=[]):
        self.category = category
        self.deviceName = deviceName
        self.deviceNameCN = deviceNameCN
        self.manufacturer = manufacturer
        self.apiVersion = apiVersion
        self.developer = developer
        self.committer = committer
        self.driverPath = driverPath
        self.talPath = talPath
        self.appPath = appPath
        self.bugFixed = bugFixed
        self.newFunction = newFunction
        self.remark = remark
        self.tags = tags

        # 自动前补0至9位
        if len(category) < 9:
            self.category = "0" * (9 - len(category)) + category

        # 根据分号分离标签
        if tags:
            self.tags = tags.split(";")
        else:
            self.tags = []

    def __str__(self):
        return f"{self.category} {self.deviceName}({self.deviceNameCN})"


def get_all_tags_from_devices(devices: List[Device]):
    tags = set()
    for device in devices:
        if device.tags:
            tags.update(device.tags)
    print(tags)
    return tags


def find_device_by_name(devices: List[Device], deviceName: str):
    for device in devices:
        if device.deviceName == deviceName:
            return device
    return None


def find_device_by_name_cn(devices: List[Device], deviceNameCN: str):
    for device in devices:
        if device.deviceNameCN == deviceNameCN:
            return device
    return None


def find_device_by_category(devices: List[Device], category: str):
    for device in devices:
        if device.category == category:
            return device
    return devices


def read_devices_from_csv(csv_path: str):
    import csv
    devices = []
    with open(csv_path, "r", encoding="utf-8-sig") as f:
        reader = csv.DictReader(f)
        for row in reader:
            device_obj = Device(**row)
            devices.append(device_obj)
    return devices
