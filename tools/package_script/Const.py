import datetime
import os

bin_infos = [
    {
        "address": "0x0",
        "src_path": "bootloader/bootloader.bin",
        "dst_path": "bootloader.bin"
    },
    {
        "address": "0x10000",
        "src_path": "SlenderWestLakeOS.bin",
        "dst_path": "SlenderWestLakeOS.bin"
    },
    {
        "address": "0x8000",
        "src_path": "partition_table/partition-table.bin",
        "dst_path": "partition-table.bin"
    },
    {
        "address": "0xd000",
        "src_path": "ota_data_initial.bin",
        "dst_path": "ota_data_initial.bin"
    },
    {
        "address": "0x311000",
        "src_path": "storage.bin",
        "dst_path": "storage.bin"
    },
]

common_info = {
    "日期": datetime.datetime.now().strftime("%Y-%m-%d_%H:%M:%S"),
    "项目": "曼哈顿",
    "分支": "slenderwestlake_manhattan_intra",
    "测试环境": "DEV"
}

file_path = os.path.abspath(__file__)
package_path = os.path.dirname(file_path)
python_lib_path = os.path.join(package_path, "python_lib")
project_path = os.path.dirname(os.path.dirname(package_path))

targetPath = os.path.join(project_path, "out", "target", "esp32c3")
talPath = os.path.join(project_path, "tal")
driverPath = os.path.join(project_path, "driver", "jeejioxie_esp32c3", "board")
appPath = os.path.join(project_path, "application")

savePath = os.path.join(package_path, "output")
saveBinPath = os.path.join(savePath, "bin")

csvFilePath = os.path.join(talPath, "build", "deviceList.csv")
configPath = os.path.join(package_path, "config")
pyqtConfigFilePath = os.path.join(configPath, "pyqtConfig.ini")
kConfigMenuFilePath = os.path.join(package_path, "config", "main.kconfig")
kConfigConfigFilePath = os.path.join(package_path, "config", "package_config")
