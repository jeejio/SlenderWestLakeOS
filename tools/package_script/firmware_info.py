import os
import datetime
import sys
from Const import *
from PackageConfig import PackageConfig

sys.path.append(python_lib_path)

has_git = True
try:
    from git import Repo


    def get_git_dir_list(project_path: str):
        result = []
        fixture_dir = ["driver", "external", "framework", "hal", "kernel", "tal"]
        dir_queue = []
        for base_dir in fixture_dir:
            dir_queue.append(os.path.join(project_path, base_dir))
            while len(dir_queue) > 0:
                now_dir = dir_queue.pop()
                if ".git" in os.listdir(now_dir):
                    result.append(now_dir)
                for name in os.listdir(now_dir):
                    if name == ".git" and os.path.isdir(os.path.join(now_dir, name)):
                        continue
                    if os.path.isdir(os.path.join(now_dir, name)):
                        dir_queue.append(os.path.join(now_dir, name))
        return result


    def get_git_log(path: str):
        repo = Repo(path)
        # log_str = repo.git.log("--pretty=format:%H\n    %s\n###")
        log_str = repo.git.log("--pretty=format:%H ###")
        log_str = log_str.split("###\n")[0]
        return log_str


    def get_all_git_log(project_name: str):
        path_list = get_git_dir_list(project_name)
        result = ""
        for path in path_list:
            relative_path = os.path.relpath(path, project_path)
            result += f"    {relative_path}: {get_git_log(path)}\n"
        return result

except ImportError:
    print("no git module")
    has_git = False


    def get_git_dir_list(project_path: str):
        return ""


    def get_git_log(path: str):
        return ""


    def get_all_git_log(project_name: str):
        return ""

info_list = ["日期", "项目", "测试环境"]

device_info_key_list = [
    {"en": "deviceNameCN", "cn": "设备名称"},
    {"en": "deviceName", "cn": "设备名称(英文)"},
    {"en": "apiVersion", "cn": "版本"},
    {"en": "bugFixed", "cn": "修复bug"},
    {"en": "newFunction", "cn": "新增功能"},
]


def get_bin_info_str(bin_infos):
    bin_info_str = ""
    for bin_info in bin_infos:
        bin_info_str += f"    {bin_info['address']}: {bin_info['dst_path']}\n"
    return bin_info_str


def generate_firmware_info(package_config: PackageConfig, project_path: str):
    info_str = ""
    for info_key in info_list:
        if info_key in common_info:
            info_str += f"{info_key}: {common_info[info_key]}\n\n"
    if package_config.device:
        for device_info_key in device_info_key_list:
            info_str += f"{device_info_key['cn']}: {package_config.device.__getattribute__(device_info_key['en'])}\n\n"

    info_str += f"分支:\n{get_all_git_log(project_path)}\n\n"
    info_str += f"下载地址:\n{get_bin_info_str(bin_infos)}"

    return info_str
