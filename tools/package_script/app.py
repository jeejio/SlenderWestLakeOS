# @Author  : QZM
# @Time    : 2023/7/1 11:27
import datetime
import os
import sys
from typing import List

# TODO 生成api列表
# TODO 查看冒泡文件

from PyQt5.QtCore import Qt, QTimer, QProcess, QSettings, QThread, pyqtSignal, QStandardPaths
from PyQt5.QtGui import QColor
from PyQt5.QtWidgets import QApplication, QWidget, QVBoxLayout, QTabWidget, QLabel, QLineEdit, QHBoxLayout, QCheckBox, \
    QMessageBox, QTableWidget, QTableWidgetItem, QHeaderView, QFormLayout, QListWidget, QPushButton, QPlainTextEdit, \
    QListWidgetItem, QGridLayout, QGroupBox, QRadioButton, QFileDialog, QInputDialog

sys.path.append(os.path.abspath(os.path.dirname(__file__)))
from Device import Device, read_devices_from_csv, get_all_tags_from_devices, find_device_by_category
from Const import csvFilePath, pyqtConfigFilePath, project_path
from PackageConfig import PackageConfig
from package import build, flash_only, package_only, command
from Command import get_command_by_platform


class PackageWork(object):
    def __init__(self, package_config: PackageConfig, opt="package"):
        self.package_config = package_config
        self.opt = opt

    def start(self):
        print(f"start {self.opt} {self.package_config}")

    def __str__(self):
        return f"PackageWork(package_config={self.package_config})"


# 创建线程类
class CommandThread(QThread):
    command_finished = pyqtSignal(bool)

    def __init__(self, parent=None):
        super(CommandThread, self).__init__(parent)
        self.commands = []
        self.callback = None
        self.result = 0

    def run(self):
        # 执行命令
        print("run", self.commands)
        import subprocess
        result = subprocess.run("&&".join(self.commands), cwd=project_path, shell=True, executable=command.executable)
        self.result = result
        success = (result.returncode == 0)
        self.command_finished.emit(success)


class FileWindow(QWidget):
    def __init__(self, parent=None):
        super().__init__(parent)

        self.setWindowTitle("File Window")
        layout = QVBoxLayout()
        self.label = QLabel(self)
        layout.addWidget(self.label)
        self.setLayout(layout)

    def set_filepath(self, filepath):
        if os.path.isfile(filepath):
            with open(filepath, "r", encoding="utf-8") as file:
                content = file.read()
                print(content)
                self.label.setText(content)
        else:
            print("文件不存在")
            self.label.setText("文件不存在")


class MainWindow(QWidget):
    def __init__(self, ):
        super().__init__()

        self.settings = QSettings(pyqtConfigFilePath, QSettings.IniFormat)
        self.name_rule = "package"
        self.package_works = []
        self.process_running = False
        self.action_button = {
            "build": {"name": "编译", "opts": ["build"], "tip": "根据上面选择逐项编译代码",
                      "i": 0, "j": 0,
                      "clicked_cb": self.action_button_clicked,
                      "show": ["system", "device"]},
            "build_package": {"name": "编译+打包", "opts": ["build", "package"], "tip": "根据上面选择编译代码并打包",
                              "i": 0, "j": 1,
                              "clicked_cb": self.action_button_clicked,
                              "show": ["system", "device"]},
            "build_flash": {"name": "编译+烧录", "opts": ["build", "flash"],
                            "tip": "根据上面选择编译代码并烧录到设备中",
                            "i": 1, "j": 0,
                            "clicked_cb": self.action_button_clicked,
                            "show": ["system", "device"]},
            "build_package_flash": {"name": "编译+打包+烧录", "opts": ["build", "package", "flash"],
                                    "tip": "根据上面选择编译代码并打包并烧录到设备中", "i": 1, "j": 1,
                                    "clicked_cb": self.action_button_clicked,
                                    "show": ["system", "device"]},
            "flash": {"name": "纯烧录", "opts": ["flash"], "tip": "烧录out文件夹下固件或zip包", "i": 2, "j": 0,
                      "clicked_cb": self.action_button_clicked,
                      "show": ["system", "device", "zip"]},
            "package": {"name": "纯打包", "opts": ["package"], "tip": "打包out文件夹下固件", "i": 2, "j": 1,
                        "clicked_cb": self.action_button_clicked,
                        "show": ["system", "device"]},
            "show_maopao": {"name": "显示冒泡文件", "opts": [], "tip": "显示tal下.maopao文件",
                            "i": 0, "j": 2,
                            "clicked_cb": self.show_maopao_file_button_clicked,
                            "show": ["device"]},
            "generate_api_list": {"name": "生成api列表excel", "opts": [], "tip": "通过.maopao文件生成api列表excel",
                                  "i": 1, "j": 2,
                                  "clicked_cb": self.generate_api_list_button_clicked,
                                  "show": ["device"]},
            "generate_tal_code": {"name": "重新生成TAL代码", "opts": [], "tip": "重新生成tal代码", "i": 2, "j": 2,
                                  "clicked_cb": self.generate_tal_code_button_clicked,
                                  "show": ["device"]},

        }

        self.tool_button = {
            "flash_key": {"name": "烧录密钥", "tip": "烧录密钥",
                          "i": 0, "j": 0,
                          "clicked_cb": self.flash_key_button_clicked},
            "read_key": {"name": "读取密钥", "tip": "读取密钥",
                         "i": 0, "j": 1,
                         "clicked_cb": self.read_key_button_clicked},
            "erase_flash": {"name": "擦除flash", "tip": "擦除flash",
                            "i": 1, "j": 0,
                            "clicked_cb": self.erase_flash_button_clicked},
        }

        self.filter_device_condition = {"category": None, "deviceName": None, "deviceNameCN": None,
                                        "tags": self.settings.value("filter_tags", [], type=list)}  # 添加设备筛选条件属性
        self.all_devices = read_devices_from_csv(csvFilePath)
        self.selected_devices = set()

        def init_selected_devices():
            self.selected_devices = set()
            selected_devices_category = self.settings.value("selected_devices_category", [], type=list)
            for category in selected_devices_category:
                self.selected_devices.add(find_device_by_category(self.all_devices, category))

        init_selected_devices()

        self.command_thread = CommandThread()
        self.command_thread.command_finished.connect(self.on_command_finished)

        self.file_window = FileWindow(None)
        self.filter_device_table = QTableWidget()
        self.file_list = QListWidget(self)
        self.log_box = QPlainTextEdit(self)
        self.timer = QTimer(self)
        self.stop_button = QPushButton("停止(功能还没做，点了没用)", self)
        self.process = QProcess()

        self.setWindowTitle("批量打包工具")
        self.resize(800, 600)

        self.tab_widget = QTabWidget()

        self.setup_project_config_tab()
        self.setup_device_filter_tab(self.all_devices)
        self.setup_action_tab()
        self.setup_tool_tab()

        self.process.setProcessChannelMode(QProcess.MergedChannels)
        self.process.readyReadStandardOutput.connect(self.read_output)
        self.process.finished.connect(self.process_finished)
        self.process.setWorkingDirectory(project_path)

        main_layout = QVBoxLayout()
        main_layout.addWidget(self.tab_widget)
        self.setLayout(main_layout)

    def on_command_finished(self, success):
        # 命令执行完成处理
        if success:
            print("命令执行成功")
            self.log_box.appendPlainText("命令执行成功")
            self.update_log_box()
            self.change_package_work()
        else:
            print("命令执行失败")
            self.log_box.appendPlainText("命令执行失败")
            self.update_log_box()
            self.package_works = []
            self.finish_package_work()

    def setup_project_config_tab(self):
        project_config_tab = QWidget()
        layout = QVBoxLayout()

        project_layout = QHBoxLayout()
        project_widget = QWidget()
        project_widget.setLayout(project_layout)

        if self.settings.value("project") is None:
            self.settings.setValue("project", "曼哈顿")

        def generate_name_rule():
            name_rule = ""
            if self.settings.value("choice_project", True, type=bool):
                name_rule += "{project}_"
            if self.settings.value("choice_device_name", True, type=bool):
                name_rule += "{deviceNameCN}_"
            if self.settings.value("choice_version", True, type=bool):
                name_rule += "{version}_"
            if self.settings.value("choice_env", True, type=bool):
                name_rule += "{env}_"
            if self.settings.value("choice_date", True, type=bool):
                name_rule += "{date}_"
            name_rule = name_rule.rstrip("_")
            if name_rule == "":
                name_rule = "package"
            self.settings.setValue("name_rule", name_rule)
            self.name_rule = name_rule
            name_rule_label.setText("命名规则: " + name_rule)
            name_example_label.setText(
                "命名示例文字: " +
                name_rule.format(project=self.settings.value("project"),
                                 deviceNameCN="蜂鸣器",
                                 version="1.0.0",
                                 env="DEV",
                                 date=datetime.datetime.now().strftime("%Y%m%d_%H%M%S")
                                 ) +
                ".zip"
            )

        def project_name_changed(text):
            self.settings.setValue("project", text)
            generate_name_rule()

        project_name_label = QLabel("项目名:")
        project_name_edit = QLineEdit()
        project_name_edit.setText(self.settings.value("project"))
        project_name_edit.textChanged.connect(lambda text: project_name_changed(text))
        project_layout.addWidget(project_name_label)
        project_layout.addWidget(project_name_edit)

        layout.addWidget(project_widget)

        package_choice_layout = QVBoxLayout()
        package_choice_layout.setAlignment(Qt.AlignLeft)
        package_choice_widget = QGroupBox("标题包名配置")
        package_choice_widget.setLayout(package_choice_layout)

        form_layout = QFormLayout()

        project_checkbox = QCheckBox("项目名")
        device_checkbox = QCheckBox("设备名")
        version_checkbox = QCheckBox("版本号")
        environment_checkbox = QCheckBox("环境")
        date_checkbox = QCheckBox("日期")
        form_layout.addWidget(project_checkbox)
        form_layout.addWidget(device_checkbox)
        form_layout.addWidget(version_checkbox)
        form_layout.addWidget(environment_checkbox)
        form_layout.addWidget(date_checkbox)

        name_rule_label = QLabel("命名规则:")
        name_example_label = QLabel("命名示例文字:")

        form_layout.addWidget(name_rule_label)
        form_layout.addWidget(name_example_label)
        package_choice_layout.addLayout(form_layout)

        layout.addWidget(package_choice_widget)

        project_checkbox.setChecked(self.settings.value("choice_project", True, type=bool))
        device_checkbox.setChecked(self.settings.value("choice_device_name", True, type=bool))
        version_checkbox.setChecked(self.settings.value("choice_version", True, type=bool))
        environment_checkbox.setChecked(self.settings.value("choice_env", True, type=bool))
        date_checkbox.setChecked(self.settings.value("choice_date", True, type=bool))

        def name_rule_changed(choice, state):
            self.settings.setValue(choice, state)
            generate_name_rule()

        project_checkbox.stateChanged.connect(lambda state: name_rule_changed("choice_project", state))
        device_checkbox.stateChanged.connect(lambda state: name_rule_changed("choice_device_name", state))
        version_checkbox.stateChanged.connect(lambda state: name_rule_changed("choice_version", state))
        environment_checkbox.stateChanged.connect(lambda state: name_rule_changed("choice_env", state))
        date_checkbox.stateChanged.connect(lambda state: name_rule_changed("choice_date", state))

        project_config_tab.setLayout(layout)
        self.tab_widget.addTab(project_config_tab, "项目配置")
        generate_name_rule()

    def setup_device_filter_tab(self, devices):
        device_filter_tab = QWidget()
        layout = QVBoxLayout()

        filter_layout = QHBoxLayout()

        # 创建左侧的筛选条件部分
        form_layout = QFormLayout()
        category_edit = QLineEdit()
        category_edit.setProperty("key", "category")
        category_edit.textChanged.connect(self.filter_device_changed)

        device_name_edit = QLineEdit()
        device_name_edit.setProperty("key", "deviceName")
        device_name_edit.textChanged.connect(self.filter_device_changed)

        device_name_cn_edit = QLineEdit()
        device_name_cn_edit.setProperty("key", "deviceNameCN")
        device_name_cn_edit.textChanged.connect(self.filter_device_changed)

        form_layout.addRow("品类码:", category_edit)
        form_layout.addRow("英文名:", device_name_edit)
        form_layout.addRow("中文名:", device_name_cn_edit)
        filter_layout.addLayout(form_layout)

        tags = get_all_tags_from_devices(devices)

        tag_table = QTableWidget()
        tag_table.setFixedHeight(100)
        tag_table.setColumnCount(1)
        tag_table.setHorizontalHeaderLabels(["筛选tags"])
        tag_table.setRowCount(len(tags))
        filter_layout.addWidget(tag_table)

        header = tag_table.horizontalHeader()
        header.setSectionResizeMode(0, QHeaderView.Stretch)

        for row, tag in enumerate(tags):
            checkbox = QCheckBox()
            checkbox.setText(tag)
            checkbox.setChecked(tag in self.filter_device_condition["tags"])
            checkbox.setProperty("tag", tag)
            checkbox.stateChanged.connect(self.filter_tag_checkbox_state_changed)
            tag_table.setCellWidget(row, 0, checkbox)

        # 将筛选条件布局和设备表格添加到整体布局中
        layout.addLayout(filter_layout)

        device_table = self.filter_device_table

        device_table.setColumnCount(5)
        device_table.setHorizontalHeaderLabels(["", "Category", "Device Name", "Device NameCN", "Tags"])
        device_table.horizontalHeader().sectionClicked.connect(self.toggle_filter_device_table_header)
        device_table.setRowCount(len(devices))

        header = device_table.horizontalHeader()
        header.setSectionResizeMode(0, QHeaderView.ResizeToContents)
        header.setSectionResizeMode(1, QHeaderView.ResizeToContents)
        header.setSectionResizeMode(2, QHeaderView.ResizeToContents)
        header.setSectionResizeMode(3, QHeaderView.ResizeToContents)
        header.setSectionResizeMode(4, QHeaderView.Stretch)

        for row, device in enumerate(devices):
            checkbox = QCheckBox()
            checkbox.setChecked(False)
            checkbox.setProperty("device", device)
            checkbox.setProperty("row", row)
            checkbox.stateChanged.connect(self.filter_device_checkbox_state_changed)
            device_table.setCellWidget(row, 0, checkbox)

            category_item = QTableWidgetItem(device.category)
            device_table.setItem(row, 1, category_item)

            device_name_item = QTableWidgetItem(device.deviceName)
            device_table.setItem(row, 2, device_name_item)

            device_name_cn_item = QTableWidgetItem(device.deviceNameCN)
            device_table.setItem(row, 3, device_name_cn_item)

            tags_item = QTableWidgetItem(", ".join(device.tags))
            device_table.setItem(row, 4, tags_item)

            if device in self.selected_devices:
                checkbox.setChecked(True)
                self.color_selected_row(device_table, row, True)

        def toggle_checkbox(row, column):
            if column:
                checkbox = device_table.cellWidget(row, 0)
                if isinstance(checkbox, QCheckBox):
                    checkbox.setChecked(not checkbox.isChecked())

        device_table.cellClicked.connect(toggle_checkbox)
        layout.addWidget(device_table)

        device_filter_tab.setLayout(layout)
        self.tab_widget.addTab(device_filter_tab, "关注设备")

        self.filter_device_table_by_condition()

    def filter_device_changed(self, text):
        print(self.sender().property("key"), text)
        self.filter_device_condition[self.sender().property("key")] = text
        self.filter_device_table_by_condition()

    def filter_tag_checkbox_state_changed(self, state):
        checkbox = self.sender()
        if isinstance(checkbox, QCheckBox):
            tag = checkbox.property("tag")
            if state == Qt.Checked:
                print("Checked: ", tag)
            else:
                print("Unchecked: ", tag)
            tag_table = checkbox.parent().parent()
            device_table = self.filter_device_table
            if isinstance(tag_table, QTableWidget) and isinstance(device_table, QTableWidget):
                selected_tags = []
                for row in range(tag_table.rowCount()):
                    checkbox = tag_table.cellWidget(row, 0)
                    if isinstance(checkbox, QCheckBox) and checkbox.isChecked():
                        selected_tags.append(checkbox.text())
                # print(selected_tags)
                self.filter_device_condition["tags"] = selected_tags
                self.settings.setValue("filter_tags", self.filter_device_condition["tags"])
                self.filter_device_table_by_condition()

    def filter_device_table_by_condition(self):
        device_table = self.filter_device_table
        condition = self.filter_device_condition
        for row in range(device_table.rowCount()):
            device = device_table.cellWidget(row, 0).property("device")
            if device is not None:
                hidden = False
                selected_tags = condition["tags"]
                if condition["category"] and condition["category"] not in device.category:
                    hidden = True
                if condition["deviceName"] and condition["deviceName"] not in device.deviceName:
                    hidden = True
                if condition["deviceNameCN"] and condition["deviceNameCN"] not in device.deviceNameCN:
                    hidden = True
                if selected_tags and not (set(selected_tags) & set(device.tags)):
                    hidden = True
                device_table.setRowHidden(row, hidden)

    def filter_device_checkbox_state_changed(self, state):
        checkbox = self.sender()
        if isinstance(checkbox, QCheckBox):
            device = checkbox.property("device")
            if state == Qt.Checked:
                print("Checked: ", device)
            else:
                print("Unchecked: ", device)

            if checkbox.isChecked():
                self.selected_devices.add(device)
            else:
                self.selected_devices.remove(device)
            self.save_selected_devices()
            self.update_file_list()

            table_widget = checkbox.parent().parent()  # 获取表格部件
            if isinstance(table_widget, QTableWidget):
                self.color_selected_row(table_widget, checkbox.property("row"), checkbox.isChecked())

    def save_selected_devices(self):
        selected_devices_category = []
        for device in self.selected_devices:
            selected_devices_category.append(device.category)
        self.settings.setValue("selected_devices_category", selected_devices_category)

    def color_selected_row(self, table_widget, row, isChecked):
        print("color_selected_row", row, isChecked)
        for column in range(table_widget.columnCount()):
            item = table_widget.item(row, column)
            if item is not None:
                if isChecked:
                    item.setBackground(QColor(184, 226, 194))
                else:
                    item.setBackground(QColor(255, 255, 255))

    def toggle_filter_device_table_header(self, index):
        if index == 0:  # 点击的是第一个列的标题栏
            device_table = self.filter_device_table
            check_num = 0
            for row in range(device_table.rowCount()):
                checkbox = device_table.cellWidget(row, 0)
                if isinstance(checkbox, QCheckBox) and not device_table.isRowHidden(row):
                    if checkbox.isChecked():
                        check_num += 1
            print(check_num)

            for row in range(device_table.rowCount()):
                checkbox = device_table.cellWidget(row, 0)
                if isinstance(checkbox, QCheckBox) and not device_table.isRowHidden(row):
                    checkbox.setChecked(check_num == 0)

    def setup_action_tab(self):
        action_tab = QWidget()
        layout = QHBoxLayout(action_tab)

        # 左侧部分布局
        left_layout = QVBoxLayout()

        # 单选框组
        group_box = QGroupBox("选择类型", self)

        # 单选框布局
        radio_layout = QVBoxLayout()

        if self.settings.value("action_mode") is None:
            self.settings.setValue("action_mode", "device")

        # 系统代码单选框
        system_code_radio = QRadioButton("系统代码", self)
        system_code_radio.setProperty("mode", "system")
        if self.settings.value("action_mode") == "system":
            system_code_radio.setChecked(True)
        system_code_radio.toggled.connect(self.action_mode_changed_cb)
        radio_layout.addWidget(system_code_radio)

        # 设备代码单选框
        device_code_radio = QRadioButton("设备代码", self)
        device_code_radio.setProperty("mode", "device")
        if self.settings.value("action_mode") == "device":
            device_code_radio.setChecked(True)
        device_code_radio.toggled.connect(self.action_mode_changed_cb)
        radio_layout.addWidget(device_code_radio)

        # ZIP包单选框
        zip_radio = QRadioButton("使用现有ZIP包(output目录下)", self)
        zip_radio.setProperty("mode", "zip")
        if self.settings.value("action_mode") == "zip":
            zip_radio.setChecked(True)
        zip_radio.toggled.connect(self.action_mode_changed_cb)
        radio_layout.addWidget(zip_radio)

        # 将单选框添加到单选框组
        group_box.setLayout(radio_layout)

        # 将单选框组添加到左侧布局
        left_layout.addWidget(group_box)

        # 文件列表控件
        file_list_layout = QVBoxLayout()
        # self.file_list.setFixedHeight(400)
        self.file_list.setVerticalScrollBarPolicy(Qt.ScrollBarAsNeeded)
        self.file_list.setSelectionMode(QListWidget.ExtendedSelection)
        file_list_layout.addWidget(self.file_list)
        file_list_layout.setAlignment(Qt.AlignTop)
        left_layout.addLayout(file_list_layout)

        # 按钮布局

        button_layout = QGridLayout()
        action_button = self.action_button
        for button_key in action_button:
            button_config = action_button[button_key]
            button = QPushButton(button_config["name"], self)
            button.setToolTip(button_config["tip"])
            button.clicked.connect(button_config["clicked_cb"])
            button.setProperty("opts", button_config["opts"])
            button_config["button"] = button
            button_layout.addWidget(button, button_config["i"], button_config["j"])
        left_layout.addLayout(button_layout)

        layout.addLayout(left_layout)

        # 右侧部分布局
        right_layout = QVBoxLayout()

        # 日志框
        log_box_layout = QVBoxLayout()
        log_box_layout.addWidget(self.log_box)
        right_layout.addLayout(log_box_layout)

        # 停止按钮
        self.stop_button.setEnabled(False)
        self.stop_button.clicked.connect(self.stop_process)
        right_layout.addWidget(self.stop_button)

        layout.addLayout(right_layout)

        # 创建定时器
        self.timer.setInterval(100)
        self.timer.timeout.connect(self.update_log_box)

        self.tab_widget.addTab(action_tab, "操作页")

    def action_mode_changed_cb(self):
        radio_button = self.sender()
        if isinstance(radio_button, QRadioButton):
            if not radio_button.isChecked():
                return
            mode = radio_button.property("mode")
            self.settings.setValue("action_mode", mode)
            self.update_file_list()
            self.update_button_status()

    def update_file_list(self):
        self.file_list.clear()

        if self.settings.value("action_mode") == "system":
            item = QListWidgetItem("只烧录系统代码，无需选择设备")
            self.file_list.addItem(item)
        elif self.settings.value("action_mode", "device") == "device":
            for device in self.selected_devices:
                item = QListWidgetItem(device.deviceNameCN)
                item.setData(Qt.UserRole, device)
                self.file_list.addItem(item)
        elif self.settings.value("action_mode") == "zip":
            from Const import savePath
            if os.path.isdir(savePath):
                zip_files = [os.path.join(savePath, file) for file in os.listdir(savePath) if file.endswith(".zip")]
                zip_files.sort(key=lambda file: os.path.getmtime(file), reverse=True)
                for zip_file in zip_files:
                    item = QListWidgetItem(os.path.basename(zip_file))
                    item.setData(Qt.UserRole, zip_file)
                    self.file_list.addItem(item)

    def update_button_status(self):
        is_process_running = self.process and self.process_running

        self.stop_button.setEnabled(not is_process_running)
        for button_key in self.action_button:
            button_config = self.action_button[button_key]
            if is_process_running:
                button_config["button"].setEnabled(False)
            else:
                action_mode = self.settings.value("action_mode")
                button_config["button"].setEnabled(action_mode in button_config["show"])

    def get_selected_data(self):
        selected_items = self.file_list.selectedItems()
        if not selected_items:
            return []
        result = []
        for item in selected_items:
            result.append(item.data(Qt.UserRole))
        return result

    def show_complete_dialog(self, text="Operation completed successfully."):
        QMessageBox.information(self, 'Complete', text)

    def show_error_message_box(self, text=""):
        message_box = QMessageBox()
        message_box.setIcon(QMessageBox.Critical)
        message_box.setWindowTitle("错误")
        message_box.setText(text)
        message_box.setStandardButtons(QMessageBox.Ok)

        # 显示错误框并等待用户响应
        message_box.exec_()
        message_box.destroy()

    def show_warning_message_box(self, text="需选择一个设备"):
        message_box = QMessageBox()
        message_box.setIcon(QMessageBox.Warning)
        message_box.setWindowTitle("警告")
        message_box.setText(text)
        message_box.setStandardButtons(QMessageBox.Ok)

        # 显示警告框并等待用户响应
        message_box.exec_()
        message_box.destroy()

    def set_choice_message_box(self, title, text):
        reply = QMessageBox.question(self, title, text,
                                     QMessageBox.Yes | QMessageBox.No)
        return reply == QMessageBox.Yes

    def action_button_clicked(self):
        button = self.sender()
        if not isinstance(button, QPushButton):
            return
        # button_text = button.text()
        # print(button_text)
        action_mode = self.settings.value("action_mode")
        opts = button.property("opts")
        print(opts)
        works = []

        if action_mode == "system":
            # 纯系统模式
            works = [
                PackageWork(PackageConfig(project=self.settings.value("project"), name=self.name_rule), opt)
                for opt in opts
            ]
        elif action_mode == "device":
            selected_data = self.get_selected_data()
            if not selected_data:
                # 没有选择设备
                if opts != ["flash"]:
                    # 纯烧录可不选择设备，其他情况要选择
                    self.show_warning_message_box()
                    return
                else:
                    works = [
                        PackageWork(PackageConfig(project=self.settings.value("project"), name=self.name_rule), opt)
                        for opt in opts
                    ]
            else:
                for data in selected_data:
                    if isinstance(data, Device):
                        device = data
                        works += [PackageWork(
                            PackageConfig(
                                project=self.settings.value("project"),
                                name=self.name_rule,
                                device=device),
                            opt
                        ) for opt in opts]
        elif action_mode == "zip":
            selected_data = self.get_selected_data()
            if not selected_data:
                self.show_warning_message_box()
                return
            else:
                for data in selected_data:
                    if isinstance(data, str):
                        zip_filepath = data
                        works += [PackageWork(
                            PackageConfig(
                                project=self.settings.value("project"),
                                name=self.name_rule,
                                zip_file_path=zip_filepath),
                            opt
                        ) for opt in opts]
        print(works)
        self.package_works = works
        self.start_package_work()

    def show_maopao_file_button_clicked(self):
        data = self.get_selected_data()
        if not data:
            self.show_warning_message_box()
        if not isinstance(data[0], Device):
            self.show_warning_message_box("需选择设备")
            return
        device = data[0]
        from Const import talPath
        maopaoFiles = [os.path.join(talPath, device.talPath, i) for i in
                       os.listdir(os.path.join(talPath, device.talPath)) if
                       i.endswith(".maopao")]
        if len(maopaoFiles) == 0:
            self.show_warning_message_box("未找到冒泡文件")
            return
        # self.hide()
        self.file_window.set_filepath(maopaoFiles[0])
        self.file_window.show()

    def generate_tal_code_button_clicked(self):
        data = self.get_selected_data()
        if not data:
            self.show_warning_message_box()
            return
        bakFile = self.set_choice_message_box("代码生成工具", "是否备份原有代码")

        for device in data:
            if isinstance(device, Device):
                from Const import talPath
                from package import execute_command
                self.log_box.appendPlainText("开始生成设备{}的TAL代码".format(device.deviceNameCN))
                commands = command.get_mprpcgen_generate_talcode_command(os.path.join(talPath, device.talPath), bakFile)
                execute_command(commands)
                self.log_box.appendPlainText("完成生成设备{}的TAL代码".format(device.deviceNameCN))

    def generate_api_list_button_clicked(self):
        data = self.get_selected_data()
        if not data:
            self.show_warning_message_box()

        devices = []
        for item in data:
            if isinstance(item, Device):
                devices.append(item)
            else:
                pass

        from GenerateApiList import get_devices_api_to_excel
        default_dir = QStandardPaths.writableLocation(QStandardPaths.DownloadLocation)
        options = QFileDialog.Options()
        options |= QFileDialog.DontUseNativeDialog
        file_filter = "Excel Files (*.xls)"
        file_name, _ = QFileDialog.getSaveFileName(self, "Save File", default_dir, file_filter, options=options)
        if file_name:
            if not file_name.endswith(".xls"):
                file_name += ".xls"
            print("Selected file:", file_name)
        else:
            return

        basePath = os.path.abspath(os.path.dirname(__file__))
        savePath = os.path.join(basePath, "output", "json")
        if not os.path.exists(savePath):
            os.makedirs(savePath)
        get_devices_api_to_excel(devices, savePath, os.path.join(basePath, file_name), command=command)
        print("生成完成")

    def execute_work(self, package_work: PackageWork):
        self.log_box.appendPlainText("\n开始执行任务：{}".format(package_work))
        if package_work.opt == "build":
            build(package_work.package_config, self.execute_command, None)
        elif package_work.opt == "flash":
            flash_only(package_work.package_config.zipFilePath, self.execute_command, None)
        elif package_work.opt == "package":
            package_only(package_work.package_config)
            self.log_box.appendPlainText("打包完成")
            self.change_package_work()

    def start_package_work(self):
        if self.process_running:
            return
        if not self.package_works:
            return

        package_work = self.package_works.pop(0)
        # 打开定时器
        self.timer.start()

        # 清空日志框
        self.log_box.clear()

        self.stop_button.setEnabled(True)
        for button in self.action_button.values():
            if "button" in button.keys():
                button["button"].setEnabled(False)

        self.process_running = True

        print(f"开始执行{package_work}")
        self.execute_work(package_work)

        pass

    def change_package_work(self):
        print("change_package_work")
        if self.package_works:
            package_work = self.package_works.pop(0)
            print(f"继续执行{package_work}")
            self.execute_work(package_work)
        else:
            self.finish_package_work()

    def finish_package_work(self):
        print("所有操作已完成")
        # 关闭定时器
        self.timer.stop()
        # 恢复按钮状态
        self.stop_button.setEnabled(False)
        for button in self.action_button.values():
            if "button" in button.keys():
                button["button"].setEnabled(True)

        # 标记命令行进程的执行状态
        self.process_running = False

    def stop_process(self):
        if self.process_running:
            # 终止命令行进程
            self.process.terminate()
            self.log_box.appendPlainText("命令行进程已终止")

    def read_output(self):
        # 获取命令行输出
        output = self.process.readAll().data().decode().strip()
        print(output)

        # 追加输出到日志框
        self.log_box.appendPlainText(output)

    def execute_command(self, commands, args):
        print("execute_command", commands)
        print(commands)
        self.log_box.appendPlainText("&&".join(commands))
        self.command_thread.commands = commands
        self.command_thread.start()

    def process_finished(self):
        self.change_package_work()

    def update_log_box(self):
        # 滚动日志框到底部
        self.log_box.verticalScrollBar().setValue(self.log_box.verticalScrollBar().maximum())

    def setup_tool_tab(self):
        tool_tab = QWidget()
        layout = QHBoxLayout(tool_tab)

        # 按钮布局

        button_layout = QGridLayout()
        tool_button = self.tool_button
        for button_key in tool_button:
            button_config = tool_button[button_key]
            button = QPushButton(button_config["name"], self)
            button.setToolTip(button_config["tip"])
            button.clicked.connect(button_config["clicked_cb"])
            button_config["button"] = button
            button_layout.addWidget(button, button_config["i"], button_config["j"])
        layout.addLayout(button_layout)

        self.tab_widget.addTab(tool_tab, "工具箱")

    def flash_key_button_clicked(self):
        default_dir = self.settings.value("binFile")
        if not default_dir or not os.path.exists(default_dir):
            default_dir = QStandardPaths.writableLocation(QStandardPaths.DownloadLocation)
        options = QFileDialog.Options()
        options |= QFileDialog.DontUseNativeDialog
        file_filter = "jkey Files (*.bin *.zip *.jk)"
        binFile, _ = QFileDialog.getOpenFileName(self, "Open File", default_dir, file_filter, options=options)
        if binFile:
            self.settings.setValue("binFile", binFile)
            print("Selected file:", binFile)
        else:
            return

        basePath = os.path.dirname(os.path.abspath(__file__))
        outputPath = os.path.join(basePath, "output", "key")
        if not os.path.exists(outputPath):
            os.makedirs(outputPath)
        print(os.path.join(os.path.dirname(basePath), "scripts", "jkey"))
        sys.path.append(os.path.join(os.path.dirname(basePath), "scripts", "jkey"))
        from jkey import get_config, decrypt_file, unzip_file, get_address_from_config, write_key

        config = get_config()

        if binFile.endswith(".jk"):
            password, ok = QInputDialog.getText(self, 'Password', 'Enter your password:',
                                                text=self.settings.value("password", ""))
            if ok and password:
                # 在这里可以继续接下来的操作，如验证密码是否正确或执行其他操作
                self.settings.setValue("password", password)
                decrypt_file(binFile, password, binFile.replace(".jk", ".zip"))
                binFile = binFile.replace(".jk", ".zip")
            else:
                return

        if binFile.endswith(".zip"):
            unzip_file(binFile, binFile.replace(".zip", ""))
            filelist = [i for i in os.listdir(binFile.replace(".zip", "")) if i.endswith(".bin")]
            if len(filelist) == 0:
                self.show_warning_message_box("未找到密钥文件")
            elif len(filelist) == 1:
                print(f"找到密钥文件：{filelist[0]}")
                binFile = os.path.join(binFile.replace(".zip", ""), filelist[0])
            else:
                selected_option, ok = QInputDialog.getItem(self, 'Selection', 'Choose an option:', filelist)
                if ok and selected_option:
                    # 在这里可以继续接下来的操作，根据选项执行相应的逻辑
                    print('Selected option:', selected_option)
                    binFile = os.path.join(binFile.replace(".zip", ""), selected_option)
                else:
                    return

        address = get_address_from_config(config, binFile)
        if not address:
            address, ok = QInputDialog.getText(self, 'Address', f'Enter flash address({os.path.basename(binFile)}):',
                                               text=self.settings.value("flash_address", ""))
            if ok and address:
                self.settings.setValue("flash_address", address)
        if address:
            try:
                write_key(address, binFile, outputPath)
                self.show_complete_dialog(f"密钥烧录完成({address})")
            except Exception as e:
                self.show_error_message_box(str(e))

    def read_key_button_clicked(self):
        basePath = os.path.dirname(os.path.abspath(__file__))
        outputPath = os.path.join(basePath, "output", "key")
        if not os.path.exists(outputPath):
            os.makedirs(outputPath)
        binFile = os.path.join(outputPath, "key.bin")
        sys.path.append(os.path.join(os.path.dirname(basePath), "scripts", "jkey"))
        from jkey import read_flash
        address, ok = QInputDialog.getText(self, 'address', 'Enter read address:',
                                           text=self.settings.value("read_flash_address", "0x3D1000"))
        if ok and address:
            # 在这里可以继续接下来的操作，如验证密码是否正确或执行其他操作
            self.settings.setValue("read_flash_address", address)

        def solve_data(address, data):

            def read_data(data):
                if data:
                    print(data)
                    length = int(data[0])
                    if length == 255:
                        return 0, ""
                    msg = data[1:length+1]
                    print(length, msg)
                    return length, msg
                return 0, ""


            data = data.rstrip(b'\xff')
            if address == "0x3D1000":
                deviceId = data[0:20]
                token = data[20:36]
                password = data[36:52]
                sn = data[52:68]
                productCode = data[68:72]
                result = f" deviceId: {deviceId}\n token: {token}\n password: {password}\n sn: {sn}\n productCode: {productCode}\n"
                return result
            elif address == "0x3D2000":
                result = ""
                if data and data[0] != b'/xff':
                    mode = data[0]
                    if mode == b'\x00':
                        result += "私有化\n"
                    else:
                        result += "调试平台\n"
                    print(result)
                    pos = 1
                    length, msg = read_data(data[pos:])
                    result += f"鉴权地址：{msg}\n"
                    pos += length + 1
                    length, msg = read_data(data[pos:])
                    result += f"MQTT地址：{msg}\n"
                    pos += length + 1
                    mqtt_address = data[pos:pos+2]
                    result += f"MQTT端口：{int.from_bytes(mqtt_address, byteorder='big')}\n"
                    pos += 2
                    length, msg = read_data(data[pos:])
                    result += f"MQTT用户名：{msg}\n"
                    pos += length + 1
                    length, msg = read_data(data[pos:])
                    result += f"MQTT密码：{msg}\n"
                    pos += length + 1
                    length, msg = read_data(data[pos:])
                    result += f"SSID：{msg}\n"
                    pos += length + 1
                    length, msg = read_data(data[pos:])
                    result += f"PWD：{msg}\n"
                    pos += length + 1
                    length, msg = read_data(data[pos:])
                    result += f"clientID前缀：{msg}\n"
                    pos += length + 1
                    length, msg = read_data(data[pos:])
                    result += f"订阅主题：{msg}\n"
                    pos += length + 1
                    length, msg = read_data(data[pos:])
                    result += f"发布主题：{msg}\n"
                    pos += length + 1
                    return result
            return data

        try:
            data = read_flash(address, 512, binFile)
            data = solve_data(address, data)
            self.show_complete_dialog(f"密钥读取完成\n{data}")
        except Exception as e:
            self.show_error_message_box(str(e))

    def erase_flash_button_clicked(self):
        from package import erase_flash
        result = self.set_choice_message_box("擦除flash", "是否擦除flash")
        if result:
            try:
                erase_flash()
                self.show_complete_dialog(f"擦除完成")
            except Exception as e:
                self.show_error_message_box(str(e))


if __name__ == "__main__":
    app = QApplication(sys.argv)
    window = MainWindow()
    window.show()
    sys.exit(app.exec_())
