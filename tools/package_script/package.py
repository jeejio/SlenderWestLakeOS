#!/usr/bin/env python3
# TODO CMakelists.txt支持传入driver和tal路径编译

import argparse
import os
import platform
import shutil
import subprocess
import sys
import time
from typing import List

sys.path.append(".")
sys.path.append("python_lib")

from Const import *
from PackageConfig import PackageConfig
from Command import get_command_by_platform

command = get_command_by_platform()


def remove_file(path):
    if os.path.exists(path):
        os.remove(path)


def copy_file(src, dst):
    remove_file(dst)
    shutil.copyfile(src, dst)


def copy_bin_files(bin_src_path, bin_dst_path):
    if not os.path.isdir(bin_dst_path):
        os.makedirs(bin_dst_path)

    for bin_info in bin_infos:
        if not os.path.isfile(os.path.join(bin_src_path, bin_info["src_path"])):
            print(f"{bin_info['src_path']} not exist")
            sys.exit(1)
        copy_file(os.path.join(bin_src_path, bin_info["src_path"]), os.path.join(bin_dst_path, bin_info["dst_path"]))


def execute_command(commands, args=None):
    print(commands)
    result = subprocess.run("&&".join(commands), cwd=project_path, shell=True, executable=command.executable)
    # print(result)
    if result.returncode != 0:
        print("build failed")
        sys.exit(1)
    return result


def build(package_config: PackageConfig, execute_fn=execute_command, execute_args=None):
    commands = []
    if package_config.device:
        print(f"build for {package_config.device.deviceNameCN}")
        commands += command.get_set_env_command("MANHATTAN_DEVICE", package_config.device.deviceName)
    commands += command.get_source_command()
    commands += command.get_build_init_command()
    commands += command.get_build_command()
    execute_fn(commands, execute_args)


def get_package_name(package_config: PackageConfig):
    # {deviceName}_{version}_{env}_{date}
    return package_config.name.format(
        project=package_config.project,
        deviceName=package_config.device.deviceName if package_config.device else "",
        deviceNameCN=package_config.device.deviceNameCN if package_config.device else "",
        version=package_config.version,
        env=package_config.env,
        date=common_info.get("日期").replace(':', '.')
    )


def package_only(package_config: PackageConfig):
    from firmware_info import generate_firmware_info

    if package_config.device:
        path = os.path.join(savePath, package_config.device.deviceName)
    else:
        path = os.path.join(savePath, "package")

    copy_bin_files(targetPath, path)
    info_to_write = generate_firmware_info(package_config, project_path)
    with open(f"{path}/固件信息.txt", "w", encoding="utf-8") as f:
        f.write(info_to_write)

    package_name = get_package_name(package_config)
    shutil.make_archive(
        os.path.join(savePath, package_name),
        "zip",
        path)
    print(f"package '{package_name}' success")


def copy_bin_to_save_bin_path():
    if not os.path.isdir(saveBinPath):
        os.makedirs(saveBinPath)
    for bin_info in bin_infos:
        shutil.copyfile(os.path.join(targetPath, bin_info["src_path"]), os.path.join(saveBinPath, bin_info["dst_path"]))


def extract_tar_gz(file_path, destination):
    import tarfile
    with tarfile.open(file_path, 'r:gz') as tar:
        tar.extractall(destination)


def extract_tar(file_path, destination):
    import tarfile
    with tarfile.open(file_path, 'r:') as tar:
        tar.extractall(destination)


def unzip_bin_to_save_bin_path(zip_file_path):
    if not os.path.exists(saveBinPath):
        os.makedirs(saveBinPath)
    else:
        shutil.rmtree(saveBinPath)
        os.makedirs(saveBinPath)

    if zip_file_path.endswith(".zip"):
        shutil.unpack_archive(zip_file_path, saveBinPath, "zip")
    elif zip_file_path.endswith(".tar.gz"):
        extract_tar_gz(zip_file_path, saveBinPath)
    elif zip_file_path.endswith(".tar"):
        extract_tar(zip_file_path, saveBinPath)
    else:
        print("not support file type")
        sys.exit(1)


def erase_flash(execute_fn=execute_command, execute_args=None):
    commands = []
    commands += command.get_source_command()
    commands += command.get_erase_command()
    execute_fn(commands, execute_args)


def flash_only(zip_file_path=None, execute_fn=execute_command, execute_args=None):
    if zip_file_path:
        unzip_bin_to_save_bin_path(zip_file_path)
    else:
        copy_bin_to_save_bin_path()
    commands = []
    commands += command.get_source_command()
    flash_bin_infos = [{"address": i["address"], "filepath": i["dst_path"]} for i in bin_infos]
    commands += command.get_flash_command(flash_bin_infos, saveBinPath)

    execute_fn(commands, execute_args)


def read_only(address, length, save_filepath, execute_fn=execute_command, execute_args=None):
    commands = []
    commands += command.get_source_command()
    commands += command.get_read_command(address, length, save_filepath)
    execute_fn(commands, execute_args)


def package_opt(package_configs: List[PackageConfig]):
    for package_config in package_configs:
        build(package_config)
        package_only(package_config)


def flash_opt(package_configs: List[PackageConfig]):
    if len(package_configs) != 1:
        print("Please select a package")
        sys.exit(1)
    package_config = package_configs[0]
    if package_config.zipFilePath:
        flash_only(package_config.zipFilePath)
    else:
        build(package_config)
        flash_only()
    pass


def build_opt(package_configs: List[PackageConfig]):
    for package_config in package_configs:
        build(package_config)
    pass


def flash_only_opt(package_configs: List[PackageConfig]):
    if len(package_configs) != 1:
        print("Please select a package")
        sys.exit(1)
    package_config = package_configs[0]
    flash_only(package_config.zipFilePath)
    pass


def package_only_opt(package_configs: List[PackageConfig]):
    if len(package_configs) != 1:
        print("Please select a package")
        sys.exit(1)
    package_config = package_configs[0]
    package_only(package_config)
    pass


if __name__ == "__main__":
    from PackageConfig import get_package_configs_from_kconfig

    opts = {
        "package": package_opt,
        "flash": flash_opt,
        "build": build_opt,
        "flash_only": flash_only_opt,
        "package_only": package_only_opt
    }

    # 创建一个ArgumentParser对象
    parser = argparse.ArgumentParser(description='打包脚本')

    # 添加命令行参数
    parser.add_argument('opt', choices=list(opts.keys()), type=str, help='需要进行的操作', nargs='?', default="package")
    parser.add_argument('--zip', type=str, help='烧录时选用的固件包')
    parser.add_argument('--device', type=str, help='打包的设备名')
    parser.add_argument('--name', type=str, help='出包名')

    # 解析命令行参数
    args = parser.parse_args()
    # print(args)

    # 获取打包配置
    packageConfigs = get_package_configs_from_kconfig(name=args.name, device=args.device, zip_file_path=args.zip)

    if args.opt not in opts.keys():
        print("opt error")
        sys.exit(1)

    # 执行操作
    opts[args.opt](packageConfigs)
