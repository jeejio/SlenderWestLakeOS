# @Author  : QZM
# @Time    : 2023/7/2 21:26
import os.path
import platform
import sys

from Device import Device


def get_device_api(device: Device, save_path, command=None, execute_fn=None, execute_args=None, format="json"):
    if execute_fn is None:
        from package import execute_command
        execute_fn = execute_command
    if command is None:
        from Command import get_command_by_platform
        command = get_command_by_platform()
    from Const import project_path
    import os
    talPath = os.path.join(project_path, "tal", device.talPath)
    maopaoFileList = [i for i in os.listdir(talPath) if i.endswith(".maopao")]
    if len(maopaoFileList) != 1:
        return None
    maopaoFilePath = os.path.join(talPath, maopaoFileList[0])
    execute_fn(command.get_mprpcgen_generate_json_command(os.path.abspath(save_path), maopaoFilePath), execute_args)
    talName = os.path.basename(maopaoFileList[0]).split(".")[0]
    jsonFilePath = os.path.join(save_path, talName + ".json")
    if not os.path.isfile(jsonFilePath):
        print(f"{jsonFilePath} not exist")
        return None
    with open(jsonFilePath, "r", encoding="utf-8") as f:
        import json
        data = json.load(f)
        if format == "dict":
            return data["function"]
        elif format == "json":
            return json.dumps(data["function"])
        elif format == "str":
            api_str = ""
            for function in data["function"]:
                api_str += f"{function['method']},{function['methodId'] if 'methodId' in function else 'null'}\n"
            return api_str


def get_devices_api_to_excel(devices, save_path, excel_filepath, command=None, execute_fn=None, execute_args=None):
    import xlwt
    workbook = xlwt.Workbook()
    worksheet = workbook.add_sheet("Sheet1")
    titles = ["英文名", "中文名", "tag", "method", "methodId"]
    h = 0
    for i in range(len(titles)):
        worksheet.write(h, i, titles[i])
    h += 1
    for device in devices:
        print(device)
        apis = get_device_api(device, save_path, command, execute_fn, execute_args, format="dict")
        if apis is None:
            continue
        for function in apis:
            worksheet.write(h, 0, device.deviceName)
            worksheet.write(h, 1, device.deviceNameCN)
            worksheet.write(h, 2, ",".join(device.tags))
            worksheet.write(h, 3, function["method"])
            if "methodId" in function:
                worksheet.write(h, 4, function["methodId"])
            h += 1
    workbook.save(excel_filepath)


if __name__ == "__main__":
    from Device import read_devices_from_csv
    from Const import csvFilePath
    from package import execute_command

    if platform.system() == "Windows":
        from Command import CommandWin

        command = CommandWin()
    elif platform.system() == "Linux" or platform.system() == "Darwin":
        from Command import CommandLinux

        command = CommandLinux()
    else:
        print("Unsupported platform")
        sys.exit(1)

    devices = read_devices_from_csv(csvFilePath)
    if not os.path.isdir("output"):
        os.mkdir("output")
    devices = [device for device in devices if device.tags]
    get_devices_api_to_excel(devices, "output", "output/api.xls", command, execute_command, None)
