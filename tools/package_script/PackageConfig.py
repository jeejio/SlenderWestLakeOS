import os
from typing import List

from Const import csvFilePath, project_path
from Device import Device, read_devices_from_csv, find_device_by_name, find_device_by_name_cn, find_device_by_category



import re
def get_version():
    macro_value = None
    with open(os.path.join(project_path, 'framework', 'ota', 'firmware_version.h'), 'r') as file:
        content = file.read()

    # 使用正则表达式匹配宏定义行
    pattern = r'#define\s+FIRMWARE_VERSION\s+"(.*?)"'
    match = re.search(pattern, content)

    if match:
        macro_value = match.group(1)
        print(f"宏定义 FIRMWARE_VERSION 的值为: {macro_value}")
    else:
        print("未找到宏定义 FIRMWARE_VERSION")

    return macro_value


def get_device_env():
    with open(os.path.join(project_path, 'framework', 'maopao', 'maopaoChannel.h'), 'r', encoding="utf-8") as file:
        content = file.read()

    # 定义宏定义列表
    macros = ['DEV', 'QA', 'RC', 'RELEASE']

    # 遍历宏定义列表
    for macro in macros:
        # 构建宏定义行的正则表达式
        pattern = fr'#define\s+{macro}\s+1'
        match = re.search(pattern, content)

        # 如果找到匹配的宏定义，输出并结束循环
        if match:
            print(f"值为1的宏定义: {match.group(0)}")
            return macro
    return None


class PackageConfig(object):
    def __init__(self, project=None, name=None, device=None, zip_file_path=None):
        self.project = project
        self.zipFilePath = zip_file_path
        self.name = name
        self.env = get_device_env()
        self.version = get_version()
        if isinstance(device, Device):
            self.device = device
        else:
            self.device = None

    def __str__(self):
        return f"PackageConfig(project={self.project}, name={self.name}, device={self.device}, zip_file_path={self.zipFilePath})"


def get_package_configs_from_kconfig(project=None, name=None, device=None, zip_file_path=None) -> List[PackageConfig]:
    from config import get_kconfig

    devices = []
    kconf = get_kconfig()
    device_list = read_devices_from_csv(csvFilePath)

    if device:
        if isinstance(device, Device):
            devices.append(device)
        elif isinstance(device, str):
            device = find_device_by_name(device_list, device)
            if device:
                devices.append(device)
            else:
                device = find_device_by_name_cn(device_list, device)
                if device:
                    devices.append(device)
                else:
                    device = find_device_by_category(device_list, device)
                    if device:
                        devices.append(device)
    else:
        for device in device_list:
            config_name = f"MANHATTAN_DEVICE_{device.category}"
            if config_name in kconf.syms:
                if kconf.syms[config_name].tri_value == 2:
                    devices.append(device)

    if not name:
        name = ""
        if kconf.syms["PACKAGE_NAME_CHOICE_PROJECT"].tri_value == 2:
            name += kconf.syms["PACKAGE_NAME"].str_value + "_"

        if kconf.syms["PACKAGE_NAME_CHOICE_DEVICE_NAME"].tri_value == 2:
            name += "{deviceNameCN}_"

        if kconf.syms["PACKAGE_NAME_CHOICE_VERSION"].tri_value == 2:
            name += "{version}_"

        if kconf.syms["PACKAGE_NAME_CHOICE_ENV"].tri_value == 2:
            name += "{env}_"

        if kconf.syms["PACKAGE_NAME_CHOICE_DATE"].tri_value == 2:
            name += "{date}_"

        if name.endswith("_"):
            name = name[:-1]
        if len(name) == 0:
            name = "package"

    if zip_file_path:
        return [PackageConfig(project=project, name=name, device=None, zip_file_path=zip_file_path)]
    elif devices:
        package_configs = []
        for device in devices:
            package_configs.append(PackageConfig(project=project, name=name, device=device, zip_file_path=None))
        return package_configs
    else:
        return [PackageConfig(project=project, name=name, device=None, zip_file_path=None)]
