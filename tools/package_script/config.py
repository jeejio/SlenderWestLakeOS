#!/usr/bin/env python3
import os
import sys

# TODO 根据历史选择偏好排序

basePath = os.path.dirname(os.path.abspath(__file__))
sys.path.append(os.path.join(basePath, 'python_lib'))
sys.path.append(basePath)
os.environ['KCONFIG_CONFIG'] = os.path.join(basePath, 'config', 'package_config')

from Const import *
from Device import read_devices_from_csv
from menuconfig import menuconfig
from kconfiglib import Kconfig
from jinja2 import Template


def get_kconfig():
    kconf = Kconfig(kConfigMenuFilePath)
    kconf.load_config(kConfigConfigFilePath)
    return kconf


def get_tags_from_devices(devices):
    tags = []
    for device in devices:
        for tag in device.tags:
            if tag not in tags:
                tags.append(tag)
    return tags


def prepare_kconfig():
    devices = read_devices_from_csv(csvFilePath)
    device_tags = get_tags_from_devices(devices)
    template = Template(open(os.path.join(basePath, 'config', 'main.template'), "r", encoding="utf-8").read())

    # 定义要传递给模板的变量
    context = {
        'devices': [device.__dict__ for device in devices],
        'device_tags': device_tags,
    }

    # print(context)

    # 渲染模板
    output = template.render(context)
    # print(output)
    with open(os.path.join(basePath, 'config', 'main.kconfig'), 'w', encoding="utf-8") as f:
        f.write(output)


if __name__ == '__main__':
    prepare_kconfig()
    kconf = Kconfig(kConfigMenuFilePath)
    menuconfig(kconf)
