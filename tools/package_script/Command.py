import os.path
from abc import abstractmethod
from Const import project_path, targetPath, saveBinPath


def get_address_and_bin_str(bin_flash_infos):
    bin_str = ""
    for bin_info in bin_flash_infos:
        bin_str += f"{bin_info['address']} {bin_info['filepath']} "
    return bin_str


class CommandPublic(object):
    executable = None
    python = "python"

    def __init__(self):
        import platform
        self.platform_name = platform.system()
        if self.platform_name == "Windows":
            self.platform_name = "Win"

    @staticmethod
    def get_build_init_command():
        return [
            f"cd {targetPath}",
            f"cmake -G Ninja -DESP_PLATFORM=1 -DJEEJIO_TARGET=esp32c3 -DCCACHE_ENABLE=1 {project_path}",
            f"cd {project_path}"
        ]

    @staticmethod
    def get_build_command():
        return ['build']

    def get_flash_command(self, bin_flash_infos, workPath=None):
        commands = []
        if workPath:
            commands += [f"cd {workPath}"]
        flashCmd = f"{self.python} -m esptool --chip esp32c3 -b 460800 --before=default_reset --after=hard_reset write_flash --flash_mode dio --flash_freq 80m --flash_size 4MB "
        flashCmd += get_address_and_bin_str(bin_flash_infos)
        commands.append(flashCmd)
        if workPath:
            commands.append(f"cd -")
        return commands

    def get_erase_command(self):
        commands = []
        eraseCmd = f"{self.python} -m esptool --chip esp32c3 erase_flash"
        commands.append(eraseCmd)
        return commands

    def get_read_command(self, address, length, save_bin_filepath):
        commands = []
        readCmd = f"{self.python} -m esptool read_flash "
        readCmd += f"{address} {length} "
        readCmd += f"{save_bin_filepath}"
        commands.append(readCmd)
        return commands

    def get_mprpcgen_generate_json_command(self, workPath, maopaoFilePath):
        mprpcgen = os.path.join(project_path, "tools", "mprpcgen", "release", f"mprpcgen_{self.platform_name}")
        commands = [f"{mprpcgen} -C {workPath} --no-bak --no-talcode --output-json {maopaoFilePath}"]
        return commands

    def get_mprpcgen_generate_talcode_command(self, workPath, bakFile=True, maopao_filepath=None):
        mprpcgen = os.path.join(project_path, "tools", "mprpcgen", "release", f"mprpcgen_{self.platform_name}")
        commands = [
            f"{mprpcgen} -C {workPath} {'--no-bak' if not bakFile else ''} {maopao_filepath if maopao_filepath else ''}"]
        return commands

    @staticmethod
    def get_clean_command():
        return ['clean']

    @abstractmethod
    def get_source_command(self):
        return []

    @abstractmethod
    def get_set_env_command(self, key, value):
        return []


class CommandWin(CommandPublic):

    def __init__(self):
        super().__init__()

    def get_build_command(self):
        return [f'ninja -C {targetPath}']

    def get_source_command(self):
        return ['build\\env.bat']

    def get_set_env_command(self, key, value):
        return [f'set {key}={value}']


class CommandLinux(CommandPublic):
    executable = "/bin/bash"
    python = "python3"

    def __init__(self):
        super().__init__()

    def get_source_command(self):
        return ['source build/env.sh']

    def get_set_env_command(self, key, value):
        return [f'export {key}={value}']


class CommandDarwin(CommandLinux):
    def __init__(self):
        super().__init__()


def get_command_by_platform():
    import platform
    if platform.system() == "Windows":
        return CommandWin()
    elif platform.system() == "Linux":
        return CommandLinux()
    elif platform.system() == "Darwin":
        return CommandDarwin()
    else:
        print("Unsupported platform")
        return None
