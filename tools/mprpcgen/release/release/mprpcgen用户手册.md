# mprpcgen用户手册

mprpcgen是一个冒泡RPC调用的代码生成工具。它接受.maopao扩展名的语法文件，其中包含TAL信息和RPC接口的定义，自动生成处理冒泡消息的service代码。

## 更新日志

| 版本  | 日期       | 修订记录                                       |
| ----- | ---------- | ---------------------------------------------- |
| 1.3.0 | 2023.04.27 | 初始版本                                       |
| 1.4.0 | 2023.05.11 | 新增用户原有代码保存功能                       |
| 1.4.1 | 2023.05.12 | 修改TAL信息字段名称                            |
| 1.4.2 | 2023.06.14 | 修复一些bug                                    |
| 1.4.3 | 2023.06.20 | 新增返回精确浮点数功能<br />新增methodId功能   |
| 1.4.4 | 2023.06.20 | 修复一些bug                                    |
| 1.4.6 | 2023.06.27 | 支持显示语法错误具体信息<br />支持更改工作路径 |





## 使用流程

1. 编写.maopao文件（详见见冒泡文件组成部分）

​	`tal_example.maopao`示例文件

```c
MAOPAO_MANUFACTURER("jeejio");
MAOPAO_DEVICE_CATEGORY("product");
MAOPAO_TAL_API_VERSION("1.0.0");
MAOPAO_TAL_DEVELOPER("author@jeejio.com");

__RPC__ int talSetLedStatus(bool status);
```

2. 使用命令运行工具生成代码（详见生成代码命令部分）

```shell
./mprpcgen_Linux tal_example.maopao
```

​	工具会自动生成四个文件：tal_example.c/.h与tal_example_service.c/.h

​	用户仅需关心`tal_example.c`文件，其他文件可不做修改

3. 编写tal实现代码

​	`tal_example.c`部分代码如下，用户仅需在BEGIN和END区间完成函数实现部分

```c
int talSetLedStatus(bool status)
{
    /* USER CODE BEGIN talSetLedStatus talSetLedStatus */

    return MAOPAO_SUCCESS;
    /* USER CODE END talSetLedStatus talSetLedStatus */
}
```

​	`tal_example.h`部分代码如下，用户仅需关心serviceGroup变量名

```c
extern maopao_service_handler_st tal_example_serviceGroup[];
```

4. 在app_main()添加冒泡服务

​	在app_main()中，添加如下代码，将tal_example_serviceGroup添加到冒泡服务中，即可实现RPC调用。

```c
maopaoServerAddServices(tal_example_serviceGroup);
```

## 冒泡文件组成

.maopao文件由TAL信息与接口定义两部分组成，TAL信息需写在文件开头。

### TAL信息

TAL信息包含厂商信息，产品信息，版本号与作者等内容，格式如下。

```c
MAOPAO_MANUFACTURER("jeejio");
MAOPAO_DEVICE_CATEGORY("product");
MAOPAO_TAL_API_VERSION("1.0.0");
MAOPAO_TAL_DEVELOPER("author@jeejio.com");
```

| 字段名称                    | 含义          | mprpcgen版本  | 瘦西湖版本 |
| --------------------------- | ------------- | ------------- | ---------- |
| MAOPAO_MANUFACTURER         | 厂商          | V1.4.1起      | V0.0.2起   |
| MAOPAO_DEVICE_CATEGORY      | 设备品类编码  | V1.4.1起      | V0.0.2起   |
| MAOPAO_TAL_API_VERSION      | Tal API版本号 | V1.4.1起      | V0.0.2起   |
| MAOPAO_TAL_DEVELOPER        | 开发者        | V1.4.1起      | V0.0.2起   |
| ~~MAOPAO_TAL_MANUFACTURER~~ | 厂商          | V1.3.0-V1.4.0 | 仅V0.0.1   |
| ~~MAOPAO_TAL_PRODUCT~~      | 产品          | V1.3.0-V1.4.0 | 仅V0.0.1   |
| ~~MAOPAO_TAL_VERSION~~      | Tal版本号     | V1.3.0-V1.4.0 | 仅V0.0.1   |
| ~~MAOPAO_TAL_AUTHOR~~       | 开发者        | V1.3.0-V1.4.0 | 仅V0.0.1   |

### 接口定义

#### 定义方式

冒泡接口定义类似于函数声明

接口的返回类型固定为int，表明tal的执行成功与失败

若接口需要被RPC调用，需要在前面添加\_\_RPC\_\_关键字

```c
__RPC__ int ltalToggleLed();
int talToggleLedWithoutRpc();
```

#### 调用名定义

用户可以通过@METHOD_ID("rpcMethodId")的注释指定rpc请求的methodId，@METHOD("rpcMethod")的注释指定rpc请求的method名。若不进行注释，则默认RPC方法名与函数名相同，methodId为空。

```c
@METHOD_ID("p0000001")
@METHOD("toggleLed")
__RPC__ int ltalToggleLed();
```

#### 参数类型

具体可见example下示例

##### 基本类型

接口可以有一个或多个输入参数，支持的基础参数类型有bool, int, char *, float

```c
//send->{"id":"1","method":"setInt","params":{"value":123}}
//recv->{"id":"1","code":200,"message":"success","jsonrpc":"1.0","result":null}
@METHOD_ID("p0000002")
@METHOD("setInt")
__RPC__ int lTalSetInt(int value);


//send->{"id":"1","method":"setFloat","params":{"value":12.45}}
//recv->{"id":"1","code":200,"message":"success","jsonrpc":"1.0","result":null}
@METHOD_ID("p0000003")
@METHOD("setFloat")
__RPC__ int lTalSetFloat(float value);


//send->{"id":"1","method":"setBool","params":{"value":true}}
//recv->{"id":"1","code":200,"message":"success","jsonrpc":"1.0","result":null}
@METHOD_ID("p0000004")
@METHOD("setBool")
__RPC__ int lTalSetBool(bool value);


//send->{"id":"1","method":"setString","params":{"value":"hello"}}
//recv->{"id":"1","code":200,"message":"success","jsonrpc":"1.0","result":null}
@METHOD_ID("p0000005")
@METHOD("setString")
__RPC__ int lTalSetString(char *value);


//send->{"id":"1","method":"setMultiValue","params":{"value1":123,"value2":"hello","value3":12.45}}
//recv->{"id":"1","code":200,"message":"success","jsonrpc":"1.0","result":null}
@METHOD_ID("p0000006")
@METHOD("setMultiValue")
__RPC__ int lTalSetMultiValue(int value1, char *value2, float value3);
```



接口可以有一个或多个输出参数，支持的基础参数类型有bool, int, char *, float，支持数组类型

```c
//send->{"id":"1","method":"getInt","params":{}}
//recv->{"id":"1","code":200,"message":"success","jsonrpc":"1.0","result":{"value":123}}
@METHOD_ID("p0000007")
@METHOD("getInt")
__RPC__ int lTalGetInt(maopaoout int value);


//send->{"id":"1","method":"getFloat","params":{}}
//recv->{"id":"1","method":"getFloat","params":{"value":12.45}}
@METHOD_ID("p0000008")
@METHOD("getFloat")
__RPC__ int lTalGetFloat(maopaoout float value);


//send->{"id":"1","method":"getBool","params":{}}
//recv->{"id":"1","code":200,"message":"success","jsonrpc":"1.0","result":{"value":true}}
@METHOD_ID("p0000009")
@METHOD("getBool")
__RPC__ int lTalGetBool(maopaoout bool value);


//send->{"id":"1","method":"getString","params":{}}
//recv->{"id":"1","code":200,"message":"success","jsonrpc":"1.0","result":{"value":"hello"}}
@METHOD_ID("p000000a")
@METHOD("getString")
__RPC__ int lTalGetString(maopaoout char *value);


//send->{"id":"1","method":"getMultiValue","params":{}}
//recv->{"id":"1","code":200,"message":"success","jsonrpc":"1.0","result":{"value1":123,"value2":"hello","value3":12.45}}
@METHOD_ID("p000000b")
@METHOD("getMultiValue")
__RPC__ int lTalGetMultiValue(maopaoout int value1, maopaoout char *value2, maopaoout float value3);
```



输入输出参数支持数组，定义时需标明数组长度

```c
//send->{"id":"1","method":"setTexts","params":{"value":["hello","world","!","!","!"]}}
//recv->{"id":"1","code":200,"message":"success","jsonrpc":"1.0","result":null}
@METHOD_ID("p000000c")
@METHOD("setTexts")
__RPC__ int lTalSetTexts(char *value[5]);


//send->{"id":"1","method":"getTexts","params":{}}
//recv->{"id":"1","code":200,"message":"success","jsonrpc":"1.0","result":{"value":["hello","world","!","!","!"]}}
@METHOD_ID("p000000d")
@METHOD("getTexts")
__RPC__ int lTalGetTexts(maopaoout char *value[5]);
```



接口可以同时有输入参数和输出参数

```c
//send->{"id":"1","method":"gpioRead","params":{"pin":3}}
//recv->{"id":"1","code":200,"message":"success","jsonrpc":"1.0","result":{"value":1}}
@METHOD_ID("p000000e")
__RPC__ int gpioRead(int pin, maopaoout int value);
```



##### 结构体类型

接口中可以自定义结构体，并用于输入与输出参数

结构体内参数类型只能为bool, int, char *, float及数组，不支持struct嵌套

```c
struct rgb_t{
    int red;
    int green;
    int blue;
};


//send->{"id":"1","method":"setColor","params":{"value":{"red":100, "green":100, "blue":100}}}
//recv->{"id":"1","code":200,"message":"success","jsonrpc":"1.0","result":null}
@METHOD_ID("p000000f")
@METHOD("setColor")
__RPC__ int lTalSetColorValue(struct rgb_t value);


//send->{"id":"1","method":"getColor","params":{}}
//recv->{"id":"1","code":200,"message":"success","jsonrpc":"1.0","result":{"value":{"red":100,"green":100,"blue":100}}}
@METHOD_ID("p000000g")
@METHOD("getColor")
__RPC__ int lTalGetColorValue(maopaoout struct rgb_t value);
```



##### 浮点数额外规则

在使用float时，返回的json可能会出现浮点不精确的问题，为返回指定位数的小数，我们提供了FloatNumber类型.

```c
//send->{"id":"1","method":"setPrecisionDecimal","params":{"value":123.456}}
//recv->{"id":"1","code":200,"message":"success","jsonrpc":"1.0","result":null}
@METHOD_ID("p000000h")
@METHOD("setPrecisionDecimal")
__RPC__ int lTalSetPrecisionDecimal(FloatNumber value);


//send->{"id":"1","method":"getPrecisionDecimal","params":{}}
//recv->{"id":"1","code":200,"message":"success","jsonrpc":"1.0","result":{"value":123.5}}
@METHOD_ID("p000000i")
@METHOD("getPrecisionDecimal")
__RPC__ int lTalGetPrecisionDecimal(maopaoout FloatNumber value);
```

```c
int lTalSetPrecisionDecimal(FloatNumber value)
{
    /* USER CODE BEGIN setPrecisionDecimal lTalSetPrecisionDecimal */
    floatValue = value.number;
    printf("set floatValue = %f\n", floatValue);
    return MAOPAO_SUCCESS;
    /* USER CODE END setPrecisionDecimal lTalSetPrecisionDecimal */
}

int lTalGetPrecisionDecimal(FloatNumber *value)
{
    /* USER CODE BEGIN getPrecisionDecimal lTalGetPrecisionDecimal */
    value->number = floatValue;
    value->decimalPlaces = 1; // 保留一位小数
    printf("get floatValue = %f\n", value->number);
    return MAOPAO_SUCCESS;
    /* USER CODE END getPrecisionDecimal lTalGetPrecisionDecimal */
}
```



##### 字符串额外规则

在输出字符串参数时，有两种指定方式

```c
__RPC__ int lTalGetText1(maopaoout char *text); //方式一
__RPC__ int lTalGetText2(maopaoout char **text);//方式二
```

在实现代码中，使用方式有如下区别

在方式一中，生成的代码会为text分配1KB的空间，用户需把字符串拷贝到text中。

在方式二中，生成的代码不会分配空间，用户需修改text指向地址为字符串地址。

```c
int lTalGetText1(char *text)
{
    /* USER CODE BEGIN lTalGetText1 lTalGetText1 */
    strcpy(text, "hello world!");
    return MAOPAO_SUCCESS;
    /* USER CODE END lTalGetText1 lTalGetText1 */
}

int lTalGetText2(char **text)
{
    /* USER CODE BEGIN lTalGetText2 lTalGetText2 */
    *text = "hello world!";
    return MAOPAO_SUCCESS;
    /* USER CODE END lTalGetText2 lTalGetText2 */
}
```



若想实现复杂的json对象，动态长度数组等数据的传输，我们提供了jsonStr数据类型，使用方式类似于char *

```c
__RPC__ int lTalGetJson1(maopaoout jsonStr value); //方式一
__RPC__ int lTalGetJson2(maopaoout jsonStr* value);//方式二
__RPC__ int lTalGetText3(maopaoout char *value); 

//使用方法与区别
//recv->{"code":200,"result":{"num":2,"data":[1,2]}}
int lTalGetJson1(jsonStr value)
{
    /* USER CODE BEGIN lTalGetJson1 lTalGetJson1 */
    strcpy(value, "{\"num\":2,\"data\":[1, 2]}");
    return MAOPAO_SUCCESS;
    /* USER CODE END lTalGetJson1 lTalGetJson1 */
}

//recv->{"code":200,"result":{"num":2,"data":[1,2]}}
int lTalGetJson2(jsonStr *value)
{
    /* USER CODE BEGIN lTalGetJson2 lTalGetJson2 */
    *value = "{\"num\":2,\"data\":[1, 2]}";
    return MAOPAO_SUCCESS;
    /* USER CODE END lTalGetJson2 lTalGetJson2 */
}

//recv->{"code":200,"result":"{\"num\":2,\"data\":[1, 2]}"}
int lTalGetText3(char *value)
{
    /* USER CODE BEGIN lTalGetText3 lTalGetText3 */
    strcpy(text, "{\"num\":2,\"data\":[1, 2]}");
    return MAOPAO_SUCCESS;
    /* USER CODE END lTalGetText3 lTalGetText3 */
}
```

#### 注意事项

1. **用户代码需填写在BEGIN和END之间，若修改.maopao文件，重新生成后源代码会保留**
2. 在maopao文件中，可以使用//与/**/的方式进行代码注释
3. 不建议在maopao文件中使用中文，以防解析时出现乱码
4. 若生成时存在同名文件，源文件将被复制为.bak文件



### 返回值说明

接口的返回值代表执行成功与否，若执行成功，需返回MAOPAO_SUCCESS(即200)；若执行失败，需返回MAOPAO_ERROR_TAL_OPERATION_FAILED(1501)。

```c
int talSetLedStatus(bool status)
{
    /* USER CODE BEGIN talSetLedStatus talSetLedStatus */

    return MAOPAO_SUCCESS;
    /* USER CODE END talSetLedStatus talSetLedStatus */
}
```



## 运行方式

### Linux环境

```shell
./mprpcgen_Linux tal_primary.maopao
```

### Mac环境

```shell
./mprpcgen_Darwin tal_primary.maopao
```

### Windows环境

pcre.dll, pcrecpp.dll, pcreposic.dll文件需与mprpcgen.exe在同一路径下

```shell
mprpcgen_Win.exe tal_primary.maopao
```

#### 备份文件

若在运行目录存在tal源代码，生成工具会自动迁移BEGIN与END之间的代码，并默认备份源文件为.bak。

**@METHOD内容与函数名不能同时修改，同时修改后函数内代码无法迁移。**

```
默认备份源代码
./mprpcgen_xxx tal_primary.maopao 

不备份源代码
./mprpcgen_xxx --no-bak tal_primary.maopao 
```

#### 指定工作路径

```
./mprpcgen_xxx -C example/tal_primary tal_primary.maopao

./mprpcgen_xxx -C example/tal_primary
```

工具会读取example/tal_primary/tal_primary.maopao文件，检测example/tal_primary下是否存在源代码，并在example/tal_primary文件夹生成源代码。

通过-C指定工作路径时，可省略.maopao文件名，工具会在该路径下搜索.maopao文件。



