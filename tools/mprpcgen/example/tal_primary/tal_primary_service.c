// mprpcgen version: 1.4.7
/* USER CODE BEGIN Header */
/**
  * @file tal_primary_service.c
  * @attention
  * Slender West Lake automatically generated code.
  *
  */
/* USER CODE END Header */

#include <stdio.h>
#include <stdlib.h>
#include <memory.h>
#include "tal_primary_service.h"
#include "tal_primary.h"
#include "auto_init.h"
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* USER CODE BEGIN Define */
#define STRING_SIZE 1024

/* USER CODE END Define */

/* USER CODE BEGIN 1 */

/* USER CODE END 1 */

TalInfo_st _talInfo = {
    .manufacturer = "jeejio",
    .category = "category_code",
    .version = "1.0.0",
    .developer = "developer@jeejio.com",
};

//Define a MSON structure for input parameter processing
MSON_DEFOBJ(maopao_lTalSetInt_inParam_t,
    int value;
);

//Define a MSON structure for output parameter processing
MSON_DEFOBJ(maopao_lTalGetInt_outParam_t,
    int value;
);

//Define a MSON structure for input parameter processing
MSON_DEFOBJ(maopao_lTalSetFloat_inParam_t,
    float value;
);

//Define a MSON structure for output parameter processing
MSON_DEFOBJ(maopao_lTalGetFloat_outParam_t,
    float value;
);

//Define a MSON structure for input parameter processing
MSON_DEFOBJ(maopao_lTalSetBool_inParam_t,
    bool value;
);

//Define a MSON structure for output parameter processing
MSON_DEFOBJ(maopao_lTalGetBool_outParam_t,
    bool value;
);

//Define a MSON structure for input parameter processing
MSON_DEFOBJ(maopao_lTalSetString_inParam_t,
    char *value;
);

//Define a MSON structure for output parameter processing
MSON_DEFOBJ(maopao_lTalGetString_outParam_t,
    char *value;
);

//Define a MSON structure for input parameter processing
MSON_DEFOBJ(maopao_lTalSetMultiValue_inParam_t,
    int value1;
    char *value2;
    float value3;
);

//Define a MSON structure for output parameter processing
MSON_DEFOBJ(maopao_lTalGetMultiValue_outParam_t,
    int value1;
    char *value2;
    float value3;
);

void __maopaoServer__lTalSetInt__service__(maopao_requestInfo_st *_requestInfo, SemaphoreHandle_t _mux)
{
    //prepare input parameter
    mson_jsonToObj(_requestInfo->params, MSON_MEMBERS_TO_STRING(maopao_lTalSetInt_inParam_t), _inObj);
    if (_inObj == NULL){
        maopaoServerSendError(_requestInfo, MAOPAO_ERROR_RPC_ANALYSE_FAILED, MAOPAO_ERROR_RPC_ANALYSE_FAILED_MSG);
        goto _exit;
    }
    maopao_lTalSetInt_inParam_t *_inParam = (maopao_lTalSetInt_inParam_t *) _inObj;
    int value = _inParam->value;

    int _errorCode;

    //call user function
    _errorCode = lTalSetInt(value);

    if (_errorCode != MAOPAO_SUCCESS){
        maopaoServerSendError(_requestInfo, MAOPAO_ERROR_TAL_OPERATION_FAILED, MAOPAO_ERROR_TAL_OPERATION_FAILED_MSG);
        goto _release_obj;
    }

    //send response to client
    maopaoServerSendResponse(_requestInfo, "null");

    //free the space
_release_obj:
    mson_releaseWithObj(_inObj);

_exit:
    //release the mutex
    xSemaphoreGive(_mux);
}

void __maopaoServer__lTalGetInt__service__(maopao_requestInfo_st *_requestInfo, SemaphoreHandle_t _mux)
{
    //prepare output parameter
    maopao_lTalGetInt_outParam_t _outParam;
    int value;

    int _errorCode;

    //call user function
    _errorCode = lTalGetInt(&value);

    if (_errorCode != MAOPAO_SUCCESS){
        maopaoServerSendError(_requestInfo, MAOPAO_ERROR_TAL_OPERATION_FAILED, MAOPAO_ERROR_TAL_OPERATION_FAILED_MSG);
        goto _release_obj;
    }

    //copy the output parameters to the structure
    _outParam.value = value;
    char *_resultJson = mson_objToJson(&_outParam, MSON_MEMBERS_TO_STRING(maopao_lTalGetInt_outParam_t));

    //send response to client
    maopaoServerSendResponse(_requestInfo, _resultJson);

    //free the space
    free(_resultJson);
_release_obj:

    //release the mutex
    xSemaphoreGive(_mux);
}

void __maopaoServer__lTalSetFloat__service__(maopao_requestInfo_st *_requestInfo, SemaphoreHandle_t _mux)
{
    //prepare input parameter
    mson_jsonToObj(_requestInfo->params, MSON_MEMBERS_TO_STRING(maopao_lTalSetFloat_inParam_t), _inObj);
    if (_inObj == NULL){
        maopaoServerSendError(_requestInfo, MAOPAO_ERROR_RPC_ANALYSE_FAILED, MAOPAO_ERROR_RPC_ANALYSE_FAILED_MSG);
        goto _exit;
    }
    maopao_lTalSetFloat_inParam_t *_inParam = (maopao_lTalSetFloat_inParam_t *) _inObj;
    float value = _inParam->value;

    int _errorCode;

    //call user function
    _errorCode = lTalSetFloat(value);

    if (_errorCode != MAOPAO_SUCCESS){
        maopaoServerSendError(_requestInfo, MAOPAO_ERROR_TAL_OPERATION_FAILED, MAOPAO_ERROR_TAL_OPERATION_FAILED_MSG);
        goto _release_obj;
    }

    //send response to client
    maopaoServerSendResponse(_requestInfo, "null");

    //free the space
_release_obj:
    mson_releaseWithObj(_inObj);

_exit:
    //release the mutex
    xSemaphoreGive(_mux);
}

void __maopaoServer__lTalGetFloat__service__(maopao_requestInfo_st *_requestInfo, SemaphoreHandle_t _mux)
{
    //prepare output parameter
    maopao_lTalGetFloat_outParam_t _outParam;
    float value;

    int _errorCode;

    //call user function
    _errorCode = lTalGetFloat(&value);

    if (_errorCode != MAOPAO_SUCCESS){
        maopaoServerSendError(_requestInfo, MAOPAO_ERROR_TAL_OPERATION_FAILED, MAOPAO_ERROR_TAL_OPERATION_FAILED_MSG);
        goto _release_obj;
    }

    //copy the output parameters to the structure
    _outParam.value = value;
    char *_resultJson = mson_objToJson(&_outParam, MSON_MEMBERS_TO_STRING(maopao_lTalGetFloat_outParam_t));

    //send response to client
    maopaoServerSendResponse(_requestInfo, _resultJson);

    //free the space
    free(_resultJson);
_release_obj:

    //release the mutex
    xSemaphoreGive(_mux);
}

void __maopaoServer__lTalSetBool__service__(maopao_requestInfo_st *_requestInfo, SemaphoreHandle_t _mux)
{
    //prepare input parameter
    mson_jsonToObj(_requestInfo->params, MSON_MEMBERS_TO_STRING(maopao_lTalSetBool_inParam_t), _inObj);
    if (_inObj == NULL){
        maopaoServerSendError(_requestInfo, MAOPAO_ERROR_RPC_ANALYSE_FAILED, MAOPAO_ERROR_RPC_ANALYSE_FAILED_MSG);
        goto _exit;
    }
    maopao_lTalSetBool_inParam_t *_inParam = (maopao_lTalSetBool_inParam_t *) _inObj;
    bool value = _inParam->value;

    int _errorCode;

    //call user function
    _errorCode = lTalSetBool(value);

    if (_errorCode != MAOPAO_SUCCESS){
        maopaoServerSendError(_requestInfo, MAOPAO_ERROR_TAL_OPERATION_FAILED, MAOPAO_ERROR_TAL_OPERATION_FAILED_MSG);
        goto _release_obj;
    }

    //send response to client
    maopaoServerSendResponse(_requestInfo, "null");

    //free the space
_release_obj:
    mson_releaseWithObj(_inObj);

_exit:
    //release the mutex
    xSemaphoreGive(_mux);
}

void __maopaoServer__lTalGetBool__service__(maopao_requestInfo_st *_requestInfo, SemaphoreHandle_t _mux)
{
    //prepare output parameter
    maopao_lTalGetBool_outParam_t _outParam;
    bool value;

    int _errorCode;

    //call user function
    _errorCode = lTalGetBool(&value);

    if (_errorCode != MAOPAO_SUCCESS){
        maopaoServerSendError(_requestInfo, MAOPAO_ERROR_TAL_OPERATION_FAILED, MAOPAO_ERROR_TAL_OPERATION_FAILED_MSG);
        goto _release_obj;
    }

    //copy the output parameters to the structure
    _outParam.value = value;
    char *_resultJson = mson_objToJson(&_outParam, MSON_MEMBERS_TO_STRING(maopao_lTalGetBool_outParam_t));

    //send response to client
    maopaoServerSendResponse(_requestInfo, _resultJson);

    //free the space
    free(_resultJson);
_release_obj:

    //release the mutex
    xSemaphoreGive(_mux);
}

void __maopaoServer__lTalSetString__service__(maopao_requestInfo_st *_requestInfo, SemaphoreHandle_t _mux)
{
    //prepare input parameter
    mson_jsonToObj(_requestInfo->params, MSON_MEMBERS_TO_STRING(maopao_lTalSetString_inParam_t), _inObj);
    if (_inObj == NULL){
        maopaoServerSendError(_requestInfo, MAOPAO_ERROR_RPC_ANALYSE_FAILED, MAOPAO_ERROR_RPC_ANALYSE_FAILED_MSG);
        goto _exit;
    }
    maopao_lTalSetString_inParam_t *_inParam = (maopao_lTalSetString_inParam_t *) _inObj;
    char *value = _inParam->value;

    int _errorCode;

    //call user function
    _errorCode = lTalSetString(value);

    if (_errorCode != MAOPAO_SUCCESS){
        maopaoServerSendError(_requestInfo, MAOPAO_ERROR_TAL_OPERATION_FAILED, MAOPAO_ERROR_TAL_OPERATION_FAILED_MSG);
        goto _release_obj;
    }

    //send response to client
    maopaoServerSendResponse(_requestInfo, "null");

    //free the space
_release_obj:
    mson_releaseWithObj(_inObj);

_exit:
    //release the mutex
    xSemaphoreGive(_mux);
}

void __maopaoServer__lTalGetString__service__(maopao_requestInfo_st *_requestInfo, SemaphoreHandle_t _mux)
{
    //prepare output parameter
    maopao_lTalGetString_outParam_t _outParam;
    char *value;
    char *value_addressRecord;
    value = malloc(STRING_SIZE);
    memset(value, 0, STRING_SIZE);
    value_addressRecord = value;

    int _errorCode;

    //call user function
    _errorCode = lTalGetString(value);

    if (_errorCode != MAOPAO_SUCCESS){
        maopaoServerSendError(_requestInfo, MAOPAO_ERROR_TAL_OPERATION_FAILED, MAOPAO_ERROR_TAL_OPERATION_FAILED_MSG);
        goto _release_obj;
    }

    //copy the output parameters to the structure
    _outParam.value = value;
    char *_resultJson = mson_objToJson(&_outParam, MSON_MEMBERS_TO_STRING(maopao_lTalGetString_outParam_t));

    //send response to client
    maopaoServerSendResponse(_requestInfo, _resultJson);

    //free the space
    free(value_addressRecord);
    free(_resultJson);
_release_obj:

    //release the mutex
    xSemaphoreGive(_mux);
}

void __maopaoServer__lTalSetMultiValue__service__(maopao_requestInfo_st *_requestInfo, SemaphoreHandle_t _mux)
{
    //prepare input parameter
    mson_jsonToObj(_requestInfo->params, MSON_MEMBERS_TO_STRING(maopao_lTalSetMultiValue_inParam_t), _inObj);
    if (_inObj == NULL){
        maopaoServerSendError(_requestInfo, MAOPAO_ERROR_RPC_ANALYSE_FAILED, MAOPAO_ERROR_RPC_ANALYSE_FAILED_MSG);
        goto _exit;
    }
    maopao_lTalSetMultiValue_inParam_t *_inParam = (maopao_lTalSetMultiValue_inParam_t *) _inObj;
    int value1 = _inParam->value1;
    char *value2 = _inParam->value2;
    float value3 = _inParam->value3;

    int _errorCode;

    //call user function
    _errorCode = lTalSetMultiValue(value1, value2, value3);

    if (_errorCode != MAOPAO_SUCCESS){
        maopaoServerSendError(_requestInfo, MAOPAO_ERROR_TAL_OPERATION_FAILED, MAOPAO_ERROR_TAL_OPERATION_FAILED_MSG);
        goto _release_obj;
    }

    //send response to client
    maopaoServerSendResponse(_requestInfo, "null");

    //free the space
_release_obj:
    mson_releaseWithObj(_inObj);

_exit:
    //release the mutex
    xSemaphoreGive(_mux);
}

void __maopaoServer__lTalGetMultiValue__service__(maopao_requestInfo_st *_requestInfo, SemaphoreHandle_t _mux)
{
    //prepare output parameter
    maopao_lTalGetMultiValue_outParam_t _outParam;
    int value1;
    char *value2;
    char *value2_addressRecord;
    value2 = malloc(STRING_SIZE);
    memset(value2, 0, STRING_SIZE);
    value2_addressRecord = value2;
    float value3;

    int _errorCode;

    //call user function
    _errorCode = lTalGetMultiValue(&value1, value2, &value3);

    if (_errorCode != MAOPAO_SUCCESS){
        maopaoServerSendError(_requestInfo, MAOPAO_ERROR_TAL_OPERATION_FAILED, MAOPAO_ERROR_TAL_OPERATION_FAILED_MSG);
        goto _release_obj;
    }

    //copy the output parameters to the structure
    _outParam.value1 = value1;
    _outParam.value2 = value2;
    _outParam.value3 = value3;
    char *_resultJson = mson_objToJson(&_outParam, MSON_MEMBERS_TO_STRING(maopao_lTalGetMultiValue_outParam_t));

    //send response to client
    maopaoServerSendResponse(_requestInfo, _resultJson);

    //free the space
    free(value2_addressRecord);
    free(_resultJson);
_release_obj:

    //release the mutex
    xSemaphoreGive(_mux);
}

maopao_service_handler_st tal_primary_serviceGroup[] = {
        {.method = "setInt", .methodId = "p0000002", .handlerCb = MAOPAO_SERVICE(lTalSetInt), .inParam = MSON_MEMBERS_TO_STRING(maopao_lTalSetInt_inParam_t), .outParam = NULL},
        {.method = "getInt", .methodId = "p0000007", .handlerCb = MAOPAO_SERVICE(lTalGetInt), .inParam = NULL, .outParam = MSON_MEMBERS_TO_STRING(maopao_lTalGetInt_outParam_t)},
        {.method = "setFloat", .methodId = "p0000003", .handlerCb = MAOPAO_SERVICE(lTalSetFloat), .inParam = MSON_MEMBERS_TO_STRING(maopao_lTalSetFloat_inParam_t), .outParam = NULL},
        {.method = "getFloat", .methodId = "p0000008", .handlerCb = MAOPAO_SERVICE(lTalGetFloat), .inParam = NULL, .outParam = MSON_MEMBERS_TO_STRING(maopao_lTalGetFloat_outParam_t)},
        {.method = "setBool", .methodId = "p0000004", .handlerCb = MAOPAO_SERVICE(lTalSetBool), .inParam = MSON_MEMBERS_TO_STRING(maopao_lTalSetBool_inParam_t), .outParam = NULL},
        {.method = "getBool", .methodId = "p0000009", .handlerCb = MAOPAO_SERVICE(lTalGetBool), .inParam = NULL, .outParam = MSON_MEMBERS_TO_STRING(maopao_lTalGetBool_outParam_t)},
        {.method = "setString", .methodId = "p0000005", .handlerCb = MAOPAO_SERVICE(lTalSetString), .inParam = MSON_MEMBERS_TO_STRING(maopao_lTalSetString_inParam_t), .outParam = NULL},
        {.method = "getString", .methodId = "p000000a", .handlerCb = MAOPAO_SERVICE(lTalGetString), .inParam = NULL, .outParam = MSON_MEMBERS_TO_STRING(maopao_lTalGetString_outParam_t)},
        {.method = "setMultiValue", .methodId = "p0000006", .handlerCb = MAOPAO_SERVICE(lTalSetMultiValue), .inParam = MSON_MEMBERS_TO_STRING(maopao_lTalSetMultiValue_inParam_t), .outParam = NULL},
        {.method = "getMultiValue", .methodId = "p000000b", .handlerCb = MAOPAO_SERVICE(lTalGetMultiValue), .inParam = NULL, .outParam = MSON_MEMBERS_TO_STRING(maopao_lTalGetMultiValue_outParam_t)},
        {.method = NULL, .handlerCb = NULL}
};

#ifdef AUTO_ADD_SERVICE_GROUP
static int lTalAutoAddServiceGroup_tal_primary(void){
    maopaoServerAddDefaultServiceGroup(tal_primary_serviceGroup);
    return 0;
}
INIT_APP_EXPORT(lTalAutoAddServiceGroup_tal_primary);
#endif //AUTO_ADD_SERVICE_GROUP

/* USER CODE BEGIN 2 */

/* USER CODE END 2 */

