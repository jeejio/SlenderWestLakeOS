// mprpcgen version: 1.4.7
/* USER CODE BEGIN Header */
/**
  * @file tal_primary.c
  * @attention
  * Slender West Lake automatically generated code.
  * mprpcgen version: 1.4.2 .
  *
  */
/* USER CODE END Header */

#include <stdio.h>
#include <stdlib.h>
#include "tal_primary.h"
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* USER CODE BEGIN 1 */
static int intValue = 0;
static float floatValue = 0.0f;
static bool boolValue = false;
static char stringValue[1024];
/* USER CODE END 1 */

int lTalSetInt(int value)
{
    /* USER CODE BEGIN setInt lTalSetInt p0000002 */
    intValue = value;
    printf("set intValue = %d\n", intValue);
    return MAOPAO_SUCCESS;
    /* USER CODE END setInt lTalSetInt p0000002 */
}

int lTalGetInt(int *value)
{
    /* USER CODE BEGIN getInt lTalGetInt p0000007 */
    *value = intValue;
    printf("get intValue = %d\n", intValue);
    return MAOPAO_SUCCESS;
    /* USER CODE END getInt lTalGetInt p0000007 */
}

int lTalSetFloat(float value)
{
    /* USER CODE BEGIN setFloat lTalSetFloat p0000003 */
    floatValue = value;
    printf("set floatValue = %f\n", floatValue);
    return MAOPAO_SUCCESS;
    /* USER CODE END setFloat lTalSetFloat p0000003 */
}

int lTalGetFloat(float *value)
{
    /* USER CODE BEGIN getFloat lTalGetFloat p0000008 */
    // 此方式返回值可能有浮点误差，如需精确到指定位数小数的功能，可参考tal_precision_decimal
    *value = floatValue;
    printf("get floatValue = %f\n", floatValue);
    return MAOPAO_SUCCESS;
    /* USER CODE END getFloat lTalGetFloat p0000008 */
}

int lTalSetBool(bool value)
{
    /* USER CODE BEGIN setBool lTalSetBool p0000004 */
    boolValue = value;
    printf("set boolValue = %d\n", boolValue);
    return MAOPAO_SUCCESS;
    /* USER CODE END setBool lTalSetBool p0000004 */
}

int lTalGetBool(bool *value)
{
    /* USER CODE BEGIN getBool lTalGetBool p0000009 */
    *value = boolValue;
    printf("get boolValue = %d\n", boolValue);
    return MAOPAO_SUCCESS;
    /* USER CODE END getBool lTalGetBool p0000009 */
}

int lTalSetString(char *value)
{
    /* USER CODE BEGIN setString lTalSetString p0000005 */
    // 注意，使用value时需拷贝内容，不能直接保存该指针
    strcpy(stringValue, value);
    printf("set stringValue = %s\n", stringValue);
    return MAOPAO_SUCCESS;
    /* USER CODE END setString lTalSetString p0000005 */
}

int lTalGetString(char *value)
{
    /* USER CODE BEGIN getString lTalGetString p000000a */
    // 注意，赋值结果时需拷贝内容，不能直接修改value的指针
    strcpy(value, stringValue);
    printf("get stringValue = %s\n", stringValue);
    return MAOPAO_SUCCESS;
    /* USER CODE END getString lTalGetString p000000a */
}

int lTalSetMultiValue(int value1, char *value2, float value3)
{
    /* USER CODE BEGIN setMultiValue lTalSetMultiValue p0000006 */
    intValue = value1;
    printf("set intValue = %d\n", intValue);

    strcpy(stringValue, value2);
    printf("set stringValue = %s\n", stringValue);

    floatValue = value3;
    printf("set floatValue = %f\n", floatValue);
    return MAOPAO_SUCCESS;
    /* USER CODE END setMultiValue lTalSetMultiValue p0000006 */
}

int lTalGetMultiValue(int *value1, char *value2, float *value3)
{
    /* USER CODE BEGIN getMultiValue lTalGetMultiValue p000000b */
    *value1 = intValue;
    printf("get intValue = %d\n", intValue);

    strcpy(value2, stringValue);
    printf("get stringValue = %s\n", stringValue);

    *value3 = floatValue;
    printf("get floatValue = %f\n", floatValue);
    return MAOPAO_SUCCESS;
    /* USER CODE END getMultiValue lTalGetMultiValue p000000b */
}

/* USER CODE BEGIN 2 */

/* USER CODE END 2 */

