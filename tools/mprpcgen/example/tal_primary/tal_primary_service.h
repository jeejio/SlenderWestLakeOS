// mprpcgen version: 1.4.7
/* USER CODE BEGIN Header */
/**
  * @file tal_primary_service.h
  * @attention
  * Slender West Lake automatically generated code.
  *
  */
/* USER CODE END Header */

#ifndef TAL_PRIMARY_SERVICE_H
#define TAL_PRIMARY_SERVICE_H

#include "maopaoServer.h"
#include "maopaoError.h"
#include "mson.h"
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* USER CODE BEGIN 1 */

/* USER CODE END 1 */

extern maopao_service_handler_st tal_primary_serviceGroup[];

/* USER CODE BEGIN 2 */

/* USER CODE END 2 */

#endif  //TAL_PRIMARY_SERVICE_H
