// mprpcgen version: 1.4.7
/* USER CODE BEGIN Header */
/**
  * @file tal_primary.h
  * @attention
  * Slender West Lake automatically generated code.
  *
  */
/* USER CODE END Header */

#ifndef TAL_PRIMARY_H
#define TAL_PRIMARY_H

#include <stdbool.h>
#include "maopaoServer.h"
#include "maopaoError.h"
#include "mson.h"
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* USER CODE BEGIN 1 */

/* USER CODE END 1 */

extern maopao_service_handler_st tal_primary_serviceGroup[];

/* USER CODE BEGIN 2 */

/* USER CODE END 2 */

/* USER CODE BEGIN 3 */

/* USER CODE END 3 */

/**
 * @brief lTalSetInt
 * @param[in] value
 * @return errorCode
 */
int lTalSetInt(int value);

/**
 * @brief lTalGetInt
 * @param[out] value
 * @return errorCode
 */
int lTalGetInt(int *value);

/**
 * @brief lTalSetFloat
 * @param[in] value
 * @return errorCode
 */
int lTalSetFloat(float value);

/**
 * @brief lTalGetFloat
 * @param[out] value
 * @return errorCode
 */
int lTalGetFloat(float *value);

/**
 * @brief lTalSetBool
 * @param[in] value
 * @return errorCode
 */
int lTalSetBool(bool value);

/**
 * @brief lTalGetBool
 * @param[out] value
 * @return errorCode
 */
int lTalGetBool(bool *value);

/**
 * @brief lTalSetString
 * @param[in] value
 * @return errorCode
 */
int lTalSetString(char *value);

/**
 * @brief lTalGetString
 * @param[out] value
 * @return errorCode
 */
int lTalGetString(char *value);

/**
 * @brief lTalSetMultiValue
 * @param[in] value1
 * @param[in] value2
 * @param[in] value3
 * @return errorCode
 */
int lTalSetMultiValue(int value1, char *value2, float value3);

/**
 * @brief lTalGetMultiValue
 * @param[out] value1
 * @param[out] value2
 * @param[out] value3
 * @return errorCode
 */
int lTalGetMultiValue(int *value1, char *value2, float *value3);

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

#endif  //TAL_PRIMARY_H
