// mprpcgen version: 1.4.7
/* USER CODE BEGIN Header */
/**
  * @file tal_precision_decimal.c
  * @attention
  * Slender West Lake automatically generated code.
  *
  */
/* USER CODE END Header */

#include <stdio.h>
#include <stdlib.h>
#include "tal_precision_decimal.h"
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* USER CODE BEGIN 1 */
float floatValue;
/* USER CODE END 1 */

int lTalSetPrecisionDecimal(FloatNumber value)
{
    /* USER CODE BEGIN setPrecisionDecimal lTalSetPrecisionDecimal p000000h */
    floatValue = value.number;
    printf("set floatValue = %f\n", floatValue);
    return MAOPAO_SUCCESS;
    /* USER CODE END setPrecisionDecimal lTalSetPrecisionDecimal p000000h */
}

int lTalGetPrecisionDecimal(FloatNumber *value)
{
    /* USER CODE BEGIN getPrecisionDecimal lTalGetPrecisionDecimal p000000i */
    value->number = floatValue;
    value->decimalPlaces = 1; // 保留一位小数
    printf("get floatValue = %f\n", value->number);
    return MAOPAO_SUCCESS;
    /* USER CODE END getPrecisionDecimal lTalGetPrecisionDecimal p000000i */
}

/* USER CODE BEGIN 2 */

/* USER CODE END 2 */

