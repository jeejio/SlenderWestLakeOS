// mprpcgen version: 1.4.7
/* USER CODE BEGIN Header */
/**
  * @file tal_precision_decimal.h
  * @attention
  * Slender West Lake automatically generated code.
  *
  */
/* USER CODE END Header */

#ifndef TAL_PRECISION_DECIMAL_H
#define TAL_PRECISION_DECIMAL_H

#include <stdbool.h>
#include "maopaoServer.h"
#include "maopaoError.h"
#include "mson.h"
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* USER CODE BEGIN 1 */

/* USER CODE END 1 */

extern maopao_service_handler_st tal_precision_decimal_serviceGroup[];

/* USER CODE BEGIN 2 */

/* USER CODE END 2 */

/* USER CODE BEGIN 3 */

/* USER CODE END 3 */

/**
 * @brief lTalSetPrecisionDecimal
 * @param[in] value
 * @return errorCode
 */
int lTalSetPrecisionDecimal(FloatNumber value);

/**
 * @brief lTalGetPrecisionDecimal
 * @param[out] value
 * @return errorCode
 */
int lTalGetPrecisionDecimal(FloatNumber *value);

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

#endif  //TAL_PRECISION_DECIMAL_H
