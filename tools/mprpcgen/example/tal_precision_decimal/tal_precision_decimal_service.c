// mprpcgen version: 1.4.7
/* USER CODE BEGIN Header */
/**
  * @file tal_precision_decimal_service.c
  * @attention
  * Slender West Lake automatically generated code.
  *
  */
/* USER CODE END Header */

#include <stdio.h>
#include <stdlib.h>
#include <memory.h>
#include "tal_precision_decimal_service.h"
#include "tal_precision_decimal.h"
#include "auto_init.h"
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* USER CODE BEGIN Define */
#define STRING_SIZE 1024

/* USER CODE END Define */

/* USER CODE BEGIN 1 */

/* USER CODE END 1 */

TalInfo_st _talInfo = {
    .manufacturer = "jeejio",
    .category = "category_code",
    .version = "1.0.0",
    .developer = "developer@jeejio.com",
};

//Define a MSON structure for input parameter processing
MSON_DEFOBJ(maopao_lTalSetPrecisionDecimal_inParam_t,
    FloatNumber value;
);

//Define a MSON structure for output parameter processing
MSON_DEFOBJ(maopao_lTalGetPrecisionDecimal_outParam_t,
    FloatNumber value;
);

void __maopaoServer__lTalSetPrecisionDecimal__service__(maopao_requestInfo_st *_requestInfo, SemaphoreHandle_t _mux)
{
    //prepare input parameter
    mson_jsonToObj(_requestInfo->params, MSON_MEMBERS_TO_STRING(maopao_lTalSetPrecisionDecimal_inParam_t), _inObj);
    if (_inObj == NULL){
        maopaoServerSendError(_requestInfo, MAOPAO_ERROR_RPC_ANALYSE_FAILED, MAOPAO_ERROR_RPC_ANALYSE_FAILED_MSG);
        goto _exit;
    }
    maopao_lTalSetPrecisionDecimal_inParam_t *_inParam = (maopao_lTalSetPrecisionDecimal_inParam_t *) _inObj;
    FloatNumber value = _inParam->value;

    int _errorCode;

    //call user function
    _errorCode = lTalSetPrecisionDecimal(value);

    if (_errorCode != MAOPAO_SUCCESS){
        maopaoServerSendError(_requestInfo, MAOPAO_ERROR_TAL_OPERATION_FAILED, MAOPAO_ERROR_TAL_OPERATION_FAILED_MSG);
        goto _release_obj;
    }

    //send response to client
    maopaoServerSendResponse(_requestInfo, "null");

    //free the space
_release_obj:
    mson_releaseWithObj(_inObj);

_exit:
    //release the mutex
    xSemaphoreGive(_mux);
}

void __maopaoServer__lTalGetPrecisionDecimal__service__(maopao_requestInfo_st *_requestInfo, SemaphoreHandle_t _mux)
{
    //prepare output parameter
    maopao_lTalGetPrecisionDecimal_outParam_t _outParam;
    FloatNumber value;

    int _errorCode;

    //call user function
    _errorCode = lTalGetPrecisionDecimal(&value);

    if (_errorCode != MAOPAO_SUCCESS){
        maopaoServerSendError(_requestInfo, MAOPAO_ERROR_TAL_OPERATION_FAILED, MAOPAO_ERROR_TAL_OPERATION_FAILED_MSG);
        goto _release_obj;
    }

    //copy the output parameters to the structure
    _outParam.value = value;
    char *_resultJson = mson_objToJson(&_outParam, MSON_MEMBERS_TO_STRING(maopao_lTalGetPrecisionDecimal_outParam_t));

    //send response to client
    maopaoServerSendResponse(_requestInfo, _resultJson);

    //free the space
    free(_resultJson);
_release_obj:

    //release the mutex
    xSemaphoreGive(_mux);
}

maopao_service_handler_st tal_precision_decimal_serviceGroup[] = {
        {.method = "setPrecisionDecimal", .methodId = "p000000h", .handlerCb = MAOPAO_SERVICE(lTalSetPrecisionDecimal), .inParam = MSON_MEMBERS_TO_STRING(maopao_lTalSetPrecisionDecimal_inParam_t), .outParam = NULL},
        {.method = "getPrecisionDecimal", .methodId = "p000000i", .handlerCb = MAOPAO_SERVICE(lTalGetPrecisionDecimal), .inParam = NULL, .outParam = MSON_MEMBERS_TO_STRING(maopao_lTalGetPrecisionDecimal_outParam_t)},
        {.method = NULL, .handlerCb = NULL}
};

#ifdef AUTO_ADD_SERVICE_GROUP
static int lTalAutoAddServiceGroup_tal_precision_decimal(void){
    maopaoServerAddDefaultServiceGroup(tal_precision_decimal_serviceGroup);
    return 0;
}
INIT_APP_EXPORT(lTalAutoAddServiceGroup_tal_precision_decimal);
#endif //AUTO_ADD_SERVICE_GROUP

/* USER CODE BEGIN 2 */

/* USER CODE END 2 */

