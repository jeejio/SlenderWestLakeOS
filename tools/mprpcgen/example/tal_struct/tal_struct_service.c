// mprpcgen version: 1.4.7
/* USER CODE BEGIN Header */
/**
  * @file tal_struct_service.c
  * @attention
  * Slender West Lake automatically generated code.
  *
  */
/* USER CODE END Header */

#include <stdio.h>
#include <stdlib.h>
#include <memory.h>
#include "tal_struct_service.h"
#include "tal_struct.h"
#include "auto_init.h"
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* USER CODE BEGIN Define */
#define STRING_SIZE 1024

/* USER CODE END Define */

/* USER CODE BEGIN 1 */

/* USER CODE END 1 */

TalInfo_st _talInfo = {
    .manufacturer = "jeejio",
    .category = "category_code",
    .version = "1.0.0",
    .developer = "developer@jeejio.com",
};

//Define a MSON structure for input parameter processing
MSON_DEFOBJ(maopao_lTalSetColorValue_inParam_t,
    struct {
    int red;
    int green;
    int blue;
    } value;
);

//Define a MSON structure for output parameter processing
MSON_DEFOBJ(maopao_lTalGetColorValue_outParam_t,
    struct {
    int red;
    int green;
    int blue;
    } value;
);

void __maopaoServer__lTalSetColorValue__service__(maopao_requestInfo_st *_requestInfo, SemaphoreHandle_t _mux)
{
    //prepare input parameter
    mson_jsonToObj(_requestInfo->params, MSON_MEMBERS_TO_STRING(maopao_lTalSetColorValue_inParam_t), _inObj);
    if (_inObj == NULL){
        maopaoServerSendError(_requestInfo, MAOPAO_ERROR_RPC_ANALYSE_FAILED, MAOPAO_ERROR_RPC_ANALYSE_FAILED_MSG);
        goto _exit;
    }
    maopao_lTalSetColorValue_inParam_t *_inParam = (maopao_lTalSetColorValue_inParam_t *) _inObj;
    struct rgb_t value;
    memcpy(&value, &_inParam->value, sizeof(struct rgb_t ));

    int _errorCode;

    //call user function
    _errorCode = lTalSetColorValue(value);

    if (_errorCode != MAOPAO_SUCCESS){
        maopaoServerSendError(_requestInfo, MAOPAO_ERROR_TAL_OPERATION_FAILED, MAOPAO_ERROR_TAL_OPERATION_FAILED_MSG);
        goto _release_obj;
    }

    //send response to client
    maopaoServerSendResponse(_requestInfo, "null");

    //free the space
_release_obj:
    mson_releaseWithObj(_inObj);

_exit:
    //release the mutex
    xSemaphoreGive(_mux);
}

void __maopaoServer__lTalGetColorValue__service__(maopao_requestInfo_st *_requestInfo, SemaphoreHandle_t _mux)
{
    //prepare output parameter
    maopao_lTalGetColorValue_outParam_t _outParam;
    struct rgb_t value;

    int _errorCode;

    //call user function
    _errorCode = lTalGetColorValue(&value);

    if (_errorCode != MAOPAO_SUCCESS){
        maopaoServerSendError(_requestInfo, MAOPAO_ERROR_TAL_OPERATION_FAILED, MAOPAO_ERROR_TAL_OPERATION_FAILED_MSG);
        goto _release_obj;
    }

    //copy the output parameters to the structure
    memcpy(&_outParam.value, &value, sizeof(struct rgb_t ));
    char *_resultJson = mson_objToJson(&_outParam, MSON_MEMBERS_TO_STRING(maopao_lTalGetColorValue_outParam_t));

    //send response to client
    maopaoServerSendResponse(_requestInfo, _resultJson);

    //free the space
    free(_resultJson);
_release_obj:

    //release the mutex
    xSemaphoreGive(_mux);
}

maopao_service_handler_st tal_struct_serviceGroup[] = {
        {.method = "setColor", .methodId = "p000000f", .handlerCb = MAOPAO_SERVICE(lTalSetColorValue), .inParam = MSON_MEMBERS_TO_STRING(maopao_lTalSetColorValue_inParam_t), .outParam = NULL},
        {.method = "getColor", .methodId = "p000000g", .handlerCb = MAOPAO_SERVICE(lTalGetColorValue), .inParam = NULL, .outParam = MSON_MEMBERS_TO_STRING(maopao_lTalGetColorValue_outParam_t)},
        {.method = NULL, .handlerCb = NULL}
};

#ifdef AUTO_ADD_SERVICE_GROUP
static int lTalAutoAddServiceGroup_tal_struct(void){
    maopaoServerAddDefaultServiceGroup(tal_struct_serviceGroup);
    return 0;
}
INIT_APP_EXPORT(lTalAutoAddServiceGroup_tal_struct);
#endif //AUTO_ADD_SERVICE_GROUP

/* USER CODE BEGIN 2 */

/* USER CODE END 2 */

