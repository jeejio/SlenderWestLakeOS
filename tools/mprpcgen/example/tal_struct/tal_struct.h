// mprpcgen version: 1.4.7
/* USER CODE BEGIN Header */
/**
  * @file tal_struct.h
  * @attention
  * Slender West Lake automatically generated code.
  *
  */
/* USER CODE END Header */

#ifndef TAL_STRUCT_H
#define TAL_STRUCT_H

#include <stdbool.h>
#include "maopaoServer.h"
#include "maopaoError.h"
#include "mson.h"
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* USER CODE BEGIN 1 */

/* USER CODE END 1 */

extern maopao_service_handler_st tal_struct_serviceGroup[];

/* USER CODE BEGIN 2 */

/* USER CODE END 2 */

struct rgb_t {
    int red;
    int green;
    int blue;
};

/* USER CODE BEGIN 3 */

/* USER CODE END 3 */

/**
 * @brief lTalSetColorValue
 * @param[in] value
 * @return errorCode
 */
int lTalSetColorValue(struct rgb_t value);

/**
 * @brief lTalGetColorValue
 * @param[out] value
 * @return errorCode
 */
int lTalGetColorValue(struct rgb_t *value);

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

#endif  //TAL_STRUCT_H
