// mprpcgen version: 1.4.7
/* USER CODE BEGIN Header */
/**
  * @file tal_struct.c
  * @attention
  * Slender West Lake automatically generated code.
  *
  */
/* USER CODE END Header */

#include <stdio.h>
#include <stdlib.h>
#include "tal_struct.h"
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* USER CODE BEGIN 1 */
int red, green, blue;
/* USER CODE END 1 */

int lTalSetColorValue(struct rgb_t value)
{
    /* USER CODE BEGIN setColor lTalSetColorValue p000000f */
    red = value.red;
    green = value.green;
    blue = value.blue;
    printf("set red = %d, green = %d, blue = %d\n", red, green, blue);
    return MAOPAO_SUCCESS;
    /* USER CODE END setColor lTalSetColorValue p000000f */
}

int lTalGetColorValue(struct rgb_t *value)
{
    /* USER CODE BEGIN getColor lTalGetColorValue p000000g */
    value->red = red;
    value->green = green;
    value->blue = blue;
    printf("get red = %d, green = %d, blue = %d\n", value->red, value->green, value->blue);
    return MAOPAO_SUCCESS;
    /* USER CODE END getColor lTalGetColorValue p000000g */
}

/* USER CODE BEGIN 2 */

/* USER CODE END 2 */

