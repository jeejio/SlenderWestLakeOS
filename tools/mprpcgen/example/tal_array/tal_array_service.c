// mprpcgen version: 1.4.7
/* USER CODE BEGIN Header */
/**
  * @file tal_array_service.c
  * @attention
  * Slender West Lake automatically generated code.
  *
  */
/* USER CODE END Header */

#include <stdio.h>
#include <stdlib.h>
#include <memory.h>
#include "tal_array_service.h"
#include "tal_array.h"
#include "auto_init.h"
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* USER CODE BEGIN Define */
#define STRING_SIZE 1024

/* USER CODE END Define */

/* USER CODE BEGIN 1 */

/* USER CODE END 1 */

TalInfo_st _talInfo = {
    .manufacturer = "jeejio",
    .category = "category_code",
    .version = "1.0.0",
    .developer = "developer@jeejio.com",
};

//Define a MSON structure for input parameter processing
MSON_DEFOBJ(maopao_lTalSetTexts_inParam_t,
    char *value[5];
);

//Define a MSON structure for output parameter processing
MSON_DEFOBJ(maopao_lTalGetTexts_outParam_t,
    char *value[5];
);

void __maopaoServer__lTalSetTexts__service__(maopao_requestInfo_st *_requestInfo, SemaphoreHandle_t _mux)
{
    //prepare input parameter
    mson_jsonToObj(_requestInfo->params, MSON_MEMBERS_TO_STRING(maopao_lTalSetTexts_inParam_t), _inObj);
    if (_inObj == NULL){
        maopaoServerSendError(_requestInfo, MAOPAO_ERROR_RPC_ANALYSE_FAILED, MAOPAO_ERROR_RPC_ANALYSE_FAILED_MSG);
        goto _exit;
    }
    maopao_lTalSetTexts_inParam_t *_inParam = (maopao_lTalSetTexts_inParam_t *) _inObj;
    char **value = _inParam->value;

    int _errorCode;

    //call user function
    _errorCode = lTalSetTexts(value);

    if (_errorCode != MAOPAO_SUCCESS){
        maopaoServerSendError(_requestInfo, MAOPAO_ERROR_TAL_OPERATION_FAILED, MAOPAO_ERROR_TAL_OPERATION_FAILED_MSG);
        goto _release_obj;
    }

    //send response to client
    maopaoServerSendResponse(_requestInfo, "null");

    //free the space
_release_obj:
    mson_releaseWithObj(_inObj);

_exit:
    //release the mutex
    xSemaphoreGive(_mux);
}

void __maopaoServer__lTalGetTexts__service__(maopao_requestInfo_st *_requestInfo, SemaphoreHandle_t _mux)
{
    //prepare output parameter
    maopao_lTalGetTexts_outParam_t _outParam;
    char *value[5];
    char *value_addressRecord[5];
    for (int i = 0; i < 5; ++i) {
        value[i] = malloc(STRING_SIZE);
        memset(value[i], 0, STRING_SIZE);
        value_addressRecord[i] = value[i];
    }

    int _errorCode;

    //call user function
    _errorCode = lTalGetTexts(value);

    if (_errorCode != MAOPAO_SUCCESS){
        maopaoServerSendError(_requestInfo, MAOPAO_ERROR_TAL_OPERATION_FAILED, MAOPAO_ERROR_TAL_OPERATION_FAILED_MSG);
        goto _release_obj;
    }

    //copy the output parameters to the structure
    memcpy(&_outParam.value, &value, sizeof(_outParam.value));
    char *_resultJson = mson_objToJson(&_outParam, MSON_MEMBERS_TO_STRING(maopao_lTalGetTexts_outParam_t));

    //send response to client
    maopaoServerSendResponse(_requestInfo, _resultJson);

    //free the space
    for (int i = 0; i < 5; ++i) {
        free(value_addressRecord[i]);
    }
    free(_resultJson);
_release_obj:

    //release the mutex
    xSemaphoreGive(_mux);
}

maopao_service_handler_st tal_array_serviceGroup[] = {
        {.method = "setTexts", .methodId = "p000000c", .handlerCb = MAOPAO_SERVICE(lTalSetTexts), .inParam = MSON_MEMBERS_TO_STRING(maopao_lTalSetTexts_inParam_t), .outParam = NULL},
        {.method = "getTexts", .methodId = "p000000d", .handlerCb = MAOPAO_SERVICE(lTalGetTexts), .inParam = NULL, .outParam = MSON_MEMBERS_TO_STRING(maopao_lTalGetTexts_outParam_t)},
        {.method = NULL, .handlerCb = NULL}
};

#ifdef AUTO_ADD_SERVICE_GROUP
static int lTalAutoAddServiceGroup_tal_array(void){
    maopaoServerAddDefaultServiceGroup(tal_array_serviceGroup);
    return 0;
}
INIT_APP_EXPORT(lTalAutoAddServiceGroup_tal_array);
#endif //AUTO_ADD_SERVICE_GROUP

/* USER CODE BEGIN 2 */

/* USER CODE END 2 */

