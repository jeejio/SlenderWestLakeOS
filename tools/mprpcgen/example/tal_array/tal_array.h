// mprpcgen version: 1.4.7
/* USER CODE BEGIN Header */
/**
  * @file tal_array.h
  * @attention
  * Slender West Lake automatically generated code.
  *
  */
/* USER CODE END Header */

#ifndef TAL_ARRAY_H
#define TAL_ARRAY_H

#include <stdbool.h>
#include "maopaoServer.h"
#include "maopaoError.h"
#include "mson.h"
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* USER CODE BEGIN 1 */

/* USER CODE END 1 */

extern maopao_service_handler_st tal_array_serviceGroup[];

/* USER CODE BEGIN 2 */

/* USER CODE END 2 */

/* USER CODE BEGIN 3 */

/* USER CODE END 3 */

/**
 * @brief lTalSetTexts
 * @param[in] value
 * @return errorCode
 */
int lTalSetTexts(char *value[5]);

/**
 * @brief lTalGetTexts
 * @param[out] value
 * @return errorCode
 */
int lTalGetTexts(char *value[5]);

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

#endif  //TAL_ARRAY_H
