// mprpcgen version: 1.4.7
/* USER CODE BEGIN Header */
/**
  * @file tal_array.c
  * @attention
  * Slender West Lake automatically generated code.
  *
  */
/* USER CODE END Header */

#include <stdio.h>
#include <stdlib.h>
#include "tal_array.h"
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* USER CODE BEGIN 1 */
char texts[5][100];
/* USER CODE END 1 */

int lTalSetTexts(char *value[5])
{
    /* USER CODE BEGIN setTexts lTalSetTexts p000000c */
    for (int i = 0; i < 5; i++) {
        sprintf(texts[i], "%s", value[i]);
        printf("set texts[%d] = %s\n", i, texts[i]);
    }
    return MAOPAO_SUCCESS;
    /* USER CODE END setTexts lTalSetTexts p000000c */
}

int lTalGetTexts(char *value[5])
{
    /* USER CODE BEGIN getTexts lTalGetTexts p000000d */
    for (int i = 0; i < 5; i++) {
        strcpy(value[i], texts[i]);
        printf("get texts[%d] = %s\n", i, value[i]);
    }
    return MAOPAO_SUCCESS;
    /* USER CODE END getTexts lTalGetTexts p000000d */
}

/* USER CODE BEGIN 2 */

/* USER CODE END 2 */

