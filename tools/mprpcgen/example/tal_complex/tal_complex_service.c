// mprpcgen version: 1.4.7
/* USER CODE BEGIN Header */
/**
  * @file tal_complex_service.c
  * @attention
  * Slender West Lake automatically generated code.
  *
  */
/* USER CODE END Header */

#include <stdio.h>
#include <stdlib.h>
#include <memory.h>
#include "tal_complex_service.h"
#include "tal_complex.h"
#include "auto_init.h"
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* USER CODE BEGIN Define */
#define STRING_SIZE 1024

/* USER CODE END Define */

/* USER CODE BEGIN 1 */

/* USER CODE END 1 */

TalInfo_st _talInfo = {
    .manufacturer = "jeejio",
    .category = "category_code",
    .version = "1.0.0",
    .developer = "developer@jeejio.com",
};

//Define a MSON structure for output parameter processing
MSON_DEFOBJ(maopao_lTalGetComplexValue1_outParam_t,
    jsonStr value;
);

//Define a MSON structure for output parameter processing
MSON_DEFOBJ(maopao_lTalGetComplexValue2_outParam_t,
    jsonStr value;
);

void __maopaoServer__lTalGetComplexValue1__service__(maopao_requestInfo_st *_requestInfo, SemaphoreHandle_t _mux)
{
    //prepare output parameter
    maopao_lTalGetComplexValue1_outParam_t _outParam;
    jsonStr value;
    jsonStr value_addressRecord;
    value = malloc(STRING_SIZE);
    memset(value, 0, STRING_SIZE);
    value_addressRecord = value;

    int _errorCode;

    //call user function
    _errorCode = lTalGetComplexValue1(value);

    if (_errorCode != MAOPAO_SUCCESS){
        maopaoServerSendError(_requestInfo, MAOPAO_ERROR_TAL_OPERATION_FAILED, MAOPAO_ERROR_TAL_OPERATION_FAILED_MSG);
        goto _release_obj;
    }

    //copy the output parameters to the structure
    _outParam.value = value;
    char *_resultJson = mson_objToJson(&_outParam, MSON_MEMBERS_TO_STRING(maopao_lTalGetComplexValue1_outParam_t));

    //send response to client
    maopaoServerSendResponse(_requestInfo, _resultJson);

    //free the space
    free(value_addressRecord);
    free(_resultJson);
_release_obj:

    //release the mutex
    xSemaphoreGive(_mux);
}

void __maopaoServer__lTalGetComplexValue2__service__(maopao_requestInfo_st *_requestInfo, SemaphoreHandle_t _mux)
{
    //prepare output parameter
    maopao_lTalGetComplexValue2_outParam_t _outParam;
    jsonStr value;
    jsonStr value_addressRecord;
    value = malloc(STRING_SIZE);
    memset(value, 0, STRING_SIZE);
    value_addressRecord = value;

    int _errorCode;

    //call user function
    _errorCode = lTalGetComplexValue2(value);

    if (_errorCode != MAOPAO_SUCCESS){
        maopaoServerSendError(_requestInfo, MAOPAO_ERROR_TAL_OPERATION_FAILED, MAOPAO_ERROR_TAL_OPERATION_FAILED_MSG);
        goto _release_obj;
    }

    //copy the output parameters to the structure
    _outParam.value = value;
    char *_resultJson = mson_objToJson(&_outParam, MSON_MEMBERS_TO_STRING(maopao_lTalGetComplexValue2_outParam_t));

    //send response to client
    maopaoServerSendResponse(_requestInfo, _resultJson);

    //free the space
    free(value_addressRecord);
    free(_resultJson);
_release_obj:

    //release the mutex
    xSemaphoreGive(_mux);
}

maopao_service_handler_st tal_complex_serviceGroup[] = {
        {.method = "getComplex1", .methodId = NULL, .handlerCb = MAOPAO_SERVICE(lTalGetComplexValue1), .inParam = NULL, .outParam = MSON_MEMBERS_TO_STRING(maopao_lTalGetComplexValue1_outParam_t)},
        {.method = "getComplex2", .methodId = NULL, .handlerCb = MAOPAO_SERVICE(lTalGetComplexValue2), .inParam = NULL, .outParam = MSON_MEMBERS_TO_STRING(maopao_lTalGetComplexValue2_outParam_t)},
        {.method = NULL, .handlerCb = NULL}
};

#ifdef AUTO_ADD_SERVICE_GROUP
static int lTalAutoAddServiceGroup_tal_complex(void){
    maopaoServerAddDefaultServiceGroup(tal_complex_serviceGroup);
    return 0;
}
INIT_APP_EXPORT(lTalAutoAddServiceGroup_tal_complex);
#endif //AUTO_ADD_SERVICE_GROUP

/* USER CODE BEGIN 2 */

/* USER CODE END 2 */

