// mprpcgen version: 1.4.7
/* USER CODE BEGIN Header */
/**
  * @file tal_complex.c
  * @attention
  * Slender West Lake automatically generated code.
  *
  */
/* USER CODE END Header */

#include <stdio.h>
#include <stdlib.h>
#include "tal_complex.h"
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* USER CODE BEGIN 1 */

/* USER CODE END 1 */

int lTalGetComplexValue1(jsonStr value)
{
    /* USER CODE BEGIN getComplex1 lTalGetComplexValue1 */
    // jsonStr可以返回动态key的json对象，或复杂的json对象
    sprintf(value, "{\"project\":\"maopao\"}");
    return MAOPAO_SUCCESS;
    /* USER CODE END getComplex1 lTalGetComplexValue1 */
}

int lTalGetComplexValue2(jsonStr value)
{
    /* USER CODE BEGIN getComplex2 lTalGetComplexValue2 */
    // jsonStr可以返回动态长度的数组
    sprintf(value, "[\"Ethernet\",\"WiFi\",\"Bluetooth\"]");
    return MAOPAO_SUCCESS;
    /* USER CODE END getComplex2 lTalGetComplexValue2 */
}

/* USER CODE BEGIN 2 */

/* USER CODE END 2 */

