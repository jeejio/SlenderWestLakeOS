//Slender West Lake automatically generated code

#ifndef TAL_OTA_H
#define TAL_OTA_H

#include <stdbool.h>
#include "maopaoServer.h"
#include "maopaoError.h"

extern maopao_service_handler_st tal_ota_serviceGroup[];

/**
 * @brief lTalOtaUpgrade
 * @param[in] url
 * @param[in] version
 * @param[in] checksum
 * @param[in] key
 * @param[in] iv
 * @param[in] size
 * @param[in] encrypted
 * @return errorCode
 */
int lTalOtaUpgrade(char *url, char *version, char *checksum, char *key, char *iv, int size, int encrypted);

#endif  //TAL_OTA_H
