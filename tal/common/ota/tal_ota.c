//Slender West Lake automatically generated code


#include <stdio.h>
#include <stdlib.h>
 #include <string.h>
#include "tal_ota_service.h"
#include "tal_ota.h"
#include "maopao_ota.h"
int lTalOtaUpgrade(char *url, char *version, char *checksum, char *key, char *iv, int size, int encrypted)
{
    //user write code here.
    lMaopaoOtaStart(url,version,checksum,key,iv,size,encrypted);
    return MAOPAO_SUCCESS;
}

maopao_service_handler_st tal_ota_serviceGroup[] = {
        {.method = "lTalOtaUpgrade", .handlerCb = MAOPAO_SERVICE(lTalOtaUpgrade)},
        {.method = NULL, .handlerCb = NULL}
};

