//Slender West Lake automatically generated code, do not edit

#ifndef TAL_OTA_SERVICE_H
#define TAL_OTA_SERVICE_H

#include "maopaoServer.h"
#include "maopaoError.h"
#include "mson.h"


void __maopaoServer__lTalOtaUpgrade__service__(maopao_requestInfo_st *_requestInfo, SemaphoreHandle_t _mux);

#endif  //TAL_OTA_SERVICE_H
