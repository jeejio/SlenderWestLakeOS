//Slender West Lake automatically generated code, do not edit


#include <stdio.h>
#include <stdlib.h>
#include <memory.h>
#include "tal_ota_service.h"
#include "tal_ota.h"

#define STRING_SIZE 1024

//Define a MSON structure for input parameter processing
MSON_DEFOBJ(maopao_lTalOtaUpgrade_inParam_t,
    char *url;
    char *version;
    char *checksum;
    char *key;
    char *iv;
    int size;
    int encrypted;
);

void __maopaoServer__lTalOtaUpgrade__service__(maopao_requestInfo_st *_requestInfo, SemaphoreHandle_t _mux)
{
    //prepare input parameter
    mson_jsonToObj(_requestInfo->params, MSON_MEMBERS_TO_STRING(maopao_lTalOtaUpgrade_inParam_t), _inObj);
    if (_inObj == NULL){
        maopaoServerSendError(_requestInfo, MAOPAO_ERROR_RPC_ANALYSE_FAILED, MAOPAO_ERROR_RPC_ANALYSE_FAILED_MSG);
        goto _exit;
    }
    maopao_lTalOtaUpgrade_inParam_t *_inParam = (maopao_lTalOtaUpgrade_inParam_t *) _inObj;
    char *url = _inParam->url;
    char *version = _inParam->version;
    char *checksum = _inParam->checksum;
    char *key = _inParam->key;
    char *iv = _inParam->iv;
    int size = _inParam->size;
    int encrypted = _inParam->encrypted;

    int _errorCode;

    //call user function
    _errorCode = lTalOtaUpgrade(url, version, checksum, key, iv, size, encrypted);

    if (_errorCode != MAOPAO_SUCCESS){
        maopaoServerSendError(_requestInfo, MAOPAO_ERROR_TAL_OPERATION_FAILED, MAOPAO_ERROR_TAL_OPERATION_FAILED_MSG);
        goto _release_obj;
    }

    //send response to client
    maopaoServerSendResponse(_requestInfo, "null");

    //free the space
_release_obj:
    mson_releaseWithObj(_inObj);

_exit:
    //release the mutex
    xSemaphoreGive(_mux);
}

