/* USER CODE BEGIN Header */
/**
  * @file tal_settings_service.h
  * @attention
  * Slender West Lake automatically generated code.
  * mprpcgen version: 1.4.1 .
  *
  */
/* USER CODE END Header */

#ifndef TAL_SETTINGS_SERVICE_H
#define TAL_SETTINGS_SERVICE_H

#include "maopaoServer.h"
#include "maopaoError.h"
#include "mson.h"


/* USER CODE BEGIN 1 */

/* USER CODE END 1 */

extern maopao_service_handler_st tal_settings_serviceGroup[];

/* USER CODE BEGIN 2 */

/* USER CODE END 2 */

#endif  //TAL_SETTINGS_SERVICE_H
