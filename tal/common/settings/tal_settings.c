/* USER CODE BEGIN Header */
/**
  * @file tal_settings.c
  * @attention
  * Slender West Lake automatically generated code.
  * mprpcgen version: 1.4.1 .
  *
  */
/* USER CODE END Header */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "tal_settings_service.h"
#include "hal_keyinfo.h"
#include "hal_netconfig.h"
#include "hal_fatfs.h"
#include "hal_setting.h"
#include "tal_settings.h"


/* USER CODE END Includes */

/* USER CODE BEGIN 1 */
void printHex(const unsigned char* data, size_t length) {
    for (size_t i = 0; i < length; i++) {
        printf("%02X ", data[i]);
    }
    printf("\n");
}

int check_data_null(char *data, uint8_t len)
{
    int i = 0;
    for(i = 0; i < len; i++)
    {
        if(data[i] != 0xff)
        {
            printf("data[%d]:%02x != 0xff", i, data[i]);
            return 1;
        }
    }
    return 0;
}

/* USER CODE END 1 */

int lTalGetSettingParameterValue(char *para, char *value)
{
    memset(value, 0, 64);

    int ret = lHalFatFSGetDataString(para, value);
    if(ret != DEV_EOK)
    {
        printf("Unable to read valid data");
        strncpy(value, "NULL", strlen("NULL") + 1);
    }  
    return (ret == DEV_EOK ? MAOPAO_SUCCESS : MAOPAO_ERROR_TAL_OPERATION_FAILED);
}


int lTalSetSettingParameterValue(char *para, char *value)
{
    return  lHalSetSettingParameterValue(para, value);
}

int lTalDeleteSettingParameterValue(char *para)
{
    return  vHalFatFSDeleteData(para);
}


int lTalGetDeviceId(char *value)
{
    /* USER CODE BEGIN getDeviceId lTalGetDeviceId */
    memset(value, 0, DEVICE_ID_SIZE);
    int ret = lHalGetKeyInfoId(value);
    printHex((unsigned char *)value, DEVICE_ID_SIZE);
    if(ret != DEV_EOK || !check_data_null(value, DEVICE_ID_SIZE))
    {
        printf("Unable to read valid data");
        strncpy(value, "NULL", strlen("NULL") + 1);
    }
    return (ret == DEV_EOK ? MAOPAO_SUCCESS : MAOPAO_ERROR_TAL_OPERATION_FAILED);
    /* USER CODE END getDeviceId lTalGetDeviceId */
}

int lTalGetSerialno(char *value)
{
    /* USER CODE BEGIN getSerialno lTalGetSerialno */
    memset(value, 0, 16);
    int ret = lHalGetKeyInfoSn(value);
    if(ret != DEV_EOK)
    {
        printf("Unable to read valid data");
        strncpy(value, "NULL", strlen("NULL") + 1);
    }    
    printf("value:%s", value);
    return (ret == DEV_EOK ? MAOPAO_SUCCESS : MAOPAO_ERROR_TAL_OPERATION_FAILED);
    /* USER CODE END getSerialno lTalGetSerialno */
}

int lTalGetKey(char *value)
{
    /* USER CODE BEGIN getKey lTalGetKey */
    memset(value, 0, PASSWORD_SIZE);
    int ret = lHalGetKeyInfoPw(value);
    printHex((unsigned char *)value, PASSWORD_SIZE);
    if(ret != DEV_EOK || !check_data_null(value, PASSWORD_SIZE))
    {
        printf("Unable to read valid data");
        strncpy(value, "NULL", strlen("NULL") + 1);
    }
    return (ret == DEV_EOK ? MAOPAO_SUCCESS : MAOPAO_ERROR_TAL_OPERATION_FAILED);
    /* USER CODE END getKey lTalGetKey */
}

int lTalGetProductCode(char *value)
{
    /* USER CODE BEGIN getProductCode lTalGetProductCode */
    memset(value, 0, 4);
    int ret = lHalGetKeyInfoProductCode(value);
    if(ret != DEV_EOK || !check_data_null(value, PRODUCT_CODE_SIZE))
    {
        printf("Unable to read valid data");
        strncpy(value, "NULL", strlen("NULL") + 1);
    }
    printf("value:%s", value);
    return (ret == DEV_EOK ? MAOPAO_SUCCESS : MAOPAO_ERROR_TAL_OPERATION_FAILED);
    /* USER CODE END getProductCode lTalGetProductCode */
}

int lTalGetToken(char *value)
{
    /* USER CODE BEGIN getToken lTalGetToken */
    memset(value, 0, TOKEN_SIZE);
    int ret = lHalGetKeyInfoToken(value);
    printHex((unsigned char *)value, TOKEN_SIZE);
    if(ret != DEV_EOK || !check_data_null(value, TOKEN_SIZE))
    {
        printf("Unable to read valid data");
        strncpy(value, "NULL", strlen("NULL") + 1);
    }
    return (ret == DEV_EOK ? MAOPAO_SUCCESS : MAOPAO_ERROR_TAL_OPERATION_FAILED);
    /* USER CODE END getToken lTalGetToken */
}

int lTalSwlGetNetworkEnvironmentMode(int *value)
{
    /* USER CODE BEGIN getNetworkEnvironmentMode lTalSwlGetNetworkEnvironmentMode */
    uint8_t flag = 0;
    int32_t ret = lHalGetEnableFlag(&flag);
    if (ret != DEV_EOK)
    {
        printf("Unable to read valid data");
        *value = -1;
    }
    *value = flag;
    return (ret == DEV_EOK ? MAOPAO_SUCCESS : MAOPAO_ERROR_TAL_OPERATION_FAILED);
    /* USER CODE END getNetworkEnvironmentMode lTalSwlGetNetworkEnvironmentMode */
}

int lTalSwlSetNetworkEnvironmentMode(int value)
{
    /* USER CODE BEGIN setNetworkEnvironmentMode lTalSwlSetNetworkEnvironmentMode */
    int32_t ret = DEV_ERROR;
    printf("input value: %d , convert value: %d+++++++++++++++++++\n", value, (uint8_t)value);
    printf("!= 0x00:%d, != 0xFF: %d \n", value != 0x00 ? TRUE : FALSE, value != 0xFF ? TRUE : FALSE);
    if (value != 0x00 && value != 0xFF)
    {
        printf("Got the wrong input value, it must be 0x00 or 0xFF.");
        return MAOPAO_ERROR_RPC_INVALID_PARAMS;
    }
    ret = lHalSetEnableFlag(((uint8_t)value));
    return (ret == DEV_EOK ? MAOPAO_SUCCESS : MAOPAO_ERROR_TAL_OPERATION_FAILED);
    /* USER CODE END setNetworkEnvironmentMode lTalSwlSetNetworkEnvironmentMode */
}

/* USER CODE BEGIN 2 */

/* USER CODE END 2 */

