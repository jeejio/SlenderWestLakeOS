/* USER CODE BEGIN Header */
/**
  * @file tal_settings.h
  * @attention
  * Slender West Lake automatically generated code.
  * mprpcgen version: 1.4.1 .
  *
  */
/* USER CODE END Header */

#ifndef TAL_SETTINGS_H
#define TAL_SETTINGS_H

#include <stdbool.h>
#include "maopaoServer.h"
#include "maopaoError.h"



/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* USER CODE BEGIN 1 */

/* USER CODE END 1 */

extern maopao_service_handler_st tal_settings_serviceGroup[];

/* USER CODE BEGIN 2 */

/* USER CODE END 2 */

/* USER CODE BEGIN 3 */

/* USER CODE END 3 */


/**
 * @brief lTalGetSettingParameterValue
 * @param[out] para value
 * @return errorCode
 */
int lTalGetSettingParameterValue(char *para, char *value);

/**
 * @brief lTalGetDeviceId
 * @param[out] value
 * @return errorCode
 */
int lTalGetDeviceId(char *value);

/**
 * @brief lTalGetSerialno
 * @param[out] value
 * @return errorCode
 */
int lTalGetSerialno(char *value);

/**
 * @brief lTalGetKey
 * @param[out] value
 * @return errorCode
 */
int lTalGetKey(char *value);

/**
 * @brief lTalGetProductCode
 * @param[out] value
 * @return errorCode
 */
int lTalGetProductCode(char *value);

/**
 * @brief lTalGetToken
 * @param[out] value
 * @return errorCode
 */
int lTalGetToken(char *value);

/**
 * @brief lTalSwlGetNetworkEnvironmentMode
 * @param[out] value
 * @return errorCode
 */
int lTalSwlGetNetworkEnvironmentMode(int *value);

/**
 * @brief lTalSwlSetNetworkEnvironmentMode
 * @param[in] value
 * @return errorCode
 */
int lTalSwlSetNetworkEnvironmentMode(int value);

/**
 * @brief lTalSetSettingParameterValue
 * @param[in] para value 
 * @return errorCode
 */
int lTalSetSettingParameterValue(char *para, char *value);


/**
 * @brief lTalDeleteSettingParameterValue
 * @param[in] para
 * @return errorCode
 */
int lTalDeleteSettingParameterValue(char *para);



/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

#endif  //TAL_SETTINGS_H
