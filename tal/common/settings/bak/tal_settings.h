/*
 * Copyright (c) 2023, JEE Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2023-02-28      lk        first version
 */

#ifndef __TAL_SETTINGS_H__
#define __TAL_SETTINGS_H__
#include <stdint.h>

/**
 * This function get ro.product.board value.
 *
 * @param value output, ro.product.board value
 */
void vTalGetProductBoard(char *value);

/**
 * This function get ro.build.version value.
 *
 * @param value output, ro.build.version value
 */
void vTalGetBuildVersion(char *value);

/**
 * This function get ro.flashsize value.
 *
 * @param value output, ro.flashsize value
 */
void vTalGetFlashSize(char *value);

/**
 * This function get ro.chip value.
 *
 * @param value output, ro.chip value
 */
void vTalGetChip(char *value);

/**
 * This function get ro.product.manufacturer value.
 *
 * @param value output, ro.product.manufacturer value
 */
void vTalGetProductManufacturer(char *value);

/**
 * This function get ro.product.device value.
 *
 * @param value output, ro.product.device value
 */
void vTalGetProductDevice(char *value);

/**
 * This function get the device ID.
 *
 * @param value output, device ID value
 */
void vTalGetDeviceId(char *data);

/**
 * This function get serialno.
 *
 * @param value output, serialno value
 */
void vTalGetSerialno(char *data);

/**
 * This function get jeejio key value.
 *
 * @param value output, jeejio key value
 */
void vTalGetKey(char *data);

/**
 * This function get product code.
 *
 * @param value output, product code value
 */
void vTalGetProductCode(char *data);

/**
 * This function get Token.
 *
 * @param value output, Token value
 */
void vTalGetToken(char *data);

#endif /* __TAL_SETTINGS_H__ */
