/*
 * Copyright (c) 2023, JEE Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2023-02-28      lk        first version
 */

#include "tal_settings.h"
#include "hal_fatfs.h"
#include "hal_keymanager.h"

#define TAG "tal_settings"

/***********ro.product.board************/
void vTalGetProductBoard(char *value)
{
    lHalFatFSGetDataString("ro.product.board", value);
}

/************ro.build.version***********/
void vTalGetBuildVersion(char *value)
{
    lHalFatFSGetDataString("ro.build.version", value);
}

/************ro.flashsize***********/
void vTalGetFlashSize(char *value)
{
    lHalFatFSGetDataString("ro.flashsize", value);
}

/************ro.chip***********/
void vTalGetChip(char *value)
{
    lHalFatFSGetDataString("ro.chip", value);
}

/************ro.product.manufacture***********/
void vTalGetProductManufacturer(char *value)
{
    lHalFatFSGetDataString("ro.product.manufacture", value);
}

/************ro.product.device***********/
void vTalGetProductDevice(char *value)
{
    lHalFatFSGetDataString("ro.product.device", value);
}

/************ro.deviceid***********/
void vTalGetDeviceId(char *data)
{
    lHalGetKeyInfoId(data);
}

/************ro.serialno***********/
void vTalGetSerialno(char *data)
{
    lHalGetKeyInfoSn(data);
}

void vTalGetKey(char *data)
{
    lHalGetKeyInfoPw(data);
}

void vTalGetProductCode(char *data)
{
    lHalGetKeyInfoProductCode(data);
}

void vTalGetToken(char *data)
{
    lHalGetKeyInfoToken(data);
}
