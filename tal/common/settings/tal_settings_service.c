/* USER CODE BEGIN Header */
/**
  * @file tal_settings_service.c
  * @attention
  * Slender West Lake automatically generated code.
  * mprpcgen version: 1.4.1 .
  *
  */
/* USER CODE END Header */

#include <stdio.h>
#include <stdlib.h>
#include <memory.h>
#include "tal_settings_service.h"
#include "tal_settings.h"

/* USER CODE BEGIN Define */
#define STRING_SIZE 1024

/* USER CODE END Define */

/* USER CODE BEGIN 1 */

/* USER CODE END 1 */

//TalInfo_st _talInfo = {
//    .manufacturer = "jeejio",
//    .category = "product",
//    .version = "1.0.0",
//    .developer = "author@jeejio.com",
//};

//Define a MSON structure for output parameter processing
MSON_DEFOBJ(maopao_lTalGetProductBoard_outParam_t,
    char *value;
);

//Define a MSON structure for output parameter processing
MSON_DEFOBJ(maopao_lTalGetBuildVersion_outParam_t,
    char *value;
);

//Define a MSON structure for output parameter processing
MSON_DEFOBJ(maopao_lTalGetFlashSize_outParam_t,
    char *value;
);

//Define a MSON structure for output parameter processing
MSON_DEFOBJ(maopao_lTalGetChip_outParam_t,
    char *value;
);

//Define a MSON structure for output parameter processing
MSON_DEFOBJ(maopao_lTalGetProductManufacturer_outParam_t,
    char *value;
);

//Define a MSON structure for output parameter processing
MSON_DEFOBJ(maopao_lTalGetProductDevice_outParam_t,
    char *value;
);

//Define a MSON structure for output parameter processing
MSON_DEFOBJ(maopao_lTalGetDeviceId_outParam_t,
    char *value;
);

//Define a MSON structure for output parameter processing
MSON_DEFOBJ(maopao_lTalGetSerialno_outParam_t,
    char *value;
);

//Define a MSON structure for output parameter processing
MSON_DEFOBJ(maopao_lTalGetKey_outParam_t,
    char *value;
);

//Define a MSON structure for output parameter processing
MSON_DEFOBJ(maopao_lTalGetProductCode_outParam_t,
    char *value;
);

//Define a MSON structure for output parameter processing
MSON_DEFOBJ(maopao_lTalGetToken_outParam_t,
    char *value;
);

//Define a MSON structure for output parameter processing
MSON_DEFOBJ(maopao_lTalSwlGetNetworkEnvironmentMode_outParam_t,
    int value;
);

//Define a MSON structure for input parameter processing
MSON_DEFOBJ(maopao_lTalSwlSetNetworkEnvironmentMode_inParam_t,
    int value;
);

void __maopaoServer__lTalGetProductBoard__service__(maopao_requestInfo_st *_requestInfo, SemaphoreHandle_t _mux)
{
    //prepare output parameter
    maopao_lTalGetProductBoard_outParam_t _outParam;
    char *value;
    char *value_addressRecord;
    value = malloc(STRING_SIZE);
    value_addressRecord = value;

    int _errorCode;

    //call user function
    _errorCode = lTalGetSettingParameterValue("ro.setting.jiejioxie.board.value", value);

    if (_errorCode != MAOPAO_SUCCESS){
        maopaoServerSendError(_requestInfo, MAOPAO_ERROR_TAL_OPERATION_FAILED, MAOPAO_ERROR_TAL_OPERATION_FAILED_MSG);
        goto _release_obj;
    }

    //copy the output parameters to the structure
    _outParam.value = value;
    char *_resultJson = mson_objToJson(&_outParam, MSON_MEMBERS_TO_STRING(maopao_lTalGetProductBoard_outParam_t));

    //send response to client
    maopaoServerSendResponse(_requestInfo, _resultJson);

    //free the space
    free(value_addressRecord);
    free(_resultJson);
_release_obj:

    //release the mutex
    xSemaphoreGive(_mux);
}

void __maopaoServer__lTalGetBuildVersion__service__(maopao_requestInfo_st *_requestInfo, SemaphoreHandle_t _mux)
{
    //prepare output parameter
    maopao_lTalGetBuildVersion_outParam_t _outParam;
    char *value;
    char *value_addressRecord;
    value = malloc(STRING_SIZE);
    value_addressRecord = value;

    int _errorCode;

    //call user function
    _errorCode = lTalGetSettingParameterValue("ro.setting.build.version.value", value);

    if (_errorCode != MAOPAO_SUCCESS){
        maopaoServerSendError(_requestInfo, MAOPAO_ERROR_TAL_OPERATION_FAILED, MAOPAO_ERROR_TAL_OPERATION_FAILED_MSG);
        goto _release_obj;
    }

    //copy the output parameters to the structure
    _outParam.value = value;
    char *_resultJson = mson_objToJson(&_outParam, MSON_MEMBERS_TO_STRING(maopao_lTalGetBuildVersion_outParam_t));

    //send response to client
    maopaoServerSendResponse(_requestInfo, _resultJson);

    //free the space
    free(value_addressRecord);
    free(_resultJson);
_release_obj:

    //release the mutex
    xSemaphoreGive(_mux);
}

void __maopaoServer__lTalGetFlashSize__service__(maopao_requestInfo_st *_requestInfo, SemaphoreHandle_t _mux)
{
    //prepare output parameter
    maopao_lTalGetFlashSize_outParam_t _outParam;
    char *value;
    char *value_addressRecord;
    value = malloc(STRING_SIZE);
    value_addressRecord = value;

    int _errorCode;

    //call user function
    _errorCode = lTalGetSettingParameterValue("ro.setting.flashsize.value", value);

    if (_errorCode != MAOPAO_SUCCESS){
        maopaoServerSendError(_requestInfo, MAOPAO_ERROR_TAL_OPERATION_FAILED, MAOPAO_ERROR_TAL_OPERATION_FAILED_MSG);
        goto _release_obj;
    }

    //copy the output parameters to the structure
    _outParam.value = value;
    char *_resultJson = mson_objToJson(&_outParam, MSON_MEMBERS_TO_STRING(maopao_lTalGetFlashSize_outParam_t));

    //send response to client
    maopaoServerSendResponse(_requestInfo, _resultJson);

    //free the space
    free(value_addressRecord);
    free(_resultJson);
_release_obj:

    //release the mutex
    xSemaphoreGive(_mux);
}

void __maopaoServer__lTalGetChip__service__(maopao_requestInfo_st *_requestInfo, SemaphoreHandle_t _mux)
{
    //prepare output parameter
    maopao_lTalGetChip_outParam_t _outParam;
    char *value;
    char *value_addressRecord;
    value = malloc(STRING_SIZE);
    value_addressRecord = value;

    int _errorCode;

    //call user function
    _errorCode = lTalGetSettingParameterValue("ro.setting.chip.value", value);

    if (_errorCode != MAOPAO_SUCCESS){
        maopaoServerSendError(_requestInfo, MAOPAO_ERROR_TAL_OPERATION_FAILED, MAOPAO_ERROR_TAL_OPERATION_FAILED_MSG);
        goto _release_obj;
    }

    //copy the output parameters to the structure
    _outParam.value = value;
    char *_resultJson = mson_objToJson(&_outParam, MSON_MEMBERS_TO_STRING(maopao_lTalGetChip_outParam_t));

    //send response to client
    maopaoServerSendResponse(_requestInfo, _resultJson);

    //free the space
    free(value_addressRecord);
    free(_resultJson);
_release_obj:

    //release the mutex
    xSemaphoreGive(_mux);
}

void __maopaoServer__lTalGetProductManufacturer__service__(maopao_requestInfo_st *_requestInfo, SemaphoreHandle_t _mux)
{
    //prepare output parameter
    maopao_lTalGetProductManufacturer_outParam_t _outParam;
    char *value;
    char *value_addressRecord;
    value = malloc(STRING_SIZE);
    value_addressRecord = value;

    int _errorCode;

    //call user function
    _errorCode = lTalGetSettingParameterValue("ro.setting.product.manufacture.value", value);

    if (_errorCode != MAOPAO_SUCCESS){
        maopaoServerSendError(_requestInfo, MAOPAO_ERROR_TAL_OPERATION_FAILED, MAOPAO_ERROR_TAL_OPERATION_FAILED_MSG);
        goto _release_obj;
    }

    //copy the output parameters to the structure
    _outParam.value = value;
    char *_resultJson = mson_objToJson(&_outParam, MSON_MEMBERS_TO_STRING(maopao_lTalGetProductManufacturer_outParam_t));

    //send response to client
    maopaoServerSendResponse(_requestInfo, _resultJson);

    //free the space
    free(value_addressRecord);
    free(_resultJson);
_release_obj:

    //release the mutex
    xSemaphoreGive(_mux);
}

void __maopaoServer__lTalGetProductDevice__service__(maopao_requestInfo_st *_requestInfo, SemaphoreHandle_t _mux)
{
    //prepare output parameter
    maopao_lTalGetProductDevice_outParam_t _outParam;
    char *value;
    char *value_addressRecord;
    value = malloc(STRING_SIZE);
    value_addressRecord = value;

    int _errorCode;

    //call user function
    _errorCode = lTalGetSettingParameterValue("ro.setting.product.device.value", value);

    if (_errorCode != MAOPAO_SUCCESS){
        maopaoServerSendError(_requestInfo, MAOPAO_ERROR_TAL_OPERATION_FAILED, MAOPAO_ERROR_TAL_OPERATION_FAILED_MSG);
        goto _release_obj;
    }

    //copy the output parameters to the structure
    _outParam.value = value;
    char *_resultJson = mson_objToJson(&_outParam, MSON_MEMBERS_TO_STRING(maopao_lTalGetProductDevice_outParam_t));

    //send response to client
    maopaoServerSendResponse(_requestInfo, _resultJson);

    //free the space
    free(value_addressRecord);
    free(_resultJson);
_release_obj:

    //release the mutex
    xSemaphoreGive(_mux);
}

void __maopaoServer__lTalGetDeviceId__service__(maopao_requestInfo_st *_requestInfo, SemaphoreHandle_t _mux)
{
    //prepare output parameter
    maopao_lTalGetDeviceId_outParam_t _outParam;
    char *value;
    char *value_addressRecord;
    value = malloc(STRING_SIZE);
    value_addressRecord = value;

    int _errorCode;

    //call user function
    _errorCode = lTalGetDeviceId(value);

    if (_errorCode != MAOPAO_SUCCESS){
        maopaoServerSendError(_requestInfo, MAOPAO_ERROR_TAL_OPERATION_FAILED, MAOPAO_ERROR_TAL_OPERATION_FAILED_MSG);
        goto _release_obj;
    }

    //copy the output parameters to the structure
    _outParam.value = value;
    char *_resultJson = mson_objToJson(&_outParam, MSON_MEMBERS_TO_STRING(maopao_lTalGetDeviceId_outParam_t));

    //send response to client
    maopaoServerSendResponse(_requestInfo, _resultJson);

    //free the space
    free(value_addressRecord);
    free(_resultJson);
_release_obj:

    //release the mutex
    xSemaphoreGive(_mux);
}

void __maopaoServer__lTalGetSerialno__service__(maopao_requestInfo_st *_requestInfo, SemaphoreHandle_t _mux)
{
    //prepare output parameter
    maopao_lTalGetSerialno_outParam_t _outParam;
    char *value;
    char *value_addressRecord;
    value = malloc(STRING_SIZE);
    value_addressRecord = value;

    int _errorCode;

    //call user function
    _errorCode = lTalGetSerialno(value);

    if (_errorCode != MAOPAO_SUCCESS){
        maopaoServerSendError(_requestInfo, MAOPAO_ERROR_TAL_OPERATION_FAILED, MAOPAO_ERROR_TAL_OPERATION_FAILED_MSG);
        goto _release_obj;
    }

    //copy the output parameters to the structure
    _outParam.value = value;
    char *_resultJson = mson_objToJson(&_outParam, MSON_MEMBERS_TO_STRING(maopao_lTalGetSerialno_outParam_t));

    //send response to client
    maopaoServerSendResponse(_requestInfo, _resultJson);

    //free the space
    free(value_addressRecord);
    free(_resultJson);
_release_obj:

    //release the mutex
    xSemaphoreGive(_mux);
}

void __maopaoServer__lTalGetKey__service__(maopao_requestInfo_st *_requestInfo, SemaphoreHandle_t _mux)
{
    //prepare output parameter
    maopao_lTalGetKey_outParam_t _outParam;
    char *value;
    char *value_addressRecord;
    value = malloc(STRING_SIZE);
    value_addressRecord = value;

    int _errorCode;

    //call user function
    _errorCode = lTalGetKey(value);

    if (_errorCode != MAOPAO_SUCCESS){
        maopaoServerSendError(_requestInfo, MAOPAO_ERROR_TAL_OPERATION_FAILED, MAOPAO_ERROR_TAL_OPERATION_FAILED_MSG);
        goto _release_obj;
    }

    //copy the output parameters to the structure
    _outParam.value = value;
    char *_resultJson = mson_objToJson(&_outParam, MSON_MEMBERS_TO_STRING(maopao_lTalGetKey_outParam_t));

    //send response to client
    maopaoServerSendResponse(_requestInfo, _resultJson);

    //free the space
    free(value_addressRecord);
    free(_resultJson);
_release_obj:

    //release the mutex
    xSemaphoreGive(_mux);
}

void __maopaoServer__lTalGetProductCode__service__(maopao_requestInfo_st *_requestInfo, SemaphoreHandle_t _mux)
{
    //prepare output parameter
    maopao_lTalGetProductCode_outParam_t _outParam;
    char *value;
    char *value_addressRecord;
    value = malloc(STRING_SIZE);
    value_addressRecord = value;

    int _errorCode;

    //call user function
    _errorCode = lTalGetProductCode(value);

    if (_errorCode != MAOPAO_SUCCESS){
        maopaoServerSendError(_requestInfo, MAOPAO_ERROR_TAL_OPERATION_FAILED, MAOPAO_ERROR_TAL_OPERATION_FAILED_MSG);
        goto _release_obj;
    }

    //copy the output parameters to the structure
    _outParam.value = value;
    char *_resultJson = mson_objToJson(&_outParam, MSON_MEMBERS_TO_STRING(maopao_lTalGetProductCode_outParam_t));

    //send response to client
    maopaoServerSendResponse(_requestInfo, _resultJson);

    //free the space
    free(value_addressRecord);
    free(_resultJson);
_release_obj:

    //release the mutex
    xSemaphoreGive(_mux);
}

void __maopaoServer__lTalGetToken__service__(maopao_requestInfo_st *_requestInfo, SemaphoreHandle_t _mux)
{
    //prepare output parameter
    maopao_lTalGetToken_outParam_t _outParam;
    char *value;
    char *value_addressRecord;
    value = malloc(STRING_SIZE);
    value_addressRecord = value;

    int _errorCode;

    //call user function
    _errorCode = lTalGetToken(value);

    if (_errorCode != MAOPAO_SUCCESS){
        maopaoServerSendError(_requestInfo, MAOPAO_ERROR_TAL_OPERATION_FAILED, MAOPAO_ERROR_TAL_OPERATION_FAILED_MSG);
        goto _release_obj;
    }

    //copy the output parameters to the structure
    _outParam.value = value;
    char *_resultJson = mson_objToJson(&_outParam, MSON_MEMBERS_TO_STRING(maopao_lTalGetToken_outParam_t));

    //send response to client
    maopaoServerSendResponse(_requestInfo, _resultJson);

    //free the space
    free(value_addressRecord);
    free(_resultJson);
_release_obj:

    //release the mutex
    xSemaphoreGive(_mux);
}

void __maopaoServer__lTalSwlGetNetworkEnvironmentMode__service__(maopao_requestInfo_st *_requestInfo, SemaphoreHandle_t _mux)
{
    //prepare output parameter
    maopao_lTalSwlGetNetworkEnvironmentMode_outParam_t _outParam;
    int value;

    int _errorCode;

    //call user function
    _errorCode = lTalSwlGetNetworkEnvironmentMode(&value);

    if (_errorCode != MAOPAO_SUCCESS){
        maopaoServerSendError(_requestInfo, MAOPAO_ERROR_TAL_OPERATION_FAILED, MAOPAO_ERROR_TAL_OPERATION_FAILED_MSG);
        goto _release_obj;
    }

    //copy the output parameters to the structure
    _outParam.value = value;
    char *_resultJson = mson_objToJson(&_outParam, MSON_MEMBERS_TO_STRING(maopao_lTalSwlGetNetworkEnvironmentMode_outParam_t));

    //send response to client
    maopaoServerSendResponse(_requestInfo, _resultJson);

    //free the space
    free(_resultJson);
_release_obj:

    //release the mutex
    xSemaphoreGive(_mux);
}

void __maopaoServer__lTalSwlSetNetworkEnvironmentMode__service__(maopao_requestInfo_st *_requestInfo, SemaphoreHandle_t _mux)
{
    //prepare input parameter
    mson_jsonToObj(_requestInfo->params, MSON_MEMBERS_TO_STRING(maopao_lTalSwlSetNetworkEnvironmentMode_inParam_t), _inObj);
    if (_inObj == NULL){
        maopaoServerSendError(_requestInfo, MAOPAO_ERROR_RPC_ANALYSE_FAILED, MAOPAO_ERROR_RPC_ANALYSE_FAILED_MSG);
        goto _exit;
    }
    maopao_lTalSwlSetNetworkEnvironmentMode_inParam_t *_inParam = (maopao_lTalSwlSetNetworkEnvironmentMode_inParam_t *) _inObj;
    int value = _inParam->value;

    int _errorCode;

    //call user function
    _errorCode = lTalSwlSetNetworkEnvironmentMode(value);

    if (_errorCode != MAOPAO_SUCCESS){
        maopaoServerSendError(_requestInfo, MAOPAO_ERROR_TAL_OPERATION_FAILED, MAOPAO_ERROR_TAL_OPERATION_FAILED_MSG);
        goto _release_obj;
    }

    //send response to client
    maopaoServerSendResponse(_requestInfo, "null");

    //free the space
_release_obj:
    mson_releaseWithObj(_inObj);

_exit:
    //release the mutex
    xSemaphoreGive(_mux);
}

maopao_service_handler_st tal_settings_serviceGroup[] = {
        {.method = "getProductBoard", .handlerCb = MAOPAO_SERVICE(lTalGetProductBoard), .inParam = NULL, .outParam = MSON_MEMBERS_TO_STRING(maopao_lTalGetProductBoard_outParam_t)},
        {.method = "getBuildVersion", .handlerCb = MAOPAO_SERVICE(lTalGetBuildVersion), .inParam = NULL, .outParam = MSON_MEMBERS_TO_STRING(maopao_lTalGetBuildVersion_outParam_t)},
        {.method = "getFlashSize", .handlerCb = MAOPAO_SERVICE(lTalGetFlashSize), .inParam = NULL, .outParam = MSON_MEMBERS_TO_STRING(maopao_lTalGetFlashSize_outParam_t)},
        {.method = "getChip", .handlerCb = MAOPAO_SERVICE(lTalGetChip), .inParam = NULL, .outParam = MSON_MEMBERS_TO_STRING(maopao_lTalGetChip_outParam_t)},
        {.method = "getProductManufacturer", .handlerCb = MAOPAO_SERVICE(lTalGetProductManufacturer), .inParam = NULL, .outParam = MSON_MEMBERS_TO_STRING(maopao_lTalGetProductManufacturer_outParam_t)},
        {.method = "getProductDevice", .handlerCb = MAOPAO_SERVICE(lTalGetProductDevice), .inParam = NULL, .outParam = MSON_MEMBERS_TO_STRING(maopao_lTalGetProductDevice_outParam_t)},
        {.method = "getDeviceId", .handlerCb = MAOPAO_SERVICE(lTalGetDeviceId), .inParam = NULL, .outParam = MSON_MEMBERS_TO_STRING(maopao_lTalGetDeviceId_outParam_t)},
        {.method = "getSerialno", .handlerCb = MAOPAO_SERVICE(lTalGetSerialno), .inParam = NULL, .outParam = MSON_MEMBERS_TO_STRING(maopao_lTalGetSerialno_outParam_t)},
        {.method = "getKey", .handlerCb = MAOPAO_SERVICE(lTalGetKey), .inParam = NULL, .outParam = MSON_MEMBERS_TO_STRING(maopao_lTalGetKey_outParam_t)},
        {.method = "getProductCode", .handlerCb = MAOPAO_SERVICE(lTalGetProductCode), .inParam = NULL, .outParam = MSON_MEMBERS_TO_STRING(maopao_lTalGetProductCode_outParam_t)},
        {.method = "getToken", .handlerCb = MAOPAO_SERVICE(lTalGetToken), .inParam = NULL, .outParam = MSON_MEMBERS_TO_STRING(maopao_lTalGetToken_outParam_t)},
        {.method = "getNetworkEnvironmentMode", .handlerCb = MAOPAO_SERVICE(lTalSwlGetNetworkEnvironmentMode), .inParam = NULL, .outParam = MSON_MEMBERS_TO_STRING(maopao_lTalSwlGetNetworkEnvironmentMode_outParam_t)},
        {.method = "setNetworkEnvironmentMode", .handlerCb = MAOPAO_SERVICE(lTalSwlSetNetworkEnvironmentMode), .inParam = MSON_MEMBERS_TO_STRING(maopao_lTalSwlSetNetworkEnvironmentMode_inParam_t), .outParam = NULL},
        {.method = NULL, .handlerCb = NULL}
};

/* USER CODE BEGIN 2 */

/* USER CODE END 2 */

