// mprpcgen version: 1.4.7
/* USER CODE BEGIN Header */
/**
  * @file tal_timer.h
  * @attention
  * Slender West Lake automatically generated code.
  *
  */
/* USER CODE END Header */

#ifndef TAL_TIMER_H
#define TAL_TIMER_H

#include <stdbool.h>
#include "maopaoServer.h"
#include "maopaoError.h"
#include "mson.h"
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* USER CODE BEGIN 1 */
#define MAX_DURATION 2592000
#define TAL_TIMER_NTP_REFRESH_INTERVAL 60 //NTP刷新间隔60mins
#define TAL_TIMER_MAX_LIVE_COUNTDOWN 10
#define TAL_TIMER_MAX_LIVE_TIMER 10
#define TAL_TIMER_MAX_DIED_COUNTDOWN 20
#define TAL_TIMER_MAX_DIED_TIMER 20
enum talTimerStatus {
    TAL_TIMER_STATUS_UNKNOWN = 0,
    TAL_TIMER_STATUS_DOING,
    TAL_TIMER_STATUS_PAUSE,
    TAL_TIMER_STATUS_COMPLETE,
    TAL_TIMER_STATUS_CANCELED,
};


#if 0
#define MAOPAO_SUCCESS 200
#define TAL_TIMER_ERROR_LIST_FULL (-1)
#define TAL_TIMER_ERROR_NOT_FOUND (-2)
#define TAL_TIMER_ERROR_DURATION_TOO_LONG (-3)
#define TAL_TIMER_ERROR_ILLEGAL_OPERATION (-4)
#define TAL_TIMER_ERROR_ILLEGAL_TIMESTAMP (-5)
#define TAL_TIMER_ERROR_MISSING_PARAMETER (-6)
#define TAL_TIMER_ERROR_OUT_OF_MEMORY (-7)

#define MAOPAO_SUCCESS_MSG "SUCCESS"
#define TAL_TIMER_ERROR_LIST_FULL_MSG "list is full"
#define TAL_TIMER_ERROR_NOT_FOUND_MSG "id not found"
#define TAL_TIMER_ERROR_DURATION_TOO_LONG_MSG "duration too long"
#define TAL_TIMER_ERROR_ILLEGAL_OPERATION_MSG "illegal operation"
#define TAL_TIMER_ERROR_ILLEGAL_TIMESTAMP_MSG "illegal timestamp"
#define TAL_TIMER_ERROR_MISSING_PARAMETER_MSG "missing parameter"
#define TAL_TIMER_ERROR_OUT_OF_MEMORY_MSG "out of memory"

#define MAOPAO_SUCCESS (200)
#define TAL_TIMER_ERROR_TASK_COUNT_OVERMUCH (11701)
#define TAL_TIMER_ERROR_TASK_DURATION_OVERMUCH (11702)
#define TAL_TIMER_ERROR_TASK_DURATION_TOO_SHORT (11703)
#define TAL_TIMER_ERROR_TASK_TIME_OVERMUCH (11704)
#define TAL_TIMER_ERROR_TASK_TIME_TOO_LITTLE (11705)
#define TAL_TIMER_ERROR_TASK_STOP_STATE_FAULT (11706)
#define TAL_TIMER_ERROR_TASK_RESUME_STATE_FAULT (11707)
#define TAL_TIMER_ERROR_TASK_STOP_OVER_TIME (11708) //TODO
#define TAL_TIMER_ERROR_TASK_RESET_FAULT (11709)
#define TAL_TIMER_ERROR_TASK_ID_NOT_FOUND (11710)
#define TAL_TIMER_ERROR_TASK_TIME_CALIBRATION_UNSUCCESSFUL (11711)
#define TAL_TIMER_ERROR_TASK_COUNTDOWN_FAILED (11712)   //TODO
#define TAL_TIMER_ERROR_TASK_TIME_FAILED (11713)    //TODO
#define TAL_TIMER_ERROR_TASK_DURATION_NOT_EXIST (11714) //TODO
#define TAL_TIMER_ERROR_TASK_TIMESTAMP_NOT_EXIST (11715)    //TODO
#define TAL_TIMER_ERROR_TASK_METHOD_NOT_EXIST (11716)
#define TAL_TIMER_ERROR_TASK_INVALID_METHOD (11717) //TODO
#define TAL_TIMER_ERROR_TASK_PARAMETER_NOT_FOUND (11718)    //TODO
#define TAL_TIMER_ERROR_TASK_INVALID_PARAMETER (11719)  //TODO

#define MAOPAO_SUCCESS_MSG "SUCCESS"
#define TAL_TIMER_ERROR_TASK_COUNT_OVERMUCH_MSG "task count overmuch"
#define TAL_TIMER_ERROR_TASK_DURATION_OVERMUCH_MSG "task duration overmuch"
#define TAL_TIMER_ERROR_TASK_DURATION_TOO_SHORT_MSG "task duration too short"
#define TAL_TIMER_ERROR_TASK_TIME_OVERMUCH_MSG "task time overmuch"
#define TAL_TIMER_ERROR_TASK_TIME_TOO_LITTLE_MSG "task time too little"
#define TAL_TIMER_ERROR_TASK_STOP_STATE_FAULT_MSG "task stop state fault"
#define TAL_TIMER_ERROR_TASK_RESUME_STATE_FAULT_MSG "task resume state fault"
#define TAL_TIMER_ERROR_TASK_STOP_OVER_TIME_MSG "task stop over time"
#define TAL_TIMER_ERROR_TASK_RESET_FAULT_MSG "task reset fault"
#define TAL_TIMER_ERROR_TASK_ID_NOT_FOUND_MSG "task id not found"
#define TAL_TIMER_ERROR_TASK_TIME_CALIBRATION_UNSUCCESSFUL_MSG "time calibration unsuccessful"
#define TAL_TIMER_ERROR_TASK_COUNTDOWN_FAILED_MSG "countdown failed"
#define TAL_TIMER_ERROR_TASK_TIME_FAILED_MSG "time failed"
#define TAL_TIMER_ERROR_TASK_DURATION_NOT_EXIST_MSG "duration not exist"
#define TAL_TIMER_ERROR_TASK_TIMESTAMP_NOT_EXIST_MSG "timestamp not exist"
#define TAL_TIMER_ERROR_TASK_METHOD_NOT_EXIST_MSG "method not exist"
#define TAL_TIMER_ERROR_TASK_INVALID_METHOD_MSG "invalid method"
#define TAL_TIMER_ERROR_TASK_PARAMETER_NOT_FOUND_MSG "parameter not found"
#define TAL_TIMER_ERROR_TASK_INVALID_PARAMETER_MSG "invalid parameter"
#endif

typedef struct rpc_t Rpc_t;
typedef struct talCountDownListHead_t TalCountDownListHead_t;
typedef struct talTimerListHead_t TalTimerListHead_t;
typedef struct talCountDown_t TalCountDown_t;
typedef struct talTimer_t TalTimer_t;

struct talCountDown_t {
    char *id;   //编号
    int status; //状态

    unsigned int duration;  //倒计时时长
    time_t alarmTime;   //倒计时到点时间
    unsigned int remainTime; //倒计时剩余时间（暂停时用）

    char *method;  //要执行的RPC调用
    char *params;

    TalCountDown_t *next;
};

struct talCountDownListHead_t {
    TalCountDown_t *countDownHead;
    time_t recentAlarmTime;

    int liveNum;
    int diedNum;
};

struct talTimer_t {
    char *id;   //编号
    int status; //状态

    time_t timestamp; //定时时间戳

    char *method;  //要执行的RPC调用
    char *params;

    TalTimer_t *next;
};

struct talTimerListHead_t {
    TalTimer_t *timerHead;
    time_t recentClockTime;

    int liveNum;
    int diedNum;
};
/* USER CODE END 1 */

extern maopao_service_handler_st tal_timer_serviceGroup[];

/* USER CODE BEGIN 2 */

/* USER CODE END 2 */

/* USER CODE BEGIN 3 */

/* USER CODE END 3 */

/**
 * @brief lTalCalibrationTime
 * @return errorCode
 */
int lTalCalibrationTime();

/**
 * @brief lTalCountDown
 * @param[in] duration
 * @param[in] method
 * @param[in] params
 * @param[out] number
 * @return errorCode
 */
int lTalCountDown(int duration, char *method, jsonStr params, char **number);

/**
 * @brief lTalGetCountdown
 * @param[in] number_in
 * @param[out] number_out
 * @param[out] method
 * @param[out] params
 * @param[out] status
 * @param[out] duration
 * @param[out] remainTime
 * @return errorCode
 */
int lTalGetCountdown(char *number_in, char **number_out, char **method, jsonStr *params, char **status, int *duration, int *remainTime);

/**
 * @brief lTalPauseCountdown
 * @param[in] number_in
 * @param[out] number_out
 * @return errorCode
 */
int lTalPauseCountdown(char *number_in, char **number_out);

/**
 * @brief lTalResumeCountdown
 * @param[in] number_in
 * @param[out] number_out
 * @return errorCode
 */
int lTalResumeCountdown(char *number_in, char **number_out);

/**
 * @brief lTalResetCountdown
 * @param[in] number_in
 * @param[out] number_out
 * @return errorCode
 */
int lTalResetCountdown(char *number_in, char **number_out);

/**
 * @brief lTalCancelCountdown
 * @param[in] number_in
 * @param[out] number_out
 * @return errorCode
 */
int lTalCancelCountdown(char *number_in, char **number_out);

/**
 * @brief lTalTimer
 * @param[in] timestamp
 * @param[in] method
 * @param[in] params
 * @param[out] number
 * @return errorCode
 */
int lTalTimer(int timestamp, char *method, jsonStr params, char **number);

/**
 * @brief lTalGetTimer
 * @param[in] number_in
 * @param[out] number_out
 * @param[out] method
 * @param[out] params
 * @param[out] status
 * @param[out] timestamp
 * @return errorCode
 */
int lTalGetTimer(char *number_in, char **number_out, char **method, jsonStr *params, char **status, int *timestamp);

/**
 * @brief lTalCancelTimer
 * @param[in] number_in
 * @param[out] number_out
 * @return errorCode
 */
int lTalCancelTimer(char *number_in, char **number_out);

/**
 * @brief lTalGetCountdownList
 * @param[out] countdownList
 * @return errorCode
 */
int lTalGetCountdownList(jsonStr *countdownList);

/**
 * @brief lTalGetTimerList
 * @param[out] timerList
 * @return errorCode
 */
int lTalGetTimerList(jsonStr *timerList);

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

#endif  //TAL_TIMER_H
