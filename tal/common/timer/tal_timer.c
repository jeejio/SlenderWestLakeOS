// mprpcgen version: 1.4.7
/* USER CODE BEGIN Header */
/**
  * @file tal_timer.c
  * @attention
  * Slender West Lake automatically generated code.
  *
  */
/* USER CODE END Header */

#include <stdio.h>
#include <stdlib.h>
#include "tal_timer.h"
/* USER CODE BEGIN Includes */
#include <string.h>
#include "esp_log.h"
#include "freertos/FreeRTOS.h"
#include "freertos/portmacro.h"
#include "freertos/FreeRTOSConfig.h"
#include "freertos/task.h"
#include "freertos/timers.h"

#include "tal_timer.h"
#include "tal_timer_service.h"

#include "osi/allocator.h"
#include "auto_init.h"
#include "semphr.h"
#include "ntp.h"

#include "hal_rtc.h"
#include "mson.h"
#include "maopaoServer.h"
#include "hal_fatfs.h"
/* USER CODE END Includes */

/* USER CODE BEGIN 1 */
#define TAG "TAL_TIMER"

// #define NTP_SYNC_DISABLE

#define TAL_TIMER_MAIN_TASK_PRIORITY 4  //定时器主任务优先级
#define TAL_TIMER_RPC_TASK_PRIORITY 5 //定时器rpc调用任务优先级
#define TAL_TIMER_MAIN_TASK_SIZE 4096 //定时器主任务堆栈大小
#define TAL_TIMER_RPC_TASK_SIZE 6144 //定时器rpc调用任务堆栈大小
#define MAX_TIMESTAMP 0x7fffffffffffffff //最大时间戳（用于比较最小值）

#define TAL_TIMER_MAX        10
#define TIMER_OK             0
#define TIMER_TIMESTAMP_ERR  1

#define KEY_TIMER_ID_MGR     "key_timer_id_mgr"

MSON_DEFOBJ(CountDownInfo_t,
            char *number;
                    char *method;
                    jsonStr params;
                    char *status;
                    int duration;
                    int remainTime;
)

MSON_DEFOBJ(TimerInfo_t,
            char *number;
                    int timestamp;
                    char *method;
                    jsonStr params;
                    char *status;
)

TalCountDownListHead_t talCountDownList = {
        .countDownHead=NULL,
        .recentAlarmTime = MAX_TIMESTAMP,
        .liveNum = 0,
        .diedNum = 0,
};

TalTimerListHead_t talTimerList = {
        .timerHead=NULL,
        .recentClockTime = MAX_TIMESTAMP,
        .liveNum = 0,
        .diedNum = 0,
};

static time_t tick = 0;
static unsigned int talTimerId = 0;

TimerHandle_t TalTimer = NULL;
static SemaphoreHandle_t xSemaphore = NULL;

typedef struct timerSave {
    char *id;
    jsonStr info;
} TimerSave_st;

char *talTimerStatusStr[] = {
        "unknown",
        "doing",
        "pause",
        "complete",
        "canceled",
};

/******************************************************************************************************/
TalCountDown_t *pNewCountDownNode(int duration, char *method, jsonStr params) {
    TalCountDown_t *pNewNode = (TalCountDown_t *) malloc(sizeof(TalCountDown_t));
    if (pNewNode == NULL) {
        return NULL;
    }
    pNewNode->id = (char *) malloc(10);
    if (pNewNode->id == NULL) {
        return NULL;
    }
    sprintf(pNewNode->id, "C%06d", ++talTimerId);
    pNewNode->status = TAL_TIMER_STATUS_DOING;
    pNewNode->duration = duration;
    pNewNode->alarmTime = tick + duration;
    pNewNode->remainTime = 0;
    pNewNode->method = strdup(method);
    if (params == NULL) {
        pNewNode->params = NULL;
    } else {
        pNewNode->params = strdup(params);
    }
    pNewNode->next = NULL;
    return pNewNode;
}

//往链表添加新倒计时
void vLinkCountDownNodeToList(TalCountDown_t *pNewNode) {
    TalCountDown_t *pTemp = talCountDownList.countDownHead;

    if (pTemp == NULL) {
        talCountDownList.countDownHead = pNewNode;
        talCountDownList.liveNum++;
        talCountDownList.recentAlarmTime = pNewNode->alarmTime;
        return;
    }
    while (pTemp->next != NULL) {
        pTemp = pTemp->next;
    }
    pTemp->next = pNewNode;
    talCountDownList.liveNum++;
    talCountDownList.recentAlarmTime = MIN(talCountDownList.recentAlarmTime, pNewNode->alarmTime);
}

//通过id找到倒计时节点
TalCountDown_t *pFindCountDownNodeById(char *id) {
    if (!id) {
        return NULL;
    }
    TalCountDown_t *pTemp = talCountDownList.countDownHead;
    while (pTemp != NULL) {
        if (strcmp(pTemp->id, id) == 0) {
            return pTemp;
        }
        pTemp = pTemp->next;
    }
    return NULL;
}

//删除指定节点
void vRemoveCountDownNode(TalCountDown_t *countDownNode) {
    if (!countDownNode) {
        return;
    }
    if (talCountDownList.countDownHead == countDownNode) {
        talCountDownList.countDownHead = countDownNode->next;
    } else {
        TalCountDown_t *n = talCountDownList.countDownHead;
        if (!n) {
            return;
        }

        while (n->next != NULL) {
            if (n->next == countDownNode) {
                n->next = countDownNode->next;
                break;
            }

            n = n->next;
        }
    }

    switch (countDownNode->status) {
        case TAL_TIMER_STATUS_COMPLETE:
        case TAL_TIMER_STATUS_CANCELED:
            talCountDownList.diedNum--;
            break;
        default:
            talCountDownList.liveNum--;
            break;
    }

    free(countDownNode->id);
    free(countDownNode->method);
    free(countDownNode->params);
    free(countDownNode);
}

//删除过期节点
void vRemoveHistoryCountDownNode() {
    while (talCountDownList.diedNum >= TAL_TIMER_MAX_DIED_COUNTDOWN) {
        TalCountDown_t *pTemp = talCountDownList.countDownHead;
        while (pTemp != NULL) {
            if (pTemp->status == TAL_TIMER_STATUS_COMPLETE || pTemp->status == TAL_TIMER_STATUS_CANCELED) {
                vRemoveCountDownNode(pTemp);
                break;
            }
            pTemp = pTemp->next;
        }
    }
}

//更新最近到期的定时器时间
void vUpdateCountDownRecentAlarmTime() {
    TalCountDown_t *pTemp = talCountDownList.countDownHead;
    time_t recentAlarmTime = MAX_TIMESTAMP;
    while (pTemp != NULL) {
        if (pTemp->status == TAL_TIMER_STATUS_DOING) {
            recentAlarmTime = MIN(recentAlarmTime, pTemp->alarmTime);
        }
        pTemp = pTemp->next;
    }
    talCountDownList.recentAlarmTime = recentAlarmTime;
}

//将节点从存活链表转为历史链表
void vMoveCountdownNodeToHistory(TalCountDown_t *pNode) {
    vRemoveHistoryCountDownNode();
    pNode->remainTime = 0;
    talCountDownList.liveNum--;
    talCountDownList.diedNum++;
    LOGD(TAG, "%s: talCountDownList.liveNum=%d, talCountDownList.diedNum=%d\n", __func__, talCountDownList.liveNum, talCountDownList.diedNum);
}

int lGetRemainTime(TalCountDown_t *countDownNode) {
    if (!countDownNode) {
        return 0;
    }
    switch (countDownNode->status) {
        case TAL_TIMER_STATUS_COMPLETE:
        case TAL_TIMER_STATUS_CANCELED:
            return 0;
        case TAL_TIMER_STATUS_DOING:
            return countDownNode->alarmTime - tick;
        case TAL_TIMER_STATUS_PAUSE:
            return countDownNode->remainTime;
    }
    return 0;
}
/******************************************************************************************************/
TalTimer_t *pNewTimerNode(int timestamp, char *method, jsonStr params, char *id) {
    TalTimer_t *timerNode = (TalTimer_t *) malloc(sizeof(TalTimer_t));
    if (timerNode == NULL) {
        return NULL;
    }

    if (!id) {
        timerNode->id = (char *) malloc(10);
        if (timerNode->id == NULL) {
            return NULL;
        }
        sprintf(timerNode->id, "T%06d", ++talTimerId);
    } else {
        timerNode->id = (char *) malloc(10);
        if (timerNode->id == NULL) {
            return NULL;
        }

        strcpy(timerNode->id, id);
        int currentId = atoi(timerNode->id + 1);
        LOGI(TAG, "%s: currentId=%06d", __func__, currentId);
        talTimerId = MAX(currentId, talTimerId);
        LOGI(TAG, "%s: talTimerId=%06d", __func__, talTimerId);
    }

    timerNode->timestamp = timestamp;
    timerNode->method = strdup(method);
    timerNode->params = strdup(params);
    timerNode->status = TAL_TIMER_STATUS_DOING;
    timerNode->next = NULL;
    return timerNode;
}

TalTimer_t *pFindTimerNodeById(char *id) {
    TalTimer_t *timerNode = talTimerList.timerHead;
    while (timerNode != NULL) {
        if (strcmp(timerNode->id, id) == 0) {
            return timerNode;
        }
        timerNode = timerNode->next;
    }
    return NULL;
}

void vLinkTimerNodeToList(TalTimer_t *timerNode) {

    LOGE(TAG, "enter %s\n", __func__);
    if (talTimerList.timerHead == NULL) {
        talTimerList.timerHead = timerNode;
    } else {
        TalTimer_t *timerNodeTmp = talTimerList.timerHead;
        while (timerNodeTmp->next != NULL) {
            timerNodeTmp = timerNodeTmp->next;
        }
        timerNodeTmp->next = timerNode;
    }
    talTimerList.liveNum++;
    if (talTimerList.recentClockTime > timerNode->timestamp) {
        talTimerList.recentClockTime = timerNode->timestamp;
    }
}

void vRemoveTimerNode(TalTimer_t *timerNode) {
    if (!timerNode) {
        return;
    }
    if (talTimerList.timerHead == timerNode) {
        talTimerList.timerHead = timerNode->next;
    } else {
        TalTimer_t *n = talTimerList.timerHead;
        if (!n) {
            return;
        }

        while (n->next != NULL) {
            if (n->next == timerNode) {
                n->next = timerNode->next;
                break;
            }

            n = n->next;
        }
    }

    switch (timerNode->status) {
        case TAL_TIMER_STATUS_COMPLETE:
        case TAL_TIMER_STATUS_CANCELED:
            talTimerList.diedNum--;
            break;
        default:
            talTimerList.liveNum--;
            break;
    }
    free(timerNode->id);
    free(timerNode->method);
    free(timerNode->params);
    free(timerNode);
}

void vRemoveHistoryTimerNode() {
    while (talTimerList.diedNum >= TAL_TIMER_MAX_DIED_TIMER) {
        TalTimer_t *pTemp = talTimerList.timerHead;
        while (pTemp != NULL) {
            if (pTemp->status == TAL_TIMER_STATUS_COMPLETE || pTemp->status == TAL_TIMER_STATUS_CANCELED) {
                vRemoveTimerNode(pTemp);
                break;
            }
            pTemp = pTemp->next;
        }
    }
}

void vMoveTimerNodeToHistory(TalTimer_t *timerNode) {
    vRemoveHistoryTimerNode();
    talTimerList.liveNum--;
    talTimerList.diedNum++;
    LOGD(TAG, "%s: talTimerList.liveNum=%d, talTimerList.diedNum=%d\n",
                     __func__, talTimerList.liveNum, talTimerList.diedNum);
}

static int lTalTimerSaveToFlash(TalTimer_t *timerNode)
{
    if(timerNode == NULL)
        return -1;

    TimerInfo_t timerInfo = {
        .number = timerNode->id,
        .method = timerNode->method,
        .params = timerNode->params,
        .status = talTimerStatusStr[timerNode->status],
        .timestamp = (int) timerNode->timestamp
    };
    
    // 打包为json字符串
    char *timerInfoStr = mson_objToJson(&timerInfo, MSON_MEMBERS_TO_STRING(TimerInfo_t));
    if (timerInfoStr == NULL) {
        return -1;
    }
    char *timerNodeId = strdup(timerInfo.number);

    // 更新timer信息
    Err_t ret = DEV_EOK;
    LOGI(TAG, "%s: save the timerinfo %s to flash\n", __func__, timerInfoStr);
    if ((ret=lHalFatFSSetDataString(timerNodeId,  timerInfoStr)) != DEV_EOK) {
        LOGE(TAG, "%s: save the timer %s to flash failed!\n", __func__, timerNodeId);
        // TODO: send err response to cloud
        goto exit;
    }

    // 更新timer id
    char idMgrBuf[128] = {0};
    ret = lHalFatFSGetDataString(KEY_TIMER_ID_MGR, idMgrBuf);
    LOGE(TAG, "%s: get the timer id %s from flash!\n", __func__, idMgrBuf);
    strcat(idMgrBuf, timerNodeId);
    strcat(idMgrBuf, ";");

    // KEY_TIMER_ID_MGR:统一管理所有timers的任务iD，更新id信息到flash中，
    // 用以开机初始化时根据id从flash中获取对应信息创建timer链表
   if ((ret=lHalFatFSSetDataString(KEY_TIMER_ID_MGR, idMgrBuf)) != DEV_EOK) {
        LOGE(TAG, "%s: save the timer id %s to flash failed!\n", __func__, timerNodeId);
   }

exit:
    // 释放空间
    free(timerInfoStr);
    free(timerNodeId);
    timerInfoStr = NULL;
    timerNodeId = NULL;

    return ret;
}

static int lTalTimerFlashUpdate(TalTimer_t *timerNode) {
    LOGI(TAG, "%s enter", __func__);

    // 删除具体id对应的timer信息
    Err_t ret = DEV_EOK;
    ret = vHalFatFSDeleteData(timerNode->id);
    if (ret != DEV_EOK) {
        LOGE(TAG, "%s: delete key %s from flash failed", __func__, timerNode->id);
        // TODO: send err response to cloud
        return ret;
    }

    // 更新KEY_TIMER_ID_MGR所对应value
    char idMgrBufPre[128] = {0};
    char idMgrBufnow[128] = {0};
    ret = lHalFatFSGetDataString(KEY_TIMER_ID_MGR, idMgrBufPre);
    LOGE(TAG, "%s: get idMgrBufPre: %s from flash", __func__, idMgrBufPre);

    char *psubstr = strstr(idMgrBufPre, timerNode->id);
    if(psubstr == NULL) // 未找到匹配的id子串
        return ret;

    int8_t nPos = psubstr-idMgrBufPre;
    memcpy(idMgrBufnow, idMgrBufPre, nPos);
    strcpy(idMgrBufnow+nPos, (psubstr+strlen(timerNode->id)+1));
    LOGE(TAG, "%s: get idMgrBufnow: %s from flash", __func__, idMgrBufnow);

    ret = lHalFatFSSetDataString(KEY_TIMER_ID_MGR, idMgrBufnow);
    if (ret != DEV_EOK) {
        LOGE(TAG, "%s: set %s failed", __func__, KEY_TIMER_ID_MGR);
    }

    return ret;
}

void vUpdateTimerRecentClockTime() {
    TalTimer_t *timerNode = talTimerList.timerHead;
    time_t recentClockTime = MAX_TIMESTAMP;
    while (timerNode != NULL) {
        if (timerNode->status == TAL_TIMER_STATUS_DOING && timerNode->timestamp < recentClockTime) {
            recentClockTime = timerNode->timestamp;
        }
        timerNode = timerNode->next;
    }
    talTimerList.recentClockTime = recentClockTime;
}
/******************************************************************************************************/
static int lTalTimerInitfromFlash(int timestamp, char *method, jsonStr params, char *id) {
#ifndef NTP_SYNC_DISABLE
    if (timestamp < time(NULL) || timestamp > time(NULL) + MAX_DURATION) {
        LOGE(TAG, "%s: timestamp is not right, it must bigger than current time\n",
                    __func__);
        return -TIMER_TIMESTAMP_ERR;
    }
#else
    timestamp += time(NULL);
#endif

    LOGD(TAG, "%s: talTimerList.liveNum=%d, talTimerList.diedNum=%d, id=%s\n",
                    __func__, talTimerList.liveNum, talTimerList.diedNum, id);

    TalTimer_t *timerNode = pNewTimerNode(timestamp, method, params, id);
    LOGD(TAG, "%s: new timer node success, timer id is %s\n",
                    __func__, timerNode->id);
    vLinkTimerNodeToList(timerNode);

    return TIMER_OK;
}
/* USER CODE END 1 */

int lTalCalibrationTime()
{
    /* USER CODE BEGIN calibrationTime lTalCalibrationTime */
    time_t t = ntp_sync_to_rtc(NULL);
    if(t == 0)
    {
        return TAL_TIMER_ERROR_TASK_TIME_CALIBRATION_UNSUCCESSFUL;
    }
    return MAOPAO_SUCCESS;
    /* USER CODE END calibrationTime lTalCalibrationTime */
}

int lTalCountDown(int duration, char *method, jsonStr params, char **number)
{
    /* USER CODE BEGIN countdown lTalCountDown */
    if (duration > MAX_DURATION) {
        *number = "";
        return TAL_TIMER_ERROR_TASK_DURATION_OVERMUCH;
    }
    else if(duration <= 0){
        *number = "";
        return TAL_TIMER_ERROR_TASK_DURATION_TOO_SHORT; 
    }

    LOGD(TAG, "%s: talCountDownList.liveNum=%d, talCountDownList.diedNum=%d\n", __func__, talCountDownList.liveNum, talCountDownList.diedNum);

    if (talCountDownList.liveNum >= TAL_TIMER_MAX_LIVE_COUNTDOWN) {
        *number = "";
        return TAL_TIMER_ERROR_TASK_COUNT_OVERMUCH;
    }

    if (!method) {
        *number = "";
        return TAL_TIMER_ERROR_TASK_METHOD_NOT_EXIST;
    }

    TalCountDown_t *countDownNode = pNewCountDownNode(duration, method, params);
    vLinkCountDownNodeToList(countDownNode);
    *number = countDownNode->id;
    return MAOPAO_SUCCESS;
    /* USER CODE END countdown lTalCountDown */
}

int lTalGetCountdown(char *number_in, char **number_out, char **method, jsonStr *params, char **status, int *duration, int *remainTime)
{
    /* USER CODE BEGIN getCountdown lTalGetCountdown */
    TalCountDown_t *countDownNode = pFindCountDownNodeById(number_in);
    if (countDownNode == NULL) {
        *number_out = number_in;
        *method = "";
        *params = "";
        *status = "";
        *duration = 0;
        *remainTime = 0;
        return TAL_TIMER_ERROR_TASK_ID_NOT_FOUND;
    }
    *number_out = countDownNode->id;
    *method = countDownNode->method;
    *params = countDownNode->params;
    *status = talTimerStatusStr[countDownNode->status];
    *duration = countDownNode->duration;
    *remainTime = lGetRemainTime(countDownNode);

    return MAOPAO_SUCCESS;
    /* USER CODE END getCountdown lTalGetCountdown */
}

int lTalPauseCountdown(char *number_in, char **number_out)
{
    /* USER CODE BEGIN pauseCountdown lTalPauseCountdown */
    *number_out = number_in;
    TalCountDown_t *countDownNode = pFindCountDownNodeById(number_in);
    if (countDownNode == NULL) {
        return TAL_TIMER_ERROR_TASK_ID_NOT_FOUND;
    }

    switch (countDownNode->status) {
        case TAL_TIMER_STATUS_COMPLETE:
        case TAL_TIMER_STATUS_CANCELED:
            return TAL_TIMER_ERROR_TASK_STOP_STATE_FAULT;
        case TAL_TIMER_STATUS_PAUSE:
            return MAOPAO_SUCCESS;
        case TAL_TIMER_STATUS_DOING:
            countDownNode->status = TAL_TIMER_STATUS_PAUSE;
            countDownNode->remainTime = countDownNode->alarmTime - tick;
            if (talCountDownList.recentAlarmTime == countDownNode->alarmTime) {
                vUpdateCountDownRecentAlarmTime();
            }
            return MAOPAO_SUCCESS;
    }
    return MAOPAO_SUCCESS;
    /* USER CODE END pauseCountdown lTalPauseCountdown */
}

int lTalResumeCountdown(char *number_in, char **number_out)
{
    /* USER CODE BEGIN resumeCountdown lTalResumeCountdown */
    *number_out = number_in;
    TalCountDown_t *countDownNode = pFindCountDownNodeById(number_in);
    if (countDownNode == NULL) {
        return TAL_TIMER_ERROR_TASK_ID_NOT_FOUND;
    }

    switch (countDownNode->status) {
        case TAL_TIMER_STATUS_COMPLETE:
        case TAL_TIMER_STATUS_CANCELED:
            return TAL_TIMER_ERROR_TASK_RESUME_STATE_FAULT;
        case TAL_TIMER_STATUS_DOING:
            return MAOPAO_SUCCESS;
        case TAL_TIMER_STATUS_PAUSE:
            countDownNode->status = TAL_TIMER_STATUS_DOING;
            countDownNode->alarmTime = tick + countDownNode->remainTime;
            talCountDownList.recentAlarmTime = MIN(talCountDownList.recentAlarmTime, countDownNode->alarmTime);
            return MAOPAO_SUCCESS;
    }
    return MAOPAO_SUCCESS;
    /* USER CODE END resumeCountdown lTalResumeCountdown */
}

int lTalResetCountdown(char *number_in, char **number_out)
{
    /* USER CODE BEGIN resetCountdown lTalResetCountdown */
    *number_out = number_in;
    TalCountDown_t *countDownNode = pFindCountDownNodeById(number_in);
    if (countDownNode == NULL) {
        return TAL_TIMER_ERROR_TASK_ID_NOT_FOUND;
    }

    switch (countDownNode->status) {
        case TAL_TIMER_STATUS_COMPLETE:
        case TAL_TIMER_STATUS_CANCELED:
            return TAL_TIMER_ERROR_TASK_RESET_FAULT;
        case TAL_TIMER_STATUS_DOING:
        case TAL_TIMER_STATUS_PAUSE:
            countDownNode->status = TAL_TIMER_STATUS_DOING;
            countDownNode->alarmTime = tick + countDownNode->duration;
            talCountDownList.recentAlarmTime = MIN(talCountDownList.recentAlarmTime, countDownNode->alarmTime);
            return MAOPAO_SUCCESS;
    }
    return MAOPAO_SUCCESS;
    /* USER CODE END resetCountdown lTalResetCountdown */
}

int lTalCancelCountdown(char *number_in, char **number_out)
{
    /* USER CODE BEGIN cancelCountdown lTalCancelCountdown */
    *number_out = number_in;
    TalCountDown_t *countDownNode = pFindCountDownNodeById(number_in);
    if (countDownNode == NULL) {
        return TAL_TIMER_ERROR_TASK_ID_NOT_FOUND;
    }

    switch (countDownNode->status) {
        case TAL_TIMER_STATUS_COMPLETE:
            countDownNode->status = TAL_TIMER_STATUS_CANCELED;
        case TAL_TIMER_STATUS_CANCELED:
            return MAOPAO_SUCCESS;
        case TAL_TIMER_STATUS_DOING:
            if (countDownNode->alarmTime == talCountDownList.recentAlarmTime) {
                vUpdateCountDownRecentAlarmTime();
            }
        case TAL_TIMER_STATUS_PAUSE:
            countDownNode->status = TAL_TIMER_STATUS_CANCELED;
            vMoveCountdownNodeToHistory(countDownNode);
            break;
    }
    return MAOPAO_SUCCESS;
    /* USER CODE END cancelCountdown lTalCancelCountdown */
}

int lTalTimer(int timestamp, char *method, jsonStr params, char **number)
{
    /* USER CODE BEGIN timer lTalTimer */
#ifndef NTP_SYNC_DISABLE
    if (timestamp > time(NULL) + MAX_DURATION) {
        *number = "";
        return TAL_TIMER_ERROR_TASK_TIME_OVERMUCH;
    }
    else if (timestamp < time(NULL)) {
        *number = "";
        return TAL_TIMER_ERROR_TASK_TIME_TOO_LITTLE;
    }
#else
    timestamp += time(NULL);
#endif

    LOGD(TAG, "%s: talTimerList.liveNum=%d, talTimerList.diedNum=%d\n",
                    __func__, talTimerList.liveNum, talTimerList.diedNum);

    if (talTimerList.liveNum >= TAL_TIMER_MAX_LIVE_TIMER) {
        *number = "";
        return TAL_TIMER_ERROR_TASK_COUNT_OVERMUCH;
    }

    TalTimer_t *timerNode = pNewTimerNode(timestamp, method, params, NULL);
    vLinkTimerNodeToList(timerNode);

    *number = timerNode->id;

    LOGI(TAG, "%s: save doing", __func__);
    lTalTimerSaveToFlash(timerNode);
    LOGI(TAG, "%s: save done", __func__);

    return MAOPAO_SUCCESS;
    /* USER CODE END timer lTalTimer */
}

int lTalGetTimer(char *number_in, char **number_out, char **method, jsonStr *params, char **status, int *timestamp)
{
    /* USER CODE BEGIN getTimer lTalGetTimer */
    TalTimer_t *timerNode = pFindTimerNodeById(number_in);
    if (timerNode == NULL) {
        *number_out = number_in;
        *method = "";
        *params = "";
        *status = "";
        *timestamp = 0;

        return TAL_TIMER_ERROR_TASK_ID_NOT_FOUND;
    }
    *number_out = timerNode->id;
    *method = timerNode->method;
    *params = timerNode->params;
    *status = talTimerStatusStr[timerNode->status];
    *timestamp = (int) timerNode->timestamp;


    return MAOPAO_SUCCESS;
    /* USER CODE END getTimer lTalGetTimer */
}

int lTalCancelTimer(char *number_in, char **number_out)
{
    /* USER CODE BEGIN cancelTimer lTalCancelTimer */
    *number_out = number_in;

    TalTimer_t *timerNode = pFindTimerNodeById(number_in);
    if (timerNode == NULL) {
        return TAL_TIMER_ERROR_TASK_ID_NOT_FOUND;
    }

    switch (timerNode->status) {
        case TAL_TIMER_STATUS_COMPLETE:
            timerNode->status = TAL_TIMER_STATUS_CANCELED;
        case TAL_TIMER_STATUS_CANCELED:
            break;
        case TAL_TIMER_STATUS_DOING:
            if (timerNode->timestamp == talTimerList.recentClockTime) {
                vUpdateTimerRecentClockTime();
            }
        default:
            if (lTalTimerFlashUpdate(timerNode) != DEV_EOK) {
                LOGE(TAG, "%s: timer %s cancelled, update flash failed", __func__, timerNode->id);
            }
            timerNode->status = TAL_TIMER_STATUS_CANCELED;
            vMoveTimerNodeToHistory(timerNode);
            break;
    }

    return MAOPAO_SUCCESS;
    /* USER CODE END cancelTimer lTalCancelTimer */
}

int lTalGetCountdownList(jsonStr *countdownList)
{
    /* USER CODE BEGIN getCountdownList lTalGetCountdownList */
        static jsonStr countdownListStr = NULL;
    if (countdownListStr) {
        free(countdownListStr);
    }

    int mallocSize = 256;
    int usedSize = 0;

    countdownListStr = (jsonStr) malloc(mallocSize);
    if (countdownListStr == NULL) {
        return MAOPAO_ERROR_OUT_OF_MEMORY;
    }

    strcpy(countdownListStr, "[");
    usedSize += 1;

    TalCountDown_t *countDownNode = talCountDownList.countDownHead;
    while (countDownNode != NULL) {
        CountDownInfo_t countDownInfo;
        countDownInfo.method = countDownNode->method;
        countDownInfo.params = countDownNode->params;
        countDownInfo.number = countDownNode->id;
        countDownInfo.status = talTimerStatusStr[countDownNode->status];
        countDownInfo.duration = countDownNode->duration;
        countDownInfo.remainTime = lGetRemainTime(countDownNode);
        char *countDownInfoStr = mson_objToJson(&countDownInfo, MSON_MEMBERS_TO_STRING(CountDownInfo_t));
        if (countDownInfoStr == NULL) {
            return MAOPAO_ERROR_RPC_ANALYSE_FAILED;
        }
        // printf("%s\n", countDownInfoStr);
        int countDownInfoStrLen = strlen(countDownInfoStr);
        while (usedSize + countDownInfoStrLen + 2 > mallocSize) {
            mallocSize += 256;
            countdownListStr = (jsonStr) realloc(countdownListStr, mallocSize);
            if (countdownListStr == NULL) {
                return MAOPAO_ERROR_OUT_OF_MEMORY;
            }
        }
        strcpy((char *)countdownListStr + usedSize, countDownInfoStr);
        usedSize += countDownInfoStrLen;
        if (countDownNode->next != NULL) {
            strcpy(countdownListStr + usedSize, ",");
            usedSize += 1;
        }
        free(countDownInfoStr);
        countDownNode = countDownNode->next;
    }

    if (usedSize + 2 > mallocSize) {
        mallocSize = usedSize + 2;
        countdownListStr = (jsonStr) realloc(countdownListStr, mallocSize);
        if (countdownListStr == NULL) {
            return MAOPAO_ERROR_OUT_OF_MEMORY;
        }
    }
    strcpy(countdownListStr + usedSize, "]");

    *countdownList = countdownListStr;
    return MAOPAO_SUCCESS;
    /* USER CODE END getCountdownList lTalGetCountdownList */
}

int lTalGetTimerList(jsonStr *timerList)
{
    /* USER CODE BEGIN getTimerList lTalGetTimerList */
    static jsonStr timerListStr = NULL;
    if (timerListStr) {
        free(timerListStr);
    }

    TalTimer_t *timerNode = talTimerList.timerHead;
    int mallocSize = 256;
    int usedSize = 0;

    timerListStr = (jsonStr) malloc(mallocSize);
    if (timerListStr == NULL) {
        return MAOPAO_ERROR_OUT_OF_MEMORY;
    }

    strcpy(timerListStr + usedSize, "[");
    usedSize += 1;

    while (timerNode != NULL) {
        TimerInfo_t timerInfo = {
                .number = timerNode->id,
                .method = timerNode->method,
                .params = timerNode->params,
                .status = talTimerStatusStr[timerNode->status],
                .timestamp = (int) timerNode->timestamp
        };
        char *timerInfoStr = mson_objToJson(&timerInfo, MSON_MEMBERS_TO_STRING(TimerInfo_t));
        if (timerInfoStr == NULL) {
            free(timerListStr);
            return MAOPAO_ERROR_RPC_ANALYSE_FAILED;
        }

        int timerInfoStrLen = strlen(timerInfoStr);
        while (timerInfoStrLen + usedSize + 2 > mallocSize) {
            mallocSize += 256;
            timerListStr = (char *) realloc(timerListStr, mallocSize);
            if (timerListStr == NULL) {
                return MAOPAO_ERROR_OUT_OF_MEMORY;
            }
        }
        strcpy((char *)timerListStr + usedSize, timerInfoStr);
        usedSize += timerInfoStrLen;
        free(timerInfoStr);

        if (timerNode->next) {
            strcpy(timerListStr + usedSize, ",");
            usedSize += 1;
        }

        timerNode = timerNode->next;
    }
    if (usedSize + 2 > mallocSize) {
        mallocSize += 2;
        timerListStr = (char *) realloc(timerListStr, mallocSize);
        if (timerListStr == NULL) {
            return MAOPAO_ERROR_OUT_OF_MEMORY;
        }
    }
    strcpy(timerListStr + usedSize, "]");
    LOGD(TAG, "%s\n", timerListStr);

    *timerList = timerListStr;

    return MAOPAO_SUCCESS;
    /* USER CODE END getTimerList lTalGetTimerList */
}

/* USER CODE BEGIN 2 */
void vTalNtpTask(void *args) {
    vTaskDelay(10000 / portTICK_PERIOD_MS);
    time_t t;

    while (1) {
        t = ntp_sync_to_rtc(NULL);
        if (!t) {
            vTaskDelay(20 * 1000 / portTICK_PERIOD_MS);
        } else {
            printf("sync %lld\n", t);
            vTaskDelay(TAL_TIMER_NTP_REFRESH_INTERVAL * 60 * 1000 / portTICK_PERIOD_MS);
        }
    }
}

void vTalCountDownCallTask(void *args) {
    LOGI(TAG, "start call task!");
    TalCountDown_t *countDownNode = (TalCountDown_t *) args;

    //本地rpc调用,并上报rpc执行结果
    LocalRpcCall(countDownNode->method, countDownNode->params, MAOPAO_LOCAL);
    countDownNode->status = TAL_TIMER_STATUS_COMPLETE;

    //上报定时器状态
    char params[128] = {0};
    sprintf(params, "{\"timestamp\":%lld,\"status\":\"complete\",\"number\":\"%s\"}", time(NULL), countDownNode->id);
    maopaoServerCallRemoteWithMethod("onReceiveTimer","p3tHFyQG",params, NULL);

    vMoveCountdownNodeToHistory(countDownNode);
    vTaskDelete(NULL);
}

//有倒计时到点
void vTalCountDownIrq(void) {
    TalCountDown_t *pTemp = talCountDownList.countDownHead;
    time_t recentAlarmTime = MAX_TIMESTAMP;

    while (pTemp != NULL) {
        if (pTemp->alarmTime <= tick && pTemp->status == TAL_TIMER_STATUS_DOING) {
            pTemp->status = TAL_TIMER_STATUS_DOING;
            xTaskCreate(vTalCountDownCallTask, /* The function that implements the task. */
                        "vTalCountDownCallTask",  /* Just a text name for the task to aid debugging. */
                        TAL_TIMER_RPC_TASK_SIZE,     /* The stack size is defined in FreeRTOSIPConfig.h. */
                        (void *) pTemp,           /* The task parameter, not used in this case. */
                        TAL_TIMER_RPC_TASK_PRIORITY,      /* The priority assigned to the task is defined in FreeRTOSConfig.h. */
                        NULL);
        } else {
            recentAlarmTime = MIN(recentAlarmTime, pTemp->alarmTime);
        }
        pTemp = pTemp->next;
    }
    talCountDownList.recentAlarmTime = recentAlarmTime;
}

void vTalTimerCallTask(void *args) {
    LOGI(TAG, "[%lld]start call task!\n", time(NULL));
    TalTimer_t *timerNode = (TalTimer_t *) args;

    //本地rpc调用,并上报rpc执行结果
    LocalRpcCall(timerNode->method, timerNode->params, MAOPAO_LOCAL);

    timerNode->status = TAL_TIMER_STATUS_COMPLETE;

    //上报定时器状态
    char params[128] = {0};
    sprintf(params, "{\"timestamp\":%lld,\"status\":\"complete\",\"number\":\"%s\"}", time(NULL), timerNode->id);
    maopaoServerCallRemoteWithMethod("onReceiveTimer","p3tHFyQG", params, NULL);

    if (lTalTimerFlashUpdate(timerNode) != DEV_EOK) {
        LOGE(TAG, "%s: timer %s complete, update flash failed", __func__, timerNode->id);
    }

    vMoveTimerNodeToHistory(timerNode);
    vTaskDelete(NULL);
}

//有定时器到点
void vTalTimerIrq(void) {
    time_t nowTimeStamp = time(NULL);
    TalTimer_t *pTemp = talTimerList.timerHead;
    time_t recentClockTime = MAX_TIMESTAMP;

    while (pTemp) {
        if (pTemp->timestamp <= nowTimeStamp && pTemp->status == TAL_TIMER_STATUS_DOING) {
            pTemp->status = TAL_TIMER_STATUS_DOING;
            xTaskCreate(vTalTimerCallTask, /* The function that implements the task. */
                        "vTalTimerCallTask",  /* Just a text name for the task to aid debugging. */
                        TAL_TIMER_RPC_TASK_SIZE,     /* The stack size is defined in FreeRTOSIPConfig.h. */
                        (void *) pTemp,           /* The task parameter, not used in this case. */
                        TAL_TIMER_RPC_TASK_PRIORITY,      /* The priority assigned to the task is defined in FreeRTOSConfig.h. */
                        NULL);
        } else {
            recentClockTime = MIN(recentClockTime, pTemp->timestamp);
        }
        pTemp = pTemp->next;
    }
    talTimerList.recentClockTime = recentClockTime;
}

void vTalTimerCallback(TimerHandle_t xTimer) {
    if (xTimer == TalTimer) {
        tick++;
        if (talCountDownList.recentAlarmTime <= tick) {
            vTalCountDownIrq();
        }

        if (talTimerList.recentClockTime <= time(NULL)) {
            vTalTimerIrq();
        }
    }
}

static void vTalTimerNodeInitfromFlash(char *key, char *value) {
    lHalFatFSGetDataString(key, value);
    LOGD(TAG, "%s: key=%s, value=%s", __func__, key, value);

    mson_jsonToObj(value, MSON_MEMBERS_TO_STRING(TimerInfo_t), _obj);
    if (_obj == NULL) {
        LOGE(TAG, "parse timerInfo failed!");
        return;
    }

    TimerInfo_t *_infoParam = (TimerInfo_t *) _obj;
    LOGD(TAG, "%s: timestamp=%d, method=%s, params=%s, number=%s", __func__,
                       _infoParam->timestamp, _infoParam->method,
                       _infoParam->params, _infoParam->number);
    lTalTimerInitfromFlash(_infoParam->timestamp, _infoParam->method,
                                    _infoParam->params, _infoParam->number);
    mson_releaseWithObj(_obj);
}

static void vTalTimerListInitAfterReboot(void) {
    char idMgrBuf[128] = {0};
    Err_t ret = lHalFatFSGetDataString(KEY_TIMER_ID_MGR, idMgrBuf);
    LOGI(TAG, "%s: enter\n", __func__);
    if (ret != DEV_EOK) {
        if (ret == DEV_EEMPTY) {
            LOGD(TAG, "%s: timer id manager information is empty!\n", __func__);
        } else {
            LOGE(TAG, "%s: get timer id manager information failed!\n", __func__);
            // TODO: send err response to cloud
        }
    } else {
        LOGI(TAG, "%s: timer id manager information is %s\n", __func__, idMgrBuf);
        char *timerId[TAL_TIMER_MAX] = {0};
        char timerInfo[512] = {0};
        timerId[0] = strtok(idMgrBuf, ";");
        if (!timerId[0]) return;
        LOGD(TAG, "%s: timerId[0] is %s\n", __func__, timerId[0]);
        vTalTimerNodeInitfromFlash(timerId[0], timerInfo);

        LOGD(TAG, "%s: timerInfo is %s\n", __func__, timerInfo);

        for (int i=1; i<TAL_TIMER_MAX; ++i) {
            timerId[i] = strtok(NULL, ";");
            if (!timerId[i]) break;
            LOGD(TAG, "%s: timerId[%d] is %s\n", __func__, i, timerId[i]);
            memset(timerInfo, 0x0, sizeof(timerInfo));
            vTalTimerNodeInitfromFlash(timerId[i], timerInfo);
        }
    }

    vUpdateTimerRecentClockTime();
}

int lTalTimerInit(void) {
    vTalTimerListInitAfterReboot();

    TalTimer = xTimerCreate("TalTimer", 1000 / portTICK_PERIOD_MS, pdTRUE, (void *) 0, vTalTimerCallback);
    xSemaphore = xSemaphoreCreateBinary();

    xTimerStart(TalTimer, 0);
#ifndef NTP_SYNC_DISABLE
    //ntp同步任务
    xTaskCreate(vTalNtpTask, /* The function that implements the task. */
                "vTalNtpTask",  /* Just a text name for the task to aid debugging. */
                TAL_TIMER_MAIN_TASK_SIZE,     /* The stack size is defined in FreeRTOSIPConfig.h. */
                (void *) NULL,           /* The task parameter, not used in this case. */
                TAL_TIMER_MAIN_TASK_PRIORITY,      /* The priority assigned to the task is defined in FreeRTOSConfig.h. */
                NULL);

    // testApp();
#endif

    return 0;
}


INIT_APP_EXPORT(lTalTimerInit)
/* USER CODE END 2 */