// mprpcgen version: 1.4.7
/* USER CODE BEGIN Header */
/**
  * @file tal_timer_service.c
  * @attention
  * Slender West Lake automatically generated code.
  *
  */
/* USER CODE END Header */

#include <stdio.h>
#include <stdlib.h>
#include <memory.h>
#include "tal_timer_service.h"
#include "tal_timer.h"
#include "auto_init.h"
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* USER CODE BEGIN Define */
#define STRING_SIZE 1024
/* USER CODE END Define */

/* USER CODE BEGIN 1 */

/* USER CODE END 1 */

//Define a MSON structure for input parameter processing
MSON_DEFOBJ(maopao_lTalCountDown_inParam_t,
    int duration;
    char *method;
    jsonStr params;
);

//Define a MSON structure for output parameter processing
MSON_DEFOBJ(maopao_lTalCountDown_outParam_t,
    char *number;
);

//Define a MSON structure for input parameter processing
MSON_DEFOBJ(maopao_lTalGetCountdown_inParam_t,
    char *number;
);

//Define a MSON structure for output parameter processing
MSON_DEFOBJ(maopao_lTalGetCountdown_outParam_t,
    char *number;
    char *method;
    jsonStr params;
    char *status;
    int duration;
    int remainTime;
);

//Define a MSON structure for input parameter processing
MSON_DEFOBJ(maopao_lTalPauseCountdown_inParam_t,
    char *number;
);

//Define a MSON structure for output parameter processing
MSON_DEFOBJ(maopao_lTalPauseCountdown_outParam_t,
    char *number;
);

//Define a MSON structure for input parameter processing
MSON_DEFOBJ(maopao_lTalResumeCountdown_inParam_t,
    char *number;
);

//Define a MSON structure for output parameter processing
MSON_DEFOBJ(maopao_lTalResumeCountdown_outParam_t,
    char *number;
);

//Define a MSON structure for input parameter processing
MSON_DEFOBJ(maopao_lTalResetCountdown_inParam_t,
    char *number;
);

//Define a MSON structure for output parameter processing
MSON_DEFOBJ(maopao_lTalResetCountdown_outParam_t,
    char *number;
);

//Define a MSON structure for input parameter processing
MSON_DEFOBJ(maopao_lTalCancelCountdown_inParam_t,
    char *number;
);

//Define a MSON structure for output parameter processing
MSON_DEFOBJ(maopao_lTalCancelCountdown_outParam_t,
    char *number;
);

//Define a MSON structure for input parameter processing
MSON_DEFOBJ(maopao_lTalTimer_inParam_t,
    int timestamp;
    char *method;
    jsonStr params;
);

//Define a MSON structure for output parameter processing
MSON_DEFOBJ(maopao_lTalTimer_outParam_t,
    char *number;
);

//Define a MSON structure for input parameter processing
MSON_DEFOBJ(maopao_lTalGetTimer_inParam_t,
    char *number;
);

//Define a MSON structure for output parameter processing
MSON_DEFOBJ(maopao_lTalGetTimer_outParam_t,
    char *number;
    char *method;
    jsonStr params;
    char *status;
    int timestamp;
);

//Define a MSON structure for input parameter processing
MSON_DEFOBJ(maopao_lTalCancelTimer_inParam_t,
    char *number;
);

//Define a MSON structure for output parameter processing
MSON_DEFOBJ(maopao_lTalCancelTimer_outParam_t,
    char *number;
);

//Define a MSON structure for output parameter processing
MSON_DEFOBJ(maopao_lTalGetCountdownList_outParam_t,
    jsonStr countdownList;
);

//Define a MSON structure for output parameter processing
MSON_DEFOBJ(maopao_lTalGetTimerList_outParam_t,
    jsonStr timerList;
);

void __maopaoServer__lTalCalibrationTime__service__(maopao_requestInfo_st *_requestInfo, SemaphoreHandle_t _mux)
{
    int _errorCode;

    //call user function
    _errorCode = lTalCalibrationTime();

    if (_errorCode != MAOPAO_SUCCESS){
        maopaoServerSendError(_requestInfo, MAOPAO_ERROR_TAL_OPERATION_FAILED, MAOPAO_ERROR_TAL_OPERATION_FAILED_MSG);
        goto _release_obj;
    }

    //send response to client
    maopaoServerSendResponse(_requestInfo, "null");

    //free the space
_release_obj:

    //release the mutex
    xSemaphoreGive(_mux);
}

void __maopaoServer__lTalCountDown__service__(maopao_requestInfo_st *_requestInfo, SemaphoreHandle_t _mux)
{
    //prepare input parameter
    mson_jsonToObj(_requestInfo->params, MSON_MEMBERS_TO_STRING(maopao_lTalCountDown_inParam_t), _inObj);
    if (_inObj == NULL){
        maopaoServerSendError(_requestInfo, MAOPAO_ERROR_RPC_ANALYSE_FAILED, MAOPAO_ERROR_RPC_ANALYSE_FAILED_MSG);
        goto _exit;
    }
    maopao_lTalCountDown_inParam_t *_inParam = (maopao_lTalCountDown_inParam_t *) _inObj;
    int duration = _inParam->duration;
    char *method = _inParam->method;
    jsonStr params = _inParam->params;

    //prepare output parameter
    maopao_lTalCountDown_outParam_t _outParam;
    char *number;

    int _errorCode;

    //call user function
    _errorCode = lTalCountDown(duration, method, params, &number);

    if (_errorCode != MAOPAO_SUCCESS){
        maopaoServerSendError(_requestInfo, MAOPAO_ERROR_TAL_OPERATION_FAILED, MAOPAO_ERROR_TAL_OPERATION_FAILED_MSG);
        goto _release_obj;
    }

    //copy the output parameters to the structure
    _outParam.number = number;
    char *_resultJson = mson_objToJson(&_outParam, MSON_MEMBERS_TO_STRING(maopao_lTalCountDown_outParam_t));

    //send response to client
    maopaoServerSendResponse(_requestInfo, _resultJson);

    //free the space
    free(_resultJson);
_release_obj:
    mson_releaseWithObj(_inObj);

_exit:
    //release the mutex
    xSemaphoreGive(_mux);
}

void __maopaoServer__lTalGetCountdown__service__(maopao_requestInfo_st *_requestInfo, SemaphoreHandle_t _mux)
{
    //prepare input parameter
    mson_jsonToObj(_requestInfo->params, MSON_MEMBERS_TO_STRING(maopao_lTalGetCountdown_inParam_t), _inObj);
    if (_inObj == NULL){
        maopaoServerSendError(_requestInfo, MAOPAO_ERROR_RPC_ANALYSE_FAILED, MAOPAO_ERROR_RPC_ANALYSE_FAILED_MSG);
        goto _exit;
    }
    maopao_lTalGetCountdown_inParam_t *_inParam = (maopao_lTalGetCountdown_inParam_t *) _inObj;
    char *number_in = _inParam->number;

    //prepare output parameter
    maopao_lTalGetCountdown_outParam_t _outParam;
    char *number_out;
    char *method;
    jsonStr params;
    char *status;
    int duration;
    int remainTime;

    int _errorCode;

    //call user function
    _errorCode = lTalGetCountdown(number_in, &number_out, &method, &params, &status, &duration, &remainTime);

    if (_errorCode != MAOPAO_SUCCESS){
        maopaoServerSendError(_requestInfo, MAOPAO_ERROR_TAL_OPERATION_FAILED, MAOPAO_ERROR_TAL_OPERATION_FAILED_MSG);
        goto _release_obj;
    }

    //copy the output parameters to the structure
    _outParam.number = number_out;
    _outParam.method = method;
    _outParam.params = params;
    _outParam.status = status;
    _outParam.duration = duration;
    _outParam.remainTime = remainTime;
    char *_resultJson = mson_objToJson(&_outParam, MSON_MEMBERS_TO_STRING(maopao_lTalGetCountdown_outParam_t));

    //send response to client
    maopaoServerSendResponse(_requestInfo, _resultJson);

    //free the space
    free(_resultJson);
_release_obj:
    mson_releaseWithObj(_inObj);

_exit:
    //release the mutex
    xSemaphoreGive(_mux);
}

void __maopaoServer__lTalPauseCountdown__service__(maopao_requestInfo_st *_requestInfo, SemaphoreHandle_t _mux)
{
    //prepare input parameter
    mson_jsonToObj(_requestInfo->params, MSON_MEMBERS_TO_STRING(maopao_lTalPauseCountdown_inParam_t), _inObj);
    if (_inObj == NULL){
        maopaoServerSendError(_requestInfo, MAOPAO_ERROR_RPC_ANALYSE_FAILED, MAOPAO_ERROR_RPC_ANALYSE_FAILED_MSG);
        goto _exit;
    }
    maopao_lTalPauseCountdown_inParam_t *_inParam = (maopao_lTalPauseCountdown_inParam_t *) _inObj;
    char *number_in = _inParam->number;

    //prepare output parameter
    maopao_lTalPauseCountdown_outParam_t _outParam;
    char *number_out;

    int _errorCode;

    //call user function
    _errorCode = lTalPauseCountdown(number_in, &number_out);

    if (_errorCode != MAOPAO_SUCCESS){
        maopaoServerSendError(_requestInfo, MAOPAO_ERROR_TAL_OPERATION_FAILED, MAOPAO_ERROR_TAL_OPERATION_FAILED_MSG);
        goto _release_obj;
    }

    //copy the output parameters to the structure
    _outParam.number = number_out;
    char *_resultJson = mson_objToJson(&_outParam, MSON_MEMBERS_TO_STRING(maopao_lTalPauseCountdown_outParam_t));

    //send response to client
    maopaoServerSendResponse(_requestInfo, _resultJson);

    //free the space
    free(_resultJson);
_release_obj:
    mson_releaseWithObj(_inObj);

_exit:
    //release the mutex
    xSemaphoreGive(_mux);
}

void __maopaoServer__lTalResumeCountdown__service__(maopao_requestInfo_st *_requestInfo, SemaphoreHandle_t _mux)
{
    //prepare input parameter
    mson_jsonToObj(_requestInfo->params, MSON_MEMBERS_TO_STRING(maopao_lTalResumeCountdown_inParam_t), _inObj);
    if (_inObj == NULL){
        maopaoServerSendError(_requestInfo, MAOPAO_ERROR_RPC_ANALYSE_FAILED, MAOPAO_ERROR_RPC_ANALYSE_FAILED_MSG);
        goto _exit;
    }
    maopao_lTalResumeCountdown_inParam_t *_inParam = (maopao_lTalResumeCountdown_inParam_t *) _inObj;
    char *number_in = _inParam->number;

    //prepare output parameter
    maopao_lTalResumeCountdown_outParam_t _outParam;
    char *number_out;

    int _errorCode;

    //call user function
    _errorCode = lTalResumeCountdown(number_in, &number_out);

    if (_errorCode != MAOPAO_SUCCESS){
        maopaoServerSendError(_requestInfo, MAOPAO_ERROR_TAL_OPERATION_FAILED, MAOPAO_ERROR_TAL_OPERATION_FAILED_MSG);
        goto _release_obj;
    }

    //copy the output parameters to the structure
    _outParam.number = number_out;
    char *_resultJson = mson_objToJson(&_outParam, MSON_MEMBERS_TO_STRING(maopao_lTalResumeCountdown_outParam_t));

    //send response to client
    maopaoServerSendResponse(_requestInfo, _resultJson);

    //free the space
    free(_resultJson);
_release_obj:
    mson_releaseWithObj(_inObj);

_exit:
    //release the mutex
    xSemaphoreGive(_mux);
}

void __maopaoServer__lTalResetCountdown__service__(maopao_requestInfo_st *_requestInfo, SemaphoreHandle_t _mux)
{
    //prepare input parameter
    mson_jsonToObj(_requestInfo->params, MSON_MEMBERS_TO_STRING(maopao_lTalResetCountdown_inParam_t), _inObj);
    if (_inObj == NULL){
        maopaoServerSendError(_requestInfo, MAOPAO_ERROR_RPC_ANALYSE_FAILED, MAOPAO_ERROR_RPC_ANALYSE_FAILED_MSG);
        goto _exit;
    }
    maopao_lTalResetCountdown_inParam_t *_inParam = (maopao_lTalResetCountdown_inParam_t *) _inObj;
    char *number_in = _inParam->number;

    //prepare output parameter
    maopao_lTalResetCountdown_outParam_t _outParam;
    char *number_out;

    int _errorCode;

    //call user function
    _errorCode = lTalResetCountdown(number_in, &number_out);

    if (_errorCode != MAOPAO_SUCCESS){
        maopaoServerSendError(_requestInfo, MAOPAO_ERROR_TAL_OPERATION_FAILED, MAOPAO_ERROR_TAL_OPERATION_FAILED_MSG);
        goto _release_obj;
    }

    //copy the output parameters to the structure
    _outParam.number = number_out;
    char *_resultJson = mson_objToJson(&_outParam, MSON_MEMBERS_TO_STRING(maopao_lTalResetCountdown_outParam_t));

    //send response to client
    maopaoServerSendResponse(_requestInfo, _resultJson);

    //free the space
    free(_resultJson);
_release_obj:
    mson_releaseWithObj(_inObj);

_exit:
    //release the mutex
    xSemaphoreGive(_mux);
}

void __maopaoServer__lTalCancelCountdown__service__(maopao_requestInfo_st *_requestInfo, SemaphoreHandle_t _mux)
{
    //prepare input parameter
    mson_jsonToObj(_requestInfo->params, MSON_MEMBERS_TO_STRING(maopao_lTalCancelCountdown_inParam_t), _inObj);
    if (_inObj == NULL){
        maopaoServerSendError(_requestInfo, MAOPAO_ERROR_RPC_ANALYSE_FAILED, MAOPAO_ERROR_RPC_ANALYSE_FAILED_MSG);
        goto _exit;
    }
    maopao_lTalCancelCountdown_inParam_t *_inParam = (maopao_lTalCancelCountdown_inParam_t *) _inObj;
    char *number_in = _inParam->number;

    //prepare output parameter
    maopao_lTalCancelCountdown_outParam_t _outParam;
    char *number_out;

    int _errorCode;

    //call user function
    _errorCode = lTalCancelCountdown(number_in, &number_out);

    if (_errorCode != MAOPAO_SUCCESS){
        maopaoServerSendError(_requestInfo, MAOPAO_ERROR_TAL_OPERATION_FAILED, MAOPAO_ERROR_TAL_OPERATION_FAILED_MSG);
        goto _release_obj;
    }

    //copy the output parameters to the structure
    _outParam.number = number_out;
    char *_resultJson = mson_objToJson(&_outParam, MSON_MEMBERS_TO_STRING(maopao_lTalCancelCountdown_outParam_t));

    //send response to client
    maopaoServerSendResponse(_requestInfo, _resultJson);

    //free the space
    free(_resultJson);
_release_obj:
    mson_releaseWithObj(_inObj);

_exit:
    //release the mutex
    xSemaphoreGive(_mux);
}

void __maopaoServer__lTalTimer__service__(maopao_requestInfo_st *_requestInfo, SemaphoreHandle_t _mux)
{
    //prepare input parameter
    mson_jsonToObj(_requestInfo->params, MSON_MEMBERS_TO_STRING(maopao_lTalTimer_inParam_t), _inObj);
    if (_inObj == NULL){
        maopaoServerSendError(_requestInfo, MAOPAO_ERROR_RPC_ANALYSE_FAILED, MAOPAO_ERROR_RPC_ANALYSE_FAILED_MSG);
        goto _exit;
    }
    maopao_lTalTimer_inParam_t *_inParam = (maopao_lTalTimer_inParam_t *) _inObj;
    int timestamp = _inParam->timestamp;
    char *method = _inParam->method;
    jsonStr params = _inParam->params;

    //prepare output parameter
    maopao_lTalTimer_outParam_t _outParam;
    char *number;

    int _errorCode;

    //call user function
    _errorCode = lTalTimer(timestamp, method, params, &number);

    if (_errorCode != MAOPAO_SUCCESS){
        maopaoServerSendError(_requestInfo, MAOPAO_ERROR_TAL_OPERATION_FAILED, MAOPAO_ERROR_TAL_OPERATION_FAILED_MSG);
        goto _release_obj;
    }

    //copy the output parameters to the structure
    _outParam.number = number;
    char *_resultJson = mson_objToJson(&_outParam, MSON_MEMBERS_TO_STRING(maopao_lTalTimer_outParam_t));

    //send response to client
    maopaoServerSendResponse(_requestInfo, _resultJson);

    //free the space
    free(_resultJson);
_release_obj:
    mson_releaseWithObj(_inObj);

_exit:
    //release the mutex
    xSemaphoreGive(_mux);
}

void __maopaoServer__lTalGetTimer__service__(maopao_requestInfo_st *_requestInfo, SemaphoreHandle_t _mux)
{
    //prepare input parameter
    mson_jsonToObj(_requestInfo->params, MSON_MEMBERS_TO_STRING(maopao_lTalGetTimer_inParam_t), _inObj);
    if (_inObj == NULL){
        maopaoServerSendError(_requestInfo, MAOPAO_ERROR_RPC_ANALYSE_FAILED, MAOPAO_ERROR_RPC_ANALYSE_FAILED_MSG);
        goto _exit;
    }
    maopao_lTalGetTimer_inParam_t *_inParam = (maopao_lTalGetTimer_inParam_t *) _inObj;
    char *number_in = _inParam->number;

    //prepare output parameter
    maopao_lTalGetTimer_outParam_t _outParam;
    char *number_out;
    char *method;
    jsonStr params;
    char *status;
    int timestamp;

    int _errorCode;

    //call user function
    _errorCode = lTalGetTimer(number_in, &number_out, &method, &params, &status, &timestamp);

    if (_errorCode != MAOPAO_SUCCESS){
        maopaoServerSendError(_requestInfo, MAOPAO_ERROR_TAL_OPERATION_FAILED, MAOPAO_ERROR_TAL_OPERATION_FAILED_MSG);
        goto _release_obj;
    }

    //copy the output parameters to the structure
    _outParam.number = number_out;
    _outParam.method = method;
    _outParam.params = params;
    _outParam.status = status;
    _outParam.timestamp = timestamp;
    char *_resultJson = mson_objToJson(&_outParam, MSON_MEMBERS_TO_STRING(maopao_lTalGetTimer_outParam_t));

    //send response to client
    maopaoServerSendResponse(_requestInfo, _resultJson);

    //free the space
    free(_resultJson);
_release_obj:
    mson_releaseWithObj(_inObj);

_exit:
    //release the mutex
    xSemaphoreGive(_mux);
}

void __maopaoServer__lTalCancelTimer__service__(maopao_requestInfo_st *_requestInfo, SemaphoreHandle_t _mux)
{
    //prepare input parameter
    mson_jsonToObj(_requestInfo->params, MSON_MEMBERS_TO_STRING(maopao_lTalCancelTimer_inParam_t), _inObj);
    if (_inObj == NULL){
        maopaoServerSendError(_requestInfo, MAOPAO_ERROR_RPC_ANALYSE_FAILED, MAOPAO_ERROR_RPC_ANALYSE_FAILED_MSG);
        goto _exit;
    }
    maopao_lTalCancelTimer_inParam_t *_inParam = (maopao_lTalCancelTimer_inParam_t *) _inObj;
    char *number_in = _inParam->number;

    //prepare output parameter
    maopao_lTalCancelTimer_outParam_t _outParam;
    char *number_out;

    int _errorCode;

    //call user function
    _errorCode = lTalCancelTimer(number_in, &number_out);

    if (_errorCode != MAOPAO_SUCCESS){
        maopaoServerSendError(_requestInfo, MAOPAO_ERROR_TAL_OPERATION_FAILED, MAOPAO_ERROR_TAL_OPERATION_FAILED_MSG);
        goto _release_obj;
    }

    //copy the output parameters to the structure
    _outParam.number = number_out;
    char *_resultJson = mson_objToJson(&_outParam, MSON_MEMBERS_TO_STRING(maopao_lTalCancelTimer_outParam_t));

    //send response to client
    maopaoServerSendResponse(_requestInfo, _resultJson);

    //free the space
    free(_resultJson);
_release_obj:
    mson_releaseWithObj(_inObj);

_exit:
    //release the mutex
    xSemaphoreGive(_mux);
}

void __maopaoServer__lTalGetCountdownList__service__(maopao_requestInfo_st *_requestInfo, SemaphoreHandle_t _mux)
{
    //prepare output parameter
    maopao_lTalGetCountdownList_outParam_t _outParam;
    jsonStr countdownList;

    int _errorCode;

    //call user function
    _errorCode = lTalGetCountdownList(&countdownList);

    if (_errorCode != MAOPAO_SUCCESS){
        maopaoServerSendError(_requestInfo, MAOPAO_ERROR_TAL_OPERATION_FAILED, MAOPAO_ERROR_TAL_OPERATION_FAILED_MSG);
        goto _release_obj;
    }

    //copy the output parameters to the structure
    _outParam.countdownList = countdownList;
    char *_resultJson = mson_objToJson(&_outParam, MSON_MEMBERS_TO_STRING(maopao_lTalGetCountdownList_outParam_t));

    //send response to client
    maopaoServerSendResponse(_requestInfo, _resultJson);

    //free the space
    free(_resultJson);
_release_obj:

    //release the mutex
    xSemaphoreGive(_mux);
}

void __maopaoServer__lTalGetTimerList__service__(maopao_requestInfo_st *_requestInfo, SemaphoreHandle_t _mux)
{
    //prepare output parameter
    maopao_lTalGetTimerList_outParam_t _outParam;
    jsonStr timerList;

    int _errorCode;

    //call user function
    _errorCode = lTalGetTimerList(&timerList);

    if (_errorCode != MAOPAO_SUCCESS){
        maopaoServerSendError(_requestInfo, MAOPAO_ERROR_TAL_OPERATION_FAILED, MAOPAO_ERROR_TAL_OPERATION_FAILED_MSG);
        goto _release_obj;
    }

    //copy the output parameters to the structure
    _outParam.timerList = timerList;
    char *_resultJson = mson_objToJson(&_outParam, MSON_MEMBERS_TO_STRING(maopao_lTalGetTimerList_outParam_t));

    //send response to client
    maopaoServerSendResponse(_requestInfo, _resultJson);

    //free the space
    free(_resultJson);
_release_obj:

    //release the mutex
    xSemaphoreGive(_mux);
}

maopao_service_handler_st tal_timer_serviceGroup[] = {
        {.method = "calibrationTime", .methodId = NULL, .handlerCb = MAOPAO_SERVICE(lTalCalibrationTime), .inParam = NULL, .outParam = NULL},
        {.method = "countdown", .methodId = NULL, .handlerCb = MAOPAO_SERVICE(lTalCountDown), .inParam = MSON_MEMBERS_TO_STRING(maopao_lTalCountDown_inParam_t), .outParam = MSON_MEMBERS_TO_STRING(maopao_lTalCountDown_outParam_t)},
        {.method = "getCountdown", .methodId = NULL, .handlerCb = MAOPAO_SERVICE(lTalGetCountdown), .inParam = MSON_MEMBERS_TO_STRING(maopao_lTalGetCountdown_inParam_t), .outParam = MSON_MEMBERS_TO_STRING(maopao_lTalGetCountdown_outParam_t)},
        {.method = "pauseCountdown", .methodId = NULL, .handlerCb = MAOPAO_SERVICE(lTalPauseCountdown), .inParam = MSON_MEMBERS_TO_STRING(maopao_lTalPauseCountdown_inParam_t), .outParam = MSON_MEMBERS_TO_STRING(maopao_lTalPauseCountdown_outParam_t)},
        {.method = "resumeCountdown", .methodId = NULL, .handlerCb = MAOPAO_SERVICE(lTalResumeCountdown), .inParam = MSON_MEMBERS_TO_STRING(maopao_lTalResumeCountdown_inParam_t), .outParam = MSON_MEMBERS_TO_STRING(maopao_lTalResumeCountdown_outParam_t)},
        {.method = "resetCountdown", .methodId = NULL, .handlerCb = MAOPAO_SERVICE(lTalResetCountdown), .inParam = MSON_MEMBERS_TO_STRING(maopao_lTalResetCountdown_inParam_t), .outParam = MSON_MEMBERS_TO_STRING(maopao_lTalResetCountdown_outParam_t)},
        {.method = "cancelCountdown", .methodId = NULL, .handlerCb = MAOPAO_SERVICE(lTalCancelCountdown), .inParam = MSON_MEMBERS_TO_STRING(maopao_lTalCancelCountdown_inParam_t), .outParam = MSON_MEMBERS_TO_STRING(maopao_lTalCancelCountdown_outParam_t)},
        {.method = "timer", .methodId = NULL, .handlerCb = MAOPAO_SERVICE(lTalTimer), .inParam = MSON_MEMBERS_TO_STRING(maopao_lTalTimer_inParam_t), .outParam = MSON_MEMBERS_TO_STRING(maopao_lTalTimer_outParam_t)},
        {.method = "getTimer", .methodId = NULL, .handlerCb = MAOPAO_SERVICE(lTalGetTimer), .inParam = MSON_MEMBERS_TO_STRING(maopao_lTalGetTimer_inParam_t), .outParam = MSON_MEMBERS_TO_STRING(maopao_lTalGetTimer_outParam_t)},
        {.method = "cancelTimer", .methodId = NULL, .handlerCb = MAOPAO_SERVICE(lTalCancelTimer), .inParam = MSON_MEMBERS_TO_STRING(maopao_lTalCancelTimer_inParam_t), .outParam = MSON_MEMBERS_TO_STRING(maopao_lTalCancelTimer_outParam_t)},
        {.method = "getCountdownList", .methodId = NULL, .handlerCb = MAOPAO_SERVICE(lTalGetCountdownList), .inParam = NULL, .outParam = MSON_MEMBERS_TO_STRING(maopao_lTalGetCountdownList_outParam_t)},
        {.method = "getTimerList", .methodId = NULL, .handlerCb = MAOPAO_SERVICE(lTalGetTimerList), .inParam = NULL, .outParam = MSON_MEMBERS_TO_STRING(maopao_lTalGetTimerList_outParam_t)},
        {.method = NULL, .handlerCb = NULL}
};

#ifdef AUTO_ADD_SERVICE_GROUP
static int lTalAutoAddServiceGroup_tal_timer(void){
    maopaoServerAddDefaultServiceGroup(tal_timer_serviceGroup);
    return 0;
}
INIT_APP_EXPORT(lTalAutoAddServiceGroup_tal_timer);
#endif //AUTO_ADD_SERVICE_GROUP

/* USER CODE BEGIN 2 */

/* USER CODE END 2 */

