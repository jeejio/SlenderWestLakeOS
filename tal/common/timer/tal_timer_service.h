// mprpcgen version: 1.4.7
/* USER CODE BEGIN Header */
/**
  * @file tal_timer_service.h
  * @attention
  * Slender West Lake automatically generated code.
  *
  */
/* USER CODE END Header */

#ifndef TAL_TIMER_SERVICE_H
#define TAL_TIMER_SERVICE_H

#include "maopaoServer.h"
#include "maopaoError.h"
#include "mson.h"
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* USER CODE BEGIN 1 */
void __maopaoServer__lTalCalibrationTime__service__(maopao_requestInfo_st *_requestInfo, SemaphoreHandle_t _mux);

void __maopaoServer__lTalCountDown__service__(maopao_requestInfo_st *_requestInfo, SemaphoreHandle_t _mux);

void __maopaoServer__lTalGetCountdown__service__(maopao_requestInfo_st *_requestInfo, SemaphoreHandle_t _mux);

void __maopaoServer__lTalPauseCountdown__service__(maopao_requestInfo_st *_requestInfo, SemaphoreHandle_t _mux);

void __maopaoServer__lTalResumeCountdown__service__(maopao_requestInfo_st *_requestInfo, SemaphoreHandle_t _mux);

void __maopaoServer__lTalResetCountdown__service__(maopao_requestInfo_st *_requestInfo, SemaphoreHandle_t _mux);

void __maopaoServer__lTalCancelCountdown__service__(maopao_requestInfo_st *_requestInfo, SemaphoreHandle_t _mux);

void __maopaoServer__lTalTimer__service__(maopao_requestInfo_st *_requestInfo, SemaphoreHandle_t _mux);

void __maopaoServer__lTalGetTimer__service__(maopao_requestInfo_st *_requestInfo, SemaphoreHandle_t _mux);

void __maopaoServer__lTalCancelTimer__service__(maopao_requestInfo_st *_requestInfo, SemaphoreHandle_t _mux);

void __maopaoServer__lTalGetCountdownList__service__(maopao_requestInfo_st *_requestInfo, SemaphoreHandle_t _mux);

void __maopaoServer__lTalGetTimerList__service__(maopao_requestInfo_st *_requestInfo, SemaphoreHandle_t _mux);
/* USER CODE END 1 */

extern maopao_service_handler_st tal_timer_serviceGroup[];

/* USER CODE BEGIN 2 */

/* USER CODE END 2 */

#endif  //TAL_TIMER_SERVICE_H
