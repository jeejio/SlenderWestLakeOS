//Slender West Lake automatically generated code, do not edit


#include <stdio.h>
#include <stdlib.h>
#include <memory.h>
#include "tal_info_service.h"
#include "tal_info.h"

#define STRING_SIZE 1024

//Define a MSON structure for output parameter processing
MSON_DEFOBJ(maopao_lMaopaoRpcList_outParam_t,
    jsonStr rpcList;
);

//Define a MSON structure for output parameter processing
MSON_DEFOBJ(maopao_getTalInfo_outParam_t,
    jsonStr talInfo;
);

maopao_service_handler_st tal_info_serviceGroup[] = {
        {.method = "rpcList", .handlerCb = MAOPAO_SERVICE(lMaopaoRpcList)},
        {.method = "talInfo", .handlerCb = MAOPAO_SERVICE(getTalInfo)},
        {.method = NULL, .handlerCb = NULL}
};

void __maopaoServer__lMaopaoRpcList__service__(maopao_requestInfo_st *_requestInfo, SemaphoreHandle_t _mux)
{
    //prepare output parameter
    maopao_lMaopaoRpcList_outParam_t _outParam;
    jsonStr rpcList;

    int _errorCode;

    //call user function
    _errorCode = lMaopaoRpcList(&rpcList);

    if (_errorCode != MAOPAO_SUCCESS){
        maopaoServerSendError(_requestInfo, MAOPAO_ERROR_TAL_OPERATION_FAILED, MAOPAO_ERROR_TAL_OPERATION_FAILED_MSG);
        goto _release_obj;
    }

    //copy the output parameters to the structure
    _outParam.rpcList = rpcList;
    char *_resultJson = mson_objToJson(&_outParam, MSON_MEMBERS_TO_STRING(maopao_lMaopaoRpcList_outParam_t));

    //send response to client
    maopaoServerSendResponse(_requestInfo, _resultJson);

    //free the space
    free(_resultJson);
_release_obj:

    //release the mutex
    xSemaphoreGive(_mux);
}

void __maopaoServer__getTalInfo__service__(maopao_requestInfo_st *_requestInfo, SemaphoreHandle_t _mux)
{
    //prepare output parameter
    maopao_getTalInfo_outParam_t _outParam;
    jsonStr talInfo;
    jsonStr talInfo_addressRecord;
    talInfo = malloc(STRING_SIZE);
    talInfo_addressRecord = talInfo;

    int _errorCode;

    //call user function
    _errorCode = getTalInfo(talInfo);

    if (_errorCode != MAOPAO_SUCCESS){
        maopaoServerSendError(_requestInfo, MAOPAO_ERROR_TAL_OPERATION_FAILED, MAOPAO_ERROR_TAL_OPERATION_FAILED_MSG);
        goto _release_obj;
    }

    //copy the output parameters to the structure
    _outParam.talInfo = talInfo;
    char *_resultJson = mson_objToJson(&_outParam, MSON_MEMBERS_TO_STRING(maopao_getTalInfo_outParam_t));

    //send response to client
    maopaoServerSendResponse(_requestInfo, _resultJson);

    //free the space
    free(talInfo_addressRecord);
    free(_resultJson);
_release_obj:

    //release the mutex
    xSemaphoreGive(_mux);
}

