//Slender West Lake automatically generated code

#include <stdio.h>
#include <stdlib.h>
#include "tal_info_service.h"
#include "tal_info.h"
#include "string.h"
#include "mson.h"

const char *const TalInfo_st_Members = "char *manufacturer;char *category;char *version;char *developer;";
const int TalInfo_st_SIZE = sizeof(TalInfo_st);

extern TalInfo_st _talInfo;

__weak_symbol TalInfo_st _talInfo = {0};

int getTalInfo(jsonStr talInfo)
{
    //user write code here.
    char *jsonStr = mson_objToJson(&_talInfo, MSON_MEMBERS_TO_STRING(TalInfo_st));
    strcpy(talInfo, jsonStr);
    free(jsonStr);
    return MAOPAO_SUCCESS;
}

