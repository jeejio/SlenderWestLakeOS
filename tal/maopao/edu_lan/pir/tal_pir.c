#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "device.h"
#include "hal_sensor.h"     //sesor IO模型驱动框架头文件
#include "esp_log.h"
#include "tal_pir.h"

#define PIR_DEV "PIR"
#define SENSOR_PIR_INIT (SENSOR_CTRL_USER_CMD_START + 1)
Device_t sensor_pir_dev = NULL;
static unsigned char sensorPirValue = 0;

void vTalPirInit(void)
{
    // 查找设备
    sensor_pir_dev = pHalDeviceFind(PIR_DEV);
    if (sensor_pir_dev == NULL)
    {
        LOGE("PIR", "can not find sensor Model\n");
        return;
    }
    else
        LOGI("PIR", "find sensor Model ok\n");

    // 打开设备
    Err_t result = DEV_EOK;
    result = lHalDeviceOpen(sensor_pir_dev, DEVICE_FLAG_RDONLY);
    if (result != DEV_EOK)
    {
        printf("can not open senor device\n");
        return;
    }
    else
        printf("open senor device ok\n");

    // 初始化设备
    lHalDeviceControl(sensor_pir_dev, SENSOR_PIR_INIT, NULL);
    
}

Base_t lTalGetIsExists(void)
{
    ulHalDeviceRead(sensor_pir_dev, 0, &sensorPirValue, 1);
    return sensorPirValue;
}