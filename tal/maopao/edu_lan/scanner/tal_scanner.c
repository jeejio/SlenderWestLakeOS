#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "device.h"
#include "hal_sensor.h" //sesor IO模型驱动框架头文件
#include "esp_log.h"
#include "tal_scanner.h"
#include <string.h>

#define SCANNER_DEV "md280"
Device_t scanner_dev = NULL;

Int32_t lTalScannerInit(void)
{

    // 查找设备
    scanner_dev = pHalDeviceFind(SCANNER_DEV);
    if (scanner_dev == NULL)
    {
        printf("can not find scan_md280 Model\n");
        return DEV_ERROR;
    }

    // 打开设备
    Err_t result = DEV_EOK;
    result = lHalDeviceOpen(scanner_dev, DEVICE_OFLAG_RDWR);
    if (result != DEV_EOK)
    {
        printf("can not open scan_md280 device\n");
        return DEV_ERROR;
    }
    return DEV_EOK;
}

Uint8_t ucTalScannerGetCode(Uint8_t *readdData)
{
    Uint8_t data[150];
    ulHalDeviceRead(scanner_dev, NULL, data, 1);
    memcpy(readdData,&data[1],data[0]);
    return data[0];
}

