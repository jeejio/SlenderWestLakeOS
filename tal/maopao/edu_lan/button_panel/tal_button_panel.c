/*
 * Copyright (c) 2022, jeejio Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author         Notes
 * 2023-02-10     xuyuhu         the first version
 */

#include <stdio.h>
#include "esp_log.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "tal_button_panel.h"
#include "freertos/device.h"
#include "hal_sensor.h"

Device_t button665_sensor_dev = NULL;
static ButtonPanelInfo_t ButtonPanelInfo ={
    .code =1,
    .button_mode =0,
    .value=0,
};
ButtonPanelInfo_t * cTalButtonPanelOnReceive(void)
{
    int buf[2];
    ulHalDeviceRead(button665_sensor_dev,NULL,(void *)buf,sizeof(buf));
    ButtonPanelInfo.button_mode=buf[1];
    ButtonPanelInfo.value=buf[0];
    return &ButtonPanelInfo;
}


int  cTalButtonPanelGetButtonCodes(void)
{
    cTalButtonPanelOnReceive();
    return ButtonPanelInfo.value;
}

int cTalButtonPanelGetButtonMode(void)
{
    cTalButtonPanelOnReceive();
    return ButtonPanelInfo.button_mode;
}

int vTalButtonPanelInit(void)
{
    // 查找设备
    button665_sensor_dev = pHalDeviceFind("BT665");
    if (button665_sensor_dev == NULL)
    {
        printf("can not find sensor Model\n");
        return -1;
    }
    else
        printf("find sensor Model ok\n");

    // 打开设备
    Err_t result = DEV_EOK;
    result = lHalDeviceOpen(button665_sensor_dev, DEVICE_FLAG_RDONLY);
    if (result != DEV_EOK)
    {
        printf("can not open senor device\n");
        return -1;
    }
    else
        printf("open senor device ok\n");
    return 0;
}
