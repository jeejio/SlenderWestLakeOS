/*
 * Copyright (c) 2022, jeejio Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author         Notes
 * 2023-03-23     xuyuhu         the first version
 */

#include <stdio.h>
#include "esp_log.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "tal_single_switch.h"
#include "freertos/device.h"
#include "hal_sensor.h"
#include "auto_init.h"

#define SINGLE_SWITCH_NAME  "SWITCH"
Device_t pSingleSwitchSensorDev = NULL;
static TalSwitchInfo_t xGetInfo;
void vTalSingleSwitchGetInfo(void)
{
    ulHalDeviceRead(pSingleSwitchSensorDev, NULL, &xGetInfo, sizeof(xGetInfo));
}
int lTalSingleSwitchGetStatus(void)
{
    vTalSingleSwitchGetInfo();
    return xGetInfo.IsPress;
}

int lTalSingleSwitchModeReceive(void)
{
    vTalSingleSwitchGetInfo();
    lHalDeviceControl(pSingleSwitchSensorDev,SENSOR_CTRL_USER_CMD_START + 1, NULL);
    return xGetInfo.IsMode;
}

int lTalSingleSwitchInit(void)
{
    // 查找设备
    pSingleSwitchSensorDev = pHalDeviceFind(SINGLE_SWITCH_NAME);
    if (pSingleSwitchSensorDev == NULL)
    {
        printf("can not find sensor Model\n");
        return -1;
    }
    else
        printf("find sensor Model ok\n");

    // 打开设备
    Err_t result = DEV_EOK;
    result = lHalDeviceOpen(pSingleSwitchSensorDev, DEVICE_FLAG_RDONLY);
    if (result != DEV_EOK)
    {
        printf("can not open senor device\n");
        return -1;
    }
    else
        printf("open senor device ok\n");
    return 0;
}

INIT_APP_EXPORT(lTalSingleSwitchInit);
