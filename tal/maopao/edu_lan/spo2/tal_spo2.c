#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/timers.h"
#include "device.h"
#include "hal_sensor.h" //sesor IO模型驱动框架头文件
#include "esp_log.h"
#include "tal_spo2.h"

#define SPO2_DEV "max30102"
Device_t spo2Dev = NULL;
static TimerHandle_t Spo2Timer = NULL;

Uint8_t onOffFlag=0;
Uint8_t dataOkFlag=0;
Int32_t Spo2Data=0;
Int32_t heartRate=0;
void vTalSpo2TimerCb(TimerHandle_t timer)
{
    Int32_t p[4];
    ulHalDeviceRead(spo2Dev, NULL, p,4);
    onOffFlag=p[0];
    dataOkFlag=p[1];
    Spo2Data=p[2];
    heartRate=p[3];
}
Int32_t lTalSpo2Init(void)
{

    // 查找设备
    spo2Dev = pHalDeviceFind(SPO2_DEV);
    if (spo2Dev == NULL)
    {
        printf("can not find lb1427b Model\n");
        return DEV_ERROR;
    }

    // 打开设备
    Err_t result = DEV_EOK;
    result = lHalDeviceOpen(spo2Dev, DEVICE_OFLAG_RDWR);
    if (result != DEV_EOK)
    {
        printf("can not open pin device\n");
        return DEV_ERROR;
    }

    Spo2Timer = xTimerCreate("Spo2Timer",
                                 pdMS_TO_TICKS(1000),
                                 pdTRUE,
                                 (void *)0,
                                 vTalSpo2TimerCb);

    xTimerStart(Spo2Timer, 0);

    return DEV_EOK;
}

void vTalSp02SetOnOff(Uint8_t OnOff)
{
    lHalDeviceControl(spo2Dev, MAX30102_SET_ONOFFCMD, (void *)&OnOff);
}

Uint8_t ucTalSpo2GetOnOff(void)
{
    return onOffFlag;
}

Uint32_t lTalSpo2GetHeartRate(void)
{
   return heartRate; 
}

Uint32_t lTalSpo2GetBloodOxygen(void)
{
   return Spo2Data; 
}

Uint8_t ucTalSpo2GetDataFlag(void)
{
   return dataOkFlag;
}










