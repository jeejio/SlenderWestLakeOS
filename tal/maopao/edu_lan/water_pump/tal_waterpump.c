#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "device.h"
#include "hal_sensor.h" //sesor IO模型驱动框架头文件
#include "esp_log.h"
#include "tal_waterpump.h"


#define WATER_PUMP_FAST_DUTY (100)
#define WATER_PUMP_MEDIAN_DUTY (70)
#define WATER_PUMP_LOW_DUTY (30)
#define WATER_PUMP_STOP_DUTY (0)

#define WATER_PUMP_DEV "lb1427b"
Device_t water_pump_dev = NULL;

uint8_t onoffFlag = 0;
static waterPumpSpeed_t speedLevel = WATER_PUMP_SPEED_STOP;

Int32_t lTalWaterPumpInit(void)
{

    // 查找设备
    water_pump_dev = pHalDeviceFind(WATER_PUMP_DEV);
    if (water_pump_dev == NULL)
    {
        printf("can not find lb1427b Model\n");
        return DEV_ERROR;
    }

    // 打开设备
    Err_t result = DEV_EOK;
    result = lHalDeviceOpen(water_pump_dev, DEVICE_OFLAG_RDWR);
    if (result != DEV_EOK)
    {
        printf("can not open pin device\n");
        return DEV_ERROR;
    }
    return DEV_EOK;
}

void vTalSetWaterPumpOnoff(Uint8_t OnOff)
{
    uint32_t duty = 0;
    onoffFlag = OnOff;
    if (OnOff == 1)
    {

        printf("water_pump_set_speed %d\n", speedLevel);
        if (speedLevel == WATER_PUMP_SPEED_LOW)
        {
            duty = WATER_PUMP_LOW_DUTY;
        }
        else if (speedLevel == WATER_PUMP_SPEED_MEDIAN)
        {
            duty = WATER_PUMP_MEDIAN_DUTY;
        }
        else if (speedLevel == WATER_PUMP_SPEED_FAST)
        {
            duty = WATER_PUMP_FAST_DUTY;
        }
        else if (speedLevel == WATER_PUMP_SPEED_STOP)
        {
            duty = WATER_PUMP_STOP_DUTY;
        }

        printf("setOnoff vel %d\n", OnOff);

        lHalDeviceControl(water_pump_dev, WATER_PUMP_SET_CMD, (void *)&duty);
    }
    else
    {
        speedLevel = WATER_PUMP_SPEED_STOP;
        duty = WATER_PUMP_STOP_DUTY;
        lHalDeviceControl(water_pump_dev, WATER_PUMP_SET_CMD, (void *)&duty);
    }
}

Uint8_t ucTalGetWaterPumpOnoff(void)
{
    Uint8_t read_data;
    ulHalDeviceRead(water_pump_dev, NULL, &read_data, 1);
    if (read_data == 0)
        return 0;
    else
        return 1;
}

void vTalSetWaterPumpMode(waterPumpSpeed_t speed)
{
    speedLevel = speed;
    uint32_t duty = 0;
    if (onoffFlag == 1)
    {

        printf("water_pump_set_speed %d\n", speed);
        if (speed == WATER_PUMP_SPEED_LOW)
        {
            duty = WATER_PUMP_LOW_DUTY;
        }
        else if (speed == WATER_PUMP_SPEED_MEDIAN)
        {
            duty = WATER_PUMP_MEDIAN_DUTY;
        }
        else if (speed == WATER_PUMP_SPEED_FAST)
        {
            duty = WATER_PUMP_FAST_DUTY;
        }
        else if (speed == WATER_PUMP_SPEED_STOP)
        {
            duty = WATER_PUMP_STOP_DUTY;
        }

        // printf("duty vel %d\n",duty);

        lHalDeviceControl(water_pump_dev, WATER_PUMP_SET_CMD, (void *)&duty);
    }
}

waterPumpSpeed_t ucTalGetWaterPumpMode(void)
{
    return speedLevel;
}
