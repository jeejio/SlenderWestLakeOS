#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "device.h"
#include "hal_sensor.h" //sesor IO模型驱动框架头文件
#include "esp_log.h"
#include "tal_getsture.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/semphr.h"
#include "freertos/timers.h"

#define GETSTURE_DEV "paj7620u2"
Device_t getstureDev = NULL;
Uint8_t getstureData = 0;
Uint8_t getstureUploadData = 0;
static TimerHandle_t GetstureTimer = NULL;

void vTalGetstureTimerCb(TimerHandle_t timer)
{
     Uint8_t data = 0;
     ulHalDeviceRead(getstureDev, NULL, &data, 1);
    if (data != 0)
    {
        getstureData = data;
        getstureUploadData = data;
    }
}

Int32_t lTalGetstureInit(void)
{
    // 查找设备
    getstureDev = pHalDeviceFind(GETSTURE_DEV);
    if (getstureDev == NULL)
    {
        printf("can not find %s Model\n", GETSTURE_DEV);
        return DEV_ERROR;
    }

    // 打开设备
    Err_t result = DEV_EOK;
    result = lHalDeviceOpen(getstureDev, DEVICE_OFLAG_RDWR);
    if (result != DEV_EOK)
    {
        printf("can not open %s device\n", GETSTURE_DEV);
        return DEV_ERROR;
    }

    GetstureTimer = xTimerCreate("GetstureTimer",
                                 pdMS_TO_TICKS(500),
                                 pdTRUE,
                                 (void *)0,
                                 vTalGetstureTimerCb);

    xTimerStart(GetstureTimer, 0);

    return DEV_EOK;
   
}

GestureList ucTalGetGetstureStatus(void)
{

    Uint8_t data = 0;
    data = getstureUploadData;
    getstureUploadData = 0;
    return data;
}

GestureList xTalGetstureGetMovingDirection(void)
{
    if (getstureData >= UpGesture && getstureData <= BackwardGesture)
    {
        Uint8_t data = getstureData;
        getstureData =0;
        return data;
    }
    else
    {
        return NoGesture;
    }
}

GestureList xTalGetstureGetRotationDirection(void)
{
    if (getstureData >= ClockwiseGesture && getstureData <= AntiClockwiseGesture)
    {
       Uint8_t data = getstureData;
        getstureData =0;
        return data;
    }
    else
    {
        return NoGesture;
    }

}

GestureList xTalGetstureGetSwingStatus(void)
{
    if (getstureData == WaveGesture )
    {
        Uint8_t data = getstureData;
        getstureData =0;
        return data;
    }
    else
    {
        return NoGesture;
    }

}