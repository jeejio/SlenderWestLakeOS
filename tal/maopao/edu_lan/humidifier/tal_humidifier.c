/*
 * Copyright (c) 2022, jeejio Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author         Notes
 * 2023-03-29     amk.lh         the first version
 */

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "device.h"
#include "hal_sensor.h" //sesor IO模型驱动框架头文件
#include "esp_log.h"
#include "tal_humidifier.h"
#include "string.h"
#include "auto_init.h"

#define DEV_HUMIDIFIER "HUMIDIFIER806"

Device_t pSensorHumidifierDev = NULL;
static uint32_t uGetInfo[6]={0}; 

int *pTalHumidifierGetInfo(void)
{
    memset(uGetInfo, 0, sizeof(uGetInfo));
    ulHalDeviceRead(pSensorHumidifierDev, NULL, uGetInfo, sizeof(uGetInfo));
    return uGetInfo;
}

void vTalHumidifierOnReceive(uint32_t *dat)
{
   pTalHumidifierGetInfo();
   /*
   printf("pTalHumidifierGetInfo after, sizeof(uGetInfo)=[%d] uGetInfo=[%d|%d|%d|%d|%d|%d]\n",
            sizeof(uGetInfo),uGetInfo[0],uGetInfo[1],uGetInfo[2],
            uGetInfo[3], uGetInfo[4], uGetInfo[5]);
    */
            
   for(int i=0; i < 6; i++)                                      
   {
    dat[i] = uGetInfo[i];
   }
}

int lTalDevSetOnOff(uint32_t onoff)
{
    int lRet = 0;
    lRet = lHalDeviceControl(pSensorHumidifierDev, HUMIDIFIER_DEV_ONOFF, &onoff);
    return lRet;
}


int lTalDevGetOnOff(void)
{
    pTalHumidifierGetInfo();
    return uGetInfo[0];
}

int lTalSetLightOnoff(uint32_t onoff)
{
    int lRet = 0;
    lRet = lHalDeviceControl(pSensorHumidifierDev, HUMIDIFIER_LIGHT_ONOFF, &onoff);
    return lRet;
}
 
int lTalGetLightOnoff(void)
{
    pTalHumidifierGetInfo();
    return uGetInfo[1];
}

int lTalSetLightMode(uint32_t mode)
{
    int lRet = 0;
    lRet = lHalDeviceControl(pSensorHumidifierDev, HUMIDIFIER_LIGHT_MODEL, &mode);
    return lRet;    
}

int lTalGetLightMode(void)
{
    pTalHumidifierGetInfo();
    return uGetInfo[2];    
}

int lTalSetLightBrightness(uint32_t brightness)
{
    int lRet = 0;
    lRet = lHalDeviceControl(pSensorHumidifierDev, HUMIDIFIER_LIGHT_BRIGHTNESS, &brightness);
    return lRet;     
}

int lTalGetLightBrightness(void)
{
    pTalHumidifierGetInfo();
    return uGetInfo[3];       
}

int lTalSetSprayOnoff(uint32_t onoff)
{
    int lRet = 0;
    lRet = lHalDeviceControl(pSensorHumidifierDev, HUMIDIFIER_SPRAY_ONOFF, &onoff);
    return lRet;
}

int  lTalGetSprayOnoff(void)
{
    pTalHumidifierGetInfo();

    return (uGetInfo[4] == 0) ? 0 : 1;
}

int lTalSetSprayMode(uint32_t mode)
{
    int lRet = 0;
    lRet = lHalDeviceControl(pSensorHumidifierDev, HUMIDIFIER_SPRAY_MODEL, &mode);
    return lRet;
}

int lTalGetSprayMode(void)
{
    pTalHumidifierGetInfo();
    return (uGetInfo[5] > 0) ? uGetInfo[5] : -1;
}

int lTalHumidifierInit(void)
{
    // 查找设备
    pSensorHumidifierDev = pHalDeviceFind(DEV_HUMIDIFIER);
    if (pSensorHumidifierDev == NULL )
    {
        printf("can not find sensor Model\n");
        return -1;
    }
    else
        printf("find sensor Model ok\n");

    // 打开设备
    Err_t result = DEV_EOK;
    result = lHalDeviceOpen(pSensorHumidifierDev, DEVICE_FLAG_RDONLY);
    if (result != DEV_EOK)
    {
        printf("can not open senor device\n");
        return -1;
    }
    else
        printf("open senor device ok\n");

    return 0;
}

#if 0
static void humidifier_tal_test(void *arg)
{
    printf("----start  humidifier_tal_test----\n");
    while(1)
    {
       int lRet = -1;
       printf("---test lTalSetLightOnoff to  on----\n");
       lTalSetLightOnoff(1);
       vTaskDelay(pdMS_TO_TICKS(5000));
       printf("---test lTalGetLightOnoff: ret=[%d]----\n", lTalGetLightOnoff());
       vTaskDelay(pdMS_TO_TICKS(5000));
       printf("---test lTalSetLightOnoff to  off----\n");
       lTalSetLightOnoff(0);
       vTaskDelay(pdMS_TO_TICKS(5000));
       printf("---test lTalGetLightOnoff: ret=[%d]----\n", lTalGetLightOnoff());

      vTaskDelay(pdMS_TO_TICKS(50));
    }

}

void vTestHumidifierTal(void)
{
    printf("-----begin vTestHumidifierTal -----\n");
    int lRet = lTalHumidifierInit();
    if(lRet == 0)
    { 
      printf("-----begin humidifier_tal_test -----\n");
      xTaskCreate(humidifier_tal_test, "humidifier_tal_test", 2048, NULL, 5, NULL);
    }
    else
    {
        printf("-----execute vTestHumidifierTal return faild!-----\n");
    }
    
}

INIT_APP_EXPORT(vTestHumidifierTal);
#endif