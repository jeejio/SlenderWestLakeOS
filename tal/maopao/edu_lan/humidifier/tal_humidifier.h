#ifndef __TAL_HUMIDIFIER_H__
#define __TAL_HUMIDIFIER_H__

#ifndef HUMIDIFIER_SPRAY_ONOFF
#define HUMIDIFIER_SPRAY_ONOFF  (SENSOR_CTRL_USER_CMD_START + 1)
#endif

#ifndef HUMIDIFIER_SPRAY_MODEL
#define HUMIDIFIER_SPRAY_MODEL  (SENSOR_CTRL_USER_CMD_START + 2)
#endif

#ifndef HUMIDIFIER_LIGHT_ONOFF
#define HUMIDIFIER_LIGHT_ONOFF  (SENSOR_CTRL_USER_CMD_START + 3)
#endif

#ifndef HUMIDIFIER_LIGHT_MODEL
#define HUMIDIFIER_LIGHT_MODEL  (SENSOR_CTRL_USER_CMD_START + 4)
#endif

#ifndef HUMIDIFIER_LIGHT_BRIGHTNESS
#define HUMIDIFIER_LIGHT_BRIGHTNESS  (SENSOR_CTRL_USER_CMD_START + 5)
#endif

#ifndef HUMIDIFIER_DEV_ONOFF
#define HUMIDIFIER_DEV_ONOFF  (SENSOR_CTRL_USER_CMD_START + 7)
#endif
/**
 * @brief   加湿器设备上电初始化
 * 
 * @return 
 */
int lTalHumidifierInit(void);


/**
 * @brief   加湿器设备状态上报
 * @param   
 * @return 
 */
void vTalHumidifierOnReceive(uint32_t *dat);

/**
 * @brief   加湿器设备开关控制
 * @param   onoff: 0：关， 1：开
 * @return      0：成功,  < 0:失败
 */
int lTalDevSetOnOff(uint32_t onoff);

/**
 * @brief   得到加湿器设备上电初始化状态
 * 
 * @return      0：关机，1：开机, < 0:失败
 */
int lTalDevGetOnOff(void);

/**
 * @brief   设置加湿器设备LED呼吸灯开关控制
 * @param   onoff: 0：关， 1：开 
 * @return   0：成功,  < 0:失败
 */
int lTalSetLightOnoff(uint32_t onoff); 

/**
 * @brief   得到加湿器设备LED呼吸灯开关状态
 * 
 * @return      0：关，1：开, < 0:失败
 */
int lTalGetLightOnoff(void);

/**
 * @brief   设置加湿器设备LED呼吸灯色彩模式
 * @param   mode: 0:彩色，  1：白色
 * @return   0：成功,  < 0:失败
 */
int lTalSetLightMode(uint32_t mode); 

/**
 * @brief   得到加湿器设备LED呼吸灯开关状态
 * 
 * @return     0:彩色，  1：白色, < 0:失败
 */
int lTalGetLightMode(void);

 /**
   * @brief       设置加湿器LED呼吸灯亮度值
   * 
   *  NOTE:   brightness: 百分值 0-100, < 0:失败
   * 
  **/
int lTalSetLightBrightness(uint32_t brightness);

 /**
   * @brief      获取加湿器LED呼吸灯亮度值
   * 
   *  NOTE:   返回： 百分值 0-100
   * 
  **/
int lTalGetLightBrightness(void);

/**
 * @brief   设置加湿器设备雾化片开关控制
 * 
 * @return      onoff: 0：关，1：开, < 0:失败
 */
int lTalSetSprayOnoff(uint32_t onoff);

/**
 * @brief   得到加湿器设备雾化片开关控制
 * 
 * @return     0：关，1：开, < 0:失败
 */
int  lTalGetSprayOnoff(void);

/**
 * @brief   设置加湿器设备雾化片工作模式
 * @param   mode: 1：连续喷雾，2：间隔喷雾
 * @return  0：成功,  < 0:失败
 */
int lTalSetSprayMode(uint32_t mode);

/**
 * @brief   获取当前加湿器设备雾化片工作模式
 * 
 * @return      1：连续喷雾，2：间隔喷雾 ， < 0:失败
 */
int lTalGetSprayMode(void);

#endif
