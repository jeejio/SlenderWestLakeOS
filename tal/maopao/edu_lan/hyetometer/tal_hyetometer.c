/*
 * Copyright (c) 2022, jeejio Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author         Notes
 * 2023-02-10     xuyuhu         the first version
 */

#include <stdio.h>
#include "esp_log.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "tal_hyetometer.h"
#include "freertos/device.h"
#include "hal_sensor.h"

Device_t hyet_sensor_dev = NULL;
static HyetomterInfo_t GetInfo;

HyetomterInfo_t * xTalHyetometerOnReceive(void)
{
    ulHalDeviceRead(hyet_sensor_dev,NULL,&GetInfo,sizeof(GetInfo));
    return &GetInfo;
}

int cTalHyetometerGetIsRaining(void)
{
    xTalHyetometerOnReceive();
    return GetInfo.isRaining;
}

int cTalHyetometerGetPecipitationLevel(void)
{
    xTalHyetometerOnReceive();
    return GetInfo.precipitationLevel;
}

int vTalHyetometerInit(void)
{
    // 查找设备
    hyet_sensor_dev = pHalDeviceFind("MK002");
    if (hyet_sensor_dev == NULL)
    {
        printf("can not find sensor Model\n");
        return -1;
    }
    else
        printf("find sensor Model ok\n");

    // 打开设备
    Err_t result = DEV_EOK;
    result = lHalDeviceOpen(hyet_sensor_dev, DEVICE_FLAG_RDONLY);
    if (result != DEV_EOK)
    {
        printf("can not open senor device\n");
        return -1;
    }
    else
        printf("open senor device ok\n");
    return 0;
}
