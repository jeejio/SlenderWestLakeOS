#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "device.h"
#include "hal_sensor.h" //sesor IO模型驱动框架头文件
#include "esp_log.h"
#include "tal_motor.h"
#include "string.h"
#define MOTOR_DEV "DC3V"
Device_t sensor_motor_dev = NULL;
static int GetInfo[3];
int MotorSetInfo;
int *lTalMotorGetInfo(void)
{
    memset(GetInfo, 0, sizeof(GetInfo));
    ulHalDeviceRead(sensor_motor_dev, NULL, GetInfo, sizeof(GetInfo));
    return GetInfo;
}

int lTalMotorGetOnoff(void)
{
    lTalMotorGetInfo();
    return GetInfo[2];
}

int lTalMotorGetRotationDirection(void)
{
    lTalMotorGetInfo();
    return GetInfo[1];
}

int lTalMotorGetSpinVelocityLevel(void)
{
    lTalMotorGetInfo();
    return GetInfo[0];
}

void vTalMotorSetSpinVelocityLevel(MotorSpeed_t speed)
{
    MotorSetInfo = speed;
    sensor_motor_dev = pHalDeviceFind(MOTOR_DEV);
    if (sensor_motor_dev == NULL)
    {
        LOGE("MOTOR", "can not find sensor Model\n");
    }
    else
    {
        LOGI("MOTOR", "find sensor Model ok\n");
        lHalDeviceControl(sensor_motor_dev, SENSOR_CTRL_USER_CMD_START + 1, &MotorSetInfo);
    }
}

void vTalMotorSetRotationDirection(MotorDirection_t direction)
{
    MotorSetInfo = direction;
    sensor_motor_dev = pHalDeviceFind(MOTOR_DEV);
    if (sensor_motor_dev == NULL)
    {
        LOGE("MOTOR", "can not find sensor Model\n");
    }
    else
    {
        LOGI("MOTOR", "find sensor Model ok\n");
        lHalDeviceControl(sensor_motor_dev, SENSOR_CTRL_USER_CMD_START + 2, &MotorSetInfo);
    }
}
void vTalMotorSetOnoff(uint8_t onoff)
{
    MotorSetInfo = onoff;
    sensor_motor_dev = pHalDeviceFind(MOTOR_DEV);
    if (sensor_motor_dev == NULL)
    {
        LOGE("MOTOR", "can not find sensor Model\n");
    }
    else
    {
        LOGI("MOTOR", "find sensor Model ok\n");
        lHalDeviceControl(sensor_motor_dev, SENSOR_CTRL_USER_CMD_START + 3, &MotorSetInfo);
    }
}

int lTalMotorInit(void)
{
    // 查找设备
    sensor_motor_dev = pHalDeviceFind(MOTOR_DEV);
    if (sensor_motor_dev == NULL)
    {
        LOGE("MOTOR", "can not find sensor Model\n");
        return -1;
    }
    else
        LOGI("MOTOR", "find sensor Model ok\n");

    // 打开设备
    Err_t result = DEV_EOK;

    result = lHalDeviceOpen(sensor_motor_dev, DEVICE_FLAG_RDWR);
    if (result != DEV_EOK)
    {
        printf("can not open senor device\n");
        return -1;
    }
    else
        printf("open senor device ok\n");
    return 0;
}
