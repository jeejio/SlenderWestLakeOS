//Slender West Lake automatically generated code, do not edit

#ifndef TAL_LED_LOCAL_TEST_SERVICE_H
#define TAL_LED_LOCAL_TEST_SERVICE_H

#include "maopaoServer.h"
#include "maopaoError.h"
#include "mson.h"


void __maopaoServer__ledSetColor__service__(maopao_requestInfo_st *_requestInfo, SemaphoreHandle_t _mux);

void __maopaoServer__ledGetColor__service__(maopao_requestInfo_st *_requestInfo, SemaphoreHandle_t _mux);

void __maopaoServer__ledSetOnoff__service__(maopao_requestInfo_st *_requestInfo, SemaphoreHandle_t _mux);

void __maopaoServer__ledGetOnoff__service__(maopao_requestInfo_st *_requestInfo, SemaphoreHandle_t _mux);

#endif  //TAL_LED_LOCAL_TEST_SERVICE_H
