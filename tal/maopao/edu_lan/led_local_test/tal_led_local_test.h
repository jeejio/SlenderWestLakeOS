//Slender West Lake automatically generated code

#ifndef TAL_LED_LOCAL_TEST_H
#define TAL_LED_LOCAL_TEST_H

#include <stdbool.h>
#include "maopaoServer.h"
#include "maopaoError.h"

extern maopao_service_handler_st tal_led_local_test_serviceGroup[];

struct rgb_t {
    int red;
    int green;
    int blue;
};

/**
 * @brief ledSetColor
 * @param[in] color
 * @return errorCode
 */
int ledSetColor(struct rgb_t color);

/**
 * @brief ledGetColor
 * @param[out] color
 * @return errorCode
 */
int ledGetColor(struct rgb_t *color);

/**
 * @brief ledSetOnoff
 * @param[in] status
 * @return errorCode
 */
int ledSetOnoff(bool status);

/**
 * @brief ledGetOnoff
 * @param[out] status
 * @return errorCode
 */
int ledGetOnoff(bool *status);

#endif  //TAL_LED_LOCAL_TEST_H
