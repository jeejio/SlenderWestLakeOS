//Slender West Lake automatically generated code, do not edit


#include <stdio.h>
#include <stdlib.h>
#include <memory.h>
#include "tal_led_local_test_service.h"
#include "tal_led_local_test.h"

#define STRING_SIZE 1024

TalInfo_st _talInfo = {
        .manufacturer = "e-star",
        .category = "led test",
        .version = "1.0.0",
        .developer = "yuanz@e-star.com",
};

//Define a MSON structure for input parameter processing
MSON_DEFOBJ(maopao_ledSetColor_inParam_t,
    struct {
    int red;
    int green;
    int blue;
    } color;
);

//Define a MSON structure for output parameter processing
MSON_DEFOBJ(maopao_ledGetColor_outParam_t,
    struct {
    int red;
    int green;
    int blue;
    } color;
);

//Define a MSON structure for input parameter processing
MSON_DEFOBJ(maopao_ledSetOnoff_inParam_t,
    bool status;
);

//Define a MSON structure for output parameter processing
MSON_DEFOBJ(maopao_ledGetOnoff_outParam_t,
    bool status;
);

maopao_service_handler_st tal_led_local_test_serviceGroup[] = {
        {.method = "setColor", .handlerCb = MAOPAO_SERVICE(ledSetColor), .inParam = MSON_MEMBERS_TO_STRING(maopao_ledSetColor_inParam_t), .outParam = NULL},
        {.method = "getColor", .handlerCb = MAOPAO_SERVICE(ledGetColor), .inParam = NULL, .outParam = MSON_MEMBERS_TO_STRING(maopao_ledGetColor_outParam_t)},
        {.method = "setOnoff", .handlerCb = MAOPAO_SERVICE(ledSetOnoff), .inParam = MSON_MEMBERS_TO_STRING(maopao_ledSetOnoff_inParam_t), .outParam = NULL},
        {.method = "getOnoff", .handlerCb = MAOPAO_SERVICE(ledGetOnoff), .inParam = NULL, .outParam = MSON_MEMBERS_TO_STRING(maopao_ledGetOnoff_outParam_t)},
        {.method = NULL, .handlerCb = NULL}
};

void __maopaoServer__ledSetColor__service__(maopao_requestInfo_st *_requestInfo, SemaphoreHandle_t _mux)
{
    //prepare input parameter
    mson_jsonToObj(_requestInfo->params, MSON_MEMBERS_TO_STRING(maopao_ledSetColor_inParam_t), _inObj);
    if (_inObj == NULL){
        maopaoServerSendError(_requestInfo, MAOPAO_ERROR_RPC_ANALYSE_FAILED, MAOPAO_ERROR_RPC_ANALYSE_FAILED_MSG);
        goto _exit;
    }
    maopao_ledSetColor_inParam_t *_inParam = (maopao_ledSetColor_inParam_t *) _inObj;
    struct rgb_t color;
    memcpy(&color, &_inParam->color, sizeof(struct rgb_t ));

    int _errorCode;

    //call user function
    _errorCode = ledSetColor(color);

    if (_errorCode != MAOPAO_SUCCESS){
        maopaoServerSendError(_requestInfo, MAOPAO_ERROR_TAL_OPERATION_FAILED, MAOPAO_ERROR_TAL_OPERATION_FAILED_MSG);
        goto _release_obj;
    }

    //send response to client
    maopaoServerSendResponse(_requestInfo, "null");

    //free the space
_release_obj:
    mson_releaseWithObj(_inObj);

_exit:
    //release the mutex
    xSemaphoreGive(_mux);
}

void __maopaoServer__ledGetColor__service__(maopao_requestInfo_st *_requestInfo, SemaphoreHandle_t _mux)
{
    //prepare output parameter
    maopao_ledGetColor_outParam_t _outParam;
    struct rgb_t color;

    int _errorCode;

    //call user function
    _errorCode = ledGetColor(&color);

    if (_errorCode != MAOPAO_SUCCESS){
        maopaoServerSendError(_requestInfo, MAOPAO_ERROR_TAL_OPERATION_FAILED, MAOPAO_ERROR_TAL_OPERATION_FAILED_MSG);
        goto _release_obj;
    }

    //copy the output parameters to the structure
    memcpy(&_outParam.color, &color, sizeof(struct rgb_t ));
    char *_resultJson = mson_objToJson(&_outParam, MSON_MEMBERS_TO_STRING(maopao_ledGetColor_outParam_t));

    //send response to client
    maopaoServerSendResponse(_requestInfo, _resultJson);

    //free the space
    free(_resultJson);
_release_obj:

    //release the mutex
    xSemaphoreGive(_mux);
}

void __maopaoServer__ledSetOnoff__service__(maopao_requestInfo_st *_requestInfo, SemaphoreHandle_t _mux)
{
    //prepare input parameter
    mson_jsonToObj(_requestInfo->params, MSON_MEMBERS_TO_STRING(maopao_ledSetOnoff_inParam_t), _inObj);
    if (_inObj == NULL){
        maopaoServerSendError(_requestInfo, MAOPAO_ERROR_RPC_ANALYSE_FAILED, MAOPAO_ERROR_RPC_ANALYSE_FAILED_MSG);
        goto _exit;
    }
    maopao_ledSetOnoff_inParam_t *_inParam = (maopao_ledSetOnoff_inParam_t *) _inObj;
    bool status = _inParam->status;

    int _errorCode;

    //call user function
    _errorCode = ledSetOnoff(status);

    if (_errorCode != MAOPAO_SUCCESS){
        maopaoServerSendError(_requestInfo, MAOPAO_ERROR_TAL_OPERATION_FAILED, MAOPAO_ERROR_TAL_OPERATION_FAILED_MSG);
        goto _release_obj;
    }

    //send response to client
    maopaoServerSendResponse(_requestInfo, "null");

    //free the space
_release_obj:
    mson_releaseWithObj(_inObj);

_exit:
    //release the mutex
    xSemaphoreGive(_mux);
}

void __maopaoServer__ledGetOnoff__service__(maopao_requestInfo_st *_requestInfo, SemaphoreHandle_t _mux)
{
    //prepare output parameter
    maopao_ledGetOnoff_outParam_t _outParam;
    bool status;

    int _errorCode;

    //call user function
    _errorCode = ledGetOnoff(&status);

    if (_errorCode != MAOPAO_SUCCESS){
        maopaoServerSendError(_requestInfo, MAOPAO_ERROR_TAL_OPERATION_FAILED, MAOPAO_ERROR_TAL_OPERATION_FAILED_MSG);
        goto _release_obj;
    }

    //copy the output parameters to the structure
    _outParam.status = status;
    char *_resultJson = mson_objToJson(&_outParam, MSON_MEMBERS_TO_STRING(maopao_ledGetOnoff_outParam_t));

    //send response to client
    maopaoServerSendResponse(_requestInfo, _resultJson);

    //free the space
    free(_resultJson);
_release_obj:

    //release the mutex
    xSemaphoreGive(_mux);
}

