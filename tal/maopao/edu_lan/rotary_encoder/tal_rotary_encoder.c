/*
 * Copyright (c) 2022, jeejio Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author         Notes
 * 2023-02-03     xuyuhu         the first version
 */

#include <stdio.h>
#include "esp_log.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "tal_rotary_encoder.h"
#include "freertos/device.h"
#include "hal_sensor.h"

Device_t re_sensor_dev = NULL;

RotaryEncoderStatus_t GetStatus;

RotaryEncoderStatus_t * xTalRotaryEncoderOnReceive(void)
{
    uint32_t databuf[3] = {0};
    ulHalDeviceRead(re_sensor_dev, NULL, (void *)databuf, sizeof(databuf));
    GetStatus.RelativePos = databuf[0];
    GetStatus.IsPressed   = databuf[1];
    GetStatus.RotationDirection =databuf[2];
    return &GetStatus;
}

int lTalRotaryEncoderGetPosition(void)
{
    xTalRotaryEncoderOnReceive();
    return GetStatus.RelativePos;
}

bool cTalRotaryEncoderGetIsPressed(void)
{
    xTalRotaryEncoderOnReceive();
    return GetStatus.IsPressed;
}

bool cTalRotaryEncoderGetRotationDirection(void)
{
    xTalRotaryEncoderOnReceive();
    return GetStatus.RotationDirection;
}


int vTalRotaryEncoderInit(void)
{
    // 查找设备
    re_sensor_dev = pHalDeviceFind("EC11");
    if (re_sensor_dev == NULL)
    {
        printf("can not find sensor Model\n");
        return -1;
    }
    else
        printf("find sensor Model ok\n");

    // 打开设备
    Err_t result = DEV_EOK;
    result = lHalDeviceOpen(re_sensor_dev, DEVICE_FLAG_RDONLY);
    if (result != DEV_EOK)
    {
        printf("can not open senor device\n");
        return -1;
    }
    else
        printf("open senor device ok\n");
    return 0;
}
