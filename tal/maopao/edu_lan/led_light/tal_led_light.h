/*
 * tal_led_light.h
 *
 *  Created on: Mar 24, 2023
 *      Author: zdh
 */

#ifndef _TAL_LED_LIGHT_H_
#define _TAL_LED_LIGHT_H_

#include "swldef.h"
#include "hal_sensor.h"


enum
{
    LEDLIGHT_MODE_COOL=1,
    LEDLIGHT_MODE_WARM,
    LEDLIGHT_MODE_COOLWARM,

};

enum
{
    SENSOR_LEDLIGHT_SET_BRIGHT=SENSOR_CTRL_USER_CMD_START + 1,
    SENSOR_LEDLIGHT_SET_MODE,
    SENSOR_LEDLIGHT_SET_CALLBACK,
    SENSOR_LEDLIGHT_SET_ONOFF,
};


Int32_t lTalLedLightInit(void);

void vTalLedLightSetOnOff(Uint8_t onOff);

void vTalLedLightSetIlluminationMode(Uint8_t model);

void vTalLedLightSetRelativeBrightness(Uint8_t bright);


Uint8_t ucTalLedLightGetOnOff(void);

Uint8_t ucTalLedLightGetIlluminationMode(void);

Uint8_t ucTalLedLightGetRelativeBrightness(void);




/*****
 *����˵���� LED�������¼��ϱ�
 * ��������� ���棬����3��buff[0]�ǿ���״̬��buff[1]�ǵƵĹ���ģʽ��buff[2]�ǵƵ�����
 * ���ز�������
 */
void vTalLedLightOnrecive(void(*cb)(uint8_t*buff,uint8_t buff_size));

#endif /* TAL_LED_LIGHT_TAL_LED_LIGHT_H_ */
