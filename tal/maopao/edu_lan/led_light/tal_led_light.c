/*
 * tal_led_light.c
 *
 *  Created on: Mar 24, 2023
 *      Author: zdh
 */

#include "tal_led_light.h"
#include "device.h"

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"

#ifndef LOG_I
#define LOG_I(format, ...)                                            \
    {                                                                 \
        printf("*******************INFO %s,%d:", __FILE__, __LINE__); \
        printf(format, ##__VA_ARGS__);                                \
        printf("\n");                                                 \
    }
#endif

#define LEDLIGHT_DEV "DC5V"

Device_t pSensorLedlightDev = NULL;

void vTalLedLightOnrecive(void (*cb)(uint8_t *buff, uint8_t buff_size))
{
    if (pSensorLedlightDev)
    {
        lHalDeviceControl(pSensorLedlightDev, SENSOR_LEDLIGHT_SET_CALLBACK, (void *)cb);
        LOG_I("set ledlight event callback");
    }
}

Int32_t lTalLedLightInit(void)
{
    LOG_I("******************************led light tal init\r\n");

    pSensorLedlightDev = pHalDeviceFind(LEDLIGHT_DEV);
    if (pSensorLedlightDev == NULL)
    {
        printf("LEDLIGHT can not find sensor Model\n");
        return -1;
    }
    else
        printf("LEDLIGHT find sensor Model ok\n");

    // 打开设备
    Err_t result = DEV_EOK;

    result = lHalDeviceOpen(pSensorLedlightDev, DEVICE_FLAG_RDWR);
    if (result != DEV_EOK)
    {
        printf("can not open senor device %s\n", LEDLIGHT_DEV);
        return -1;
    }
    else
        printf("open senor device %s ok\n", LEDLIGHT_DEV);

    return 0;
}

void vTalLedLightSetOnOff(Uint8_t onOff)
{
    if (pSensorLedlightDev)
    {
        lHalDeviceControl(pSensorLedlightDev, SENSOR_LEDLIGHT_SET_ONOFF, (void *)onOff);
        LOG_I("set ledlight onoff:%d", onOff);
    }
}

void vTalLedLightSetRelativeBrightness(Uint8_t bright)
{
    LOG_I("set ledlight brightness:%d", bright);
    if (pSensorLedlightDev)
    {
        lHalDeviceControl(pSensorLedlightDev, SENSOR_LEDLIGHT_SET_BRIGHT, (void *)bright);
    }
}

void vTalLedLightSetIlluminationMode(Uint8_t mode)
{
    if (pSensorLedlightDev)
    {
        lHalDeviceControl(pSensorLedlightDev, SENSOR_LEDLIGHT_SET_MODE, (void *)mode);
    }
    LOG_I("set ledlight mode:%d", mode);
}

Uint8_t ucTalLedLightGetOnOff(void)
{
    uint8_t info[3];
    if (pSensorLedlightDev)
    {
        ulHalDeviceRead(pSensorLedlightDev, NULL, &info, sizeof(info));
    }
    LOG_I("get ledlight onoff:%d", info[0]);
    return info[0];
}

Uint8_t ucTalLedLightGetRelativeBrightness(void)
{
    uint8_t info[3];
    if (pSensorLedlightDev)
    {
        ulHalDeviceRead(pSensorLedlightDev, NULL, &info, sizeof(info));
    }
    LOG_I("get ledlight brightness:%d", info[2]);
    return info[2];
}

Uint8_t ucTalLedLightGetIlluminationMode(void)
{
    uint8_t info[3];
    if (pSensorLedlightDev)
    {
        ulHalDeviceRead(pSensorLedlightDev, NULL, &info, sizeof(info));
    }
    LOG_I("get ledlight mode:%d", info[1]);
    return info[1];
}
