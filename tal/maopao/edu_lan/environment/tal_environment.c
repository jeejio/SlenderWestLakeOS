#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "device.h"
#include "hal_sensor.h" //sesor IO模型驱动框架头文件
#include "esp_log.h"
#include "tal_environment.h"


#define ambient_light_dev "bh1750fvi"
Device_t ambientLightDev = NULL;

#define tem_hum_dev "DHT20"
Device_t temHumDev = NULL;

#define ioexpand_dev "pca9536"
Device_t ioexpandDev = NULL;

#define mq2_dev "mq2"
Device_t Mq2Dev = NULL;

#define mq7_dev "mq7"
Device_t Mq7Dev = NULL;

#define mq135_dev "mq135"
Device_t Mq135Dev = NULL;

Int32_t lTalAmbientLightInit(void)
{
    // 查找设备
    ambientLightDev = pHalDeviceFind(ambient_light_dev);
    if (ambientLightDev == NULL)
    {
        printf("can not find %s Model\n",ambient_light_dev);
        return DEV_ERROR;
    }

    // 打开设备
    Err_t result = DEV_EOK;
    result = lHalDeviceOpen(ambientLightDev, DEVICE_OFLAG_RDWR);
    if (result != DEV_EOK)
    {
        printf("can not open %s device\n",ambient_light_dev);
        return DEV_ERROR;
    }
    return DEV_EOK;
}

Uint16_t usTalGetAmbientLightData(void)
{
    Uint16_t data = 0;
    ulHalDeviceRead(ambientLightDev, NULL, &data, 2);
    return data;
}

Int32_t lTalTempHumInit(void)
{
    // 查找设备
    temHumDev = pHalDeviceFind(tem_hum_dev);
    if (temHumDev == NULL)
    {
        printf("can not find %s Model\n",tem_hum_dev);
        return DEV_ERROR;
    }

    // 打开设备
    Err_t result = DEV_EOK;
    result = lHalDeviceOpen(temHumDev, DEVICE_OFLAG_RDWR);
    if (result != DEV_EOK)
    {
        printf("can not open %s device\n",tem_hum_dev);
        return DEV_ERROR;
    }
    return DEV_EOK;
}

void vTalGetTemHumData(Int32_t *data)
{
    ulHalDeviceRead(temHumDev, NULL, data, 2);
}

Int32_t lTalMq2Init(void)
{
    Mq2Dev = pHalDeviceFind(mq2_dev);
    if (Mq2Dev == NULL)
    {
        printf("can not find %s Model\n",mq2_dev);
        return DEV_ERROR;
    }

    // 打开设备
    Err_t result = DEV_EOK;
    result = lHalDeviceOpen(Mq2Dev, DEVICE_OFLAG_RDWR);
    if (result != DEV_EOK)
    {
        printf("can not open %s device\n",mq2_dev);
        return DEV_ERROR;
    }
    return DEV_EOK;
}

Uint8_t ucTalMq2GetSmokeStatus(void)
{
    Uint8_t data=0;
    ulHalDeviceRead(Mq2Dev, NULL, &data, 1);
    return data;
}

Int32_t lTalMq7Init(void)
{
    Mq7Dev = pHalDeviceFind(mq7_dev);
    if (Mq7Dev == NULL)
    {
        printf("can not find %s Model\n",mq7_dev);
        return DEV_ERROR;
    }

    // 打开设备
    Err_t result = DEV_EOK;
    result = lHalDeviceOpen(Mq7Dev, DEVICE_OFLAG_RDWR);
    if (result != DEV_EOK)
    {
        printf("can not open %s device\n",mq7_dev);
        return DEV_ERROR;
    }
    return DEV_EOK;
}

Uint8_t ucTalMq7GetCoStatus(void)
{
    Uint8_t data=0;
    ulHalDeviceRead(Mq7Dev, NULL, &data, 1);
    return data;
}


Int32_t lTalMq135Init(void)
{
    Mq135Dev = pHalDeviceFind(mq135_dev);
    if (Mq135Dev == NULL)
    {
        printf("can not find %s Model\n",mq135_dev);
        return DEV_ERROR;
    }

    // 打开设备
    Err_t result = DEV_EOK;
    result = lHalDeviceOpen(Mq135Dev, DEVICE_OFLAG_RDWR);
    if (result != DEV_EOK)
    {
        printf("can not open %s device\n",mq135_dev);
        return DEV_ERROR;
    }
    return DEV_EOK;
}

Uint8_t ucTalMq135GetAirStatus(void)
{
    Uint8_t data=0;
    ulHalDeviceRead(Mq135Dev, NULL, &data, 1);
    return data;
}
