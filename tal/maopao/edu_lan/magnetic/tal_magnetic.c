#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "device.h"
#include "hal_sensor.h" //sesor IO模型驱动框架头文件
#include "esp_log.h"
#include "tal_magnetic.h"
#include "swldef.h"

#define MAG_DEV "cc6201st"
Device_t sensor_mag_dev = NULL;

Int32_t lTalMagneticInit(void)
{

    // 查找设备
    sensor_mag_dev = pHalDeviceFind(MAG_DEV);
    if (sensor_mag_dev == NULL)
    {
        printf("can not find cc6201st Model\n");
        return DEV_ERROR;
    }

    // 打开设备
    Err_t result = DEV_EOK;
    result = lHalDeviceOpen(sensor_mag_dev, DEVICE_OFLAG_RDWR);
    if (result != DEV_EOK)
    {
        printf("can not open pin device\n");
        return DEV_ERROR;
    }
    return DEV_EOK;
}

Uint8_t ucTalGetMagStatus(void)
{
    Uint8_t data = 0;
    ulHalDeviceRead(sensor_mag_dev, NULL, &data, 1);

    return data;
}

Uint8_t ucTalgetMagnetic(void)
{
    return ucTalGetMagStatus();
}

Uint8_t ucTalGetIsMagnetic(Uint8_t id)
{
    Uint8_t data = ucTalGetMagStatus();
    Uint8_t res = 0;
    switch (id)
    {
    case 1:
    {
        if (data & 0x01)
        {
            res = 1;
        }
        else
        {
            res = 0;
        }
    }
    break;
    case 2:
    {
        if (data & 0x02)
        {
            res = 1;
        }
        else
        {
            res = 0;
        }
    }
    break;
    case 3:
    {
        if (data & 0x04)
        {
            res = 1;
        }
        else
        {
            res = 0;
        }
    }
    break;
    default:

        break;
    }
    return res;
}
