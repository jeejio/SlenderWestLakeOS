#include <stdio.h>
#include <string.h>
#include "aes.h"

void show(const char *tips, const unsigned char *p, size_t len)
{
    size_t i;

    printf("%s\n", tips);
    for (i = 0; i < len; i++)
    {
        printf("%02X ", p[i]);
        if ((0 != i))
        {
            if (((i + 1) % 16 == 0))
            {
                printf("\n");
            }
        }
    }
    printf("\n");
}

void write2file(const char *path, void *data, size_t length)
{
    FILE *fp = fopen(path, "wb");
    if (!fp)
    {
        fprintf(stderr, "fopen()");
        goto e_open;
    }

    fwrite(data, 1, length, fp);

    fclose(fp);
e_open:
    return;
}

#define BASE_PATH "/tmp/bxtmp/aes"
// #define PATH_DATA BASE_PATH "/rand.dat"
#define PATH_DATA BASE_PATH "/rand_no_aligned.dat"
#define PATH_C_ENC BASE_PATH "/c.enc"
#define PATH_C_DEC BASE_PATH "/c.dat"

#define BUFSIZE (1024)

long get_file_size(FILE *fp)
{
    fseek(fp, 0, SEEK_END);
    long file_size = ftell(fp);
    printf("file szie = %ld\n", file_size);
    rewind(fp);
    return file_size;
}

int descrypt(const unsigned char *key, const unsigned char *iv)
{
    unsigned char ivsec[16];
    AES_KEY aes_key_dec;

    unsigned char enc_data[BUFSIZE];
    unsigned char plain_data[BUFSIZE];
    size_t read_len = -1;
    size_t write_len = -1;

    if (AES_set_decrypt_key(key, 128, &aes_key_dec) != 0)
    {
        fprintf(stderr, "AES_set_decrypt_key()\n");
        return 1;
    }

    memcpy(ivsec, iv, 16);

    FILE *fpr = fopen(PATH_C_ENC, "rb");
    FILE *fpw = fopen(PATH_C_DEC, "wb");

    long file_size = get_file_size(fpr);
    long cur_pos = 0;
    unsigned char padding = 0;

    while ((read_len = fread(enc_data, 1, BUFSIZE, fpr)))
    {
        cur_pos += read_len;
        AES_cbc_encrypt(enc_data, plain_data, read_len, &aes_key_dec, ivsec, AES_DECRYPT);
        write_len = read_len;
        if (cur_pos == file_size) // Last packet.
        {
            padding = plain_data[read_len - 1];
            write_len -= padding; // Remove padding data.
        }
        if (write_len > 0)
        {
            fwrite(plain_data, 1, write_len, fpw);
        }
    }
    fflush(fpw);

    fclose(fpw);
    fclose(fpr);

    return 0;
}

int encrypt(const unsigned char *key, const unsigned char *iv)
{
    unsigned char ivsec[16];
    AES_KEY aes_key_enc;

    unsigned char plain_data[BUFSIZE];
    unsigned char enc_data[BUFSIZE];
    size_t read_len = -1;

    if (AES_set_encrypt_key(key, 128, &aes_key_enc) != 0)
    {
        fprintf(stderr, "AES_set_encrypt_key()\n");
        return 1;
    }

    memcpy(ivsec, iv, 16);

    FILE *fpr = fopen(PATH_DATA, "rb");
    FILE *fpw = fopen(PATH_C_ENC, "wb");

    long file_size = get_file_size(fpr);
    long cur_pos = 0;
    unsigned char padding = file_size % 16 == 0 ? 16 : 16 - file_size % 16;
    printf("padding=%02X\n", padding);

    while ((read_len = fread(plain_data, 1, BUFSIZE, fpr)))
    {
        cur_pos += read_len;
        if (cur_pos == file_size) // Last packet.
        {
            // PKCS5Padding
            if (padding < 16)
            {
                memset(plain_data + read_len, padding, padding);
                AES_cbc_encrypt(plain_data, enc_data, read_len + padding, &aes_key_enc, ivsec, AES_ENCRYPT);
                fwrite(enc_data, 1, read_len + padding, fpw);
                break;
            }
        }
        AES_cbc_encrypt(plain_data, enc_data, read_len, &aes_key_enc, ivsec, AES_ENCRYPT);
        fwrite(enc_data, 1, read_len, fpw);
    }

    // PKCS5Padding
    if (16 == padding)
    {

        memset(plain_data, padding, padding);
        AES_cbc_encrypt(plain_data, enc_data, padding, &aes_key_enc, ivsec, AES_ENCRYPT);
        fwrite(enc_data, 1, padding, fpw);
    }
    fflush(fpw);

    fclose(fpw);
    fclose(fpr);

    return 0;
}

int main(void)
{
    const unsigned char key[] = {0x28, 0x30, 0xA5, 0x5B, 0x78, 0x0C, 0x95, 0xE2, 0x11, 0x5F, 0x39, 0x3B, 0x9C, 0xE0, 0xC8, 0xB4};
    const unsigned char iv[] = {0x8E, 0x9E, 0x90, 0x81, 0xA3, 0x45, 0x56, 0x5F, 0x19, 0x4F, 0xFF, 0xEF, 0x4C, 0x5C, 0x41, 0x84};

    encrypt(key, iv);
    descrypt(key, iv);

    return 0;
}
