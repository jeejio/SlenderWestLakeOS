/*
 * Copyright (c) 2023, jeejio Development Team
 *
 * Change Logs:
 * Date           Author         Notes
 * 2023-03-29     quziming      the first version
 */

//本样例展示了基本的json转结构体的功能
#include <stdio.h>
#include "mson.h"

MSON_DEFOBJ(Payload,
            int code;
                    char *msg;
                    int value;
);


int main() {
    const char *json = "{\"code\": 123,\"value\": 666, \"msg\": \"Hello World!\"}";
    mson_jsonToObj(json, MSON_MEMBERS_TO_STRING(Payload), obj); //json转结构体，并将结果保存在obj1中
    if (!obj){
        printf("null\n");
        return 0;
    }
    Payload *payload = (Payload *) obj;
    printf("code:%d, value:%d, msg:%s\n", payload->code, payload->value, payload->msg);
    mson_releaseWithObj(obj); //释放内部变量__msonMemberInfo的空间，并释放obj1内相关成员的空间（如类型为char *的成员）
}