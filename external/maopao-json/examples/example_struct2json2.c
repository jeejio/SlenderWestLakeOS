/*
 * Copyright (c) 2023, jeejio Development Team
 *
 * Change Logs:
 * Date           Author         Notes
 * 2023-03-29     quziming      the first version
 */

//本样例展示了json字符串二次组装功能
#include <stdio.h>
#include <stdlib.h>
#include "mson.h"

MSON_DEFOBJ(Payload,
            int code;
                    jsonStr result;
                    jsonStr result2[2];
);


int main() {
    Payload payload = {
            .code=200,
            .result="[{\"test\":1},{\"test\":1}]",
            .result2 = {"[{\"test\":1},{\"test\":1}]","[{\"test\":1},{\"test\":1}]"}
    };
    char *jsonOut = mson_objToJson(&payload, MSON_MEMBERS_TO_STRING(Payload));//结构体转json
    printf("%s\n", jsonOut);//{"code":200,"result":{"test":1}}

    free(jsonOut);//注意要释放json字符串的内存
}