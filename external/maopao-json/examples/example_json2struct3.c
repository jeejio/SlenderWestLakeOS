/*
 * Copyright (c) 2023, jeejio Development Team
 *
 * Change Logs:
 * Date           Author         Notes
 * 2023-03-29     quziming      the first version
 */
/**
 * 本样例展示了含数组json的使用示例
 * @note 对于json与结构体中数组长度不一致的情况，将取两者中最小长度进行处理
 *       若json中数组长度大于结构体中数组长度，则多余部分被抛弃
 *       若json中数组长度小于结构体中数组长度，则结构体多余部分被0填充
 */

#include <stdio.h>
#include "mson.h"

MSON_DEFOBJ(Payload,
            int code;
                    char *msg;
                    struct {
                        int num;
                        int data[5];
                    } result[5];
);


int main() {
    const char *json = "{\"code\":200,\"result\":[{\"num\":2,\"data\":[1,2]},{\"num\":1,\"data\":[7]},{\"num\":4,\"data\":[123,3542,53,12]},{\"num\":3,\"data\":[9,3,6]}],\"msg\":\"OK\"}";
    mson_jsonToObj(json, MSON_MEMBERS_TO_STRING(Payload), obj); //json转结构体，并将结果保存在obj中
    Payload *payload = (Payload *) obj;
    printf("code=%d\n", payload->code);
    printf("msg=%s\n", payload->msg);
    printf("result=\n");
    for (int i = 0; i < 5; ++i) {
        printf("%d:num=%d data=", i, payload->result[i].num);
        for (int j = 0; j < payload->result[i].num; ++j) {
            printf("%d,", payload->result[i].data[j]);
        }
        printf("\n");
    }
    mson_releaseWithObj(obj); //释放内部变量__msonMemberInfo的空间，并释放obj内相关成员的空间（如类型为char *的成员）
}