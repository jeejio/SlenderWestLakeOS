/*
 * Copyright (c) 2023, jeejio Development Team
 *
 * Change Logs:
 * Date           Author         Notes
 * 2023-03-29     quziming      the first version
 */

//本样例展示了多层object嵌套的json转结构体功能
#include <stdio.h>
#include "mson.h"

MSON_DEFOBJ(Payload,
            int code;
                    char *msg;
                    struct {
                        struct {
                            int value1;
                            int value2;
                        } value;
                        char *status;
                    } result;
);


int main() {
    const char *json = "{\"code\":200,\"result\":{\"value\":{\"value1\":123,\"value2\":321},\"status\":\"OK\"},\"msg\":\"Hello World!\"}";
    mson_jsonToObj(json, MSON_MEMBERS_TO_STRING(Payload), obj); //json转结构体，并将结果保存在obj中
    Payload *payload = (Payload *) obj;
    printf("code=%d\n", payload->code);
    printf("msg=%s\n", payload->msg);
    printf("result.value.value1=%d\n", payload->result.value.value1);
    printf("result.value.value2=%d\n", payload->result.value.value2);
    printf("result.status=%s\n", payload->result.status);
    mson_releaseWithObj(obj); //释放内部变量__msonMemberInfo的空间，并释放obj内相关成员的空间（如类型为char *的成员）
}