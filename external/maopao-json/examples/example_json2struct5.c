/*
 * Copyright (c) 2023, jeejio Development Team
 *
 * Change Logs:
 * Date           Author         Notes
 * 2023-03-29     quziming      the first version
 */

/**
 * 本样例展示了错误处理流程
 */

#include <stdio.h>
#include "mson.h"

MSON_DEFOBJ(Payload,
            int Code;
                    char *msg;
                    char data[4];
                    int num;
                    char *params;
);


int main() {
    const char *json = "{\"code\":200,\"msg\":\"OK\",\"data\":\"TEST\",\"num\":\"3\",\"params\":{\"r\":255,\"g\":255,\"b\":255}}";
    mson_jsonToObj(json, MSON_MEMBERS_TO_STRING(Payload), obj); //json转结构体，并将结果保存在obj中
    Payload *payload = (Payload *) obj;
    printf("code=%d\n", payload->Code); //打印"code=0"，结构体定义为"Code"，json中key为"code"，大小写不匹配，视为json未传递此参数
    printf("msg=%s\n", payload->msg); //打印"msg=OK"
    printf("data=%s\n", payload->data);//打印"data1=TES"，结构体定义长度为4的字符数组，其中一位为结束符，只能保存长度为3的字符串，多余部分被截断
    printf("num=%d\n", payload->num);//打印"num=0"，结构体定义为int类型，json中传递的值为字符串类型，类型不匹配
    printf("params=%s\n",
           payload->params);//打印"params={"r":255,"g":255,"b":255}"，若json中值为object，结构体成员类型为字符串，则会将object部分转换为字符串保存
    mson_releaseWithObj(obj); //释放内部变量__msonMemberInfo的空间，并释放obj内相关成员的空间（如类型为char *的成员）
}