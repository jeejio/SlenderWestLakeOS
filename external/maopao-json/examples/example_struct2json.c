/*
 * Copyright (c) 2023, jeejio Development Team
 *
 * Change Logs:
 * Date           Author         Notes
 * 2023-03-29     quziming      the first version
 */

//本样例展示了基本的结构体转json的功能
#include <stdio.h>
#include <stdlib.h>
#include "mson.h"

MSON_DEFOBJ(Payload,
            int code;
                    char *msg;
                    int value[5];
);


int main() {
    Payload payload = {
            .code=200,
            .msg="test",
            .value={1, 2, 3, 4, 5}
    };
    char *jsonOut = mson_objToJson(&payload, MSON_MEMBERS_TO_STRING(Payload));//结构体转json
    printf("%s\n", jsonOut);//{"code":200,"msg":"test","value":[1,2,3,4,5]}

    free(jsonOut);//注意要释放json字符串的内存
}