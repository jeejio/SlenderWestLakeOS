/*
 * Copyright (c) 2023, jeejio Development Team
 *
 * Change Logs:
 * Date           Author         Notes
 * 2023-03-29     quziming      the first version
 */

/**
 * 本样例展示了字符串的两种用法
 * @note 用法一：在结构体中定义为char *，转换函数会为字符串分配空间。
 *       用法二：在结构体中定义为字符数组，转换函数会将字符串拷贝到字符数组中，若实际字符串长度超出定义数组长度，则超出部分被舍弃。
 */

#include <stdio.h>
#include "mson.h"

MSON_DEFOBJ(Payload,
int code;
char *msg;
char data[3][10];
int num;
);


int main() {
    const char *json = "{\"code\":200,\"msg\":\"OK\",\"data\":[\"OK1\",\"OK2\",\"OK3\"],\"num\":3}";
    mson_jsonToObj(json, MSON_MEMBERS_TO_STRING(Payload), obj); //json转结构体，并将结果保存在obj中
    Payload *payload = (Payload *) obj;
    printf("code=%d\n", payload->code);
    printf("msg=%s\n", payload->msg);
    for (int i = 0; i < 3; ++i) {
        printf("%s\n", payload->data[i]);
    }
    mson_releaseWithObj(obj); //释放内部变量__msonMemberInfo的空间，并释放obj内相关成员的空间（如类型为char *的成员）
}