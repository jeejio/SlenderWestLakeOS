
#include "device.h"
#include "hal_sensor.h" //sesor IO模型驱动框架头文件
#include "ky024.h"      //厂家驱动头文件
#include "string.h"
#include "auto_init.h"
#include "hal_gpio.h"
#include "esp_log.h"
#include "drv_sensor_ky024.h"

#define SENSOR_DOOR_NAME "door"

/**
 * @brief       读取door状态
 *
 * NOTE:        -
 *
 * @param[in]   sensor    - 传感器指针
 * @param[out]  buf      - 出参指针，读取的状态通过该指针带出
 * @param[in]   len      - 入参指针，指示入参指针的数据长度
 * @return      固定为0
 *
 */
Size_t sensorDoorReadStatus(struct SensorDevice_st *sensor, void *buf, Size_t len)
{
    int *pbuf = buf;
    if (buf == NULL)
        return 0;
    pbuf[0] = lKy024ReadStatus();
    pbuf[1] = lKy024GetOvertimeNotCloseAlarmOnoff();
    pbuf[2] = lKy024GetOvertimeTime();
    pbuf[3] = lKy024GetIsOvertimeNotCloseAlarm();
    return 0;
}

/**
 * @brief       sensor指令处理函数
 *
 * NOTE:        -
 *
 * @param[in]   sensor    - 传感器指针
 * @param[in]   cmd       - 命令，用于制定对传感器的操作类型
 * @param[in]   arg       - 入参指针，用于传入各种类型的数据
 * @return      固定为DEV_EOK
 *
 */
Err_t sensorDoorControl(struct SensorDevice_st *sensor, int cmd, void *arg)
{
    switch (cmd)
    {
    case SENSOR_CTRL_SET_POWER:
    {
        int power_mode_type = (int)arg;
        switch (power_mode_type)
        {
        case SENSOR_POWER_NORMAL:
        {
            printf(" start : vKy024Init \n");
            vKy024Init();
            printf(" end   : vKy024Init \n");
        }
        break;
        case SENSOR_POWER_DOWN:
        {
        }
        break;
        default:
            break;
        }
    }
    break;
    case  SENSOR_DOOR_SENSOR_SET_ALARM_ONOFF:
    {
        vKy024SetOvertimeNotCloseAlarmOnoff(*(bool *)arg);
    }
    break;
    case SENSOR_DOOR_SENSOR_SET_ALARM_TIME:   
    {
        vKy024SetOvertimeTime(*(int *)arg);
    }
    break;
    case SENSOR_DOOR_SENSOR_SET_STATE_CHANGED_CB:
    {
        vKy024SetStateChangedCb((vDoorStateChanged)arg);
    }
    break;
    default:break;
    }
    return DEV_EOK;
}

static struct SensorOps_st sensor_ops =
{
    sensorDoorReadStatus,
    sensorDoorControl
};

 /**
   * @brief       sensor框架注册
   *
   * NOTE:        -
   * 
   * @param[in]   sensor    - 设备名称
   * @param[in]   cfg       - 传感器的配置参数
   * @return      返回sensor框架注册结果
   *
   */
int sensorDoorInit(const char *name, struct SensorConfig_st *cfg)
{
    Int8_t result;
    Sensor_t sensor_door = 0;
    /* sensor register */
    sensor_door = pvPortMalloc(sizeof(struct SensorDevice_st));
    if (sensor_door == 0)
        return -1;
    memset(sensor_door, 0, sizeof(struct SensorDevice_st));
    sensor_door->info.type = SENSOR_CLASS_DOOR;
    sensor_door->info.vendor = SENSOR_VENDOR_UNKNOWN;
    sensor_door->info.model = "door";

    if (cfg != 0)
        memcpy(&sensor_door->config, cfg, sizeof(struct SensorConfig_st));

    sensor_door->ops = &sensor_ops;

    result = lHalHwSensorRegister(sensor_door, name, DEVICE_FLAG_RDWR, 0);
    if (result != DEV_EOK)
    {
        // LOG_E("device register err code: %d", result);
        goto __exit;
    }

    return DEV_EOK;

__exit:
    if (sensor_door)
        vPortFree(sensor_door);

    return -DEV_ERROR;
}

 /**
   * @brief       sensor初始化
   *
   * NOTE:        -
   * 
   * @return      固定为DEV_EOK
   *
   */
int lDrvHwSensorDoorInit()
{
    struct SensorConfig_st cfg = {0};
    cfg.intf.dev_name = SENSOR_DOOR_NAME;
    sensorDoorInit(SENSOR_DOOR_NAME, &cfg);
    return DEV_EOK;
}

INIT_DEVICE_EXPORT(lDrvHwSensorDoorInit);
