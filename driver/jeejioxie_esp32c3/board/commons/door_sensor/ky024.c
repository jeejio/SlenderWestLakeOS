#include "ky024.h"
#include "hal_gpio.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "swldef.h"

#define DOOR_PIN_NUM (3)

static int32_t alarmOnff = 0;
static int32_t alarmTime = 0;

static int32_t doorState = 0;
static int32_t doorAlarmStatus = 0;
static int32_t doorOnTick = 0;
static vDoorStateChanged OnDoorStateChangedFunc = NULL;

static void ky024_task(void *arg)
{
    Err_t stChangedFlag = 0;
    
    while (1)
    {
		Err_t ret = (lHalPinRead(DOOR_PIN_NUM)?0:1);
		if (ret != doorState)
		{
			stChangedFlag = 1;
			doorState = ret;
		}
		
		Err_t alarmSt = 0;
		if(doorState)
		{
			doorOnTick++;
			if(alarmOnff)
			{
				alarmSt = doorOnTick > alarmTime * 5;
			}
		}
		else
		{
			doorOnTick = 0;
		}
		
		if(alarmSt != doorAlarmStatus)
		{	
			stChangedFlag = 1;
			doorAlarmStatus = alarmSt;
		}
		
		if(stChangedFlag)
		{
            stChangedFlag = 0;
            if (OnDoorStateChangedFunc)
            {
                OnDoorStateChangedFunc();
            }
		}

        vTaskDelay(pdMS_TO_TICKS(200));
    }
}

void vKy024Init(void)
{
    vHalPinMode(DOOR_PIN_NUM, PIN_MODE_INPUT);

    xTaskCreate(ky024_task, "ky024_task", 4096, NULL, configMAX_PRIORITIES, NULL);
}


Err_t lKy024ReadStatus(void)
{
	printf("lKy024ReadStatus%ld\n", doorState);
    return doorState;
}

Err_t lKy024GetOvertimeNotCloseAlarmOnoff(void)
{
	printf("lKy024GetOvertimeNotCloseAlarmOnoff%ld\n", alarmOnff);
    return alarmOnff;
}

Err_t lKy024GetOvertimeTime(void)
{
	printf("lKy024ReadStatus%ld\n", alarmTime);
    return alarmTime;
}

Err_t lKy024GetIsOvertimeNotCloseAlarm(void)
{
	printf("lKy024GetIsOvertimeNotCloseAlarm%ld\n", doorAlarmStatus);
    return doorAlarmStatus;
}

void vKy024SetOvertimeNotCloseAlarmOnoff(Err_t onoff)
{
    alarmOnff = onoff;
}

void vKy024SetOvertimeTime(Err_t time)
{
    alarmTime = time;
}

void vKy024SetStateChangedCb(vDoorStateChanged fun)
{
    OnDoorStateChangedFunc = fun;
}