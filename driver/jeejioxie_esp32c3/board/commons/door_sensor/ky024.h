#ifndef __KY024_H__
#define __KY024_H__

#include "swldef.h"

typedef void (*vDoorStateChanged)();

 /**
   * @brief       KY024硬件初始化
   *
   * NOTE:        -
   *
   */
void vKy024Init(void);

 /**
   * @brief       读取KY024的数据
   *
   * NOTE:        -
   * 
   * @return      1:开门 0：关门
   *
   */
Base_t lKy024ReadStatus(void);

 /**
   * @brief       获取DOOR_SENSOR门开超时报警
   *
   * NOTE:        
   *
   * @return      1 - 使能，0 - 关闭
   *
   */
Base_t lKy024GetOvertimeNotCloseAlarmOnoff(void);

 /**
   * @brief       获取DOOR_SENSOR门开超时报警时间
   *
   * NOTE:        
   *
   * @return      时间(s)
   *
   */
Base_t lKy024GetOvertimeTime(void);

 /**
   * @brief       获取DOOR_SENSOR门开超时报警状态
   *
   * NOTE:        
   *
   * @return      1、报警 0：无报警
   *
   */
Base_t lKy024GetIsOvertimeNotCloseAlarm(void);

 /**
   * @brief       DOOR_SENSOR设置报警开关
   *
   * @param[in]   开关值      
   *
   */
void vKy024SetOvertimeNotCloseAlarmOnoff(Base_t onoff);

 /**
   * @brief       DOOR_SENSOR设置报警时间
   *
   * @param[in]   时间(s)     
   *
   */
void vKy024SetOvertimeTime(Base_t time);

 /**
   * @brief      设置回调
   *
   * NOTE:       信息包括：速度，开关状态
   *           
   * @param[out]   返回信息
   */
void vKy024SetStateChangedCb(vDoorStateChanged fun);

#endif
