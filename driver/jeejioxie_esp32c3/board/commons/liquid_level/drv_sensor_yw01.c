/*
 * Copyright (c) 2022, jeejio Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author         Notes
 * 2023-03-23     df            the first version
 */

#include "drv_sensor_yw01.h"
#include "device.h"
#include "hal_sensor.h" //sesor IO模型驱动框架头文件
#include "string.h"
#include "auto_init.h"
#include "hal_gpio.h"
#include "hal_pwm.h"
#include "yw01.h" //厂家驱动头文件

#define SENSOR_LIQUID_LEVEL_NAME "LIQUID_LEVEL_YW01"

static const char *TAG = "LIQUID_LEVEL";

enum
{
    SENSOR_LIQUID_LEVEL_SET_RANGE_MAX = SENSOR_CTRL_USER_CMD_START + 1,
    SENSOR_LIQUID_LEVEL_SET_RANGE_MIN,
    SENSOR_LIQUID_LEVEL_SET_BUZZER_STATUS,
    SENSOR_LIQUID_LEVEL_SET_BUZZER_HEIGHT,
    SENSOR_LIQUID_LEVEL_SET_STANDARDDEPTH,
    SENSOR_LIQUID_LEVEL_SET_CALLBACK,
};

static Size_t uDrvSensorLiquidLevelFetchData(struct SensorDevice_st *sensor, void *buf, Size_t len)
{
    if (buf == NULL)
        return 0;
    float *info = (float *)buf;
    int index = 0;

    info[index++] = fYw01GetDepth();
    info[index++] = iYw01GetPressure();
    info[index++] = iYw01GetDensity();
    info[index++] = iYw01GetAlarm();
    info[index++] = iYw01GetTamperAlarm();
    info[index++] = iYw01GetHeight();
    info[index++] = iYw01GetStandardDepth();
    info[index++] = iYw01GetLiquidLevelRatio();
    info[index++] = iYw01GetLiquidLevelRatio();
    info[index++] = vYw01GetRangeMax();
    info[index++] = vYw01GetRangeMin();
    return index;
}

static Err_t lDrvSensorLiquidLevelControl(struct SensorDevice_st *sensor, int cmd, void *arg)
{
    LOGI(TAG, "cmd %d \n", cmd);
    switch (cmd)
    {
    case SENSOR_CTRL_SET_POWER:
    {
        int power_mode_type = (int)arg;
        switch (power_mode_type)
        {
        case SENSOR_POWER_NORMAL:
        {
            LOGI(TAG, " start : vDrvSensorInit \n");
            vYw01Init();
            LOGI(TAG, " end   : vDrvSensorInit \n");
        }
        break;
        case SENSOR_POWER_DOWN:
        {
            
        }
        break;
        default:break;
        }
    }
    break;
    case SENSOR_LIQUID_LEVEL_SET_RANGE_MAX:
    {
        float value = *((float *)arg);
        vYw01SetRangeMax(value);
    }
    break;
    case SENSOR_LIQUID_LEVEL_SET_RANGE_MIN:
    {
        float value = *((float *)arg);
        vYw01SetRangeMin(value);
    }
    break;
    case SENSOR_LIQUID_LEVEL_SET_BUZZER_STATUS:
    {
        int32_t value = *((int32_t *)arg);
        vYw01SetBuzzerStatus(value);
    }
    break;
    case SENSOR_LIQUID_LEVEL_SET_BUZZER_HEIGHT:
    {
        float value = *((float *)arg);
        vYw01SetHeight(value);
    }
    break;
    case SENSOR_LIQUID_LEVEL_SET_STANDARDDEPTH:
    {
        float value = *((float *)arg);
        vYw01SetStandardDepth(value);
    }
    break;
    case SENSOR_LIQUID_LEVEL_SET_CALLBACK:
    {
        vYw01RegisterEventCallback(arg);
    }
    break;
    default:break;
    }
    return DEV_EOK;
    ;
}

static struct SensorOps_st xSensorOps =
{
    uDrvSensorLiquidLevelFetchData,
    lDrvSensorLiquidLevelControl
};

static int lDrvSensorLiquidLevelInit(const char *name, struct SensorConfig_st *cfg)
{
    Int8_t result;
    Sensor_t sensor_liquid_level = NULL;
    /* sensor register */
    sensor_liquid_level = pvPortMalloc(sizeof(struct SensorDevice_st));
    if (sensor_liquid_level == NULL)
        return -1;
    memset(sensor_liquid_level, 0, sizeof(struct SensorDevice_st));
    sensor_liquid_level->info.vendor = "xwalk";//厂商名
    sensor_liquid_level->info.model = "liquid_level_yw01";//型号

    if (cfg != NULL)
        memcpy(&sensor_liquid_level->config, cfg, sizeof(struct SensorConfig_st));

    sensor_liquid_level->ops = &xSensorOps;

    result = lHalHwSensorRegister(sensor_liquid_level, name, DEVICE_FLAG_RDWR, NULL);
    if (result != DEV_EOK)
    {
        LOGE(TAG,"device register err code: %d", result);
        goto __exit;
    }

    return DEV_EOK;

__exit:
    if (sensor_liquid_level)
        vPortFree(sensor_liquid_level);

    return -DEV_ERROR;
}

static int lDrvHwSensorLiquidLevelInit()
{
    LOGI(TAG,"begin to execute lDrvHwSensorInit\n");
    struct SensorConfig_st cfg = {0};
    cfg.intf.dev_name = SENSOR_LIQUID_LEVEL_NAME;
    lDrvSensorLiquidLevelInit(SENSOR_LIQUID_LEVEL_NAME, &cfg);
    LOGI(TAG,"finish to execute lDrvHwSensorInit\n");

    return DEV_EOK;
}

INIT_DEVICE_EXPORT(lDrvHwSensorLiquidLevelInit);
