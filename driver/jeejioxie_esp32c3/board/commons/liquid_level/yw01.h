#ifndef __YW01_H
#define __YW01_H

#include "swldef.h"

void vYw01Init(void);

void vYw01SetRangeMax(float value);

void vYw01SetRangeMin(float value);

void vYw01SetBuzzerStatus(int32_t value);

void vYw01SetHeight(float value);

void vYw01SetStandardDepth(float value);

float fYw01GetDepth(void);

int32_t iYw01GetPressure(void);

int32_t iYw01GetDensity(void);

int32_t iYw01GetAlarm(void);

int32_t iYw01GetTamperAlarm(void);

float iYw01GetHeight(void);

float iYw01GetStandardDepth(void);

int32_t iYw01GetLiquidLevelRatio(void);

int32_t iYw01GetBuzzerStatus(void);

float vYw01GetRangeMax(void);

float vYw01GetRangeMin(void);

void vYw01RegisterEventCallback(void (*cb)(float *data, uint8_t size));

#endif