#include "yw01.h"
#include <stdio.h>
#include "hal_serial.h"
#include "hal_sensor.h"
#include "drv_uart_iomodel.h"
#include "hal/gpio_types.h"
#include "uart.h"
#include "string.h"
#include "hal_adc.h"

#define IN_RANGE(val, Range, err) (abs(val - range) < err)

#define ADC_DEV_NAME "adc1"
#define ADC_DEV_CHANNEL 3
#define SENSOR_INPUT_IO (10)

static AdcDevice_t ADC1Dev = NULL;

static float rangeMax = 0;
static float rangeMin = 0;
static int32_t buzzerStatus = 0;
static float height = 0;
static float standardDepth = 90;
static float depth = 0;
static int32_t pressure = 0;
static int32_t density = 0;
static int32_t alarm = 0;
static int32_t tamperAlarm = 0;
static int32_t liquidLevelRatio = 0;

static void (*event_update_callback)(float *data, uint8_t size) = NULL;

static void sensor_monitor(void *arg)
{
    float eventInfo[6];
    uint8_t infoIndex = 0;
    int16_t triggerEvent = 0;

    int32_t curliquidLevelRatio = 0;

    int8_t sensorInput = 0;
    uint32_t ulVoltage = 0;
    while (1)
    {        
        sensorInput = lHalPinRead(SENSOR_INPUT_IO);

        ulVoltage = uHalAdcRead(ADC1Dev, ADC_DEV_CHANNEL);


        if (ulVoltage < 110)
        {
            curliquidLevelRatio = 0;
        }
        else if(ulVoltage < 130)
        {
            curliquidLevelRatio = 10;
        }
        else if(ulVoltage < 150)
        {
            curliquidLevelRatio = 20;
        }
        else if(ulVoltage < 170)
        {
            curliquidLevelRatio = 30;
        }
        else if(ulVoltage < 190)
        {
            curliquidLevelRatio = 40;
        }
        else if(ulVoltage < 220)
        {
            curliquidLevelRatio = 50;
        }
        else if(ulVoltage < 280)
        {
            curliquidLevelRatio = 60;
        }
        else if(ulVoltage < 360)
        {
            curliquidLevelRatio = 70;
        }
        else if(ulVoltage < 510)
        {
            curliquidLevelRatio = 80;
        }
        else if(ulVoltage < 800)
        {
            curliquidLevelRatio = 90;
        }
        else 
        {
            curliquidLevelRatio = 100;
        }
        
        if (curliquidLevelRatio != liquidLevelRatio)
        {
            liquidLevelRatio = curliquidLevelRatio;
            triggerEvent = 1;
        }

         depth = height + standardDepth * liquidLevelRatio / 100.;

        int32_t tmpAlarmFlag = (liquidLevelRatio == 0);
        if (tmpAlarmFlag != tamperAlarm)
        {
            tamperAlarm = tmpAlarmFlag;
            triggerEvent = 1;
        }
        
        tmpAlarmFlag = depth < rangeMin || depth > rangeMax;
        if (tmpAlarmFlag != alarm)
        {
            alarm = tmpAlarmFlag;
            triggerEvent = 1;
        }

        if (triggerEvent)
        {
            triggerEvent = 0;
            if (event_update_callback)
            {
                infoIndex = 0;
                eventInfo[infoIndex++] = fYw01GetDepth();
                eventInfo[infoIndex++] = iYw01GetPressure();
                eventInfo[infoIndex++] = iYw01GetDensity();
                eventInfo[infoIndex++] = iYw01GetAlarm();
                eventInfo[infoIndex++] = iYw01GetTamperAlarm();
                eventInfo[infoIndex++] = iYw01GetLiquidLevelRatio();
                event_update_callback(eventInfo, infoIndex);
            }
        }

         vTaskDelay(200 / portTICK_PERIOD_MS);
    }
}

void vYw01Init(void)
{
    printf("vYw01Init\n");
    vHalPinMode(SENSOR_INPUT_IO,PIN_MODE_INPUT);

    ADC1Dev = (AdcDevice_t)pHalDeviceFind(ADC_DEV_NAME);
    if (ADC1Dev == NULL)
    {
        printf("can not find %s Model\n", ADC_DEV_NAME);
        return;
    }
    
    lHalAdcEnable(ADC1Dev, 1 << ADC_DEV_CHANNEL);

    xTaskCreate(sensor_monitor, "sensor_monitor_task", 1024 * 4, NULL, 10, NULL);
}

void vYw01SetRangeMax(float value)
{
    rangeMax = value;
}

void vYw01SetRangeMin(float value)
{
    rangeMin = value;
}

void vYw01SetBuzzerStatus(int32_t value)
{
    buzzerStatus = value;
}

void vYw01SetHeight(float value)
{
    height = value;
}

void vYw01SetStandardDepth(float value)
{
    standardDepth = value;
}

float fYw01GetDepth(void)
{
    return depth;
}

int32_t iYw01GetPressure(void)
{
    return pressure;
}

int32_t iYw01GetDensity(void)
{
    return density;
}

int32_t iYw01GetAlarm(void)
{
    return alarm;
}

int32_t iYw01GetTamperAlarm(void)
{
    return tamperAlarm;
}

float iYw01GetHeight(void)
{
    return height;
}

float iYw01GetStandardDepth(void)
{
    return standardDepth;
}

int32_t iYw01GetLiquidLevelRatio(void)
{
    return liquidLevelRatio;
}

int32_t iYw01GetBuzzerStatus(void)
{
    return buzzerStatus;
}

float vYw01GetRangeMax(void)
{
    return rangeMax;
}

float vYw01GetRangeMin(void)
{
    return rangeMin;
}

void vYw01RegisterEventCallback(void (*cb)(float* data, uint8_t size))
{
    event_update_callback = cb;
}
