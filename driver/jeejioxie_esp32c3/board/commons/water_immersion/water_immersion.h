#ifndef __WATER_IMMERSION_H__
#define __WATER_IMMERSION_H__

#include "stdint.h"


 /**
   * @brief       单键开关初始化 
   * 
   *   NOTE:      gpio 设置为输入模式
   * 
   */
void waterimmer_key_init(void);


void waterimmer_alarm_switch_set(uint8_t value);

uint8_t waterimmer_alarm_switch_get(void);

uint8_t waterimmer_alarm_status_get(void);

void waterimmer_alarm_mute(void);
/**
 * @brief 获取是否泡水
*/
uint8_t is_waterimmer_soak_in_water(void);

void waterimmer_pwm_init(void);

/**
 * @brief 报警器PWM初始化
*/
void waterimmer_pwm_init(void);
/**
 * @brief 水浸探测IO初始化及创建任务检测
*/
void waterimmer_check_io_init(void);
#endif
