#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "esp_log.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/queue.h"
#include "hal_gpio.h"
#include "auto_init.h"
#include "water_immersion.h"
// #include "flexible_button.h"
#include "hal_pwm.h"
#include "hal_adc.h"

/*********************common***********************************/
enum
{
    E_STATE_NOT_IN_WATER = 0,
    E_STATE_IN_WATER,
};
typedef struct
{
    uint8_t alarm_switch;     // 报警开关
    uint8_t alarm_status;     // 警报器状态
    uint8_t is_soak_in_water; // 是否浸水中
} dev_all_state;
static dev_all_state device_state_info = {
    .alarm_switch = 1,
    .alarm_status = false,
    .is_soak_in_water = E_STATE_NOT_IN_WATER,
};
/*****KEY*************************************************/
#define WATER_IMMER_KEY_IO (2)

#define WATERIMMER_DEV_NAME "adc1"
static AdcDevice_t waterimmer_dev = NULL;
#define ADC_DEV_CHANNEL 4

#define VAL_IN_WATER 1500
#define VAL_NOT_IN_WATER 1800

/*********************PWM*************************************/
#define TIMER_NUM_ALARM 1
#define IO_NUM_ALARM (8) // Define the output GPIO
#define CHANNEL_NUMALARM 2
#define DUTY_ALARM_OFF (0)
#define DUTY_ALARM_ON (4095)

static Device_t alarm_pwm_dev = NULL;

static struct PwmConfiguration_st pwm_cfg_alarm = {
    .channel = CHANNEL_NUMALARM,
    .speed_mode = 0,
    .period = 1000 * 347, // 单位ns,这里等价于2.88khz
    .timer_num = TIMER_NUM_ALARM,
    .pin_num = IO_NUM_ALARM,
    .duty = 0,
    .bit_resolution = 13,
};

void waterimmer_alarm_switch_set(uint8_t value)
{
    device_state_info.alarm_switch = value;
}

uint8_t waterimmer_alarm_switch_get(void)
{
    return device_state_info.alarm_switch;
}

uint8_t waterimmer_alarm_status_get(void)
{
    return device_state_info.alarm_status;
}


// 临时关闭报警
void waterimmer_alarm_mute(void)
{
    device_state_info.alarm_status = 0;
    pwm_cfg_alarm.duty = DUTY_ALARM_OFF;
    lHalDeviceControl(alarm_pwm_dev, PWM_CMD_SET_DUTY, (void *)&pwm_cfg_alarm);
    lHalDeviceControl(alarm_pwm_dev, PWM_CMD_UPDATE_DUTY, (void *)&pwm_cfg_alarm);
}

/**
 * 获取是否泡水
 */
uint8_t is_waterimmer_soak_in_water(void)
{
    printf("Get checkwater state\n");
    return device_state_info.is_soak_in_water;
}

void waterimmer_pwm_init(void)
{
    alarm_pwm_dev = pHalDeviceFind("pwm");
    if (alarm_pwm_dev == NULL)
    {
        printf("can not find pwm Model\n");
        return;
    }

    // 打开设备
    Err_t result = DEV_EOK;
    result = lHalDeviceOpen(alarm_pwm_dev, DEVICE_OFLAG_RDWR);
    if (result != DEV_EOK)
    {
        printf("can not open pwm device\n");
        return;
    }

    // 初始化设备
    lHalDeviceControl(alarm_pwm_dev, PWM_CMD_INIT, (void *)&pwm_cfg_alarm);
    pwm_cfg_alarm.duty = DUTY_ALARM_OFF;
    lHalDeviceControl(alarm_pwm_dev, PWM_CMD_SET_DUTY, (void *)&pwm_cfg_alarm);
    lHalDeviceControl(alarm_pwm_dev, PWM_CMD_UPDATE_DUTY, (void *)&pwm_cfg_alarm);
}

void waterimmer_check_water_task(void *arg)
{
    static uint32_t statekeep_t = 0;
    while (1)
    {
        static uint32_t average_val = 0;
        int MAX_CHECK_T = 20;
        static int acount_t = 0;

        if (acount_t < MAX_CHECK_T)
        {
            int voltage = uHalAdcRead(waterimmer_dev, ADC_DEV_CHANNEL);
            average_val += voltage;
            acount_t++;
        }
        else
        {
            acount_t = 0;
            average_val = average_val / MAX_CHECK_T;
            if (average_val < VAL_IN_WATER)
            {
                // 如果是从未浸水状态到浸水状态，则执行以下代码
                if (device_state_info.is_soak_in_water != E_STATE_IN_WATER)
                {
                    if (device_state_info.alarm_switch == 1)
                    {
                        device_state_info.alarm_status = 1;
                        // 蜂鸣器响起
                        pwm_cfg_alarm.duty = DUTY_ALARM_ON;
                        lHalDeviceControl(alarm_pwm_dev, PWM_CMD_SET_DUTY, (void *)&pwm_cfg_alarm);
                        lHalDeviceControl(alarm_pwm_dev, PWM_CMD_UPDATE_DUTY, (void *)&pwm_cfg_alarm);
                    }
                    else
                    {
                        // 蜂鸣器关闭
                        device_state_info.alarm_status = 0;
                        pwm_cfg_alarm.duty = DUTY_ALARM_OFF;
                        lHalDeviceControl(alarm_pwm_dev, PWM_CMD_SET_DUTY, (void *)&pwm_cfg_alarm);
                        lHalDeviceControl(alarm_pwm_dev, PWM_CMD_UPDATE_DUTY, (void *)&pwm_cfg_alarm);
                    }
                    device_state_info.is_soak_in_water = E_STATE_IN_WATER;
                }
            }
            else if (average_val > VAL_NOT_IN_WATER)
            {
                if (device_state_info.is_soak_in_water != E_STATE_NOT_IN_WATER)
                {
                    device_state_info.is_soak_in_water = E_STATE_NOT_IN_WATER;
                    device_state_info.alarm_status = 0;
                    pwm_cfg_alarm.duty = DUTY_ALARM_OFF;
                    lHalDeviceControl(alarm_pwm_dev, PWM_CMD_SET_DUTY, (void *)&pwm_cfg_alarm);
                    lHalDeviceControl(alarm_pwm_dev, PWM_CMD_UPDATE_DUTY, (void *)&pwm_cfg_alarm);
                }
            }
            average_val = 0;
        }
        vTaskDelay(pdMS_TO_TICKS(20));
    }
}

/*水浸检测IO*/
void waterimmer_check_io_init(void)
{
    waterimmer_dev = (AdcDevice_t)pHalDeviceFind(WATERIMMER_DEV_NAME);
    char *result_str = (waterimmer_dev == NULL) ? "failure" : "success";
    printf("probe %s \n", result_str);
    lHalAdcEnable(waterimmer_dev, 1 << ADC_DEV_CHANNEL);
    printf("Enable ADC\n");
    xTaskCreate(waterimmer_check_water_task, "check_water_task", 2048, NULL, 5, NULL);
}

static uint8_t ucDrvSensorReadKey(void)
{
    return lHalPinRead(WATER_IMMER_KEY_IO) ? 1 : 0;
}

static void user_button_scan(void)
{
    static uint32_t acount_press_t = 0;

    if (0 == ucDrvSensorReadKey())
    {
        acount_press_t++;
        if (acount_press_t == 4)
        {
            printf("Clear alarm --- >>\n");
            // 触发关闭报警
            if (1 == waterimmer_alarm_switch_get())
            {
                waterimmer_alarm_mute();
            }
        }
    }
    else
    {
        acount_press_t = 0;
    }
}

void waterimmer_key_scan_task(void *arg)
{
    while (1)
    {
        user_button_scan();
        vTaskDelay(pdMS_TO_TICKS(20));
    }
}

void waterimmer_key_init(void)
{
    printf("waterimmer key init ----------\n");
    vHalPinMode(WATER_IMMER_KEY_IO, PIN_MODE_INPUT_PULLUP);

    xTaskCreate(waterimmer_key_scan_task, "waterimmer_key_scan_task", 2048, NULL, 5, NULL);
}
