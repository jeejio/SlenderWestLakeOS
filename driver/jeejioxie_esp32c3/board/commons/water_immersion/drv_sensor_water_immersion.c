/*
 * Copyright (c) 2022, jeejio Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author         Notes
 * 2023-03-23     dengjiabao     the first version
 */

#include "device.h"
#include "hal_sensor.h" //sesor IO模型驱动框架头文件
#include "string.h"
#include "auto_init.h"
#include "water_immersion.h" //
#include "drv_sensor_water_immersion.h"

#define WATERIMMER_SET_ALARM_ONOFF (SENSOR_CTRL_USER_CMD_START + 1)
#define WATERIMMER_SET_ALARM_MUTE (SENSOR_CTRL_USER_CMD_START + 2)
#define WATERIMMER_GET_ALARM_STATUS (SENSOR_CTRL_USER_CMD_START + 3)
#define WATERIMMER_GET_ALARM_ONOFF (SENSOR_CTRL_USER_CMD_START + 4)
#define WATERIMMER_GET_SOAK_IN_WATER (SENSOR_CTRL_USER_CMD_START + 5)

Size_t sensorWaterImmerFetchData(struct SensorDevice_st *sensor, void *buf, Size_t len)
{
    uint8_t *send_buf = buf;
    Size_t cmd = sensor->parent.user_data;
    printf("Set Cmd: 0x%04x\n", cmd);
    switch (cmd)
    {
    case WATERIMMER_GET_ALARM_ONOFF:
    {
        send_buf[0] = waterimmer_alarm_switch_get();
    }
    break;
    case WATERIMMER_GET_SOAK_IN_WATER:
    {
        send_buf[0] = is_waterimmer_soak_in_water();
    }
    break;
    case WATERIMMER_GET_ALARM_STATUS:
    {
        send_buf[0] = waterimmer_alarm_status_get();
    }
    break;
    }
    return (1);
}

Err_t sensorWaterImmerControl(struct SensorDevice_st *sensor, int cmd, void *arg)
{
    switch (cmd)
    {
    case SENSOR_CTRL_SET_POWER:
    {
        int power_mode_type = (int)arg;
        switch (power_mode_type)
        {
        case SENSOR_POWER_NORMAL:
        {
            printf(" start : water immersion init \n");
            waterimmer_key_init();
            waterimmer_pwm_init();
            waterimmer_check_io_init();
            printf(" end   : water immersion init \n");
        }
        break;
        case SENSOR_POWER_DOWN:
        {
        }
        break;
        default:
            break;
        }
    }
    break;
    case WATERIMMER_SET_ALARM_ONOFF:
    {
        waterimmer_alarm_switch_set(*(bool *)arg);
    }
    break;
    case WATERIMMER_SET_ALARM_MUTE:
    {
        waterimmer_alarm_mute();
    }
    break;

    default:
        break;
    }

    return DEV_EOK;
}

static struct SensorOps_st sensor_ops =
    {
        sensorWaterImmerFetchData,
        sensorWaterImmerControl};

int sensorWaterImmersionInit(const char *name, struct SensorConfig_st *cfg)
{
    int8_t result;
    Sensor_t sensor = NULL;

    /* sensor register */
    sensor = pvPortMalloc(sizeof(struct SensorDevice_st));
    if (sensor == NULL)
        return -1;

    memset(sensor, 0, sizeof(struct SensorDevice_st));

    sensor->info.type = SENSOR_CLASS_HUMI; // FIXME!!!
    sensor->info.vendor = SENSOR_VENDOR_UNKNOWN;
    sensor->info.model = "water_immersion";

    if (cfg != NULL)
        memcpy(&sensor->config, cfg, sizeof(struct SensorConfig_st));

    sensor->ops = &sensor_ops;

    result = lHalHwSensorRegister(sensor, name, DEVICE_FLAG_RDONLY, NULL);
    if (result != DEV_EOK)
    {
        printf("result != DEV_EOK \n");
        goto __exit;
    }

    return DEV_EOK;

__exit:
    if (sensor)
        vPortFree(sensor);

    return -DEV_ERROR;
}

#define WATER_IMMER_NAME "GPIO_WATERLMMERSION"
int jeeHwSensorWaterImmersionInit(void)
{
    struct SensorConfig_st cfg = {0};
    cfg.intf.dev_name = WATER_IMMER_NAME;
    sensorWaterImmersionInit("WATERIMMER", &cfg);
    return DEV_EOK;
}

INIT_DEVICE_EXPORT(jeeHwSensorWaterImmersionInit);
