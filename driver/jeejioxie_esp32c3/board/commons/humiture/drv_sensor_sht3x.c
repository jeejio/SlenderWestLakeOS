/*
 * Copyright (c) 2022, jeejio Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author         Notes
 * 2023-03-23     df            the first version
 */

#include "drv_sensor_sht3x.h"
#include "device.h"
#include "hal_sensor.h" //sesor IO模型驱动框架头文件
#include "string.h"
#include "auto_init.h"
#include "hal_gpio.h"
#include "hal_pwm.h"
#include "sht3x.h" //厂家驱动头文件

#define SENSOR_HUMI_NAME "HUMI_SHT3X"

static const char *TAG = "HUMI";

enum
{
    SENSOR_HUMI_SET_TEMP_HIGH_ALARM_THREHOLD = SENSOR_CTRL_USER_CMD_START + 1,
    SENSOR_HUMI_SET_TEMP_LOW_ALARM_THREHOLD,
    SENSOR_HUMI_SET_TEMP_SENSITIVITY,
    SENSOR_HUMI_SET_HIGH_ALARM_THREHOLD,
    SENSOR_HUMI_SET_LOW_ALARM_THREHOLD,
    SENSOR_HUMI_SET_SENSITIVITY,
    SENSOR_HUMI_SET_CALLBACK,
};

static Size_t uDrvSensorHumiFetchData(struct SensorDevice_st *sensor, void *buf, Size_t len)
{
    if (buf == NULL)
        return 0;
    float *info = (float *)buf;
    int index = 0;
    info[index++] = iSht3xGetTemperature();
    info[index++] = iSht3xGetHumidity();
    info[index++] = iSht3xGetIsAlarm();
    info[index++] = iSht3xGetAlarmType();
    info[index++] = fSht3xGetHighTemperatureAlarmThreshold();
    info[index++] = fSht3xGetLowTemperatureAlarmThreshold();
    info[index++] = iSht3xGetHighHumidityAlarmThreshold();
    info[index++] = iSht3xGetLowHumidityAlarmThreshold();
    info[index++] = fSht3xGetTemperatureSensitivity();
    info[index++] = fSht3xGetHumiditySensitivity();
    return index;
}

static Err_t lDrvSensorHumiControl(struct SensorDevice_st *sensor, int cmd, void *arg)
{
    LOGI(TAG, "cmd %d \n", cmd);
    switch (cmd)
    {
    case SENSOR_CTRL_SET_POWER:
    {
        int power_mode_type = (int)arg;
        switch (power_mode_type)
        {
        case SENSOR_POWER_NORMAL:
        {
            LOGI(TAG, " start : vDrvSensorInit \n");
            vSht3xInit();
            LOGI(TAG, " end   : vDrvSensorInit \n");
        }
        break;
        case SENSOR_POWER_DOWN:
        {
            
        }
        break;
        default:
            break;
        }
    }
    break;
    case SENSOR_HUMI_SET_TEMP_HIGH_ALARM_THREHOLD:
    {
        float value = *((float *)arg);
        vSht3xSetHighTemperatureAlarmThreshold(value);
    }
    break;
    case SENSOR_HUMI_SET_TEMP_LOW_ALARM_THREHOLD:
    {
        float value = *((float *)arg);
        vSht3xSetLowTemperatureAlarmThreshold(value);
    }
    break;
    case SENSOR_HUMI_SET_TEMP_SENSITIVITY:
    {
        float value = *((float *)arg);
        vSht3xSetTemperatureSensitivity(value);
    }
    break;
    case SENSOR_HUMI_SET_HIGH_ALARM_THREHOLD:
    {
        int16_t value = *((int16_t *)arg);
        vSht3xSetHighHumidityAlarmThreshold(value);
    }
    break;
    case SENSOR_HUMI_SET_LOW_ALARM_THREHOLD:
    {
        int16_t value = *((int16_t *)arg);
        vSht3xSetLowHumidityAlarmThreshold(value);
    }
    break;
    case SENSOR_HUMI_SET_SENSITIVITY:
    {
        float value = *((float *)arg);
        vSht3xSetHumiditySensitivity(value);
    }
    break;
    case SENSOR_HUMI_SET_CALLBACK:
    {
        vSht3xRegisterEventCallback(arg);
    }
    break;
    default:break;
    }
    return DEV_EOK;
    ;
}

static struct SensorOps_st xSensorOps =
{
    uDrvSensorHumiFetchData,
    lDrvSensorHumiControl
};

static int lDrvSensorHumiInit(const char *name, struct SensorConfig_st *cfg)
{
    Int8_t result;
    Sensor_t sensor_humi = NULL;
    /* sensor register */
    sensor_humi = pvPortMalloc(sizeof(struct SensorDevice_st));
    if (sensor_humi == NULL)
        return -1;
    memset(sensor_humi, 0, sizeof(struct SensorDevice_st));
    sensor_humi->info.vendor = "xwalk";//厂商名
    sensor_humi->info.model = "humi_sht3x";//型号

    if (cfg != NULL)
        memcpy(&sensor_humi->config, cfg, sizeof(struct SensorConfig_st));

    sensor_humi->ops = &xSensorOps;

    result = lHalHwSensorRegister(sensor_humi, name, DEVICE_FLAG_RDWR, NULL);
    if (result != DEV_EOK)
    {
        LOGE(TAG,"device register err code: %d", result);
        goto __exit;
    }

    return DEV_EOK;

__exit:
    if (sensor_humi)
        vPortFree(sensor_humi);

    return -DEV_ERROR;
}

static int lDrvHwSensorHumiInit()
{
    LOGI(TAG,"begin to execute lDrvHwSensorInit\n");
    struct SensorConfig_st cfg = {0};
    cfg.intf.dev_name = SENSOR_HUMI_NAME;
    lDrvSensorHumiInit(SENSOR_HUMI_NAME, &cfg);
    LOGI(TAG,"finish to execute lDrvHwSensorInit\n");

    return DEV_EOK;
}

INIT_DEVICE_EXPORT(lDrvHwSensorHumiInit);
