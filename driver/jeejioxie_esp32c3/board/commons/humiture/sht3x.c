#include "sht3x.h"
#include <stdio.h>
#include "hal_serial.h"
#include "hal_sensor.h"
#include "hal/gpio_types.h"
#include "uart.h"
#include "string.h"
#include "hal_i2c.h"
#include "driver/i2c.h"

#define I2C_NAME "i2c0hard"
#define SHT3X_ADDR (0x44)

#define I2C_MASTER_SCL_IO 9         /*!< GPIO number used for I2C master clock */
#define I2C_MASTER_SDA_IO 8   

static struct I2cBusDevice_st* i2cDev = NULL;

static float tempHighAlarmThreshold = 0;
static float tempLowAlarmThreshold = 0;
static float tempSensitivity = 0.1;
static int16_t humiHighAlarmThreshold = 0;
static int16_t humiLowAlarmThreshold = 0;
static float humiSensitivity = 1;
static float temperature = 0;
static float humidity = 0;
static int16_t alarmState = 0;

static void (*event_update_callback)(float *data, uint8_t size) = NULL;

uint8_t calc_crc8(uint8_t *data, uint8_t len)
{
    uint8_t i;
    uint8_t byte;
    uint8_t crc = 0xff;
    for (byte = 0; byte < len; byte++)
    {
        crc ^= (data[byte]);
        for (i = 8; i > 0; --i)
        {
            if (crc & 0x80)
            {
                crc = (crc << 1) ^ 0x31;
            }
            else
            {
                crc = (crc << 1);
            }
        }
    }
    return crc;
}

static void sht3x_read(float* temp, float* humi)
{
    uint8_t cmd[2] = {0x24, 0x0B};
    ulHalI2cMasterSend(i2cDev, SHT3X_ADDR, I2C_STOP, cmd, 2);
    vTaskDelay(20 / portTICK_PERIOD_MS);
    uint8_t readData[6] = {0};
    ulHalI2cMasterRecv(i2cDev, SHT3X_ADDR, I2C_STOP, readData, 6);

    if (calc_crc8(readData, 2) == readData[2])
    {
	    *temp = (175.0f * ((uint16_t)((readData[0] << 8) | readData[1]) / 65535.0f) - 45.0f);    // T = -45 + 175 * tem / (2^16-1)
    }

    if ( calc_crc8(readData + 3, 2) == readData[5])
    {
        *humi = (100.0f * (uint16_t)((readData[3] << 8) | readData[4]) / 65535.0f);  
    }

    //printf("%d %d %d %d %d %d\n", readData[0], readData[1],readData[2],readData[3],readData[4],readData[5]);
    //printf("%f %f\n", *temp, *humi);
}

static void i2c_read_task(void *arg)
{
    float eventInfo[10];
    uint8_t infoIndex = 0;
    int16_t triggerEvent = 0;

    float curTemp = 0.0;
    float curHumi = 0.0;
    while (1)
    {
        if (!event_update_callback /*同时需要检测网络状态*/) /*如果回调未注册或者网络未连接，却发生了状态更新，则会上报一次事件，但事件却无法发送*/
        {
            vTaskDelay(1000 / portTICK_PERIOD_MS);
            continue;
        }

        curTemp = 0.;
        curHumi = 0.;
        sht3x_read(&curTemp, &curHumi);
        temperature = ((int)(curTemp * (1 / tempSensitivity))) * tempSensitivity;
        humidity = ((int)(curHumi * (1 / humiSensitivity))) * humiSensitivity;

        int16_t curAlarmState = 0;
        if(temperature < tempLowAlarmThreshold)
        {
            curAlarmState = 1;
        }
        else if(temperature > tempHighAlarmThreshold)
        {
            curAlarmState = 2;
        }

        if(humidity < humiLowAlarmThreshold)
        {
            curAlarmState |= (int16_t)1 << 8;
        }
        else if(humidity > humiHighAlarmThreshold)
        {
            curAlarmState |= (int16_t)2 << 8;
        }

        if (curAlarmState != alarmState)
        {
            alarmState = curAlarmState;
            triggerEvent = true;
        }
        
        if (triggerEvent)
        {
            triggerEvent = 0;
            if (event_update_callback)
            {
                infoIndex = 0;
                eventInfo[infoIndex++] = iSht3xGetTemperature();
                eventInfo[infoIndex++] = iSht3xGetHumidity();
                eventInfo[infoIndex++] = iSht3xGetIsAlarm();
                eventInfo[infoIndex++] = iSht3xGetAlarmType();
                eventInfo[infoIndex++] = fSht3xGetHighTemperatureAlarmThreshold();
                eventInfo[infoIndex++] = fSht3xGetLowTemperatureAlarmThreshold();
                eventInfo[infoIndex++] = iSht3xGetHighHumidityAlarmThreshold();
                eventInfo[infoIndex++] = iSht3xGetLowHumidityAlarmThreshold();
                eventInfo[infoIndex++] = fSht3xGetTemperatureSensitivity();
                eventInfo[infoIndex++] = fSht3xGetHumiditySensitivity();
                event_update_callback(eventInfo, infoIndex);
            }
        }

         vTaskDelay(200 / portTICK_PERIOD_MS);
    }
}

void vSht3xInit(void)
{
    i2cDev = (struct I2cBusDevice_st*)pHalDeviceFind(I2C_NAME);
    if (i2cDev == NULL)
    {
        printf("can not find %s Model\n", I2C_NAME);
        return;
    }

    // 打开设备
    Err_t result = DEV_EOK;
    result = lHalDeviceOpen(i2cDev, DEVICE_OFLAG_RDWR);
    if (result != DEV_EOK)
    {
        printf("can not open %s device\n", I2C_NAME);
        return;
    }

    xTaskCreate(i2c_read_task, "i2c_read_task", 1024 * 4, NULL, 10, NULL);
}

void vSht3xSetHighTemperatureAlarmThreshold(float value)
{
    tempHighAlarmThreshold = value;
}

void vSht3xSetLowTemperatureAlarmThreshold(float value)
{
    tempLowAlarmThreshold = value;
}

void vSht3xSetTemperatureSensitivity(float value)
{
    tempSensitivity = value;
}

void vSht3xSetHighHumidityAlarmThreshold(int16_t value)
{
    humiHighAlarmThreshold = value;
}

void vSht3xSetLowHumidityAlarmThreshold(int16_t value)
{
    humiLowAlarmThreshold = value;
}

void vSht3xSetHumiditySensitivity(float value)
{
    humiSensitivity = value;
}

float iSht3xGetTemperature(void)
{
    return temperature;
}

float iSht3xGetHumidity(void)
{
    return humidity;
}

int16_t iSht3xGetIsAlarm(void)
{
    return alarmState != 0;
}

int16_t iSht3xGetAlarmType(void)
{
    return alarmState;
}

float fSht3xGetHighTemperatureAlarmThreshold(void)
{
    return tempHighAlarmThreshold;
}

float fSht3xGetLowTemperatureAlarmThreshold(void)
{
    return tempLowAlarmThreshold;
}

int16_t iSht3xGetHighHumidityAlarmThreshold(void)
{
    return humiHighAlarmThreshold;
}

int16_t iSht3xGetLowHumidityAlarmThreshold(void)
{
    return humiLowAlarmThreshold;
}

float fSht3xGetTemperatureSensitivity(void)
{
    return tempSensitivity;
}

float fSht3xGetHumiditySensitivity(void)
{
    return humiSensitivity;
}

void vSht3xRegisterEventCallback(void (*cb)(float *int16_t, uint8_t size))
{
    event_update_callback = cb;
}
