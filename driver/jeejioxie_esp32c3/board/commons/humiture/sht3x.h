#ifndef __SHT3X_H
#define __SHT3X_H

#include "swldef.h"

void vSht3xInit(void);

void vSht3xSetHighTemperatureAlarmThreshold(float value);

void vSht3xSetLowTemperatureAlarmThreshold(float value);

void vSht3xSetTemperatureSensitivity(float value);

void vSht3xSetHighHumidityAlarmThreshold(int16_t value);

void vSht3xSetLowHumidityAlarmThreshold(int16_t value);

void vSht3xSetHumiditySensitivity(float value);

float iSht3xGetTemperature(void);

float iSht3xGetHumidity(void);

int16_t iSht3xGetIsAlarm(void);

int16_t iSht3xGetAlarmType(void);

float fSht3xGetHighTemperatureAlarmThreshold(void);

float fSht3xGetLowTemperatureAlarmThreshold(void);

int16_t iSht3xGetHighHumidityAlarmThreshold(void);

int16_t iSht3xGetLowHumidityAlarmThreshold(void);

float fSht3xGetTemperatureSensitivity(void);

float fSht3xGetHumiditySensitivity(void);

void vSht3xRegisterEventCallback(void (*cb)(float *data, uint8_t size));

#endif