#ifndef __JW01_H
#define __JW01_H

#include "swldef.h"

void vJw01Init(void);

void vJw01setAlarmThreshold(int16_t value);

int16_t iJw01GetAlarmThreshold(void);

int16_t iJw01GetAlarmStatus(void);

int16_t iJw01GetGetDensity(void);

void vJw01RegisterEventCallback(void (*cb)(int16_t *data, uint8_t size));

#endif