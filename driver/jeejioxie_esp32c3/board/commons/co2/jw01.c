#include "jw01.h"
#include <stdio.h>
#include "hal_serial.h"
#include "hal_sensor.h"
#include "drv_uart_iomodel.h"
#include "hal/uart_types.h"
#include "hal/gpio_types.h"
#include "uart.h"
#include "string.h"

#define UART1_NAME "uart1"
static Device_t uart1Dev = NULL;

#define BUF_SIZE (128)

static int16_t co2AlarmThreshold = 0;
static int16_t co2AlarmStatus = 0;
static int16_t co2Density = 0;

static void (*event_update_callback)(int16_t *data, uint8_t size) = NULL;

static void rx_task(void *arg)
{
    int16_t eventInfo[2];
    uint8_t infoIndex = 0;

    uint8_t *rdata = (uint8_t *)malloc(BUF_SIZE);
    memset(rdata, 0, BUF_SIZE);

    struct uart_plat_data {
        Uint8_t *rxBuf;
        Uint8_t *txBuf;
        Uint32_t bufSize;
        Int32_t  real_size;
        Uint32_t intr_flag;
        intr_handler_t cb;
        void *cb_arg;
        int rd_len;
        Uint32_t rxfifo_len;
    }xUartData;

    xUartData.rxBuf = rdata;
    xUartData.bufSize = BUF_SIZE;

    int16_t curDensity = 0;
    int16_t curAlarmStatus = 0;
    int16_t triggerEvent = 0;

    while (1)
    {
        lHalDeviceControl(uart1Dev, DEVICE_CTRL_UART_READ, (void *)&xUartData);

        if (xUartData.real_size > 0)
        {
            bool isVaildData = xUartData.real_size == 6;

            uint8_t verifyNum = 0;
            if (isVaildData)
            {
                for (size_t i = 0; i < 5; i++)
                {
                    verifyNum += xUartData.rxBuf[i];
                }
                isVaildData = verifyNum == xUartData.rxBuf[5];
            }
            
            if (isVaildData)
            {
                curDensity = xUartData.rxBuf[1] * 256 + xUartData.rxBuf[2];
            }

            memset(xUartData.rxBuf, 0, BUF_SIZE);
        }

        if (curDensity != co2Density)
        {
            co2Density = curDensity;
            //triggerEvent = 1;
        }

        curAlarmStatus = co2Density > co2AlarmThreshold;
        if (curAlarmStatus != co2AlarmStatus)
        {
            co2AlarmStatus = curAlarmStatus;
            triggerEvent = 1;
        }
        
        if (triggerEvent)
        {
            triggerEvent = 0;
            if (event_update_callback)
            {
                infoIndex = 0;
                eventInfo[infoIndex++] = iJw01GetGetDensity();
                eventInfo[infoIndex++] = iJw01GetAlarmStatus();
                event_update_callback(eventInfo, infoIndex);
            }
        }

         vTaskDelay(200 / portTICK_PERIOD_MS);

    }
    free(rdata);
}

void vJw01Init(void)
{
    uart1Dev = pHalDeviceFind(UART1_NAME);
    if (uart1Dev == NULL)
    {
        printf("can not find %s Model\n", UART1_NAME);
        return;
    }
    
    // 打开设备
    Err_t result = DEV_EOK;
    result = lHalDeviceOpen(uart1Dev, DEVICE_OFLAG_RDWR);
    if (result != DEV_EOK)
    {
        printf("can not open %s device\n", UART1_NAME);
        return;
    }

    xTaskCreate(rx_task, "uart_rx_task", 1024 * 4, NULL, 10, NULL);
}

void vJw01setAlarmThreshold(int16_t value)
{
    co2AlarmThreshold = value;
}

int16_t iJw01GetAlarmThreshold(void)
{
    return co2AlarmThreshold;
}

int16_t iJw01GetAlarmStatus(void)
{
    return co2AlarmStatus;
}

int16_t iJw01GetGetDensity(void)
{
    return co2Density;
}

void vJw01RegisterEventCallback(void (*cb)(int16_t *int16_t, uint8_t size))
{
    event_update_callback = cb;
}
