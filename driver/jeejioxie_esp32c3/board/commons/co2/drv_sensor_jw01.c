/*
 * Copyright (c) 2022, jeejio Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author         Notes
 * 2023-03-23     df            the first version
 */

#include "drv_sensor_jw01.h"
#include "device.h"
#include "hal_sensor.h" //sesor IO模型驱动框架头文件
#include "string.h"
#include "auto_init.h"
#include "hal_gpio.h"
#include "hal_pwm.h"
#include "jw01.h" //厂家驱动头文件

#define SENSOR_CO2_NAME "CO2_JW01"

static const char *TAG = "CO2";

enum
{
    SENSOR_CO2_SET_ALARM_THREHOLD = SENSOR_CTRL_USER_CMD_START + 1,
    SENSOR_CO2_SET_CALLBACK,
};

static Size_t uDrvSensorCo2FetchData(struct SensorDevice_st *sensor, void *buf, Size_t len)
{
    if (buf == NULL)
        return 0;
    int16_t *info = (int16_t *)buf;
    int index = 0;
    info[index++] = iJw01GetAlarmThreshold();
    info[index++] = iJw01GetAlarmStatus();
    info[index++] = iJw01GetGetDensity();
    return index;
}

static Err_t lDrvSensorCo2Control(struct SensorDevice_st *sensor, int cmd, void *arg)
{
    LOGI(TAG, "cmd %d \n", cmd);
    switch (cmd)
    {
    case SENSOR_CTRL_SET_POWER:
    {
        int power_mode_type = (int)arg;
        switch (power_mode_type)
        {
        case SENSOR_POWER_NORMAL:
        {
            LOGI(TAG, " start : vDrvSensorInit \n");
            vJw01Init();
            LOGI(TAG, " end   : vDrvSensorInit \n");
        }
        break;
        case SENSOR_POWER_DOWN:
        {
            
        }
        break;
        default:
            break;
        }
    }
    break;
    case SENSOR_CO2_SET_ALARM_THREHOLD:
    {
        int16_t value = *((int16_t *)arg);
        vJw01setAlarmThreshold(value);
    }
    break;
    case SENSOR_CO2_SET_CALLBACK:
    {
        vJw01RegisterEventCallback(arg);
    }
    break;
    default:
        break;
    }
    return DEV_EOK;
    ;
}

static struct SensorOps_st xSensorOps =
{
    uDrvSensorCo2FetchData,
    lDrvSensorCo2Control
};

static int lDrvSensorCo2Init(const char *name, struct SensorConfig_st *cfg)
{
    Int8_t result;
    Sensor_t sensor_co2 = NULL;
    /* sensor register */
    sensor_co2 = pvPortMalloc(sizeof(struct SensorDevice_st));
    if (sensor_co2 == NULL)
        return -1;
    memset(sensor_co2, 0, sizeof(struct SensorDevice_st));
    sensor_co2->info.vendor = "xwalk";//厂商名
    sensor_co2->info.model = "co2_jw01";//型号

    if (cfg != NULL)
        memcpy(&sensor_co2->config, cfg, sizeof(struct SensorConfig_st));

    sensor_co2->ops = &xSensorOps;

    result = lHalHwSensorRegister(sensor_co2, name, DEVICE_FLAG_RDWR, NULL);
    if (result != DEV_EOK)
    {
        LOGE(TAG,"device register err code: %d", result);
        goto __exit;
    }

    return DEV_EOK;

__exit:
    if (sensor_co2)
        vPortFree(sensor_co2);

    return -DEV_ERROR;
}

static int lDrvHwSensorCo2Init()
{
    LOGI(TAG,"begin to execute lDrvHwSensorInit\n");
    struct SensorConfig_st cfg = {0};
    cfg.intf.dev_name = SENSOR_CO2_NAME;
    lDrvSensorCo2Init(SENSOR_CO2_NAME, &cfg);
    LOGI(TAG,"finish to execute lDrvHwSensorInit\n");

    return DEV_EOK;
}

INIT_DEVICE_EXPORT(lDrvHwSensorCo2Init);
