/*
 * Copyright (c) 2022, jeejio Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author         Notes
 * 2023-03-23     df            the first version
 */

#include "drv_sensor_atgm336h.h"
#include "device.h"
#include "hal_sensor.h" //sesor IO模型驱动框架头文件
#include "string.h"
#include "auto_init.h"
#include "hal_gpio.h"
#include "hal_pwm.h"
#include "atgm336h.h" //厂家驱动头文件

#define SENSOR_GPS_NAME "GPS_ATGM336H"

static const char *TAG = "GPS";

enum
{
    SENSOR_GPS_SET_REPORT_FREQUENCY = SENSOR_CTRL_USER_CMD_START + 1,
    SENSOR_GPS_SET_CALLBACK,
};

static Size_t uDrvSensorGpsFetchData(struct SensorDevice_st *sensor, void *buf, Size_t len)
{
    if (buf == NULL)
        return 0;
    float *info = (float *)buf;
    int index = 0;
    info[index++] = fAtgm336hGetLongitude();
    info[index++] = fAtgm336hGetLatitude();
    info[index++] = fAtgm336hGetAltitude();
    info[index++] = fAtgm336hGetReportFrequency();
    info[index++] = fAtgm336hGetSignalIntensity();
    return index;
}

static Err_t lDrvSensorGpsControl(struct SensorDevice_st *sensor, int cmd, void *arg)
{
    LOGI(TAG, "cmd %d \n", cmd);
    switch (cmd)
    {
    case SENSOR_CTRL_SET_POWER:
    {
        int power_mode_type = (int)arg;
        switch (power_mode_type)
        {
        case SENSOR_POWER_NORMAL:
        {
            LOGI(TAG, " start : vDrvSensorInit \n");
            vAtgm336hInit();
            LOGI(TAG, " end   : vDrvSensorInit \n");
        }
        break;
        case SENSOR_POWER_DOWN:
        {
            
        }
        break;
        default:
            break;
        }
    }
    break;
    case SENSOR_GPS_SET_REPORT_FREQUENCY:
    {
        int16_t bright = *((int16_t *)arg);
        vAtgm336hSetReportFrequency(bright);
    }
    break;
    case SENSOR_GPS_SET_CALLBACK:
    {
        vAtgm336hRegisterEventCallback(arg);
    }
    break;
    default:
        break;
    }
    return DEV_EOK;
    ;
}

static struct SensorOps_st xSensorOps =
{
    uDrvSensorGpsFetchData,
    lDrvSensorGpsControl
};

static int lDrvSensorGpsInit(const char *name, struct SensorConfig_st *cfg)
{
    Int8_t result;
    Sensor_t sensor_gps = NULL;
    /* sensor register */
    sensor_gps = pvPortMalloc(sizeof(struct SensorDevice_st));
    if (sensor_gps == NULL)
        return -1;
    memset(sensor_gps, 0, sizeof(struct SensorDevice_st));
    sensor_gps->info.vendor = "xwalk";//厂商名
    sensor_gps->info.model = "gps_atgm336h";//型号

    if (cfg != NULL)
        memcpy(&sensor_gps->config, cfg, sizeof(struct SensorConfig_st));

    sensor_gps->ops = &xSensorOps;

    result = lHalHwSensorRegister(sensor_gps, name, DEVICE_FLAG_RDWR, NULL);
    if (result != DEV_EOK)
    {
        LOGE(TAG,"device register err code: %d", result);
        goto __exit;
    }

    return DEV_EOK;

__exit:
    if (sensor_gps)
        vPortFree(sensor_gps);

    return -DEV_ERROR;
}

static int lDrvHwSensorGpsInit()
{
    LOGI(TAG,"begin to execute lDrvHwSensorInit\n");
    struct SensorConfig_st cfg = {0};
    cfg.intf.dev_name = SENSOR_GPS_NAME;
    lDrvSensorGpsInit(SENSOR_GPS_NAME, &cfg);
    LOGI(TAG,"finish to execute lDrvHwSensorInit\n");

    return DEV_EOK;
}

INIT_DEVICE_EXPORT(lDrvHwSensorGpsInit);
