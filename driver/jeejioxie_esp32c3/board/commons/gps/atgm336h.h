#ifndef __ATGM336H_H
#define __ATGM336H_H

#include "swldef.h"

void vAtgm336hInit(void);

void vAtgm336hSetReportFrequency(int16_t value);

float fAtgm336hGetLongitude(void);

float fAtgm336hGetLatitude(void);

int16_t fAtgm336hGetAltitude(void);

int16_t fAtgm336hGetReportFrequency(void);

int16_t fAtgm336hGetSignalIntensity(void);

void vAtgm336hRegisterEventCallback(void (*cb)(float *data, uint8_t size));

#endif