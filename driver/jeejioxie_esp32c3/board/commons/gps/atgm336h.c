#include "atgm336h.h"
#include <stdio.h>
#include "hal_serial.h"
#include "hal_sensor.h"
#include "drv_uart_iomodel.h"
#include "hal/uart_types.h"
#include "hal/gpio_types.h"
#include "uart.h"
#include "string.h"

#define UART1_NAME "uart1"
static Device_t uart1Dev = NULL;

#define BUF_SIZE (128)

static int16_t reportFrequency = 1;
static float gpsLongitude = 0.;
static float gpsLatitude = 0.;
static int16_t gpsAltitude = 0.;
static int16_t gpsSignalIntensity = 0.;
static void (*event_update_callback)(float *data, uint8_t size) = NULL;
static int16_t curTick = 0;

static struct uart_plat_data {
    Uint8_t *rxBuf;
    Uint8_t *txBuf;
    Uint32_t bufSize;
    Int32_t  real_size;
    Uint32_t intr_flag;
    intr_handler_t cb;
    void *cb_arg;
    int rd_len;
    Uint32_t rxfifo_len;
}xUartData;

static char* get_split_string(const char* orgStr, char split, int index)
{
    char* ret = NULL;

    int startPos = 0;
    int endPos = 0;

    const char* pTmpStr = orgStr;
    if(index == 0)
    {
        endPos = strlen(orgStr);
        pTmpStr = strchr(pTmpStr, split);
        if(pTmpStr != NULL)
        {
            endPos = pTmpStr - orgStr;
        }
    }
    else
    {
        for(int i = 0; i < index && pTmpStr != NULL; i++)
        {
            if(i == 0)
            {
                pTmpStr = strchr(pTmpStr, split);
            }
            else
            {
                pTmpStr = strchr(pTmpStr + 1, split);
            }
        }
        
        if(pTmpStr != NULL)
        {
            startPos = pTmpStr - orgStr + 1;
            endPos = strlen(orgStr);
            pTmpStr = strchr(pTmpStr + 1, split);
            if(pTmpStr != NULL)
            {
                endPos = pTmpStr - orgStr;
            }
        }
    }

    if(endPos >= startPos)
    {
        ret = (char*)malloc(endPos - startPos + 1);
        memset(ret, 0, endPos - startPos + 1);
        memcpy(ret, orgStr + startPos, endPos - startPos);
    }

    return ret;
}

//经度
float getLongitude(const char* valStr)
{
    char intPart[4] = {0};
    memcpy(intPart, valStr, 3);
    return atof(valStr + 3) / 60. + atoi(intPart);
}

//纬度
float getLatitude(const char* valStr)
{
    char intPart[3] = {0};
    memcpy(intPart, valStr, 2);
    return atof(valStr + 2) / 60. + atoi(intPart);
}

static void rx_task(void *arg)
{
    static float eventInfo[5];
    uint8_t infoIndex = 0;

    uint8_t rdata = 0;

    uint8_t *frameData = (uint8_t *)malloc(BUF_SIZE);
    memset(frameData, 0, BUF_SIZE);
    uint8_t rIndex = 0;

    xUartData.rxBuf = &rdata;
    xUartData.bufSize = 1;

    while (1)
    {
        rIndex = 0;
        do
        {
            rdata = 0;
            lHalDeviceControl(uart1Dev, DEVICE_CTRL_UART_READ, (void *)&xUartData);
            if (xUartData.real_size == 1)
            {
                frameData[rIndex++] = rdata;
            }
            
        }while (xUartData.real_size == 1 && rdata != '\n');
        
        bool isValidData = rIndex > 0 && frameData[0] == '$';
    
        if(isValidData)
        {
            char checkSumStr[3] = {0};
            uint8_t checkSum = 0;
            int i = 0;
            for (i = 1; i < rIndex && frameData[i] != '*'; i++)
            {                
                checkSum ^= frameData[i];
            }
            sprintf(checkSumStr, "%02X", checkSum);
            if (i+2 < rIndex &&
                checkSumStr[0] == frameData[i+1] &&
                checkSumStr[1] == frameData[i+2])
            {
                memset(frameData + i, 0, rIndex - i);
            }
            else
            {
                isValidData = false;
                //printf("check sum : %s | %s\n", checkSumStr, frameData);
            }
        }

        if (isValidData)
        {
            if (strstr((char*)frameData, "$GNRMC") != NULL)
            {//经纬度
                char* str2 = get_split_string((char*)frameData, ',', 2);//A=有效定位，V=无效定位
                if (str2)
                {
                    if (strcmp(str2, "A") == 0)
                    {
                        char* str3 = get_split_string((char*)frameData, ',', 3);//纬度ddmm.mmmm（度分）
                        if (str3)
                        {
                            gpsLatitude = getLatitude(str3);
                            free(str3);
                        }
                        
                        char* str5 = get_split_string((char*)frameData, ',', 5);//经度dddmm.mmmm（度分）
                        if (str5)
                        {
                            gpsLongitude = getLongitude(str3);
                            free(str5);
                        }
                    }
                    
                    free(str2);
                }
            }
            else if (strstr((char*)frameData, "$GPGSV") != NULL)
            {//信号强度
                //printf("%s\n", frameData);
                int16_t signalDb = 0;
                int16_t tmpSignalDb = 0;
                for(int i = 0; i < 4; i++)
                {
                    char* str7 = get_split_string((char*)frameData, ',', 7 + i * 4);//信噪比（C/No），00至99dB
                    if (str7)
                    {
                        if(strlen(str7))
                        {
                            tmpSignalDb = atoi(str7);
                            if (signalDb < tmpSignalDb)
                            {
                                signalDb = tmpSignalDb;
                            }
                        }
                        free(str7);
                    }
                }
                gpsSignalIntensity =  signalDb;
            }
            else if (strstr((char*)frameData, "$GNGGA") != NULL)
            {//海拔
                char* str9 = get_split_string((char*)frameData, ',', 9);//海拔高度（-9999.9~99999.9）
                if (str9)
                {
                    gpsAltitude = atoi(str9);
                    free(str9);
                }
            }
        }
        
         memset(frameData , 0, BUF_SIZE);

         vTaskDelay(50 / portTICK_PERIOD_MS);

         curTick++;

         if (curTick >= reportFrequency * 20 * 60)
         {
            curTick = 0;
            if (event_update_callback)
            {
                infoIndex = 0;
                eventInfo[infoIndex++] = fAtgm336hGetLongitude();
                eventInfo[infoIndex++] = fAtgm336hGetLatitude();
                eventInfo[infoIndex++] = fAtgm336hGetAltitude();
                eventInfo[infoIndex++] = fAtgm336hGetReportFrequency();
                eventInfo[infoIndex++] = fAtgm336hGetSignalIntensity();
                event_update_callback(eventInfo, infoIndex);
            }
         }
    }
    free(frameData);
}

void vAtgm336hInit(void)
{
    uart1Dev = pHalDeviceFind(UART1_NAME);
    if (uart1Dev == NULL)
    {
        printf("can not find %s Model\n", UART1_NAME);
        return;
    }
    
    // 打开设备
    Err_t result = DEV_EOK;
    result = lHalDeviceOpen(uart1Dev, DEVICE_OFLAG_RDWR);
    if (result != DEV_EOK)
    {
        printf("can not open %s device\n", UART1_NAME);
        return;
    }

    xTaskCreate(rx_task, "uart_rx_task", 1024 * 4, NULL, 10, NULL);
}

void vAtgm336hSetReportFrequency(int16_t value)
{
    if (value > 0)
    {
         reportFrequency = value;
    }
}

float fAtgm336hGetLongitude(void)
{
    return gpsLongitude;
}

float fAtgm336hGetLatitude(void)
{
    return gpsLatitude;
}

int16_t fAtgm336hGetAltitude(void)
{
    return gpsAltitude;
}

int16_t fAtgm336hGetReportFrequency(void)
{
    return reportFrequency;
}

int16_t fAtgm336hGetSignalIntensity(void)
{
    return gpsSignalIntensity;
}

void vAtgm336hRegisterEventCallback(void (*cb)(float *data, uint8_t size))
{
    event_update_callback = cb;
}
