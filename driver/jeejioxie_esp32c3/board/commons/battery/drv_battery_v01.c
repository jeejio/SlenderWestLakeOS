/*
 * Copyright (c) 2022, jeejio Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author         Notes
 * 2023-05-19     amerk.LBH         the first version
 */
#include "drv_battery_v01.h"
#include "device.h"
#include "hal_sensor.h"        //sesor IO模型驱动框架头文件
#include "battery_v01.h"      //厂家驱动头文件  :电子锁模块
#include "string.h"
#include "auto_init.h"

#define ELECTRIC_LOCK_DEV_NAME "BATTERY"
static const char *TAG = "BATTERY V01";

Size_t uSensorBatteryFetchData(struct SensorDevice_st *sensor, void *buf, Size_t len)
{
    if (buf == NULL)
        return 0;

    ((int *)buf)[0] = lGetBatteryLeveL();
    ((int *)buf)[1] = lGetBatteryChargeStatus();
    ((int *)buf)[2] = lGetBatterydDischargeTime();
    ((int *)buf)[3] = lGetBatteryChargeTime();
    return 4;
}

Err_t lSensorBatteryControl(struct SensorDevice_st *sensor, int cmd, void *arg)
{
     LOGI(TAG, "cmd %d \n", cmd);
    switch (cmd)
    {
    case SENSOR_CTRL_SET_POWER: //设备上下电
    {
        int power_mode_type = (int)arg;
        switch (power_mode_type)
        {
        case SENSOR_POWER_NORMAL:
        {
            printf(" start :  lBatteryInit \n");
            lBatteryInit();
            printf(" end   : lBatteryInit \n");
        }
        break;
        case SENSOR_POWER_DOWN:  //设备下电
        {
            
        }
        break;
        default:
            break;
        }
    }
    break;
    default:
        break;
    }

    return DEV_EOK;
}

static struct SensorOps_st xSensorOps =
 {
        uSensorBatteryFetchData,
        lSensorBatteryControl
 };

int lSensorBatteryInit(const char *name, struct SensorConfig_st *cfg)
{
    Int8_t result;
    Sensor_t sensor = NULL;

    /* sensor register */
    sensor = pvPortMalloc(sizeof(struct SensorDevice_st));
    if (sensor == NULL)
        return -1;

    memset(sensor, 0, sizeof(struct SensorDevice_st));

    //sensor->info.type = SENSOR_CLASS_IOEXPAND;
    sensor->info.vendor = "qiu";
    sensor->info.model = "BATTERY_V01";

    if (cfg != NULL)
        memcpy(&sensor->config, cfg, sizeof(struct SensorConfig_st));

    sensor->ops = &xSensorOps;

    result = lHalHwSensorRegister(sensor, name, DEVICE_FLAG_RDWR, NULL);
    if (result != DEV_EOK)
    {
        printf("result != DEV_EOK \n");
        goto __exit;
    }

    return DEV_EOK;

__exit:
    if (sensor)
        vPortFree(sensor);

    return -DEV_ERROR;
}


int lDrvHwSensorBatteryInit()
{
    struct SensorConfig_st cfg = {0};
    cfg.intf.dev_name = ELECTRIC_LOCK_DEV_NAME;
    lSensorBatteryInit(ELECTRIC_LOCK_DEV_NAME, &cfg);
    return DEV_EOK;
}

INIT_DEVICE_EXPORT(lDrvHwSensorBatteryInit);
