#ifndef __BATTERY_V01_H__
#define __BATTERY_V01_H__

typedef enum 
{
    CHARGING=1,
    NOTCHARGING,
    CHARGFULL,
    CHARG_ERR=-1
}BatteryChargStatus_t;



int lBatteryInit(void);

int lGetBatteryLeveL(void);

BatteryChargStatus_t lGetBatteryChargeStatus(void);

int lGetBatterydDischargeTime(void);

int lGetBatteryChargeTime(void);


#endif
