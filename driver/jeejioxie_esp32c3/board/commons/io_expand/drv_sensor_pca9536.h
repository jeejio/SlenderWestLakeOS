/*
 * Copyright (c) 2022, jeejio Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author         Notes
 * 2022-12-13     zhengqian      the first version
 */

#ifndef __JEE_SENSOR_PCA9536_H__
#define __JEE_SENSOR_PCA9536_H__

#include "hal_sensor.h" //sensor IO模型驱动框架头文件


int jeeHwSensorPca9536Init();

#endif