/*
 * Copyright (c) 2022, jeejio Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author         Notes
 * 2023-03-23     df            the first version
 */

#include "drv_sensor_tcs3472.h"
#include "device.h"
#include "hal_sensor.h" //sesor IO模型驱动框架头文件
#include "string.h"
#include "auto_init.h"
#include "hal_gpio.h"
#include "hal_pwm.h"
#include "tcs3472.h" //厂家驱动头文件

#define SENSOR_NAME "COLOR_TCS3472"

static const char *TAG = "COLOR";

enum
{
    SENSOR_COLOR_SET_SWITCH = SENSOR_CTRL_USER_CMD_START + 1,
    SENSOR_COLOR_RESET,
    SENSOR_COLOR_SET_CALLBACK,
};

static Size_t uDrvSensorColorFetchData(struct SensorDevice_st *sensor, void *buf, Size_t len)
{
    if (buf == NULL)
        return 0;
    int16_t *info = (int16_t *)buf;
    int index = 0;
    info[index++] = iTcs3472GetSwitch();
    info[index++] = iTcs3472GetColorR();
    info[index++] = iTcs3472GetColorG();
    info[index++] = iTcs3472GetColorB();
    return index;
}

static Err_t lDrvSensorColorControl(struct SensorDevice_st *sensor, int cmd, void *arg)
{
    LOGI(TAG, "cmd %d \n", cmd);
    switch (cmd)
    {
    case SENSOR_CTRL_SET_POWER:
    {
        int power_mode_type = (int)arg;
        switch (power_mode_type)
        {
        case SENSOR_POWER_NORMAL:
        {
            LOGI(TAG, " start : vDrvSensorInit \n");
            vTcs3472Init();
            LOGI(TAG, " end   : vDrvSensorInit \n");
        }
        break;
        case SENSOR_POWER_DOWN:
        {
            
        }
        break;
        default:
            break;
        }
    }
    break;
    case SENSOR_COLOR_SET_SWITCH:
    {
        int16_t value = *((int16_t *)arg);
        vTcs3472SetSwitch(value);
    }
    break;
    case SENSOR_COLOR_RESET:
    {
        vTcs3472Reset();
    }
    break;
    case SENSOR_COLOR_SET_CALLBACK:
    {
        vTcs3472RegisterEventCallback(arg);
    }
    break;
    default:break;
    }
    return DEV_EOK;
    ;
}

static struct SensorOps_st xSensorOps =
{
    uDrvSensorColorFetchData,
    lDrvSensorColorControl
};

static int lDrvSensorColorInit(const char *name, struct SensorConfig_st *cfg)
{
    Int8_t result;
    Sensor_t sensor_color = NULL;
    /* sensor register */
    sensor_color = pvPortMalloc(sizeof(struct SensorDevice_st));
    if (sensor_color == NULL)
        return -1;
    memset(sensor_color, 0, sizeof(struct SensorDevice_st));
    sensor_color->info.vendor = "xwalk";//厂商名
    sensor_color->info.model = "color_tcs3472";//型号

    if (cfg != NULL)
        memcpy(&sensor_color->config, cfg, sizeof(struct SensorConfig_st));

    sensor_color->ops = &xSensorOps;

    result = lHalHwSensorRegister(sensor_color, name, DEVICE_FLAG_RDWR, NULL);
    if (result != DEV_EOK)
    {
        LOGE(TAG,"device register err code: %d", result);
        goto __exit;
    }

    return DEV_EOK;

__exit:
    if (sensor_color)
        vPortFree(sensor_color);

    return -DEV_ERROR;
}

static int lDrvHwSensorColorInit()
{
    LOGI(TAG,"begin to execute lDrvHwSensorInit\n");
    struct SensorConfig_st cfg = {0};
    cfg.intf.dev_name = SENSOR_NAME;
    lDrvSensorColorInit(SENSOR_NAME, &cfg);
    LOGI(TAG,"finish to execute lDrvHwSensorInit\n");

    return DEV_EOK;
}

INIT_DEVICE_EXPORT(lDrvHwSensorColorInit);
