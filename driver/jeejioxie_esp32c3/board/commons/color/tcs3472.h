#ifndef __TCS3472_H
#define __TCS3472_H

#include "swldef.h"

void vTcs3472Init(void);

void vTcs3472SetSwitch(int16_t value);

void vTcs3472Reset();

int16_t iTcs3472GetSwitch(void);

int16_t iTcs3472GetColorR(void);

int16_t iTcs3472GetColorG(void);

int16_t iTcs3472GetColorB(void);

void vTcs3472RegisterEventCallback(void (*cb)(int16_t *data, uint8_t size));

#endif