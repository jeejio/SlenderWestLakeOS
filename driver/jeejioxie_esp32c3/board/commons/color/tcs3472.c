#include "tcs3472.h"
#include <stdio.h>
#include "hal_serial.h"
#include "hal_sensor.h"
#include "hal/gpio_types.h"
#include "string.h"
#include "hal_i2c.h"
#include "driver/i2c.h"
#include "math.h"

#define TCS34725_ID               (0x12)    /* 0x44 = TCS34721/TCS34725, 0x4D = TCS34723/TCS34727 */
#define TCS34725_COMMAND_BIT      (0x80)

#define TCS34725_ENABLE           (0x00)
#define TCS34725_ATIME            (0x01)    /* Integration time */
#define TCS34725_CONTROL          (0x0F)    /* Set the gain level for the sensor */

#define TCS34725_STATUS           (0x13)

#define TCS34725_CDATAL           (0x14)    /* Clear channel data */
#define TCS34725_CDATAH           (0x15)
#define TCS34725_RDATAL           (0x16)    /* Red channel data */
#define TCS34725_RDATAH           (0x17)
#define TCS34725_GDATAL           (0x18)    /* Green channel data */
#define TCS34725_GDATAH           (0x19)
#define TCS34725_BDATAL           (0x1A)    /* Blue channel data */
#define TCS34725_BDATAH           (0x1B)

#define TCS34725_INTEGRATIONTIME_240MS    0x9C   /**<  50ms  - 20 cycles  - Max Count: 20480 */
#define TCS34725_GAIN_1X                  0x00  // 1X增益
#define TCS34725_GAIN_4X                  0x01   // 4X增益
#define TCS34725_GAIN_16X                 0x02   // 16X增益
#define TCS34725_GAIN_60X                 0x03    // 60X增益


#define I2C_NAME "i2c0hard"
#define TCS3472_ADDR (0x29)

#define I2C_MASTER_SCL_IO 9         /*!< GPIO number used for I2C master clock */
#define I2C_MASTER_SDA_IO 8   

static struct I2cBusDevice_st* i2cDev = NULL;

static int16_t moudleSwitch = 0;
static int16_t colorR = 0;
static int16_t colorG = 0;
static int16_t colorB = 0;
static int16_t reset_flag = 0;

static void (*event_update_callback)(int16_t *data, uint8_t size) = NULL;

static bool tcs3472_read_addr(uint8_t subAddr, uint8_t* dataBuffer, uint8_t bytesNumber)
{
    uint8_t cmd[1] = {TCS34725_COMMAND_BIT | subAddr};
    ulHalI2cMasterSend(i2cDev, TCS3472_ADDR, 0, cmd, sizeof(cmd));
    ulHalI2cMasterRecv(i2cDev, TCS3472_ADDR, I2C_STOP, dataBuffer, bytesNumber);

    return true;
}

static void tcs3472_write_addr(uint8_t subAddr, uint8_t* dataBuffer, uint8_t bytesNumber)
{    
    uint8_t cmd[1] = {TCS34725_COMMAND_BIT | subAddr};
    ulHalI2cMasterSend(i2cDev, TCS3472_ADDR, 0, cmd, sizeof(cmd));
	ulHalI2cMasterSend(i2cDev, TCS3472_ADDR, I2C_STOP, dataBuffer, bytesNumber);
}

static bool tcs3472_read_color(uint16_t* c, uint16_t* r, uint16_t* g, uint16_t* b)
{
    uint8_t status = 0;
	
	tcs3472_read_addr(TCS34725_STATUS, &status, 1);
	
	if(status & 0x01)
    {
        uint8_t readData[2] = {0};

        memset(readData, 0, sizeof(readData));
        tcs3472_read_addr(TCS34725_CDATAL, readData ,2);
        *c = (uint16_t)((readData[1] << 8) | readData[0]);
        
        memset(readData, 0, sizeof(readData));
        tcs3472_read_addr(TCS34725_RDATAL, readData ,2);
        *r = (uint16_t)((readData[1] << 8) | readData[0]);

        memset(readData, 0, sizeof(readData));
        tcs3472_read_addr(TCS34725_GDATAL, readData ,2);
        *g = (uint16_t)((readData[1] << 8) | readData[0]);

        memset(readData, 0, sizeof(readData));
        tcs3472_read_addr(TCS34725_BDATAL, readData ,2);
        *b = (uint16_t)((readData[1] << 8) | readData[0]);

        return true;
    }

    return false;
}

static void i2c_read_task(void *arg)
{
    int16_t eventInfo[4];
    uint8_t infoIndex = 0;
    int16_t triggerEvent = 0;

    uint16_t c = 0;
    uint16_t r = 0;
    uint16_t g = 0;
    uint16_t b = 0;

    uint8_t time = TCS34725_INTEGRATIONTIME_240MS;
    tcs3472_write_addr(TCS34725_ATIME, &time, 1);

    uint8_t gain = TCS34725_GAIN_60X;
    tcs3472_write_addr(TCS34725_CONTROL, &gain, 1);

    uint8_t enable = 0x03;
    tcs3472_write_addr(TCS34725_ENABLE, &enable, 1);

    while (1)
    {
        if (reset_flag)
        {
            reset_flag = 0;

            uint8_t time = TCS34725_INTEGRATIONTIME_240MS;
            tcs3472_write_addr(TCS34725_ATIME, &time, 1);

            uint8_t gain = TCS34725_GAIN_4X;
            tcs3472_write_addr(TCS34725_CONTROL, &gain, 1);

            uint8_t enable = 0x03;
            tcs3472_write_addr(TCS34725_ENABLE, &enable, 1);
        }

        if (moudleSwitch)
        {
            if(tcs3472_read_color(&c, &r, &g, &b))
            {
                //printf("C:%d, R:%d G:%d B:%d\n",c, r , g, b);
                
                //r = (double)r / c * 255;
                //g = (double)g / c * 255;
                //b = (double)b / c * 255;

                float fi = 0, fr = r, fg = g, fb = b;
                if ((fr >= fg) && (fr >= fb))
                    fi = fr/255 + 1;
                else if( (fg >= fr) && (fg >= fb) )
                    fi = fg/255 + 1;
                else if( (fb >= fg) && (fb >= fr) )
                    fi = fb/255 + 1;

                if (fi != 0)
                {
                    fr /= fi;
                    fg /= fi;
                    fb /= fi;
                }

                if (fr > 30)
                    fr -= 30;
                if (fg > 30)
                    fg -= 30;
                if (fb > 30)
                    fb -= 30;

                fr = fr * 255 / 225;
                fg = fg * 255 / 225;
                fb = fb * 255 / 225;

                if (fr > 255)
                    fr = 255;
                if (fg > 255)
                    fg = 255;
                if (fb >= 255)
                    fb = 255;

                r = fr, g = fg, b = fb;

                if (colorR != r ||
                    colorG != g ||
                    colorB != b )
                {
                    colorR = r;
                    colorG = g;
                    colorB = b;
                    triggerEvent = 1;
                }
                
                printf("C:%d, R:%d G:%d B:%d\n",c, r , g, b);
            }
            
            if (triggerEvent)
            {
                triggerEvent = 0;
                if (event_update_callback)
                {
                    infoIndex = 0;
                    eventInfo[infoIndex++] = iTcs3472GetSwitch();
                    eventInfo[infoIndex++] = iTcs3472GetColorR();
                    eventInfo[infoIndex++] = iTcs3472GetColorG();
                    eventInfo[infoIndex++] = iTcs3472GetColorB();
                    event_update_callback(eventInfo, infoIndex);
                }
            }
        }
        
         vTaskDelay(200 / portTICK_PERIOD_MS);
    }
}

void vTcs3472Init(void)
{
    i2cDev = (struct I2cBusDevice_st*)pHalDeviceFind(I2C_NAME);
    if (i2cDev == NULL)
    {
        printf("can not find %s Model\n", I2C_NAME);
        return;
    }

    // 打开设备
    Err_t result = DEV_EOK;
    result = lHalDeviceOpen(i2cDev, DEVICE_OFLAG_RDWR);
    if (result != DEV_EOK)
    {
        printf("can not open %s device\n", I2C_NAME);
        return;
    }

    xTaskCreate(i2c_read_task, "i2c_read_task", 1024 * 4, NULL, 10, NULL);
}

void vTcs3472SetSwitch(int16_t value)
{
    moudleSwitch = value;
}

void vTcs3472Reset()
{
    colorR = 0;
    colorG = 0;
    colorB = 0;
    reset_flag = 1;
}

int16_t iTcs3472GetSwitch(void)
{
    return moudleSwitch;
}

int16_t iTcs3472GetColorR(void)
{
    return colorR;
}

int16_t iTcs3472GetColorG(void)
{
    return colorG;  
}

int16_t iTcs3472GetColorB(void)
{
    return colorB;
}

void vTcs3472RegisterEventCallback(void (*cb)(int16_t *data, uint8_t size))
{
    event_update_callback = cb;
}
