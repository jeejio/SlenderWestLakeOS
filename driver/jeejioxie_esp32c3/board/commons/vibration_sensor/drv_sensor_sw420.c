/*
 * Copyright (c) 2022, jeejio Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author         Notes
 * 2023-03-23     df            the first version
 */

#include "drv_sensor_sw420.h"
#include "device.h"
#include "hal_sensor.h" //sesor IO模型驱动框架头文件
#include "string.h"
#include "auto_init.h"
#include "hal_gpio.h"
#include "hal_pwm.h"
#include "sw420.h" //厂家驱动头文件

#define SENSOR_NAME "SHOCK_SW420"

static const char *TAG = "SHOCK";

enum
{
    SENSOR_SHOCK_SET_SWITCH = SENSOR_CTRL_USER_CMD_START + 1,
    SENSOR_SHOCK_SET_ALARM_THRESHOLD,
    SENSOR_SHOCK_SET_CALLBACK,
};

static Size_t uDrvSensorShockFetchData(struct SensorDevice_st *sensor, void *buf, Size_t len)
{
    if (buf == NULL)
        return 0;
    int16_t *info = (int16_t *)buf;
    int index = 0;

    info[index++] = iSw420GetSwitch();
    info[index++] = iSw420GetAlarmThreshold();
    info[index++] = iSw420GetAlarmStatus();
    info[index++] = iSw420GetValue();
    return index;
}

static Err_t lDrvSensorShockControl(struct SensorDevice_st *sensor, int cmd, void *arg)
{
    LOGI(TAG, "cmd %d \n", cmd);
    switch (cmd)
    {
    case SENSOR_CTRL_SET_POWER:
    {
        int power_mode_type = (int)arg;
        switch (power_mode_type)
        {
        case SENSOR_POWER_NORMAL:
        {
            LOGI(TAG, " start : vDrvSensorInit \n");
            vSw420Init();
            LOGI(TAG, " end   : vDrvSensorInit \n");
        }
        break;
        case SENSOR_POWER_DOWN:
        {
            
        }
        break;
        default:break;
        }
    }
    break;
    case SENSOR_SHOCK_SET_SWITCH:
    {
        int16_t value = *((int16_t *)arg);
        vSw420SetSwitch(value);
    }
    break;
    case SENSOR_SHOCK_SET_ALARM_THRESHOLD:
    {
        int16_t value = *((int16_t *)arg);
        vSw420SetAlarmThreshold(value);
    }
    break;
    case SENSOR_SHOCK_SET_CALLBACK:
    {
        vSw420RegisterEventCallback(arg);
    }
    break;
    default:break;
    }
    return DEV_EOK;
    ;
}

static struct SensorOps_st xSensorOps =
{
    uDrvSensorShockFetchData,
    lDrvSensorShockControl
};

static int lDrvSensorShockInit(const char *name, struct SensorConfig_st *cfg)
{
    Int8_t result;
    Sensor_t sensor_shock = NULL;
    /* sensor register */
    sensor_shock = pvPortMalloc(sizeof(struct SensorDevice_st));
    if (sensor_shock == NULL)
        return -1;
    memset(sensor_shock, 0, sizeof(struct SensorDevice_st));
    sensor_shock->info.vendor = "xwalk";//厂商名
    sensor_shock->info.model = "shock_sw420";//型号

    if (cfg != NULL)
        memcpy(&sensor_shock->config, cfg, sizeof(struct SensorConfig_st));

    sensor_shock->ops = &xSensorOps;

    result = lHalHwSensorRegister(sensor_shock, name, DEVICE_FLAG_RDWR, NULL);
    if (result != DEV_EOK)
    {
        LOGE(TAG,"device register err code: %d", result);
        goto __exit;
    }

    return DEV_EOK;

__exit:
    if (sensor_shock)
        vPortFree(sensor_shock);

    return -DEV_ERROR;
}

static int lDrvHwSensorShockInit()
{
    LOGI(TAG,"begin to execute lDrvHwSensorInit\n");
    struct SensorConfig_st cfg = {0};
    cfg.intf.dev_name = SENSOR_NAME;
    lDrvSensorShockInit(SENSOR_NAME, &cfg);
    LOGI(TAG,"finish to execute lDrvHwSensorInit\n");

    return DEV_EOK;
}

INIT_DEVICE_EXPORT(lDrvHwSensorShockInit);
