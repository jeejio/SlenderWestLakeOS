#include "sw420.h"
#include <stdio.h>
#include "hal_serial.h"
#include "hal_sensor.h"
#include "hal/gpio_types.h"
#include "string.h"
#include "hal_adc.h"
#include <math.h>

#define SENSOR_INPUT_IO (3)

static AdcDevice_t ADC1Dev = NULL;

static int16_t moudleSwitch = 0;
static int16_t alarmThreshold = 20;
static int16_t alarmStatus = 0;
static int16_t shockStatus = 0;
static int16_t raisingCnt = 0;
static int16_t shockValue = 0;

static void (*event_update_callback)(int16_t *data, uint8_t size) = NULL;

static void sensor_monitor(void *arg)
{
    int16_t eventInfo[4];
    uint8_t infoIndex = 0;
    int16_t triggerEvent = 0;

    printf("task start\n");
    while (1)
    {    
        if (moudleSwitch)
        {
            //通过200ms内上升沿个数
            if (raisingCnt > 50)
            {
                raisingCnt = 50;
            }
            shockValue = raisingCnt * 2;

            raisingCnt = 0;

            bool curAlarm = shockValue > alarmThreshold ;
            if (curAlarm != alarmStatus)
            {
                alarmStatus = curAlarm;
                triggerEvent = 1;
            }
            
            if (triggerEvent)
            {
                triggerEvent = 0;
                if (event_update_callback)
                {
                    infoIndex = 0;
                    eventInfo[infoIndex++] = iSw420GetSwitch();
                    eventInfo[infoIndex++] = iSw420GetAlarmThreshold();
                    eventInfo[infoIndex++] = iSw420GetAlarmStatus();
                    eventInfo[infoIndex++] = iSw420GetValue();
                    event_update_callback(eventInfo, infoIndex);
                }
            }
        }
        else
        {
            alarmStatus = 0;
        }
            
         vTaskDelay(200 / portTICK_PERIOD_MS);
    }
}

// 中断内不能调用打印函数，否则会异常
void vAppIRQGpioCallback(void *arg)
{
    if (moudleSwitch)
    {
        raisingCnt++;
    }
}

void vSw420Init(void)
{
    printf("vSw420Init\n");
    Base_t pinNum = SENSOR_INPUT_IO;
    vHalPinMode(pinNum,PIN_MODE_INPUT);
    lHalPinAttachIrq(pinNum, PIN_IRQ_MODE_RISING, vAppIRQGpioCallback, (void *)&pinNum);

    xTaskCreate(sensor_monitor, "sensor_monitor_task", 1024 * 4, NULL, 10, NULL);
}

void vSw420SetSwitch(int16_t value)
{
    moudleSwitch = value;
}

void vSw420SetAlarmThreshold(int16_t value)
{
    alarmThreshold = value;
}

int16_t iSw420GetSwitch(void)
{
    return moudleSwitch;
}

int16_t iSw420GetAlarmThreshold(void)
{
    return alarmThreshold;
}

int16_t iSw420GetAlarmStatus(void)
{
    return alarmStatus;
}

int16_t iSw420GetValue(void)
{
    return shockValue;
}

void vSw420RegisterEventCallback(void (*cb)(int16_t* data, uint8_t size))
{
    event_update_callback = cb;
}
