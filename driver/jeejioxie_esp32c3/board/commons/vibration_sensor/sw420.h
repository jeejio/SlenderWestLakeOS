#ifndef __SW420_H
#define __SW420_H

#include "swldef.h"

void vSw420Init(void);

void vSw420SetSwitch(int16_t value);

void vSw420SetAlarmThreshold(int16_t value);

int16_t iSw420GetSwitch(void);

int16_t iSw420GetAlarmThreshold(void);

int16_t iSw420GetAlarmStatus(void);

int16_t iSw420GetValue(void);

void vSw420RegisterEventCallback(void (*cb)(int16_t *data, uint8_t size));

#endif