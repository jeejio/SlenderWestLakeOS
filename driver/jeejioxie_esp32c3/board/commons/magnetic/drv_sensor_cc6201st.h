/*
 * Copyright (c) 2022, jeejio Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author         Notes
 * 2022-12-13     zhengqian      the first version
 */

#ifndef __DRV_SENSOR_CC6201st_H__
#define __DRV_SENSOR_CC6201st_H__

#include "hal_sensor.h" //sensor IO模型驱动框架头文件

int jeeHwSensorCc6201stInit();

#endif