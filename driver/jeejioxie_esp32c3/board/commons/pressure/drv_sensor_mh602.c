/*
 * Copyright (c) 2022, jeejio Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author         Notes
 * 2023-03-23     df            the first version
 */

#include "drv_sensor_mh602.h"
#include "device.h"
#include "hal_sensor.h" //sesor IO模型驱动框架头文件
#include "string.h"
#include "auto_init.h"
#include "hal_gpio.h"
#include "hal_pwm.h"
#include "mh602.h" //厂家驱动头文件

#define SENSOR_LIQUID_LEVEL_NAME "PRESSURE_MH602"

static const char *TAG = "PRESSURE";

enum
{
    SENSOR_PRESSURE_SET_SWITCH = SENSOR_CTRL_USER_CMD_START + 1,
    SENSOR_PRESSURE_SET_ALARM_THRESHOLD,
    SENSOR_PRESSURE_SET_CALLBACK,
};

static Size_t uDrvSensorPressureFetchData(struct SensorDevice_st *sensor, void *buf, Size_t len)
{
    if (buf == NULL)
        return 0;
    int16_t *info = (int16_t *)buf;
    int index = 0;

    info[index++] = iMh602GetSwitch();
    info[index++] = iMh602GetAlarmThreshold();
    info[index++] = iMh602GetAlarmStatus();
    info[index++] = iMh602GetValue();
    return index;
}

static Err_t lDrvSensorPressureControl(struct SensorDevice_st *sensor, int cmd, void *arg)
{
    LOGI(TAG, "cmd %d \n", cmd);
    switch (cmd)
    {
    case SENSOR_CTRL_SET_POWER:
    {
        int power_mode_type = (int)arg;
        switch (power_mode_type)
        {
        case SENSOR_POWER_NORMAL:
        {
            LOGI(TAG, " start : vDrvSensorInit \n");
            vMh602Init();
            LOGI(TAG, " end   : vDrvSensorInit \n");
        }
        break;
        case SENSOR_POWER_DOWN:
        {
            
        }
        break;
        default:break;
        }
    }
    break;
    case SENSOR_PRESSURE_SET_SWITCH:
    {
        int16_t value = *((int16_t *)arg);
        vMh602SetSwitch(value);
    }
    break;
    case SENSOR_PRESSURE_SET_ALARM_THRESHOLD:
    {
        int16_t value = *((int16_t *)arg);
        vMh602SetAlarmThreshold(value);
    }
    break;
    case SENSOR_PRESSURE_SET_CALLBACK:
    {
        vMh602RegisterEventCallback(arg);
    }
    break;
    default:break;
    }
    return DEV_EOK;
    ;
}

static struct SensorOps_st xSensorOps =
{
    uDrvSensorPressureFetchData,
    lDrvSensorPressureControl
};

static int lDrvSensorPressureInit(const char *name, struct SensorConfig_st *cfg)
{
    Int8_t result;
    Sensor_t sensor_liquid_level = NULL;
    /* sensor register */
    sensor_liquid_level = pvPortMalloc(sizeof(struct SensorDevice_st));
    if (sensor_liquid_level == NULL)
        return -1;
    memset(sensor_liquid_level, 0, sizeof(struct SensorDevice_st));
    sensor_liquid_level->info.vendor = "xwalk";//厂商名
    sensor_liquid_level->info.model = "pressure_mh602";//型号

    if (cfg != NULL)
        memcpy(&sensor_liquid_level->config, cfg, sizeof(struct SensorConfig_st));

    sensor_liquid_level->ops = &xSensorOps;

    result = lHalHwSensorRegister(sensor_liquid_level, name, DEVICE_FLAG_RDWR, NULL);
    if (result != DEV_EOK)
    {
        LOGE(TAG,"device register err code: %d", result);
        goto __exit;
    }

    return DEV_EOK;

__exit:
    if (sensor_liquid_level)
        vPortFree(sensor_liquid_level);

    return -DEV_ERROR;
}

static int lDrvHwSensorPressureInit()
{
    LOGI(TAG,"begin to execute lDrvHwSensorInit\n");
    struct SensorConfig_st cfg = {0};
    cfg.intf.dev_name = SENSOR_LIQUID_LEVEL_NAME;
    lDrvSensorPressureInit(SENSOR_LIQUID_LEVEL_NAME, &cfg);
    LOGI(TAG,"finish to execute lDrvHwSensorInit\n");

    return DEV_EOK;
}

INIT_DEVICE_EXPORT(lDrvHwSensorPressureInit);
