#include "mh602.h"
#include <stdio.h>
#include "hal_serial.h"
#include "hal_sensor.h"
#include "hal/gpio_types.h"
#include "string.h"
#include "hal_adc.h"
#include <math.h>


#define ADC_DEV_NAME "adc1"
#define ADC_DEV_CHANNEL 3
//#define SENSOR_INPUT_IO (10)

static AdcDevice_t ADC1Dev = NULL;

static int16_t moudleSwitch = 0;
static int16_t alarmThreshold = 50;
static int16_t alarmStatus = 0;
static int16_t pressureValue = 0;

static void (*event_update_callback)(int16_t *data, uint8_t size) = NULL;

static void sensor_monitor(void *arg)
{
    int16_t eventInfo[4];
    uint8_t infoIndex = 0;
    int16_t triggerEvent = 0;

    //int8_t sensorInput = 0;
    uint32_t ulVoltage = 0;

    printf("task start\n");
    while (1)
    {    
        if (moudleSwitch)
        {
            //sensorInput = lHalPinRead(SENSOR_INPUT_IO);

            ulVoltage = uHalAdcRead(ADC1Dev, ADC_DEV_CHANNEL);

            if (ulVoltage >= 2846)
            {//无效数据
                pressureValue = 0;
            }
            else
            {
                pressureValue = (3300 - ulVoltage) / 3300. * 100;
            }

            //printf("%d\n", pressureValue);

            int16_t curAlarmStatus = pressureValue > alarmThreshold;
            if (curAlarmStatus != alarmStatus)
            {
                triggerEvent = 1;
                alarmStatus = curAlarmStatus;
            }
            
            if (triggerEvent)
            {
                triggerEvent = 0;
                if (event_update_callback)
                {
                    infoIndex = 0;
                    eventInfo[infoIndex++] = iMh602GetSwitch();
                    eventInfo[infoIndex++] = iMh602GetAlarmThreshold();
                    eventInfo[infoIndex++] = iMh602GetAlarmStatus();
                    eventInfo[infoIndex++] = iMh602GetValue();
                    event_update_callback(eventInfo, infoIndex);
                }
            }
        }
        else
        {
            alarmStatus = 0;
            pressureValue = 0;
        }
            
         vTaskDelay(200 / portTICK_PERIOD_MS);
    }
}

void vMh602Init(void)
{
    printf("vMh602Init\n");
    //vHalPinMode(SENSOR_INPUT_IO,PIN_MODE_INPUT);

    ADC1Dev = (AdcDevice_t)pHalDeviceFind(ADC_DEV_NAME);
    if (ADC1Dev == NULL)
    {
        printf("can not find %s Model\n", ADC_DEV_NAME);
        return;
    }
    
    lHalAdcEnable(ADC1Dev, 1 << ADC_DEV_CHANNEL);

    xTaskCreate(sensor_monitor, "sensor_monitor_task", 1024 * 4, NULL, 10, NULL);
}

void vMh602SetSwitch(int16_t value)
{
    moudleSwitch = value;
}

void vMh602SetAlarmThreshold(int16_t value)
{
    alarmThreshold = value;
}

int16_t iMh602GetSwitch(void)
{
    return moudleSwitch;
}

int16_t iMh602GetAlarmThreshold(void)
{
    return alarmThreshold;
}

int16_t iMh602GetAlarmStatus(void)
{
    return alarmStatus;
}

int16_t iMh602GetValue(void)
{
    return pressureValue;
}

void vMh602RegisterEventCallback(void (*cb)(int16_t* data, uint8_t size))
{
    event_update_callback = cb;
}
