#ifndef __MH602_H
#define __MH602_H

#include "swldef.h"

void vMh602Init(void);

void vMh602SetSwitch(int16_t value);

void vMh602SetAlarmThreshold(int16_t value);

int16_t iMh602GetSwitch(void);

int16_t iMh602GetAlarmThreshold(void);

int16_t iMh602GetAlarmStatus(void);

int16_t iMh602GetValue(void);

void vMh602RegisterEventCallback(void (*cb)(int16_t *data, uint8_t size));

#endif