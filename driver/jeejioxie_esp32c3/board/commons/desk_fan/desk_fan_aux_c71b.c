#include "desk_fan_aux_c71b.h"
#include "hal_pwm.h"
#include "hal_gpio.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"

static const char *TAG = "DESK FAN";
#define LEDC_MODE 0   /*!< LEDC high speed speed_mode */
#define LEDC_DUTY (255)               
#define TARGET_MCPWM_UNIT MCPWM_UNIT_0

#define LEDC_TIMER_FOR 0
#define LEDC_OUTPUT_IO_FOR (3) // Define the output GPIO
#define LEDC_CHANNEL_FOR 1

#define FAST_SPEED (190)
#define MIDDLE_SPEED (165)
#define SLOW_SPEED (144)

#define KEY_PIN_NUM (10)
//#define FAN_PIN_NUM (9)
//#define LED1_PIN_NUM (3)
//#define LED2_PIN_NUM (10)
//#define LED3_PIN_NUM (7)

static struct PwmConfiguration_st pwm_cfg_for = {
    .channel = LEDC_CHANNEL_FOR,
    .speed_mode = LEDC_MODE,
    .period = 1000 * 50, // 单位ns,
    .timer_num = LEDC_TIMER_FOR,
    .pin_num = LEDC_OUTPUT_IO_FOR,
    .bit_resolution = 8
    };
static Device_t fan_pwm_dev = NULL;

static fan_speed_t motor_speed = FAN_SPEED_SLOW;
static int8_t OnOff_flag = 0;
static vFanStateChanged fanstateFunc = NULL;

typedef struct
{
    /* data */
    fan_speed_t motor_speed;
    int8_t OnOff_flag;
} fan_set_t;
fan_set_t fan_set;

static void vDrvSensorDeskFanChangeDutyFunc(uint32_t duty)
{
    pwm_cfg_for.duty = duty;
    lHalDeviceControl(fan_pwm_dev, PWM_CMD_SET_DUTY, (void *)&pwm_cfg_for);
    lHalDeviceControl(fan_pwm_dev, PWM_CMD_UPDATE_DUTY, (void *)&pwm_cfg_for);
}

static uint32_t uDrvSensorDeskFanSpeedToDuty(fan_speed_t speed)
{
    uint32_t duty = SLOW_SPEED;
    if (speed == FAN_SPEED_SLOW)
    {
        duty = SLOW_SPEED;
    }
    else if (speed == FAN_SPEED_MIDDLE)
    {
        duty = MIDDLE_SPEED;
    }
    else if (speed == FAN_SPEED_FAST)
    {
        duty = FAST_SPEED;
    }
    return duty;
}

static void vDrvSensorDeskFanLedDisplayState(int8_t onoff, fan_speed_t speed)
{
    // if (onoff == 0)
    // {
    //     vHalPinWrite(LED1_PIN_NUM, 1);
    //     vHalPinWrite(LED2_PIN_NUM, 1);
    //     vHalPinWrite(LED3_PIN_NUM, 1);
    // }
    // else
    // {
    //     if (speed == FAN_SPEED_SLOW)
    //     {
    //         vHalPinWrite(LED1_PIN_NUM, 0);
    //         vHalPinWrite(LED2_PIN_NUM, 1);
    //         vHalPinWrite(LED3_PIN_NUM, 1);
    //     }
    //     else if (speed == FAN_SPEED_MIDDLE)
    //     {
    //         vHalPinWrite(LED1_PIN_NUM, 1);
    //         vHalPinWrite(LED2_PIN_NUM, 0);
    //         vHalPinWrite(LED3_PIN_NUM, 1);
    //     }
    //     else if (speed == FAN_SPEED_FAST)
    //     {
    //         vHalPinWrite(LED1_PIN_NUM, 1);
    //         vHalPinWrite(LED2_PIN_NUM, 1);
    //         vHalPinWrite(LED3_PIN_NUM, 0);
    //     }
    // }
}

static void vDrvSensorDeskFanControlTask(void *arg)
{
    int stateChangedFlag = 0;
    while (1)
    {
        if(fan_set.OnOff_flag != OnOff_flag)
        {
            OnOff_flag = fan_set.OnOff_flag;
            //vHalPinWrite(FAN_PIN_NUM, OnOff_flag);
            if (OnOff_flag)
            {
                vDrvSensorDeskFanChangeDutyFunc(uDrvSensorDeskFanSpeedToDuty(motor_speed));
            }
            else
            {
                vDrvSensorDeskFanChangeDutyFunc(0);
            }
            
            LOGI(TAG,"OnOff_flag change:%d\n", OnOff_flag);
            stateChangedFlag = 1;
        }
        if(OnOff_flag && fan_set.motor_speed != motor_speed)
        {
            vDrvSensorDeskFanChangeDutyFunc(uDrvSensorDeskFanSpeedToDuty(fan_set.motor_speed));
            motor_speed = fan_set.motor_speed;

            LOGI(TAG,"motor_speed change:%d\n", motor_speed);
            stateChangedFlag = 1;
        }

        if (stateChangedFlag == 1)
        {
            stateChangedFlag = 0;
            if(fanstateFunc)
            {
                fanstateFunc(OnOff_flag, motor_speed);
            }
            vDrvSensorDeskFanLedDisplayState(OnOff_flag, motor_speed);

        }
        

        vTaskDelay(pdMS_TO_TICKS(50));
    }
}

static void vDrvSensorDeskFanKeyTask(void *arg)
{
    int8_t cur_key_State = 0;
    int8_t old_key_State = 0;
    int8_t key_release_flag = 0;
    fan_speed_t speed_value = FAN_SPEED_SLOW;
    int onOff_value = 0;

    while(1)
    {
        cur_key_State = lHalPinRead(KEY_PIN_NUM);

        if(cur_key_State != old_key_State)
        {
            vTaskDelay(pdMS_TO_TICKS(20));
            cur_key_State = lHalPinRead(KEY_PIN_NUM);
            if(cur_key_State != old_key_State)
            {
                old_key_State = cur_key_State;
                if(cur_key_State == 0)
                {
                    key_release_flag = 1;
                }
            }
        }

        if(key_release_flag)
        {
            key_release_flag = 0;
            LOGI(TAG,"key clicked\n");
            onOff_value = lDrvSensorDeskFanGetSwitchStatus();
            if(onOff_value == 0)
            {
                vDrvSensorDeskFanSetSpeed(FAN_SPEED_SLOW);
                vDrvSensorDeskFanSetSwitch(1);
            }
            else
            {
                speed_value = ucDrvSensorDeskFanGetSpeed();
                switch(speed_value)
                {
                    case FAN_SPEED_SLOW:
                        vDrvSensorDeskFanSetSpeed(FAN_SPEED_MIDDLE);
                    break;
                    case FAN_SPEED_MIDDLE:
                        vDrvSensorDeskFanSetSpeed(FAN_SPEED_FAST);
                    break;
                    case FAN_SPEED_FAST:
                        vDrvSensorDeskFanSetSpeed(FAN_SPEED_SLOW);
                        vDrvSensorDeskFanSetSwitch(0);
                    break;
                    default:break;
                }
            }
        }
        
        vTaskDelay(pdMS_TO_TICKS(50));
    }
}

void vDrvSensorDeskFanInit(void)
{
    vHalPinMode(KEY_PIN_NUM, PIN_MODE_INPUT_PULLUP);
    //vHalPinMode(FAN_PIN_NUM, PIN_MODE_OUTPUT);

    //vHalPinMode(LED1_PIN_NUM, PIN_MODE_OUTPUT);
    //vHalPinMode(LED2_PIN_NUM, PIN_MODE_OUTPUT);
    //vHalPinMode(LED3_PIN_NUM, PIN_MODE_OUTPUT);
    // 查找设备
    fan_pwm_dev = pHalDeviceFind("pwm");
    if (fan_pwm_dev == NULL)
    {
        LOGI(TAG,"can not find pwm Model\n");
        return;
    }

    // 打开设备
    Err_t result = DEV_EOK;
    result = lHalDeviceOpen(fan_pwm_dev, DEVICE_OFLAG_RDWR);
    if (result != DEV_EOK)
    {
        LOGI(TAG,"can not open pwm device\n");
        return;
    }

    // 初始化设备
    lHalDeviceControl(fan_pwm_dev, PWM_CMD_INIT, (void *)&pwm_cfg_for);

    memset(&fan_set, 0, sizeof(fan_set));
    fan_set.motor_speed = motor_speed;
    fan_set.OnOff_flag = OnOff_flag;
    vDrvSensorDeskFanChangeDutyFunc(0);
    vDrvSensorDeskFanLedDisplayState(motor_speed, OnOff_flag);
    
    xTaskCreate(vDrvSensorDeskFanControlTask, "vDrvSensorDeskFanControlTask", 4096, NULL, configMAX_PRIORITIES, NULL);
    xTaskCreate(vDrvSensorDeskFanKeyTask, "vDrvSensorDeskFanKeyTask", 4096, NULL, configMAX_PRIORITIES, NULL);
}

void vDrvSensorDeskFanSetSpeed(fan_speed_t speed)
{
    if (speed > 3)
        return;
    fan_set.OnOff_flag = 1;
    fan_set.motor_speed = speed;
    vDrvSensorDeskFanChangeDutyFunc(uDrvSensorDeskFanSpeedToDuty(fan_set.motor_speed));
}

fan_speed_t ucDrvSensorDeskFanGetSpeed(void)
{
    return motor_speed;
}

int lDrvSensorDeskFanGetSwitchStatus(void)
{
    return OnOff_flag;
}

void vDrvSensorDeskFanSetSwitch(int onff)
{
    fan_set.OnOff_flag = onff;
    if(fan_set.OnOff_flag)
    {
        vDrvSensorDeskFanChangeDutyFunc(uDrvSensorDeskFanSpeedToDuty(fan_set.motor_speed));
    }
    else
    {
         vDrvSensorDeskFanChangeDutyFunc(0);   
    }
}

void vDrvSensorDeskFanGetInfo(int *buf)
{
    buf[0] = motor_speed;
    buf[1] = OnOff_flag;
}

void vDrvSensorDeskFanSetStateChangedCb(vFanStateChanged fun)
{
    fanstateFunc = fun;
}
