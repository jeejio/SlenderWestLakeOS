#include "device.h"
#include "hal_sensor.h" //sesor IO模型驱动框架头文件
#include "string.h"
#include "auto_init.h"
#include "hal_gpio.h"
#include "desk_fan_aux_c71b.h"  
#include "drv_sensor_desk_fan_aux_c71b.h"

#define SENSOR_FAN_NAME "motor_pwm"

static Size_t llDrvSensorFanReadStatus(struct SensorDevice_st *sensor, void *buf, Size_t len)
{
    if (buf == NULL)
        return 0;

    vDrvSensorDeskFanGetInfo((int *)buf);
    return (strlen(buf) - 1);
}

static Err_t lDrvSensorFanControl(struct SensorDevice_st *sensor, int cmd, void *arg)
{
    LOGI("DESK FAN","cmd %d \n", cmd);
    switch (cmd)
    {
    case SENSOR_CTRL_SET_POWER:
    {
        int power_mode_type = (int)arg;
        switch (power_mode_type)
        {
        case SENSOR_POWER_NORMAL:
        {
            LOGI("DESK FAN"," start : motor_init \n");
            vDrvSensorDeskFanInit();
            LOGI("DESK FAN"," end   : motor_init \n");
            return DEV_EOK;
        }
        break;
        case SENSOR_POWER_DOWN:
        {
        }
        break;
        default:break;
        }
    }
    break;
    case SENSOR_FAN_SET_SPEED:
    {
        vDrvSensorDeskFanSetSpeed(*(int *)arg);
        return DEV_EOK;
    }
    break;
    case SENSOR_FAN_SET_ONOFF:
    {
        vDrvSensorDeskFanSetSwitch(*(int *)arg);
        return DEV_EOK;
    }
    break;
    case SENSOR_FAN_SET_STATE_CHANGED_CB:
    {
        vDrvSensorDeskFanSetStateChangedCb((vFanStateChanged)arg);
        return DEV_EOK;
    }
    break;
    default:
        break;
    }
    return DEV_EOK;
}

static struct SensorOps_st xSensorOps =
{
    llDrvSensorFanReadStatus,
    lDrvSensorFanControl
};

static int lDrvSensorFanInit(const char *name, struct SensorConfig_st *cfg)
{
    Int8_t result;
    Sensor_t sensor_motor = NULL;
    /* sensor register */
    sensor_motor = pvPortMalloc(sizeof(struct SensorDevice_st));
    if (sensor_motor == NULL)
        return -1;
    memset(sensor_motor, 0, sizeof(struct SensorDevice_st));
    sensor_motor->info.vendor = "Aux";
    sensor_motor->info.model = "desk fan";

    if (cfg != NULL)
        memcpy(&sensor_motor->config, cfg, sizeof(struct SensorConfig_st));

    sensor_motor->ops = &xSensorOps;

    result = lHalHwSensorRegister(sensor_motor, name, DEVICE_FLAG_RDWR, NULL);
    if (result != DEV_EOK)
    {
        // LOG_E("device register err code: %d", result);
        goto __exit;
    }

    return DEV_EOK;

__exit:
    if (sensor_motor)
        vPortFree(sensor_motor);

    return -DEV_ERROR;
}

int lDrvSensorHwSensorFanInit()
{
    struct SensorConfig_st cfg = {0};
    cfg.intf.dev_name = SENSOR_FAN_NAME;
    lDrvSensorFanInit("DeskFan", &cfg);
    return DEV_EOK;
}

INIT_DEVICE_EXPORT(lDrvSensorHwSensorFanInit);
