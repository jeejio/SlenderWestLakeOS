#ifndef __DESK_FAN_AUX_C71B_H__
#define __DESK_FAN_AUX_C71B_H__

typedef void (*vFanStateChanged)(int, int);

typedef enum
{
    FAN_SPEED_SLOW = 0,
    FAN_SPEED_MIDDLE,
    FAN_SPEED_FAST
} fan_speed_t;

 /**
   * @brief       初始化函数
   *
   * NOTE:        
   * 
   */
void vDrvSensorDeskFanInit(void);

 /**
   * @brief      设置电机速度
   *
   * NOTE:       一共两个电机，设置的是需要工作的电机的速度，
   *             如两个都要工作，则设置的是两个电机的速度
   * 
   * @param[in]   speed 设置电机的速度 
   *            
   *  
   */
void vDrvSensorDeskFanSetSpeed(fan_speed_t speed);

 /**
   * @brief      获取电机速度
   *
   * NOTE:       一共两个电机，获取的是需要工作的电机的速度，
   *             需要工作的电机的速度总是同样的
   * 
   * @return    电机的速度
 
   */
fan_speed_t ucDrvSensorDeskFanGetSpeed(void);

 /**
   * @brief      获取开关状态
   *
   * NOTE:       
   *             
   * 
   * @return    电机的速度
 
   */
int lDrvSensorDeskFanGetSwitchStatus(void);

 /**
   * @brief      设置电机开关
   *
   * NOTE:       电机开启后将按照设置好的方向、速度启动
   * 
   * @param[in]  onff  0:关闭，1:开启
   * 
   *
   */
void vDrvSensorDeskFanSetSwitch(int onff);

 /**
   * @brief      获取信息
   *
   * NOTE:       信息包括：速度，开关状态
   *           
   * @param[out]   返回信息
   */
void vDrvSensorDeskFanGetInfo(int *buf);

 /**
   * @brief      获取信息
   *
   * NOTE:       信息包括：速度，开关状态
   *           
   * @param[out]   返回信息
   */
void vDrvSensorDeskFanSetStateChangedCb(vFanStateChanged fun);


#endif
