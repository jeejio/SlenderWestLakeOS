#include <drv_storage.h>
#include "esp_flash_encrypt.h"
#include "esp_log.h"
#include "swldef.h"

static wl_handle_t s_wl_handle = WL_INVALID_HANDLE;

static const char* TAG = "DRV_STORAGE";

static int32_t lHalReadPartition(int offset, int size, char *read_data, const char *partition_label)
{
    esp_err_t ret = DEV_ERROR;
    const esp_partition_t *partition = esp_partition_find_first(ESP_PARTITION_TYPE_DATA, ESP_PARTITION_SUBTYPE_ANY, partition_label);
    if(partition == NULL)
    {
        ESP_LOGE(TAG, "No partition is found!\n");
        return ret;
    }

    ret = esp_partition_read(partition, offset, read_data, size);
    if(ret != DEV_EOK)
    {
        ESP_LOGE(TAG, "Failed to Read partition!\n");
        return ret;
    }
    return ret;
}

static int32_t lHalEraseAndWritePartition(int offset, int size,  char *data, const char *partition_label)
{
    esp_err_t ret = DEV_ERROR;
    const esp_partition_t *partition = esp_partition_find_first(ESP_PARTITION_TYPE_DATA, ESP_PARTITION_SUBTYPE_ANY, partition_label);
    if (partition == NULL)
    {
        ESP_LOGE(TAG, "No partition is found!\n");
        return ret;
    }
    if (size %16 != 0)
    {
        ESP_LOGE(TAG, "size must be mutiple of 16 bytes\n");
        return ret;
    }

    uint32_t erase_size = size % 4096 != 0 ? (size / 4096 + 1) * 4096 : size; 
    ret = esp_partition_erase_range(partition, offset, erase_size);
    if (ret != ESP_OK) 
    {
        ESP_LOGE(TAG, "Failed to erase partition, error code: %d\n", ret);
        ret = DEV_ERROR;
        return ret;
    }

    ret = esp_partition_write(partition, offset, data, size);
    if(ret != ESP_OK)
    {
        ESP_LOGE(TAG, "Failed to Write partition!\n");
        ret = DEV_ERROR;
        return ret;
    }
    ret = DEV_EOK;
    return ret;
}

int32_t lHalReadKeyInfo(int offset,int size,char *read_data)
{
    return lHalReadPartition(offset, size, read_data, "jee_key");
}

int32_t lHalReadNetConfig(int offset, int size, char *read_data)
{
    return lHalReadPartition(offset, size, read_data, "net_info");
}

int32_t lHalWriteNetConfig(int offset, int size, char *data)
{
    return lHalEraseAndWritePartition(offset, size, data, "net_info");
}

int32_t lHalVfsFatSpiflashMountRW(const char* base_path, const char* partition_label)
{
    const esp_vfs_fat_mount_config_t mount_config = {
        .max_files = 8,
        .format_if_mount_failed = false,
        .allocation_unit_size = CONFIG_WL_SECTOR_SIZE};
    esp_err_t err;
    err = esp_vfs_fat_spiflash_mount_rw_wl(base_path, partition_label, &mount_config, &s_wl_handle);
    return err;
}


