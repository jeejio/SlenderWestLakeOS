/*
 * SPDX-FileCopyrightText: 2015-2022 Espressif Systems (Shanghai) CO LTD
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#ifndef __DRV_PARTITION_H__
#define __DRV_PARTITION_H__

#ifdef __cplusplus
extern "C" {
#endif

#include <esp_partition.h>
#include "esp_vfs.h"
#include "esp_vfs_fat.h"

int32_t lHalReadKeyInfo(int offset,int size,char *read_data);
int32_t lHalReadNetConfig(int offset, int size, char *read_data);
int32_t lHalWriteNetConfig(int offset, int size, char *data);
int32_t lHalVfsFatSpiflashMountRW(const char* base_path, const char* partition_label);

#ifdef __cplusplus
}
#endif

#endif /* __ESP_PARTITION_H__ */

