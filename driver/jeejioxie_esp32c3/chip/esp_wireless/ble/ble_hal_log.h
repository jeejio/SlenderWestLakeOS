
/**
 * @file ble_hal_log.h
 */

#ifndef _BLE_HAL_LOG_H_
#define _BLE_HAL_LOG_H_

#include "esp_log.h"

#define BLE_LOG_LEVEL_LOCAL(level, tag, format, ...) do {               \
        if ( LOG_LOCAL_LEVEL >= level ) JEE_LOG_LEVEL(level, tag, format, ##__VA_ARGS__); \
    } while(0)

#endif /* ifndef _AWS_BLE_INTERNALS_H_ */
