/*
 * Copyright (c) 2023, JEE Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2023-02-28      lk        first version
 */

#ifndef __DRV_WIFI_IOMODEL_H__
#define __DRV_WIFI_IOMODEL_H__

#include <hal_wlan_dev.h>
#include <hal_wlan_mgnt.h>

struct wlan_trans_ops {
    Err_t (*wlan_drv_input)(void *wlanif, void *buffer, size_t len, void* l2_buff);
    Err_t (*wlan_drv_output)(void *wlanif, void *pbuf);
};

int lDrvHWWifiInit(void);

extern void example_disp_buf(char* title, char* buf, int length);

#endif /* __DRV_WIFI_IOMODEL_H__ */
