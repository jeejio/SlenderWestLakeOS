/*
 * Copyright (c) 2023, JEE Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2023-02-28      lk        first version
 */

#include <string.h>
#include "drv_wifi_iomodel.h"
#include "esp_wifi_types.h"
#include "esp_log.h"
#include "esp_wifi.h"
#include "esp_netif.h"
#include "nvs_flash.h"
#include <swldef.h>
#include "hal_wlan_dev.h"
#include "hal_config.h"
#include "auto_init.h"
#include "hal_wlan_mgnt.h"
#include <lwip/netif.h>
#include <lwip/pbuf.h>
#include <ethernetif_port.h>
#include <esp_netif_net_stack.h>
#include <io_dev_mgt.h>
#include "hal_wlan_lwip.h"
#include "lwip/esp_netif_net_stack.h"

#define DBG_ENABLE
#define DBG_LEVEL DBG_INFO
#define DBG_SECTION_NAME  "WIFI"
#define DBG_COLOR
static const char *TAG = "drv_wifi_iomodel";

#define MAX_ADDR_LEN        (6)

#ifndef MAC2STR
#define MAC2STR(a) (a)[0], (a)[1], (a)[2], (a)[3], (a)[4], (a)[5]
#endif

struct drv_wifi
{
    struct WlanDevice_st *wlan;
    Uint8_t dev_addr[MAX_ADDR_LEN];
};

static struct drv_wifi lHalWifiSta;
static struct drv_wifi lHalWifiAp;

static char countryTable[COUNTRY_UNKNOWN + 1][3] = {
    "AF",
    "AL",
    "DZ",
    "AS",
    "AO",
    "AI",
    "AG",
    "AR",
    "AM",
    "AW",
    "AU",
    "AU",
    "AZ",
    "BS",
    "BH",
    "XX", // COUNTRY_BAKER_ISLAND
    "BD",
    "BB",
    "BY",
    "BE",
    "BZ",
    "BJ",
    "BM",
    "BT",
    "BO",
    "BA",
    "BW",
    "BR",
    "IO",
    "BN",
    "BG",
    "BF",
    "BI",
    "KH",
    "CM",
    "CA",
    "CV",
    "KY",
    "CF",
    "TD",
    "CL",
    "CN",
    "CX",
    "CO",
    "KM",
    "CG",
    "CD",
    "CR",
    "CI",
    "HR",
    "CU",
    "CY",
    "CZ",
    "DK",
    "DJ",
    "DM",
    "DO",
    "XX", // COUNTRY_DOWN_UNDER:
    "EC",
    "EG",
    "SV",
    "GQ",
    "ER",
    "EE",
    "ET",
    "FK",
    "FO",
    "FJ",
    "FI",
    "FR",
    "GF",
    "PF",
    "TF",
    "GA",
    "GM",
    "GE",
    "DE",
    "GH",
    "GI",
    "GR",
    "GD",
    "GP",
    "GU",
    "GT",
    "GG",
    "GN",
    "GW",
    "GY",
    "HT",
    "VA",
    "HN",
    "HK",
    "HU",
    "IS",
    "IN",
    "ID",
    "IR",
    "IQ",
    "IE",
    "IL",
    "IT",
    "JM",
    "JP",
    "JE",
    "JO",
    "KZ",
    "KE",
    "KI",
    "KR",
    "XK",
    "KW",
    "KG",
    "LA",
    "LV",
    "LB",
    "LS",
    "LR",
    "LY",
    "FL",
    "LT",
    "LU",
    "MO",
    "MK",
    "MG",
    "MW",
    "MY",
    "MV",
    "ML",
    "MT",
    "IM",
    "MQ",
    "MR",
    "MU",
    "YT",
    "MX",
    "FM",
    "MD",
    "MC",
    "MN",
    "ME",
    "MS",
    "MA",
    "MZ",
    "MM",
    "NA",
    "NR",
    "NP",
    "NL",
    "AN",
    "NC",
    "NZ",
    "NI",
    "NE",
    "NG",
    "NF",
    "MP",
    "NO",
    "OM",
    "PK",
    "PW",
    "PA",
    "PG",
    "PY",
    "PE",
    "PH",
    "PL",
    "PT",
    "US",
    "QA",
    "RE",
    "RO",
    "RU",
    "RW",
    "KN",
    "LC",
    "PM",
    "VC",
    "WS",
    "XX", // COUNTRY_SANIT_MARTIN_SINT_MARTEEN
    "ST",
    "SA",
    "SN",
    "RS",
    "SC",
    "SL",
    "SG",
    "SK",
    "SI",
    "SB",
    "SO",
    "ZA",
    "ES",
    "LK",
    "SR",
    "SZ",
    "SE",
    "CH",
    "SY",
    "TW",
    "TJ",
    "TZ",
    "TH",
    "TG",
    "TO",
    "TT",
    "TN",
    "TR",
    "TM",
    "TC",
    "TV",
    "UG",
    "UA",
    "AE",
    "GB",
    "US",
    "XX", // COUNTRY_UNITED_STATES_REV4:
    "XX", // COUNTRY_UNITED_STATES_NO_DFS
    "UM",
    "UY",
    "UZ",
    "VU",
    "VE",
    "VN",
    "VG",
    "VI",
    "WF",
    "PS", // COUNTRY_WEST_BANK 约旦河西岸，属于巴勒斯坦
    "EH",
    "XX", // COUNTRY_WORLD_WIDE_XX
    "YE",
    "ZM",
    "ZW",
    "XX"  // COUNTRY_UNKNOWN
};

/**
 * @brief debug buffer data
 */
void example_disp_buf(char* title, char* buf, int length)
{
    printf("\n========[%s]===========\n", title);
    for (int i = 0; i < length; i++)
    {
        printf("%02x ", buf[i]);
        if ((i + 1) % 32 == 0)
        {
            printf("\n");
        }
    }
    printf("\n===========================\n");
}

static void vGetCountryStrFromCode(CountryCode_t country_code, char *country)
{
    if (country_code >= COUNTRY_AFGHANISTAN && country_code <= COUNTRY_UNKNOWN)
    {
        memcpy(country, countryTable[country_code], 2);
    }
}

static void vGetCountryCodeFromStr(const char *country, CountryCode_t *country_code)
{
    for (int i = COUNTRY_AFGHANISTAN; i <= COUNTRY_UNKNOWN; i++)
    {
        if (!strncmp(country, countryTable[i], 2))
        {
            *country_code = i;
        }
    }
}

#if defined(ETH_RX_DUMP) ||  defined(ETH_TX_DUMP)
static void packet_dump(const char *msg, const void *ptr, Uint32_t len)
{
    Uint32_t j;
    Uint8_t *p = (Uint8_t *)ptr;
    kprintf("%s %d byte\n", msg, len);

#ifdef MINI_DUMP
    return;
#endif

    for (j = 0; j < len; j++)
    {
        if ((j % 8) == 0)
        {
            kprintf("  ");
        }

        if ((j % 16) == 0)
        {
            kprintf("\r\n");
        }
        kprintf("%02x ", *p ++);
    }

    kprintf("\n\n");
}
#endif /* dump */

static void vDrvWlanClientEvent(Uint8_t *mac, wifi_event_t event)
{
    struct WlanBuff_st buff;
    struct WlanInfo_st sta;
    memcpy(sta.bssid, mac, MAX_ADDR_LEN);
    buff.data = &sta;
    buff.len = sizeof(sta);

    if (WIFI_EVENT_AP_STACONNECTED == event)
    {
        vHalWlanDevIndicateEventHandle(lHalWifiAp.wlan, WLAN_DEV_EVT_AP_ASSOCIATED, &buff);
    }
    if (WIFI_EVENT_AP_STADISCONNECTED == event)
    {
        vHalWlanDevIndicateEventHandle(lHalWifiAp.wlan, WLAN_DEV_EVT_AP_DISASSOCIATED, &buff);
    }
}

static void vWlanPromiscDataFrameCallback(struct WlanDevice_st *device, void *data, int len)
{
    vHalWlanDevPromiscHandler(device, data, len);
}

static void vWlanEspPromiscDdataFrameCallback(void *buf, wifi_promiscuous_pkt_type_t type)
{
    wifi_promiscuous_pkt_t *pkt = (wifi_promiscuous_pkt_t *)buf;
    vWlanPromiscDataFrameCallback(lHalWifiAp.wlan, (void *)(pkt->payload), pkt->rx_ctrl.sig_len);
}

static void vDrvWlanScanCallback(void)
{
    struct WlanInfo_st wlanInfo;
    struct WlanBuff_st buff;
    wifi_ap_record_t *apListBuffer;
    uint16_t apCount = 0;

    if (esp_wifi_scan_get_ap_num(&apCount) != ESP_OK)
    {
        ESP_LOGE(TAG, "Failed to get scan ap num");
        return;
    }
    LOGI(TAG, "vDrvWlanScanCallback esp_wifi_scan_get_ap_num apCount = %d", apCount);

    apListBuffer = malloc(apCount * sizeof(wifi_ap_record_t));
    if (apListBuffer == NULL) {
        ESP_LOGE(TAG, "Failed to malloc buffer to print scan results");
        return;
    }
    else
    {
        memset(apListBuffer, 0, sizeof(apCount * sizeof(wifi_ap_record_t)));
    }

    if (esp_wifi_scan_get_ap_records(&apCount, apListBuffer) == ESP_OK) {
        for (int i = 0; i < apCount; i++) {
            // ESP_LOGI(TAG, "[%s][rssi=%d]", apListBuffer[i].ssid, apListBuffer[i].rssi);

            memset(&wlanInfo, 0, sizeof(wlanInfo));

            memcpy(&wlanInfo.bssid[0], apListBuffer[i].bssid, 6);
            memcpy(wlanInfo.ssid.val, apListBuffer[i].ssid, strlen((char*)(apListBuffer[i].ssid)));
            wlanInfo.ssid.len = strlen((char*)(apListBuffer[i].ssid));
            if (wlanInfo.ssid.len)
                wlanInfo.hidden = 0;
            else
                wlanInfo.hidden = 1;

            wlanInfo.channel = (Int16_t)(apListBuffer[i].primary); // ap channel
            wlanInfo.rssi = -(char)(0x100 - apListBuffer[i].rssi);

            wlanInfo.band = BAND_802_11_2_4GHZ;

            wlanInfo.security = SECURITY_OPEN;
            if (WIFI_AUTH_WEP & apListBuffer[i].authmode)
                wlanInfo.security |= WEP_ENABLED;
            if (WIFI_AUTH_WPA_PSK & apListBuffer[i].authmode)
                wlanInfo.security |= WPA_SECURITY | TKIP_ENABLED | AES_ENABLED;
            if (WIFI_AUTH_WPA2_PSK & apListBuffer[i].authmode)
                wlanInfo.security |= WPA2_SECURITY | TKIP_ENABLED | AES_ENABLED;
            if (WIFI_AUTH_WPA_WPA2_PSK & apListBuffer[i].authmode)
                wlanInfo.security |= WPA_SECURITY | WPA2_SECURITY | TKIP_ENABLED | AES_ENABLED;
            if (apListBuffer[i].wps)
                wlanInfo.security |= WPS_ENABLED;
            if (WIFI_AUTH_MAX == apListBuffer[i].authmode)
                wlanInfo.security = SECURITY_UNKNOWN;

            /* rtt incompleted... */
            if (wlanInfo.security & SECURITY_WPA2_MIXED_PSK)
                wlanInfo.security = SECURITY_WPA2_MIXED_PSK;
            else if (wlanInfo.security & SECURITY_WPA2_TKIP_PSK)
                wlanInfo.security = SECURITY_WPA2_TKIP_PSK;
            else if (wlanInfo.security & SECURITY_WPA2_AES_PSK)
                wlanInfo.security = SECURITY_WPA2_AES_PSK;
            else if (wlanInfo.security & SECURITY_WPA_AES_PSK)
                wlanInfo.security = SECURITY_WPA_AES_PSK;
            else if (wlanInfo.security & SECURITY_WPA_TKIP_PSK)
                wlanInfo.security = SECURITY_WPA_TKIP_PSK;
            else if (wlanInfo.security & SECURITY_WEP_PSK)
                wlanInfo.security = SECURITY_WEP_PSK;
            else if ((SECURITY_UNKNOWN == wlanInfo.security) && apListBuffer[i].wps)
                wlanInfo.security = SECURITY_WPS_SECURE;

            buff.data = &wlanInfo;
            buff.len = sizeof(wlanInfo);
            vHalWlanDevIndicateEventHandle(lHalWifiSta.wlan, WLAN_DEV_EVT_SCAN_REPORT, &buff);
        }
    }
    free(apListBuffer);

    vHalWlanDevIndicateEventHandle(lHalWifiSta.wlan, WLAN_DEV_EVT_SCAN_DONE, NULL);
}

static void event_handler(void* arg, esp_event_base_t event_base,
                                int32_t event_id, void* event_data)
{
    if (event_base == WIFI_EVENT && event_id == WIFI_EVENT_STA_CONNECTED)
    {
        LOGI(TAG,"get WIFI_EVENT_STA_CONNECTED event");
        vHalWlanDevIndicateEventHandle(lHalWifiSta.wlan, WLAN_DEV_EVT_CONNECT, NULL);
    }
    else if (event_base == WIFI_EVENT && event_id == WIFI_EVENT_STA_DISCONNECTED)
    {
        vHalWlanDevIndicateEventHandle(lHalWifiSta.wlan, WLAN_DEV_EVT_DISCONNECT, NULL);
    }
    else if (event_base == WIFI_EVENT && event_id == WIFI_EVENT_SCAN_DONE)
    {
        LOGI(TAG,"get WIFI_EVENT_SCAN_DONE event");
        vDrvWlanScanCallback();
    }
    else if (event_base == IP_EVENT && event_id == IP_EVENT_STA_GOT_IP)
    {
        ip_event_got_ip_t* event = (ip_event_got_ip_t*) event_data;
        LOGI(TAG, "got ip:" IPSTR, IP2STR(&event->ip_info.ip));
        vHalWlanDevIndicateEventHandle(lHalWifiSta.wlan, WLAN_DEV_EVT_GOT_IP, NULL);
    }
    else if (event_id == WIFI_EVENT_AP_STACONNECTED)
    {
        wifi_event_ap_staconnected_t *event = (wifi_event_ap_staconnected_t *)event_data;
        LOGI(TAG, "station %02x:%02x:%02x:%02x:%02x:%02x join, AID=%d", MAC2STR(event->mac), event->aid);
        vDrvWlanClientEvent(event->mac, WIFI_EVENT_AP_STACONNECTED);
    }
    else if (event_id == WIFI_EVENT_AP_STADISCONNECTED)
    {
        wifi_event_ap_stadisconnected_t *event = (wifi_event_ap_stadisconnected_t *)event_data;
        LOGI(TAG, "station %02x:%02x:%02x:%02x:%02x:%02x leave, AID=%d", MAC2STR(event->mac), event->aid);
        vDrvWlanClientEvent(event->mac, WIFI_EVENT_AP_STADISCONNECTED);
    }
    else if (event_id == WIFI_EVENT_AP_START)
    {
        vHalWlanDevIndicateEventHandle(lHalWifiAp.wlan, WLAN_DEV_EVT_AP_START, NULL);
    }
    else if (event_id == WIFI_EVENT_AP_STOP)
    {
        vHalWlanDevIndicateEventHandle(lHalWifiAp.wlan, WLAN_DEV_EVT_AP_STOP, NULL);
    }
}

static Err_t lDrvWlanInit(struct WlanDevice_st *wlan)
{
    LOGI(TAG, "============[enter lDrvWlanInit]==================== wlan->mode = %d", wlan->mode);
    esp_err_t ret = ESP_OK;

    if (nvs_flash_init() != ESP_OK)
    {
        LOGE(TAG, "F:%s L:%d nvs_flash_init fail", __FUNCTION__, __LINE__);
        return DEV_ERROR;
    }

    if (esp_netif_init() != ESP_OK)
    {
        LOGE(TAG, "F:%s L:%d esp_netif_init fail", __FUNCTION__, __LINE__);
        return DEV_ERROR;
    }

    if (esp_event_loop_create_default() != ESP_OK)
    {
        LOGE(TAG, "F:%s L:%d esp_event_loop_create_default fail", __FUNCTION__, __LINE__);
        return DEV_ERROR;
    }

    if (wlan->mode == WLAN_MODE_STATION)
    {
        esp_netif_create_default_wifi_sta();
        LOGI(TAG, "lDrvWlanInit esp_netif_create_default_wifi_sta");
    }
    else if (wlan->mode == WLAN_MODE_AP)
    {
        esp_netif_create_default_wifi_ap();
        LOGI(TAG, "lDrvWlanInit esp_netif_create_default_wifi_ap");
    }
    vTaskDelay(50 / portTICK_PERIOD_MS);
    wifi_init_config_t cfg = WIFI_INIT_CONFIG_DEFAULT();
    ret = esp_wifi_init(&cfg);
    if (ret != ESP_OK)
    {
        LOGE(TAG, "F:%s L:%d esp_wifi_init fail", __FUNCTION__, __LINE__);
        return DEV_ERROR;
    }

    esp_event_handler_instance_t instanceAnyId;
    esp_event_handler_instance_t instanceGotIp;

    ret = esp_event_handler_instance_register(WIFI_EVENT, ESP_EVENT_ANY_ID, &event_handler, NULL, &instanceAnyId);
    LOGI(TAG, "lDrvWlanInit ESP_EVENT_ANY_ID ret = %d", ret);
    ret = esp_event_handler_instance_register(IP_EVENT, IP_EVENT_STA_GOT_IP, &event_handler, NULL, &instanceGotIp);
    LOGI(TAG, "lDrvWlanInit IP_EVENT_STA_GOT_IP ret = %d", ret);

    return ret == ESP_OK ? DEV_EOK : DEV_ERROR;
}

static Err_t lDrvWlanMode(struct WlanDevice_st *wlan, WlanMode_t mode)
{
    wifi_mode_t wifiMode;

    switch (mode)
    {
        case WLAN_MODE_NONE:
            wifiMode = WIFI_MODE_NULL;
            break;
        case WLAN_MODE_STATION:
            wifiMode = WIFI_MODE_STA;
            break;
        case WLAN_MODE_AP:
            wifiMode = WIFI_MODE_AP;
            break;
        case WLAN_MODE_MAX:
            wifiMode = WIFI_MODE_MAX;
            break;
        default:
            wifiMode = WIFI_MODE_NULL;
            break;
    }

    esp_err_t ret = esp_wifi_set_mode(wifiMode);
    LOGI(TAG, "lDrvWlanMode esp_wifi_set_mode ret = %d, wifiMode = %d", ret, wifiMode);

    return ret == ESP_OK ? DEV_EOK : DEV_ERROR;
}

static Err_t lDrvWlanScan(struct WlanDevice_st *wlan, struct ScanInfo_st *scan_info)
{
    esp_err_t ret = esp_wifi_start();
    if (ret != ESP_OK)
    {
        LOGE(TAG, "F:%s L:%d esp_wifi_start fail", __FUNCTION__, __LINE__);
        return DEV_ERROR;
    }

    ret = esp_wifi_scan_start(NULL, true);
    LOGI(TAG, "lDrvWlanScan esp_wifi_scan_start ret = %d", ret);

    return ret == ESP_OK ? DEV_EOK : DEV_ERROR;
}

static Err_t lDrvWlanJoin(struct WlanDevice_st *wlan, struct StaInfo_st *sta_info)
{
    LOGI(TAG, "F:%s L:%d", __FUNCTION__, __LINE__);

    wifi_config_t wifiConfig;
    bzero(&wifiConfig, sizeof(wifi_config_t));

    if (wlan->mode == WLAN_MODE_STATION)
    {
        wifiConfig.sta.bssid_set = 0;
        memcpy(wifiConfig.sta.ssid, sta_info->ssid.val, sta_info->ssid.len);
        memcpy(wifiConfig.sta.password, sta_info->key.val, sta_info->key.len);
        wifiConfig.sta.threshold.authmode = WIFI_AUTH_WPA2_PSK;
        wifiConfig.sta.sae_pwe_h2e = WPA3_SAE_PWE_BOTH;
    }
    else
    {
        LOGE(TAG, "F:%s L:%d wlan mode not sta", __FUNCTION__, __LINE__);
        return DEV_ERROR;
    }

    esp_err_t ret = esp_wifi_set_config(WIFI_IF_STA, &wifiConfig);
    if (ret != ESP_OK)
    {
        LOGE(TAG, "F:%s L:%d esp_wifi_set_config fail", __FUNCTION__, __LINE__);
        return DEV_ERROR;
    }

    vTaskDelay(10 / portTICK_PERIOD_MS);
    ret = esp_wifi_start();
    if (ret != ESP_OK)
    {
        LOGE(TAG, "F:%s L:%d esp_wifi_start fail", __FUNCTION__, __LINE__);
        return DEV_ERROR;
    }

    ret = esp_wifi_connect();
    LOGI(TAG, "lDrvWlanJoin esp_wifi_connect ret = %d", ret);

    return ret == ESP_OK ? DEV_EOK : DEV_ERROR;
}

static Err_t lDrvWlanSoftAP(struct WlanDevice_st *wlan, struct ApInfo_st *ap_info)
{
    LOGI(TAG, "F:%s L:%d", __FUNCTION__, __LINE__);

    wifi_config_t wifiConfig;
    memset(&wifiConfig, 0, sizeof(wifi_config_t));
    memcpy(wifiConfig.ap.ssid, ap_info->ssid.val, ap_info->ssid.len);
    memcpy(wifiConfig.ap.password, ap_info->key.val, ap_info->key.len);
    wifiConfig.ap.ssid_len = ap_info->ssid.len;
    wifiConfig.ap.max_connection = 2;
    wifiConfig.ap.authmode = WIFI_AUTH_WPA_WPA2_PSK;
    wifiConfig.ap.channel = ap_info->channel;
    wifiConfig.ap.beacon_interval = 100;

    if (ap_info->key.len == 0)
    {
        wifiConfig.ap.authmode = WIFI_AUTH_OPEN;
    }

    esp_err_t ret = esp_wifi_set_config(WIFI_IF_AP, &wifiConfig);
    if (ret != ESP_OK)
    {
        LOGE(TAG, "F:%s L:%d esp_wifi_set_config fail", __FUNCTION__, __LINE__);
        return DEV_ERROR;
    }

    ret = esp_wifi_start();
    LOGI(TAG, "lDrvWlanSoftAP esp_wifi_start ret = %d", ret);

    return ret == ESP_OK ? DEV_EOK : DEV_ERROR;
}

static Err_t lDrvWlanDisconnect(struct WlanDevice_st *wlan)
{
    LOGI(TAG, "F:%s L:%d", __FUNCTION__, __LINE__);

    esp_err_t ret = esp_wifi_disconnect();
    LOGI(TAG, "lDrvWlanDisconnect esp_wifi_disconnect ret = %d", ret);

    return ret == ESP_OK ? DEV_EOK : DEV_ERROR;
}

static Err_t lDrvWlanAPStop(struct WlanDevice_st *wlan)
{
    LOGI(TAG, "F:%s L:%d", __FUNCTION__, __LINE__);
    esp_err_t ret = esp_wifi_stop();
    LOGI(TAG, "lDrvWlanAPStop esp_wifi_stop ret = %d", ret);

    return ret == ESP_OK ? DEV_EOK : DEV_ERROR;
}

static Err_t lDrvWlanAPDeauth(struct WlanDevice_st *wlan, Uint8_t mac[])
{
    LOGI(TAG, "F:%s L:%d", __FUNCTION__, __LINE__);
    uint16_t aid = 0;

    esp_err_t ret = esp_wifi_ap_get_sta_aid(mac, &aid);
    if (ret != ESP_OK)
    {
        LOGE(TAG, "F:%s L:%d esp_wifi_ap_get_sta_aid fail", __FUNCTION__, __LINE__);
        return DEV_ERROR;
    }

    ret = esp_wifi_deauth_sta(aid);
    LOGI(TAG, "lDrvWlanAPDeauth esp_wifi_deauth_sta ret = %d", ret);

    return ret == ESP_OK ? DEV_EOK : DEV_ERROR;
}

static Err_t lDrvWlanScanStop(struct WlanDevice_st *wlan)
{
    LOGI(TAG, "F:%s L:%d", __FUNCTION__, __LINE__);

    esp_err_t ret = esp_wifi_scan_stop();
    LOGI(TAG, "lDrvWlanScanStop esp_wifi_scan_stop ret = %d", ret);

    return ret == ESP_OK ? DEV_EOK : DEV_ERROR;
}

static int lDrvWlanGetRssi(struct WlanDevice_st *wlan)
{
    LOGI(TAG, "F:%s L:%d", __FUNCTION__, __LINE__);

    wifi_ap_record_t apRecords;
    memset(&apRecords, 0, sizeof(wifi_ap_record_t));

    esp_err_t ret = esp_wifi_sta_get_ap_info(&apRecords);

    return -apRecords.rssi - 1;
}

static Err_t lDrvWlanSetPowerSave(struct WlanDevice_st *wlan, int level)
{
    LOGI(TAG, "F:%s L:%d", __FUNCTION__, __LINE__);

    wifi_ps_type_t type;

    if (level == 2)
    {
        type = WIFI_PS_MAX_MODEM;
    }
    else if (level == 1)
    {
        type = WIFI_PS_MIN_MODEM;
    }
    else
    {
        type = WIFI_PS_NONE;
    }

    esp_err_t ret = esp_wifi_set_ps(type);
    LOGI(TAG, "lDrvWlanSetPowerSave esp_wifi_set_ps ret = %d", ret);

    return ret == ESP_OK ? DEV_EOK : DEV_ERROR;
}

static int lDrvWlanGetPowerSave(struct WlanDevice_st *wlan)
{
    LOGI(TAG, "F:%s L:%d", __FUNCTION__, __LINE__);

    wifi_ps_type_t type = WIFI_PS_NONE;

    esp_err_t ret = esp_wifi_get_ps(&type);
    LOGI(TAG, "lDrvWlanGetPowerSave esp_wifi_get_ps ret = %d", ret);

    return (int)type;
}

static Err_t lDrvWlanCfgPromisc(struct WlanDevice_st *wlan, Bool_t start)
{
    LOGI(TAG, "F:%s L:%d", __FUNCTION__, __LINE__);

    esp_err_t ret = esp_wifi_set_promiscuous(start);
    LOGI(TAG, "lDrvWlanCfgPromisc esp_wifi_set_promiscuous ret = %d", ret);

    if (ret != ESP_OK)
    {
        LOGE(TAG, "F:%s L:%d fail", __FUNCTION__, __LINE__);
        return DEV_ERROR;
    }

    if (TRUE == start)
    {
        ret = esp_wifi_set_promiscuous_rx_cb(vWlanEspPromiscDdataFrameCallback);
    }
    else
    {
        ret = esp_wifi_set_promiscuous_rx_cb(NULL);
    }

    return ret == ESP_OK ? DEV_EOK : DEV_ERROR;
}

static Err_t lDrvWlanCfgFilter(struct WlanDevice_st *wlan, struct WlanFilter_st *filter)
{
    LOGI(TAG, "F:%s L:%d", __FUNCTION__, __LINE__);

    wifi_promiscuous_filter_t proFilter;
    esp_wifi_set_promiscuous_filter(&proFilter);

    return DEV_EINVAL;/* not support */
}

static Err_t lDrvWlanSetChannel(struct WlanDevice_st *wlan, int channel)
{
    LOGI(TAG, "F:%s L:%d", __FUNCTION__, __LINE__);

    esp_err_t ret = esp_wifi_set_channel(channel, WIFI_SECOND_CHAN_NONE);
    LOGI(TAG, "lDrvWlanSetChannel esp_wifi_set_channel ret = %d", ret);

    return ret == ESP_OK ? DEV_EOK : DEV_ERROR;
}

static int lDrvWlanGetChannel(struct WlanDevice_st *wlan)
{
    LOGI(TAG, "F:%s L:%d", __FUNCTION__, __LINE__);

    uint8_t primary;
    wifi_second_chan_t second;
    esp_err_t ret = esp_wifi_get_channel(&primary, &second);
    LOGI(TAG, "lDrvWlanGetChannel esp_wifi_get_channel ret = %d", ret);

    return primary;
}

static Err_t lDrvWlanSetCountry(struct WlanDevice_st *wlan, CountryCode_t countryCode)
{
    char country[3] = {0};

    vGetCountryStrFromCode(countryCode, country);

    esp_err_t ret = esp_wifi_set_country_code(country, false);
    LOGI(TAG, "lDrvWlanSetCountry esp_wifi_set_country_code ret = %d", ret);

    return ret == ESP_OK ? DEV_EOK : DEV_ERROR;
}

static CountryCode_t xDrvWlanGetCountry(struct WlanDevice_st *wlan)
{
    CountryCode_t countryCode = COUNTRY_UNKNOWN;
    char country[3] = {0};

    esp_err_t ret = esp_wifi_get_country_code(country);
    LOGI(TAG, "xDrvWlanGetCountry esp_wifi_get_country_code ret = %d, country = %s", ret, country);

    vGetCountryCodeFromStr(country, &countryCode);
    LOGI(TAG, "xDrvWlanGetCountry country_code = %d", countryCode);

    return countryCode;
}

static Err_t lDrvWlanSetMac(struct WlanDevice_st *wlan, Uint8_t mac[])
{
    if (wlan->mode != WLAN_MODE_STATION)
    {
        LOGE(TAG, "%s %d wlan mode not sta", __FUNCTION__, __LINE__);
        return DEV_ERROR;
    }

    esp_err_t ret = esp_wifi_set_mac(WIFI_IF_STA, mac);
    LOGI(TAG, "lDrvWlanSetMac esp_wifi_set_mac ret = %d", ret);

    return ret == ESP_OK ? DEV_EOK : DEV_ERROR;
}

static Err_t lDrvWlanGetMac(struct WlanDevice_st *wlan, Uint8_t mac[])
{
    if (wlan->mode != WLAN_MODE_STATION)
    {
        LOGE(TAG, "%s %d wlan mode not sta", __FUNCTION__, __LINE__);
        return DEV_ERROR;
    }

    esp_err_t ret = esp_wifi_get_mac(WIFI_IF_STA, mac);
    LOGI(TAG, "lDrvWlanGetMac esp_wifi_get_mac ret = %d", ret);

    return ret == ESP_OK ? DEV_EOK : DEV_ERROR;
}

static Err_t lDrvWlanLowLevelOutput(struct netif *netif, struct pbuf *p)
{
    esp_netif_t *esp_netif = netif->state;
    if (esp_netif == NULL)
    {
        return DEV_ERROR;
    }

    struct pbuf *q = p;
    esp_err_t ret;

    if(q->next == NULL)
    {
        ret = esp_netif_transmit_wrap(esp_netif, q->payload, q->len, q);
    }
    else
    {
        LWIP_DEBUGF(PBUF_DEBUG, ("low_level_output: pbuf is a list, application may has bug"));
        q = pbuf_alloc(PBUF_RAW_TX, p->tot_len, PBUF_RAM);
        if (q != NULL)
        {
            pbuf_copy(q, p);
        }
        else
        {
            return ERR_MEM;
        }
        ret = esp_netif_transmit_wrap(esp_netif, q->payload, q->len, q);

        pbuf_free(q);
    }

    if (ret == ESP_OK)
    {
        return DEV_EOK;
    }
    if (ret == ESP_ERR_NO_MEM)
    {
        return DEV_ENOMEM;
    }
    if (ret == ESP_ERR_INVALID_ARG)
    {
        return DEV_EINVAL;
    }
    return DEV_ERROR;
}

static int lDrvWlanRecv(struct WlanDevice_st *wlan, void *buff, int len)
{
    return DEV_EOK;
}

static int lDrvWlanSend(struct WlanDevice_st *wlan, void *buff, int len)
{
    struct LwipProtDes_st *lwip_prot = container_of(((struct WlanProt_st *)(wlan->prot)), struct LwipProtDes_st, prot);
    struct pbuf *p = container_of(&buff, struct pbuf, payload);

    return lDrvWlanLowLevelOutput(lwip_prot->eth.netif, p);
}

static Err_t lDrvWlanInput(struct WlanDevice_st *wlan, void *args)
{
    struct WlanTransInputFuncArgs_st *wiargs = (struct WlanTransInputFuncArgs_st *)args;

    wlanif_input(wiargs->wlanif, wiargs->buffer, wiargs->len, wiargs->l2_buff);
    return DEV_EOK;
}

static Err_t lDrvWlanOutput(struct WlanDevice_st *wlan, void *args)
{
    struct WlanTransOutputFuncArgs_st *woargs = (struct WlanTransOutputFuncArgs_st *)args;

    wlanif_output(woargs->wlanif, woargs->pbuf);
    return DEV_EOK;
}


const struct WlanDevOps_st ops =
{
    .wlan_init              = lDrvWlanInit,
    .wlan_mode              = lDrvWlanMode,
    .wlan_scan              = lDrvWlanScan,
    .wlan_join              = lDrvWlanJoin,
    .wlan_softap            = lDrvWlanSoftAP,
    .wlan_disconnect        = lDrvWlanDisconnect,
    .wlan_ap_stop           = lDrvWlanAPStop,
    .wlan_ap_deauth         = lDrvWlanAPDeauth,
    .wlan_scan_stop         = lDrvWlanScanStop,
    .wlan_get_rssi          = lDrvWlanGetRssi,
    .wlan_set_powersave     = lDrvWlanSetPowerSave,
    .wlan_get_powersave     = lDrvWlanGetPowerSave,
    .wlan_cfg_promisc       = lDrvWlanCfgPromisc,
    .wlan_cfg_filter        = lDrvWlanCfgFilter,
    .wlan_set_channel       = lDrvWlanSetChannel,
    .wlan_get_channel       = lDrvWlanGetChannel,
    .wlan_set_country       = lDrvWlanSetCountry,
    .wlan_get_country       = xDrvWlanGetCountry,
    .wlan_set_mac           = lDrvWlanSetMac,
    .wlan_get_mac           = lDrvWlanGetMac,
    .wlan_recv              = lDrvWlanRecv,
    .wlan_send              = lDrvWlanSend,
    .wlan_drv_input         = lDrvWlanInput,
    .wlan_drv_output        = lDrvWlanOutput,
};

int lDrvHWWifiInit(void)
{
    static struct WlanDevice_st wlan;
    static struct WlanDevice_st wlan2;

    memset(&lHalWifiSta, 0, sizeof(lHalWifiSta));
    Err_t ret = lHalWlanDevRegister(&wlan, WLAN_DEVICE_STA_NAME, &ops, 0, &lHalWifiSta);
    lHalWifiSta.wlan = &wlan;

    memset(&lHalWifiAp, 0, sizeof(lHalWifiAp));
    ret |= lHalWlanDevRegister(&wlan2, WLAN_DEVICE_AP_NAME, &ops, 0, &lHalWifiAp);
    lHalWifiAp.wlan = &wlan2;

    return ret;
}
INIT_PREV_EXPORT(lDrvHWWifiInit);
