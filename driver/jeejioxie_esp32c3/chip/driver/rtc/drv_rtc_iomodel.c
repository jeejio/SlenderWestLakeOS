/*
 * Copyright (c) 2022, jeejio Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author         Notes
 * 2023-2-7       zhengqian      the first version
*/

#include "freertos/FreeRTOS.h"
#include "freertos/device.h"
#include "hal_rtc.h"
#include "driver/gptimer.h"
#include <sys/time.h>
#include "auto_init.h"
#include "string.h"
#include "esp_log.h"

#if defined(USING_SOFT_RTC)
#error "Please CANCEL the USING_SOFT_RTC option. Make sure there is only one RTC device on the system."
#endif

static struct RtcDevice_st rtc_device;


static Err_t lHalRtcInit(void)
{

    return DEV_EOK;
}


Err_t lDrvGetSecs(time_t *sec)
{
    struct timeval tv;
    gettimeofday(&tv, NULL);
    *sec = (time_t)tv.tv_sec + (time_t)(tv.tv_usec/(1e6));

    return DEV_EOK;
}

Err_t lDrvSetSecs(time_t *sec)
{
    struct timeval tv = {0};
    tv.tv_sec = *sec;
    settimeofday(&tv, NULL);    

    return DEV_EOK;
}

Err_t lDrvGetTimeval(struct timeval *tv)
{
    gettimeofday(tv, NULL);

    return DEV_EOK;
}

Err_t lDrvSetTimeval(struct timeval *tv)
{
    settimeofday(tv, NULL);

    return DEV_EOK;
}

struct RtcOps_st rtc_ops = {
    lHalRtcInit,
    lDrvGetSecs,
    lDrvSetSecs,
    lDrvGetTimeval,
    lDrvSetTimeval
};

int lDrvHwRtcInit(void)
{
    rtc_device.ops = &rtc_ops;
    lHalHwRtcRegister(&rtc_device, "rtc", DEVICE_FLAG_RDWR, NULL);

    return 0;
}
INIT_DEVICE_EXPORT(lDrvHwRtcInit);
