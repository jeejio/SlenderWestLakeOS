/*
 * Copyright (c) 2022, jeejio Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author         Notes
 * 2022-12-07                    the first version
*/

#ifndef __DRV_UART_IOMODEL_H__
#define __DRV_UART_IOMODEL_H__

#ifdef __cplusplus
extern "C" {
#endif

#define DEVICE_CTRL_UART_READ  0xF1
#define DEVICE_CTRL_UART_WRITE 0xF2
#define DEVICE_CTRL_UART_GET_FIFO_LEN 0xF3
#define DEVICE_CTRL_UART_FIFO_READ 0xF4
#define DEVICE_CRTL_UART_FIFO_SEND 0xF5
#define DEVICE_CTRL_UART_SET_CALLBACK 0xF6
#define DEVICE_CTRL_UART_CLR_MASK 0xF7

#define UART_READ    0x1
#define UART_WRITE   0x2

#define UART_READ_TIMEOUT_MS  20

#define BSP_USING_UART0
#define BSP_USING_UART1

#define UART_EMPTY_THRESH_DEFAULT       (10)
#define UART_FULL_THRESH_DEFAULT        (120)
#define UART_TOUT_THRESH_DEFAULT        (10)

// Define UART interrupts
typedef enum {
    UART_RXFIFO_FULL      = (0x1 << 0),
    UART_RXFIFO_OVF       = (0x1 << 4),
    UART_RXFIFO_TOUT      = (0x1 << 8),
} uartIntr_t;;

int lDrvHwUartInit(void);

#ifdef __cplusplus
}
#endif

#endif /* __DRV_UART_IOMODEL_H__ */
