/*
 * Copyright (c) 2023, JEE Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2023-02-28      lk        first version
 */

#ifndef __DRV_AUDIO_SOUND_IOMODEL_H__
#define __DRV_AUDIO_SOUND_IOMODEL_H__

#include "swldef.h"

int lDrvAudioSoundInit(void);

#endif /* __DRV_AUDIO_SOUND_IOMODEL_H__ */
