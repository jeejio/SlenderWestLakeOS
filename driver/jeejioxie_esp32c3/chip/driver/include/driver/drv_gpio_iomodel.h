/*
 * Copyright (c) 2022, jeejio Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author         Notes
 * 2022-12-07     zhengqian      the first version
*/

#ifndef __DRV_GPIO_H__
#define __DRV_GPIO_H__


int lDrvHwGpioInit(void);

#endif

