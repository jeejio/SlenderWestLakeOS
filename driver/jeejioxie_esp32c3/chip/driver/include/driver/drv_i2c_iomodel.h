/*
 * Copyright (c) 2023, Jeejio Technology Co. Ltd
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2023-03-08     Alpha        First Verison
 *
 */
#ifndef __DRV_I2C_H__
#define __DRV_I2C_H__

#ifdef __cplusplus
extern "C" {
#endif

#define SDA_GPIO (8)
#define SCL_GPIO (9)
#define I2C_DEV_NAME    "i2c0hard"
#define TAG "DHT20"

/*iic Éè±¸³õÊ¼»¯*/
#define IIC_BUS_CMD_INIT        (1)

extern int lDrvHwI2cInit(void);
extern void i2c_master_init(void);

#ifdef __cplusplus
}
#endif


#endif  /* __DRV_I2C_H__ */
