/*
 * Copyright (c) 2023, Jeejio Technology Co. Ltd
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2023-03-08     Alpha        First Verison
 *
 */
#ifndef __DRV_SPI_MASTER_H__
#define __DRV_SPI_MASTER_H__

#ifdef __cplusplus
extern "C" {
#endif

#define HWTIMER_RESOLUTION_HZ 1000000 // 1MHz resolution


extern int hw_timer_init(void);

#ifdef __cplusplus
}
#endif


#endif  /* __DRV_HWTIMER_H__ */