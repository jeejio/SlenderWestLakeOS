/*
 * Copyright (c) 2023, Jeejio Technology Co. Ltd
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2023-03-08     Alpha        First Verison
 *
 */
#ifndef __DRV_SPI_SLAVE_H__
#define __DRV_SPI_SLAVE_H__

#ifdef __cplusplus
extern "C" {
#endif
#include <swldef.h>
#include <spi_common.h>
#include <driver/gpio.h>

#define GPIO_HANDSHAKE 3
#define PIN_NUM_MISO 2
#define PIN_NUM_MOSI 7
#define PIN_NUM_CLK 6
#define PIN_NUM_CS 10
#define SPI_BUS_NAME    "spi2"
#define SPI_DEV_NAME    "spi20"
#define SLAVE_HOST SPI2_HOST


extern int lDrvSpiSlaveInit(void);
extern Err_t esp_spi_bus_attach_device(const char *bus_name, const char *device_name, Uint32_t pin);

#ifdef __cplusplus
}
#endif


#endif  /* __DRV_I2C_H__ */