/*
 * Copyright (c) 2022-2023, Jeejio Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2023-02-24     None         the first version
 */

#ifndef __DRV_HWTIMER_H__
#define __DRV_HWTIMER_H__

#include "swldef.h"
#include "hal_hrtimer.h"
#include "hal_config.h"

#ifdef __cplusplus
extern "C" {
#endif

#ifdef SWL_USING_HWTIMER
struct hrtimerData
{
    EHwtimerMode_t timerMode;
    HwtimerVal_t   timeoutVal;
    esp_timer_create_args_t crtTimerArgs;
    esp_timer_handle_t timerHandle;
};


#ifdef USING_PERIODIC_TIMER_SPLIT
void vDrvHwtimerPeriodicTimerCbRegister(void* cb);
#endif

#ifdef USING_ONESHOT_TIMER_SPLIT
void vDrvHwtimerOneshotTimerCbRegister(void* cb);
#endif

#ifdef USING_TIMER_BIND_UNIQUE
void vDrvHwtimerHookInfoRegister(void *cb, void *arg, HwtimerVal_t *toutVal, char *name);
#endif

#ifdef SWL_USING_HWTIMER
//int lDrvHrtimerEsp32c3DevInit(void);
#endif

#endif /* SWL_USING_HWTIMER */

#ifdef __cplusplus
}
#endif

#endif
