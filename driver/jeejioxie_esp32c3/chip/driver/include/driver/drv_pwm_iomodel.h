/*
 * Copyright (c) 2022, jeejio Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author         Notes
 * 2023-2-3       zhengqian      the first version
*/

#ifndef __DRV_PWM_H__
#define __DRV_PWM_H__

int lDrvHwPwmInit(void);

#endif
