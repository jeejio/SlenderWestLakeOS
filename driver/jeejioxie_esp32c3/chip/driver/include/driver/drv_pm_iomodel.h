/*
 * Copyright (c) 2022, jeejio Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author         Notes
 * 2023-2-28       lk            the first version
*/

#ifndef __DRV_PM_H__
#define __DRV_PM_H__

#include <freertos/FreeRTOS.h>

#ifdef CONFIG_PM_ENABLE

#include "hal_config.h"

#ifdef __cplusplus
extern "C" {
#endif


int lDrvPMHWInit(void);

#ifdef __cplusplus
}
#endif

#endif /* CONFIG_PM_ENABLE */

#endif /* __DRV_PM_H__ */
