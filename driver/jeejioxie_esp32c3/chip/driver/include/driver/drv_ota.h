#ifndef __DRV_OTA_H__
#define __DRV_OTA_H__

#include "stdint.h"
// #include "stddef.h"

#if 0
int lDrvOtaGetState(void);

char* pcDrvOtaGetVersion(void);

int lDrvOtaUpgrade(char *url, char *key, char *ivec);

int lDrvOtaDownloadPre(void);
#endif

int lDrvOtaInit(void);

int lDrvOtaGetUpdateParSize(void);

int lDrvOtaBegin(void);

int lDrvOtaWriteFlash(uint8_t *data,uint16_t len);

int lDrvOtaFinish(void);

void vDrvRestart(void);

int lDrvOtaFwVerify(char *data);
#endif