#include "drv_ota.h"
#include "stdint.h"
#include "stddef.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/semphr.h"
#include "esp_log.h"
#include "esp_system.h"
#include "esp_ota_ops.h"
#include "esp_app_format.h"
#include "esp_flash_partitions.h"
#include "esp_partition.h"
#include "aes.h"
#include "webclient.h"
#include "mbedtls/sha256.h"
#include "mbedtls/platform_util.h"
#include "mbedtls/error.h"

#define TAG "drv_ota"

esp_ota_handle_t update_handle = 0;
const esp_partition_t *update_partition = NULL;
static mbedtls_sha256_context ctx;
int sha256_flag=0;
static unsigned char sha256sum[32];

int StringToHex(char *str, unsigned char *out, unsigned int *outlen)
{
    char *p = str;
    char high = 0, low = 0;
    int tmplen = strlen(p), cnt = 0;
    tmplen = strlen(p);
    while(cnt < (tmplen / 2))
    {
        high = ((*p > '9') && ((*p <= 'F') || (*p <= 'f'))) ? *p - 48 - 7 : *p - 48;
        low = (*(++ p) > '9' && ((*p <= 'F') || (*p <= 'f'))) ? *(p) - 48 - 7 : *(p) - 48;
        out[cnt] = ((high & 0x0f) << 4 | (low & 0x0f));
        p ++;
        cnt ++;
    }
    if(tmplen % 2 != 0) out[cnt] = ((*p > '9') && ((*p <= 'F') || (*p <= 'f'))) ? *p - 48 - 7 : *p - 48;
    
    if(outlen != NULL) *outlen = tmplen / 2 + tmplen % 2;
    return tmplen / 2 + tmplen % 2;
}
            
int lDrvOtaInit(void)
{
    esp_err_t err;

    const esp_partition_t *configured = esp_ota_get_boot_partition();
    const esp_partition_t *running = esp_ota_get_running_partition();

    if (configured!=NULL && running!=NULL && configured != running)
    {
        ESP_LOGW(TAG, "Configured OTA boot partition at offset 0x%08lx, but running from offset 0x%08lx",
                 configured->address, running->address);
        ESP_LOGW(TAG, "(This can happen if either the OTA boot data or preferred boot image become corrupted somehow.)");
    }
    if ( running!=NULL){
    ESP_LOGW(TAG, "Running partition type %d subtype %d (offset 0x%08lx)",
             running->type, running->subtype, running->address);
    }

    mbedtls_sha256_init( &ctx );
    sha256_flag=0;
    if( ( mbedtls_sha256_starts( &ctx, 0 ) ) != 0 )
           sha256_flag=-1;
    return 0;


}

int lDrvOtaGetUpdateParSize(void)
{
    update_partition = esp_ota_get_next_update_partition(NULL);
    assert(update_partition != NULL);
    if(update_partition!=NULL){
    ESP_LOGI(TAG, "Writing to partition subtype %d at offset 0x%lx",
             update_partition->subtype, update_partition->address);
    return update_partition->size;
    }
    else
    {
        return 0;
    }
}

int lDrvOtaBegin(void)
{
    int err = 0;

    err = esp_ota_begin(update_partition, OTA_WITH_SEQUENTIAL_WRITES, &update_handle);
    if (err != ESP_OK)
    {
        ESP_LOGE(TAG, "esp_ota_begin failed (%s)", esp_err_to_name(err));
        esp_ota_abort(update_handle);
    }
    else
    {
        ESP_LOGI(TAG, "esp_ota_begin succeeded");
    }
    return err;
}

int lDrvOtaWriteFlash(uint8_t *data, uint16_t len)
{
    int err = 0;
    err = esp_ota_write(update_handle, (const void *)data, len);
    if (err != ESP_OK)
    {
        esp_ota_abort(update_handle);
    }
    if( mbedtls_sha256_update( &ctx, data, len )!=0)
     sha256_flag|=-1;
    return err;
}

int lDrvOtaFinish(void)
{
    int err = 0;
    err = esp_ota_end(update_handle);
    err = esp_ota_set_boot_partition(update_partition);
    if (err != ESP_OK)
    {
        ESP_LOGE(TAG, "esp_ota_set_boot_partition failed (%s)!", esp_err_to_name(err));
    }
    return err;
}

void vDrvRestart(void)
{
    esp_restart();
}

int lDrvOtaFwVerify(char *data)
{
    if( ( mbedtls_sha256_finish( &ctx, sha256sum ) ) != 0 )
     sha256_flag |=-1;
    if(sha256_flag!=0 || data==NULL) return -1;
    uint8_t buff[32];
    uint32_t len =0;
    StringToHex(data,buff,&len);
    if(memcmp(buff,sha256sum,32)==0)
    {
        return 0;
    }
    return -1;
}