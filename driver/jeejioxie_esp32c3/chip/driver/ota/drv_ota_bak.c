#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/semphr.h"
#include "drv_ota.h"
// #include "sys_data_config.h"
#include "aes.h"
#include "errno.h"
#include "esp_system.h"
#include "esp_ota_ops.h"
#include "esp_app_format.h"
#include "esp_flash_partitions.h"
#include "esp_partition.h"
#include "nvs.h"
#include "nvs_flash.h"
#include "esp_log.h"
#include "webclient.h"

#define TAG "hal_ota"
#define HTTP_OTA_BUFF_LEN 4096

static SemaphoreHandle_t ota_semphr = NULL;

int download_per = 0;
int upgrade_flag = 0;

int32_t http_ota_fw_download_task(const int8_t *uri, const int8_t *key, const int8_t *vi);
void http_ota_task(void *para)
{
    int32_t ret = -1;
    const int8_t *pUpgradeKey = NULL;
    const int8_t *pUpgradeIV = NULL;
    const int8_t *pUpgradeURL = NULL;
    const uint8_t *pversion;
    const uint8_t *pfirmware_id;
    uint8_t sendBuffer[128] = {0};

    ota_semphr = xSemaphoreCreateBinary();
    if (ota_semphr == NULL)
    {
        LOGE(TAG, "ota semphr create failed!");
    }
    else
    {
        xSemaphoreTake(ota_semphr, 0);
    }
    while (1)
    {
        xSemaphoreTake(ota_semphr, portMAX_DELAY);
        upgrade_flag = 1;

        // pUpgradeKey = sys_config_manage(SYS_UPGRADE_KEY, SYS_READ, NULL, 0);
        // pUpgradeIV = sys_config_manage(SYS_UPGRADE_IV, SYS_READ, NULL, 0);
        // pUpgradeURL = sys_config_manage(SYS_UPGRADE_URL, SYS_READ, NULL, 0);
        ret = http_ota_fw_download_task(pUpgradeURL, pUpgradeKey, pUpgradeIV);
        if (ret < 0)
        {
            upgrade_flag = 0;
            clear_download_per();
            // pfirmware_id = sys_config_manage(SYS_FIRMWARE_ID_OTA, SYS_READ, NULL, NULL);
            // pversion = sys_config_manage(SYS_VERSION_OTA, SYS_READ, NULL, NULL);
            // LOGI(TAG, "ota  version  %s\r\n", pversion);

            // sprintf((char *)sendBuffer, "{\"firmwareID\":\"%s\",\"feedback\":%d,\"version\":%d}", pfirmware_id, 2, versionStringToint(pversion));
            // jeesdk_send_to_server(__jeesdk, -3000000000000010004, UDFB, sendBuffer, strlen((const char *)sendBuffer), NULL);

            LOGE(TAG, "ota fail --------");
        }
    }
}

void start_ota_process(void)
{
    xSemaphoreGive(ota_semphr);
}

int32_t http_ota_fw_download_task(const int8_t *uri, const int8_t *key, const int8_t *vi)
{
    int32_t length, total_length = 0;
    uint8_t need_reboot = 0;
    int32_t resp_status = (-1);

    uint8_t *buffer_read = NULL;
    uint8_t *buffer_swap = NULL;

    struct webclient_session *session = NULL;
    esp_err_t err;
    esp_ota_handle_t update_handle = 0;
    const esp_partition_t *update_partition = NULL;

    LOGI(TAG, "Starting OTA example");

    const esp_partition_t *configured = esp_ota_get_boot_partition();
    const esp_partition_t *running = esp_ota_get_running_partition();

    if (configured != running)
    {
        LOGW(TAG, "Configured OTA boot partition at offset 0x%08x, but running from offset 0x%08x",
                 (unsigned int)(long)&(configured->address), (unsigned int)(long)&(running->address));
        LOGW(TAG, "(This can happen if either the OTA boot data or preferred boot image become corrupted somehow.)");
    }
    LOGI(TAG, "Running partition type %d subtype %d (offset 0x%08x)",
             running->type, running->subtype, (unsigned int)(long)&(running->address));

    AES_KEY aes_key_dec;
    uint8_t decryptData[HTTP_OTA_BUFF_LEN];

    session = webclient_session_create(1024);
    if (!session)
    {
        LOGE(TAG, "open uri failed.");
        goto __exit;
    }

    resp_status = webclient_get(session, (char *)uri);
    if (resp_status == 304)
    {
        LOGE(TAG, "Firmware download failed! Server http response : 304 not modified!");
        goto __exit;
    }

    if (resp_status != 200)
    {
        LOGE(TAG, "Firmware download failed! Server http response : %ld !", resp_status);
        goto __exit;
    }

    if (session->content_length == 0)
    {
        LOGE(TAG, "Request file size is 0!");
        goto __exit;
    }

    update_partition = esp_ota_get_next_update_partition(NULL);
    assert(update_partition != NULL);
    // LOGI(TAG, "Writing to partition subtype %d at offset 0x%2x",
    //          update_partition->subtype, update_partition->address);
    bool image_header_was_checked = false;

    // Check if the OTA file is too large
    if (session->content_length > update_partition->size)
    {
        LOGE(TAG, "Firmware download failed! OTA file is too big!");
        goto __exit;
    }

    LOGI(TAG, "ota partition size  : %ld\n", update_partition->size);
    LOGI(TAG, "OTA file size is (%d)", session->content_length);

    buffer_read = web_malloc(HTTP_OTA_BUFF_LEN);
    buffer_swap = web_malloc(HTTP_OTA_BUFF_LEN);
    if ((buffer_read == NULL) || (buffer_swap == NULL))
    {
        LOGE(TAG, "No memory for http ota!");
        goto __exit;
    }
    if (AES_set_decrypt_key((unsigned char *)key, 128, &aes_key_dec) != 0)
    {
        LOGE(TAG, "AES_set_decrypt_key()  error\r\n");
        goto __exit;
    }

    do
    {
        uint32_t nw;
        uint32_t page_pos;

        nw = HTTP_OTA_BUFF_LEN;
        if ((nw + total_length) > session->content_length)
        {
            nw = session->content_length - total_length;
        }
        page_pos = 0;
        do
        {
            length = webclient_read(session, buffer_read + page_pos, nw - page_pos);
            if (length <= 0)
            {
                LOGE(TAG, "read error: %ld, content_pos:%ld, page_pos:%ld\n", length, total_length, page_pos);
                break;
            }
            page_pos += length;
        } while (page_pos < nw); /* read */

        if (page_pos != nw)
        {
            LOGE(TAG, "page_pos error, %ld:%ld\n", page_pos, nw);
            goto __exit;
        }
        AES_cbc_encrypt(buffer_read, decryptData, nw, &aes_key_dec, vi, AES_DECRYPT);
        if (image_header_was_checked == false)
        {
            esp_app_desc_t new_app_info;
            memcpy(&new_app_info, &decryptData[sizeof(esp_image_header_t) + sizeof(esp_image_segment_header_t)], sizeof(esp_app_desc_t));
            LOGI(TAG, "New firmware version: %s", new_app_info.version);

            esp_app_desc_t running_app_info;
            if (esp_ota_get_partition_description(running, &running_app_info) == ESP_OK)
            {
                LOGI(TAG, "Running firmware version: %s", running_app_info.version);
            }

            const esp_partition_t *last_invalid_app = esp_ota_get_last_invalid_partition();
            esp_app_desc_t invalid_app_info;
            if (esp_ota_get_partition_description(last_invalid_app, &invalid_app_info) == ESP_OK)
            {
                LOGI(TAG, "Last invalid firmware version: %s", invalid_app_info.version);
            }

            // check current version with last invalid partition
            if (last_invalid_app != NULL)
            {
                if (memcmp(invalid_app_info.version, new_app_info.version, sizeof(new_app_info.version)) == 0)
                {
                    LOGW(TAG, "New version is the same as invalid version.");
                    LOGW(TAG, "Previously, there was an attempt to launch the firmware with %s version, but it failed.", invalid_app_info.version);
                    LOGW(TAG, "The firmware has been rolled back to the previous version.");
                }
            }
            image_header_was_checked = true;

            err = esp_ota_begin(update_partition, OTA_WITH_SEQUENTIAL_WRITES, &update_handle);
            if (err != ESP_OK)
            {
                LOGE(TAG, "esp_ota_begin failed (%s)", esp_err_to_name(err));
                esp_ota_abort(update_handle);
            }
            else
            {
                LOGI(TAG, "esp_ota_begin succeeded");
            }
        }

        err = esp_ota_write(update_handle, (const void *)decryptData, nw);
        if (err != ESP_OK)
        {
            esp_ota_abort(update_handle);
        }
        total_length += nw;
        print_progress(total_length, session->content_length);
    } while (total_length != session->content_length);

    if (total_length == session->content_length)
    {
        need_reboot = 1;
        err = esp_ota_end(update_handle);
        err = esp_ota_set_boot_partition(update_partition);
        if (err != ESP_OK)
        {
            LOGE(TAG, "esp_ota_set_boot_partition failed (%s)!", esp_err_to_name(err));
        }
    }

__exit:
    if (session != NULL)
        webclient_close(session);
    if (buffer_read != NULL)
        web_free(buffer_read);
    if (buffer_swap != NULL)
        web_free(buffer_swap);

    /* Reset the device, Start new firmware */
    if (need_reboot)
    {
        vTaskDelay(100);

        esp_restart();
    }

    return -1;
}

void print_progress(size_t cur_size, size_t total_size)
{
    static uint8_t progress_sign[100 + 1];
    uint8_t i, per = cur_size * 100 / total_size;

    if (per > 100)
    {
        per = 100;
    }
    download_per = per;
    for (i = 0; i < 100; i++)
    {
        if (i < per)
        {
            progress_sign[i] = '=';
        }
        else if (per == i)
        {
            progress_sign[i] = '>';
        }
        else
        {
            progress_sign[i] = ' ';
        }
    }

    progress_sign[sizeof(progress_sign) - 1] = '\0';

    // LOGI(TAG, "\r"
    //        "\033[36;22m"
    //        "Download: [%s] %d%% "
    //        "\033[0m",
    //        progress_sign, per);
}

int32_t http_ota_init(void)
{
    int ret = 0;

    ret = xTaskCreate(http_ota_task, "http_ota", 0x2000, (void *)0, 5, NULL);
    if (ret != pdPASS)
    {
        LOGE(TAG, "https demo thread failed\r\n");
    }

    return ret;
}

int get_download_per(void)
{
    return download_per;
}

void clear_download_per(void)
{
    download_per = 0;
}

int http_ota_get_flag(void)
{
    return upgrade_flag;
}