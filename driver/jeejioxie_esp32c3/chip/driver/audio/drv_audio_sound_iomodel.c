/*
 * Copyright (c) 2023, JEE Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2023-02-28      lk        first version
 */

#include "hal_audio.h"

#define TX_FIFO_SIZE (4096)

struct sound_device
{
    struct AudioDevice_st audio;
    struct AudioConfigure_st replay_config;
    Uint8_t volume;
    Uint8_t *tx_fifo;
};

static struct sound_device snd_dev;

static Err_t lDrvAudioInit(struct AudioDevice_st *audio)
{
    // TODO

    return DEV_EOK;
}

static Err_t lDrvAudioStart(struct AudioDevice_st *audio, int stream)
{
    configASSERT(audio != NULL);

    if (stream == AUDIO_STREAM_REPLAY)
    {
        struct AudioCaps_st caps;
        caps.main_type = AUDIO_TYPE_MIXER;
        caps.sub_type = AUDIO_MIXER_VOLUME;
        audio->ops->getcaps(audio, &caps);
        audio->ops->configure(audio, &caps);
        vHalAudioTXComplete(audio);
    }
    return DEV_EOK;
}

static Err_t lDrvAudioStop(struct AudioDevice_st *audio, int stream)
{
    if (stream == AUDIO_STREAM_REPLAY)
    {
        // TODO
    }
    return DEV_EOK;
}

static Err_t lDrvAudioGetCaps(struct AudioDevice_st *audio, struct AudioCaps_st *caps)
{
    Err_t result = DEV_EOK;
    struct sound_device *snd_dev;

    configASSERT(audio != NULL);
    snd_dev = (struct sound_device *)audio->parent.user_data;

    switch (caps->main_type)
    {
    case AUDIO_TYPE_QUERY: /* qurey the types of hw_codec device */
    {
        switch (caps->sub_type)
        {
        case AUDIO_TYPE_QUERY:
            caps->udata.mask = AUDIO_TYPE_OUTPUT | AUDIO_TYPE_MIXER;
            break;

        default:
            result = -DEV_ERROR;
            break;
        }

        break;
    }

    case AUDIO_TYPE_OUTPUT: /* Provide capabilities of OUTPUT unit */
    {
        switch (caps->sub_type)
        {
        case AUDIO_DSP_PARAM:
            caps->udata.config.samplerate   = snd_dev->replay_config.samplerate;
            caps->udata.config.channels     = snd_dev->replay_config.channels;
            caps->udata.config.samplebits   = snd_dev->replay_config.samplebits;
            break;

        case AUDIO_DSP_SAMPLERATE:
            caps->udata.config.samplerate   = snd_dev->replay_config.samplerate;
            break;

        case AUDIO_DSP_CHANNELS:
            caps->udata.config.channels     = snd_dev->replay_config.channels;
            break;

        case AUDIO_DSP_SAMPLEBITS:
            caps->udata.config.samplebits   = snd_dev->replay_config.samplebits;
            break;

        default:
            result = -DEV_ERROR;
            break;
        }

        break;
    }

    case AUDIO_TYPE_MIXER: /* report the Mixer Units */
    {
        switch (caps->sub_type)
        {
        case AUDIO_MIXER_QUERY:
            caps->udata.mask = AUDIO_MIXER_VOLUME;
            break;

        case AUDIO_MIXER_VOLUME:
            caps->udata.value = snd_dev->volume;
            break;

        default:
            result = -DEV_ERROR;
            break;
        }

        break;
    }

    default:
        result = -DEV_ERROR;
        break;
    }

    return result;
}

static Err_t lDrvAudioConfigure(struct AudioDevice_st *audio, struct AudioCaps_st *caps)
{
    Err_t result = DEV_EOK;
    struct sound_device *snd_dev = audio->parent.user_data;

    switch (caps->main_type)
    {
    case AUDIO_TYPE_MIXER:
    {
        switch (caps->sub_type)
        {
        case AUDIO_MIXER_MUTE:
        {
            // TODO
            snd_dev->volume = 0;
            break;
        }

        case AUDIO_MIXER_VOLUME:
        {
            int volume = caps->udata.value / 2;
            // TODO
            snd_dev->volume = volume;
            break;
        }
        }

        break;
    }
    case AUDIO_TYPE_OUTPUT:
    {
        switch (caps->sub_type)
        {
        case AUDIO_DSP_PARAM:
        {
            struct AudioConfigure_st config = caps->udata.config;
            snd_dev->replay_config.channels = config.channels;
            snd_dev->replay_config.samplebits = config.samplebits;
            snd_dev->replay_config.samplerate = config.samplerate;
            break;
        }

        case AUDIO_DSP_SAMPLERATE:
        {
            struct AudioConfigure_st config = caps->udata.config;
            snd_dev->replay_config.samplerate = config.samplerate;
            break;
        }

        default:
            result = -DEV_ERROR;
            break;
        }
        break;
    }
    }

    return result;
}

static Size_t xDrvAudioTransmit(struct AudioDevice_st *audio, const void *writeBuf, void *readBuf, Size_t size)
{
    configASSERT(audio != NULL);
    // TODO

    return DEV_EOK;
}

static void vDrvAudioBufferInfo(struct AudioDevice_st *audio, struct AudioBufInfo_st *info)
{
    configASSERT(audio != NULL);
    /**
     *               TX_FIFO
     * +----------------+----------------+
     * |     block1     |     block2     |
     * +----------------+----------------+
     *  \  block_size  /
     */
    info->buffer      = snd_dev.tx_fifo;
    info->total_size  = TX_FIFO_SIZE;
    info->block_size  = TX_FIFO_SIZE / 2;
    info->block_count = 2;
}

static struct AudioOps_st xAudioOps =
{
    .getcaps = lDrvAudioGetCaps,
    .configure = lDrvAudioConfigure,
    .init = lDrvAudioInit,
    .start = lDrvAudioStart,
    .stop = lDrvAudioStop,
    .transmit = xDrvAudioTransmit,
    .buffer_info = vDrvAudioBufferInfo,
};

int lDrvAudioSoundInit(void)
{
    Uint8_t *tx_fifo = NULL;

    tx_fifo = malloc(TX_FIFO_SIZE);
    if (tx_fifo == NULL)
    {
        return -DEV_ENOMEM;
    }
    snd_dev.tx_fifo = tx_fifo;

    /* init default configuration */
    {
        snd_dev.replay_config.samplerate = 44100;
        snd_dev.replay_config.channels   = 2;
        snd_dev.replay_config.samplebits = 16;
        snd_dev.volume                   = 30;
    }

    snd_dev.audio.ops = &xAudioOps;
    lHalAudioRegister(&snd_dev.audio, "sound0", DEVICE_FLAG_WRONLY, &snd_dev);
    return DEV_EOK;
}
// INIT_DEVICE_EXPORT(lDrvAudioSoundInit);
