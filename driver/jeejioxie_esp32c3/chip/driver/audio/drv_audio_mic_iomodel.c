/*
 * Copyright (c) 2023, JEE Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2023-02-28      lk        first version
 */

#include "hal_audio.h"

struct mic_device
{
    struct AudioDevice_st audio;
    struct AudioConfigure_st config;
    Uint8_t *rx_fifo;
};

#define RX_DMA_FIFO_SIZE (2048)

struct mic_device mic_dev;

static Err_t lDrvAudioInit(struct AudioDevice_st *audio)
{
    // TODO
    return DEV_EOK;
}

static Err_t lDrvAudioStart(struct AudioDevice_st *audio, int stream)
{
    struct mic_device *mic_dev = (struct mic_device *)audio->parent.user_data;
    if (stream == AUDIO_STREAM_RECORD)
    {
        // TODO
    }
    return DEV_EOK;
}

static Err_t lDrvAudioStop(struct AudioDevice_st *audio, int stream)
{
    struct mic_device *mic_dev = (struct mic_device *)audio->parent.user_data;
    if (stream == AUDIO_STREAM_RECORD)
    {
        // TODO
    }
    return DEV_EOK;
}

static Err_t lDrvAudioGetCaps(struct AudioDevice_st *audio, struct AudioCaps_st *caps)
{
    // TODO
    return DEV_EOK;
}

static Err_t lDrvAudioConfigure(struct AudioDevice_st *audio, struct AudioCaps_st *caps)
{
    // TODO
    return DEV_EOK;
}

static struct AudioOps_st _mic_audio_ops =
{
    .getcaps     = lDrvAudioGetCaps,
    .configure   = lDrvAudioConfigure,
    .init        = lDrvAudioInit,
    .start       = lDrvAudioStart,
    .stop        = lDrvAudioStop,
    .transmit    = NULL,
    .buffer_info = NULL,
};

int lDrvAudioMicInit(void)
{
    struct AudioDevice_st *audio = &mic_dev.audio;
    /* mic default */
    mic_dev.rx_fifo = calloc(1, RX_DMA_FIFO_SIZE);
    if (mic_dev.rx_fifo == NULL)
    {
        return -DEV_ENOMEM;
    }

    mic_dev.config.channels = 1;
    mic_dev.config.samplerate = 16000;
    mic_dev.config.samplebits = 16;

    /* register mic device */
    audio->ops = &_mic_audio_ops;
    lHalAudioRegister(audio, "mic0", DEVICE_FLAG_RDONLY, (void *)&mic_dev);

    return DEV_EOK;
}
// INIT_DEVICE_EXPORT(lDrvAudioMicInit);
