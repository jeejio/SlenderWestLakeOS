/*
 * Copyright (c) 2022, jeejio Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author         Notes
 * 2022-12-07     zhengqian      the first version
*/

#ifndef __DRV_GPIO_H__
#define __DRV_GPIO_H__

#include "device.h"
#include "hal_gpio.h"           //gpio IO模型驱动框架头文件
#include "driver/gpio.h"        //厂家驱动头文件
#include "auto_init.h"
#include "log.h"


static void vDrvGpioWrite(struct Device_st *device, Base_t pin, Base_t value)
{
    gpio_set_level(pin, value);
    //printf("set %d level %d \n", (int)pin, (int)value);
}

static int lDrvGpioRead(struct Device_st *device, Base_t pin)
{
    return gpio_get_level(pin);
}

static void vDrvGpioMode(struct Device_st *device, Base_t pin, Base_t mode)
{
    gpio_config_t io_conf;
    io_conf.intr_type = GPIO_INTR_DISABLE;
    
    io_conf.pin_bit_mask = (1ULL << pin);
    io_conf.pull_down_en = 0;
    io_conf.pull_up_en = 0;

    if (mode == PIN_MODE_OUTPUT)
    {
        io_conf.mode = GPIO_MODE_OUTPUT;
        //gpio_set_direction(pin, GPIO_MODE_OUTPUT);
        printf("set mode  PIN_MODE_OUTPUT \n");
    }
    else if(mode == PIN_MODE_INPUT)
    {
        io_conf.mode = GPIO_MODE_INPUT;
        io_conf.pull_down_en = 0;
        io_conf.pull_up_en = 0;
        //gpio_set_direction(pin, GPIO_MODE_INPUT);
    }
    else if(mode == PIN_MODE_OUTPUT_OD)
    {
        io_conf.mode = GPIO_MODE_OUTPUT_OD;
        io_conf.pull_down_en = 0;
        io_conf.pull_up_en = 0;
        //gpio_set_direction(pin, GPIO_MODE_OUTPUT_OD);
    }
    else if(mode == PIN_MODE_INPUT_PULLUP)
    {
        io_conf.pull_down_en = 0;
        io_conf.pull_up_en = 1;
        io_conf.mode = GPIO_MODE_INPUT;
    }
    else if(mode == PIN_MODE_INPUT_PULLDOWN)
    {
        io_conf.pull_down_en = 1;
        io_conf.pull_up_en = 0;
        io_conf.mode = GPIO_MODE_INPUT;
    }
    gpio_config(&io_conf);
}

static Err_t lDrvGpioAttachIrq(struct Device_st *device, Int32_t pin,
                                  Uint32_t mode, void (*hdr)(void *args), void *args)
{
    static Bool_t isIsrSvcInstalled = FALSE;

    if (pin < 0)
    {
        LogError("GPIO.drv", "%s: parameter pin %d is invalid\r\n", __func__, (int)pin);
        return DEV_ENOSYS;
    }

    /* irq mode set */
    switch (mode)
    {
    case PIN_IRQ_MODE_RISING:
        gpio_set_intr_type(pin, GPIO_INTR_POSEDGE); 
        break;
    case PIN_IRQ_MODE_FALLING:
        gpio_set_intr_type(pin, GPIO_INTR_NEGEDGE); 
        break;
    case PIN_IRQ_MODE_RISING_FALLING:
        gpio_set_intr_type(pin, GPIO_INTR_ANYEDGE); 
        break;
    case PIN_IRQ_MODE_HIGH_LEVEL:
        gpio_set_intr_type(pin, GPIO_INTR_HIGH_LEVEL); 
        break;
    case PIN_IRQ_MODE_LOW_LEVEL:
        gpio_set_intr_type(pin, GPIO_INTR_LOW_LEVEL); 
        break;
    default:
        LogWarn("GPIO.drv", "%s: parameter mode %lu is not support\r\n", __func__, mode);
        return DEV_ENOSYS;
    }

    // install gpio isr service
    if (!isIsrSvcInstalled) {
        if (gpio_install_isr_service(ESP_INTR_FLAG_LEVEL1) != ESP_OK)  //注册中断处理程序
        {
            LogError("GPIO.drv", "%s: install isr service failed\r\n", __func__);
            return DEV_ERROR;
        }

        isIsrSvcInstalled = TRUE;
    }

    // hook isr handler for specific gpio pin
    if (gpio_isr_handler_add(pin, hdr, args) != ESP_OK)  //注册中断处理程序
    {
        LogError("GPIO.drv", "%s: add isr hander for gpio%d failed\r\n", __func__, (int)pin);
        return DEV_ERROR;
    }

    return DEV_EOK;
}

static Err_t lDrvGpioDetachIrq(struct Device_st *device, Int32_t pin)
{
    if (pin < 0)
    {
        LogError("GPIO.drv", "%s: parameter pin %d is invalid\r\n", __func__, (int)pin);
        return DEV_ENOSYS;
    }

    if (gpio_isr_handler_remove(pin) != ESP_OK) //删除中断处理服务
    {
        LogError("GPIO.drv", "%s: remove isr hander for gpio%d failed\r\n", __func__, (int)pin);
        return DEV_ERROR;
    }

    return DEV_EOK;
}

static Err_t lDrvGpioIrqEnable(struct Device_st *device, Base_t pin, Uint32_t enabled)
{
    if (pin < 0)
    {
        LogError("GPIO.drv", "%s: parameter pin %d is invalid\r\n", __func__, (int)pin);
        return DEV_ENOSYS;
    }

    if (enabled == PIN_IRQ_ENABLE)
    {
        if (gpio_intr_enable(pin) != ESP_OK)
        {
            LogError("GPIO.drv", "%s: enable gpio%d interrupt failed\r\n", __func__, (int)pin);
            return DEV_ERROR;
        }

        return DEV_EOK;
    }
    else if (enabled == PIN_IRQ_DISABLE)
    {
        if (gpio_intr_disable(pin) != ESP_OK)
        {
            LogError("GPIO.drv", "%s: disable gpio%d interrupt failed\r\n", __func__, (int)pin);
            return DEV_ERROR;
        }
        return DEV_EOK;
    }
    else
    {
        return DEV_ENOSYS;
    }
}


struct PinOps_st xGpioOps =
{
    vDrvGpioMode,
    vDrvGpioWrite,
    lDrvGpioRead,
    lDrvGpioAttachIrq,
    lDrvGpioDetachIrq,
    lDrvGpioIrqEnable,
    NULL
};

int lDrvHwGpioInit(void)
{
    printf("begin to lDrvHwGpioInit\n");
    int ret = -1;
    ret = lHalDevicePinRegister("pin", &xGpioOps, NULL);

    printf("finish to lDrvHwGpioInit\n");
    return ret;
}

INIT_BOARD_EXPORT(lDrvHwGpioInit);

#endif