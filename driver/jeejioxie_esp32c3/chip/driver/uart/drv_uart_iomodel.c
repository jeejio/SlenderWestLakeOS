/*
 * Copyright (c) 2023, Jeejio Technology Co. Ltd
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2023-03-08     Alpha        First Verison
 *
 */
#include "device.h"
#include "hal_serial.h"
#include "driver/uart.h"
#include "driver/drv_uart_iomodel.h"
#include "hal/uart_hal.h"
#include "sdkconfig.h"
#include "auto_init.h"
#include "soc/uart_periph.h"
#include "esp_log.h"
#include "esp32c3/rom/ets_sys.h"
#ifdef CONFIG_PM_ENABLE
#include "hal_pm.h"
#endif

#ifdef BSP_USING_UART0
#define UART0_NAME "uart0"
#define BSP_UART0_PORT    0
#define BSP_UART0_TX_PIN  21
#define BSP_UART0_RX_PIN  20
#define BSP_UART0_RTS_PIN (UART_PIN_NO_CHANGE)
#define BSP_UART0_CTS_PIN (UART_PIN_NO_CHANGE)
#endif

#ifdef BSP_USING_UART1
#define UART1_NAME "uart1"
#define BSP_UART1_PORT    1
#define BSP_UART1_TX_PIN  3
#define BSP_UART1_RX_PIN  2
#define BSP_UART1_RTS_PIN (UART_PIN_NO_CHANGE)
#define BSP_UART1_CTS_PIN (UART_PIN_NO_CHANGE)
#endif

#define UART_RX_BUF_SIZE      256
#define UART_TX_BUF_SIZE      0
#define UART_EVENT_QUEUE_SIZE 0

#define UART_CLKDIV_FRAG_BIT_WIDTH      (3)
#define UART_TX_IDLE_NUM_DEFAULT        (0)
#define UART_PATTERN_DET_QLEN_DEFAULT   (10)
#define UART_MIN_WAKEUP_THRESH          (UART_LL_MIN_WAKEUP_THRESH)

#ifdef BSP_USING_UART0
static struct SerialDevice_st serial0;
static uart_hal_context_t uart_hal = {.dev = UART_LL_GET_HW(UART_NUM_0)};
#endif

#ifdef BSP_USING_UART0
static struct SerialDevice_st serial1;
#endif

static __attribute__((unused)) const char *TAG = "DRV_UART";


static Err_t mcu_uart_configure(struct SerialDevice_st *serial, struct SerialConfigure_st *cfg);

static int mcu_uart_putc(struct SerialDevice_st *serial, char c);
static int mcu_uart_getc(struct SerialDevice_st *serial);
static Size_t mcu_uart_transmit(struct SerialDevice_st *serial, Uint8_t *buf, Size_t size, Uint32_t devFlag);

struct uart_plat_data {
    Uint8_t *rxBuf;
    Uint8_t *txBuf;
    Uint32_t bufSize;
    Int32_t  real_size;
    Uint32_t intr_flag;
    intr_handler_t cb;
    void *cb_arg;
    int rd_len;
    Uint32_t rxfifo_len;
};

/**
 * @brief This function will perform a configuration of the uart.
 *
 * @param serial is the pointer of serial device driver structure.
 *
 * @param SerialConfigure_st is the parameters of the uart device.
 *
 * @param arg is the argument of command.
 *
 * @return the result, -DEV_ENOSYS for failed.
 */
static Err_t mcu_uart_configure(struct SerialDevice_st *serial, struct SerialConfigure_st *cfg)
{
    uart_port_t port = (uart_port_t)serial->parent.user_data;
    int intr_alloc_flags = 0;

    configASSERT(cfg != NULL);

    uart_config_t uart_config = {
        .baud_rate = cfg->baud_rate,
        .data_bits = cfg->data_bits - 5,
        .stop_bits = cfg->stop_bits,
        .source_clk = UART_SCLK_APB,
    };

    if (cfg->parity == PARITY_ODD) {
        uart_config.parity = UART_PARITY_ODD;
    }
    if (cfg->flowcontrol == SERIAL_FLOWCONTROL_CTSRTS) {
        uart_config.flow_ctrl = UART_HW_FLOWCTRL_CTS_RTS;
    }

#if CONFIG_UART_ISR_IN_IRAM
    intr_alloc_flags = ESP_INTR_FLAG_IRAM;
#endif
    uart_driver_install(port, UART_RX_BUF_SIZE, 0, 0, NULL, intr_alloc_flags);
    ESP_ERROR_CHECK(uart_param_config(port, &uart_config));
    ESP_ERROR_CHECK(uart_set_pin(port, cfg->txPinNo, cfg->rxPinNo, cfg->rtsPinNo, cfg->ctsPinNo));
    return DEV_EOK;
}

static Err_t mcu_uart_control(struct SerialDevice_st *serial, int cmd, void *arg)
{
    uart_port_t port = (uart_port_t)serial->parent.user_data;
    struct uart_plat_data *pUartData = (struct uart_plat_data *)arg;

    switch (cmd) {
    case DEVICE_CTRL_CLOSE:
        /* Uninstall the uart driver */
        uart_driver_delete(port);
        break;

    case DEVICE_CTRL_CLR_INT:
        /* Disable the UART Interrupt */
        uart_disable_rx_intr(port);
        uart_disable_tx_intr(port);
        break;

    case DEVICE_CTRL_SET_INT:
        /* Enable the UART Interrupt */
        uart_enable_rx_intr(port);
        uart_enable_tx_intr(port, 1, UART_EMPTY_THRESH_DEFAULT);
        break;

    case DEVICE_CTRL_GET_INT:
        pUartData->intr_flag = uart_hal_get_intsts_mask(&uart_hal);
        break;

    case DEVICE_CTRL_UART_GET_FIFO_LEN:
        pUartData->rxfifo_len = uart_hal_get_rxfifo_len(&uart_hal);
        break;

    case DEVICE_CTRL_UART_FIFO_READ:
        uart_hal_read_rxfifo(&uart_hal,pUartData->rxBuf, &pUartData->rd_len);
        break;

    case DEVICE_CTRL_UART_SET_CALLBACK:
        uart_intr_config_t uart_intr = {
            .intr_enable_mask = UART_INTR_RXFIFO_FULL | UART_INTR_RXFIFO_TOUT | UART_INTR_RXFIFO_OVF,
            .rxfifo_full_thresh = UART_FULL_THRESH_DEFAULT,
            .rx_timeout_thresh = UART_TOUT_THRESH_DEFAULT,
            .txfifo_empty_intr_thresh = UART_EMPTY_THRESH_DEFAULT,
        };
        intr_handle_t uart_intr_handle =  uart_get_rx_intr_handle(port);
        esp_intr_free(uart_intr_handle);
        esp_intr_alloc(uart_periph_signal[UART_NUM_0].irq, 0, pUartData->cb, pUartData->cb_arg, &uart_intr_handle);
        uart_intr_config(UART_NUM_0, &uart_intr);
        break;
    case DEVICE_CTRL_UART_CLR_MASK:
        uart_hal_clr_intsts_mask(&uart_hal, UART_INTR_RXFIFO_TOUT | UART_INTR_RXFIFO_FULL);
        break;
    case DEVICE_CRTL_UART_FIFO_SEND:
        ets_printf("%s",pUartData->txBuf);
        break;

    case DEVICE_CTRL_UART_READ:
        pUartData->real_size = mcu_uart_transmit(serial, pUartData->rxBuf,
                                              pUartData->bufSize, UART_READ);
        if (pUartData->real_size < 0) {
            return DEV_ERROR;
        }
        break;

    case DEVICE_CTRL_UART_WRITE:
        pUartData->real_size = mcu_uart_transmit(serial, pUartData->txBuf,
                                              pUartData->bufSize, UART_WRITE);
        if (pUartData->real_size <= 0) {
            return DEV_ERROR;
        }
        break;

    default:
        LogError(TAG, "%s: This command is unrecognized!\n", __func__);
        break;
    }

    return DEV_EOK;
}

static int mcu_uart_putc(struct SerialDevice_st *serial, char c)
{
    return 0;
}

static int mcu_uart_getc(struct SerialDevice_st *serial)
{
    return 1;
}

static Size_t mcu_uart_transmit(struct SerialDevice_st *serial, Uint8_t *buf, Size_t size, Uint32_t devFlag)
{
    uart_port_t port = (uart_port_t)serial->parent.user_data;

    if (devFlag == UART_READ) {
        return uart_read_bytes(port, buf, size, UART_READ_TIMEOUT_MS / portTICK_PERIOD_MS);
    } else if (devFlag == UART_WRITE) {
        return uart_write_bytes(port, buf, size);
    }

    return 0;
}

static const struct UartOps_st _uart_ops =
{
    mcu_uart_configure,
    mcu_uart_control,
    mcu_uart_putc,
    mcu_uart_getc,
    mcu_uart_transmit,
};

#if defined(CONFIG_PM_ENABLE)
/* device pm suspend() entry. */
static int lDrvUartPmSuspend(const struct Device_st *device, uint8_t mode)
{
    Err_t result;
    struct SerialDevice_st *serial;

    configASSERT(device != NULL);
    serial = (struct SerialDevice_st *)device;

    switch (mode)
    {
    case PM_SLEEP_MODE_LIGHT:
    case PM_SLEEP_MODE_DEEP:
        LOGI(TAG, "%s", __func__);
        break;

    default:
        break;
    }

    return (int)DEV_EOK;
}

/* device pm resume() entry. */
static void vDrvUartPmResume(const struct Device_st *device, uint8_t mode)
{
    Err_t result;
    struct SerialDevice_st *serial;

    configASSERT(device != NULL);
    serial = (struct SerialDevice_st *)device;

    switch (mode)
    {
    case PM_SLEEP_MODE_LIGHT:
    case PM_SLEEP_MODE_DEEP:
        LOGI(TAG, "%s", __func__);
        break;

    default:
        break;
    }
}

static struct PmDeviceOps_st uartPmOps =
{
    .suspend = lDrvUartPmSuspend,
    .resume = vDrvUartPmResume,
    .frequencyChange = NULL
};
#endif /* CONFIG_PM_ENABLE */

int lDrvHwUartInit(void)
{
    static struct SerialDevice_st *_serial;
#ifdef BSP_USING_UART0
    struct SerialConfigure_st xUart0Config = SERIAL_CONFIG_DEFAULT;
#endif
#ifdef BSP_USING_UART1
    struct SerialConfigure_st xUart1Config = SERIAL_CONFIG_DEFAULT;
#endif

#ifdef BSP_USING_UART0
    _serial  = &serial0;
    xUart0Config.rxPinNo = BSP_UART0_RX_PIN;
    xUart0Config.txPinNo = BSP_UART0_TX_PIN;
    xUart0Config.rtsPinNo = BSP_UART0_RTS_PIN;
    xUart0Config.ctsPinNo = BSP_UART0_CTS_PIN;
    _serial->config = xUart0Config;
    _serial->ops = &_uart_ops;

    lHalHwSerialRegister(_serial, UART0_NAME, DEVICE_FLAG_RDWR | DEVICE_FLAG_INT_RX, (void *)BSP_UART0_PORT);
#endif
#ifdef BSP_USING_UART1
    _serial  = &serial1;
    xUart1Config.baud_rate= BAUD_RATE_9600;
    xUart1Config.rxPinNo = BSP_UART1_RX_PIN;
    xUart1Config.txPinNo = BSP_UART1_TX_PIN;
    xUart1Config.rtsPinNo = BSP_UART1_RTS_PIN;
    xUart1Config.ctsPinNo = BSP_UART1_CTS_PIN;
    _serial->config = xUart1Config;
    _serial->ops = &_uart_ops;

    lHalHwSerialRegister(_serial, UART1_NAME, DEVICE_FLAG_RDWR | DEVICE_FLAG_INT_RX, (void *)BSP_UART1_PORT);
#endif

#if defined(CONFIG_PM_ENABLE)
    vHalPMDeviceRegister(&_serial->parent, &uartPmOps);
#endif

    return 0;
}
INIT_PREV_EXPORT(lDrvHwUartInit);


