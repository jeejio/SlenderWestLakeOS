#include <swldef.h>
#include <driver/drv_hwtimer_iomodel.h>
#include "esp_log.h"
#include "auto_init.h"
#include "hal_hwtimer.h"
#include "soc/soc.h"
#include "driver/timer.h"

#define TAG "drv_hwtimer_iomodel"


struct hwtimer_Type
{
    //enum tls_timer_unit unit;
    timer_group_t timer_group;
    timer_idx_t timer_idx;
};

#if SOC_TIMER_GROUP_TOTAL_TIMERS >= 4
    #define USING_HW_TIMER0
    static Hwtimer_t _timer0;
    static struct hwtimer_Type hwtimer0;

    #define USING_HW_TIMER1
    static Hwtimer_t _timer1;
    static struct hwtimer_Type hwtimer1;

    #define USING_HW_TIMER2
    static Hwtimer_t _timer2;
    static struct hwtimer_Type hwtimer2;

    #define USING_HW_TIMER3
    static Hwtimer_t _timer3;
    static struct hwtimer_Type hwtimer3;

#elif SOC_TIMER_GROUP_TOTAL_TIMERS >= 2
    #define USING_HW_TIMER0
    static Hwtimer_t _timer0;
    static struct hwtimer_Type hwtimer0;

    #if SOC_TIMER_GROUP_TIMERS_PER_GROUP > 1
     #define USING_HW_TIMER1
    static Hwtimer_t _timer1;
    static struct hwtimer_Type hwtimer1;
    #endif

    #if SOC_TIMER_GROUPS > 1
    #define USING_HW_TIMER2
    static Hwtimer_t _timer2;
    static struct hwtimer_Type hwtimer2;
    #endif

#else
    #define USING_HW_TIMER0
    static Hwtimer_t _timer0;
    static struct hwtimer_Type hwtimer0;
#endif


static void hwtimer_isr(void *arg)
{

#ifdef USING_HW_TIMER0
    vHalDeviceHwtimerIsr(&_timer0);
#endif
#ifdef USING_HW_TIMER1
    vHalDeviceHwtimerIsr(&_timer1);
#endif
#ifdef USING_HW_TIMER2
    vHalDeviceHwtimerIsr(&_timer2);
#endif
#ifdef USING_HW_TIMER3
    vHalDeviceHwtimerIsr(&_timer3);
#endif
}

/* 定时器初始化函数 */
static void hwtimer_init(Hwtimer_t *timer, Uint32_t state)
{
    struct hwtimer_Type *hwtimer = (struct hwtimer_Type *)timer->parent.user_data;

     timer_config_t config = {
        .clk_src = TIMER_SRC_CLK_DEFAULT,
        .divider = APB_CLK_FREQ / HWTIMER_RESOLUTION_HZ,
        .alarm_en = TIMER_ALARM_EN,
        .auto_reload = TIMER_AUTORELOAD_EN,
        .counter_dir = timer->info->cntmode, //TIMER_COUNT_DOWN,
        .counter_en = TIMER_PAUSE,
        .intr_type = TIMER_INTR_LEVEL,
    };
    if(state == 1)
    {
        /* 初始化定时器 */
        timer_init(hwtimer->timer_group, hwtimer->timer_idx, &config);
        timer_set_counter_value(hwtimer->timer_group, hwtimer->timer_idx, 0);
        
        /* 注册定时器中断回调函数 */
        timer_isr_callback_add(hwtimer->timer_group, hwtimer->timer_idx, hwtimer_isr, NULL, ESP_INTR_FLAG_LOWMED);
    }
    else if(state == 0)
    {
        timer_deinit(hwtimer->timer_group, hwtimer->timer_idx);
    }
}

static Err_t hwtimer_start(Hwtimer_t *timer, Uint32_t t, HwtimerMode_t opmode)
{
    struct hwtimer_Type *hwtimer = (struct hwtimer_Type *)timer->parent.user_data;
    /*设置超时时间*/
    timer_set_alarm_value(hwtimer->timer_group, hwtimer->timer_idx, t);
    timer_set_alarm(hwtimer->timer_group, hwtimer->timer_idx, TIMER_ALARM_EN);
    /* 启动定时器 */
    timer_start(hwtimer->timer_group, hwtimer->timer_idx);

    return 0;
}

static void hwtimer_stop(Hwtimer_t *timer)
{
    struct hwtimer_Type *hwtimer = (struct hwtimer_Type *)timer->parent.user_data;
    timer_pause(hwtimer->timer_group, hwtimer->timer_idx);
}

static Uint32_t hwtimer_get(Hwtimer_t *timer)
{
    uint64_t timer_val;
    struct hwtimer_Type *hwtimer = (struct hwtimer_Type *)timer->parent.user_data;
    timer_get_counter_value(hwtimer->timer_group, hwtimer->timer_idx,&timer_val);

    return timer_val;
}

static Err_t hwtimer_ctrl(Hwtimer_t *timer, Uint32_t cmd, void *arg)
{
    if (cmd != HWTIMER_CTRL_FREQ_SET)
    {
        return -DEV_ENOSYS;
    }
    if ( *(Uint32_t*)arg == 1000000)
    {
        return DEV_EOK;
    }
    else
    {
        return -DEV_ENOSYS;
    }
}

static const struct HwtimerInfo_st _info =
{
    1000000,            /* the maximum count frequency can be set */
    1000,            /* the minimum count frequency can be set */
    0xFFFFFFFF,         /* the maximum counter value */
    HWTIMER_CNTMODE_UP, //HWTIMER_CNTMODE_DW, /* Increment or Decreasing count mode */
};


/* JEE 驱动对象 */
static const struct HwtimerOps_st _ops =
{
    hwtimer_init,
    hwtimer_start,
    hwtimer_stop,
    hwtimer_get,
    hwtimer_ctrl,
};

/* 驱动注册函数 */
int hw_timer_init(void)
{
#ifdef USING_HW_TIMER0
    hwtimer0.timer_group = TIMER_GROUP_0;
    hwtimer0.timer_idx = TIMER_0;

    _timer0.info = &_info;
    _timer0.ops = &_ops;

    lHalDeviceHwtimerRegister(&_timer0, "timer0", &hwtimer0);
#endif
#ifdef USING_HW_TIMER1
    hwtimer0.timer_group = TIMER_GROUP_0;
    hwtimer0.timer_idx = TIMER_1;

    _timer1.info = &_info;
    _timer1.ops = &_ops;

    lHalDeviceHwtimerRegister(&_timer1, "timer1", &hwtimer1);
#endif
#ifdef USING_HW_TIMER2
    hwtimer2.timer_group = TIMER_GROUP_1;
    hwtimer2.timer_idx = TIMER_0;

    _timer2.info = &_info;
    _timer2.ops = &_ops;

    lHalDeviceHwtimerRegister(&_timer2, "timer2", &hwtimer2);
#endif
#ifdef USING_HW_TIMER3
    hwtimer3.timer_group = TIMER_GROUP_1;
    hwtimer3.timer_idx = TIMER_1;

    _timer3.info = &_info;
    _timer3.ops = &_ops;

    lHalDeviceHwtimerRegister(&_timer3, "timer3", &hwtimer3);
#endif
    return 0;
}
INIT_BOARD_EXPORT(hw_timer_init);



