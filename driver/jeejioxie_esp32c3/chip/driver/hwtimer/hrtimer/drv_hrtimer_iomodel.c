/*
 * Copyright (c) 2022-2023, Jeejio Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2023-02-24     None         the first version
 */

#include <device.h>
#include "esp_timer.h"
#include "driver/drv_hrtimer_iomodel.h"
#include "esp_log.h"
#include "esp_sleep.h"
#include "auto_init.h"

#ifdef SWL_USING_HWTIMER
#if defined(USING_SPILIT_TOUT_VAL) || defined(USING_TIMER_BIND_UNIQUE)
struct hrtimerCfg
{
    HwtimerVal_t timeoutVal;
};
#endif

static const char* TAG = "drv_hrtimer";

#ifdef USING_TIMER_BIND_UNIQUE
static struct hrtimerData _timerPriData;

static void (*vTimerCallback)(void* arg);
static void *timerCrtArg = NULL;
static char *timerCrtName = NULL;
HwtimerVal_t periodToutVal;
#endif


#ifdef USING_PERIODIC_TIMER_SPLIT
static HwtimerDev_t _periodicTimer;
static struct hrtimerData periodicTimerData;

static void (*vPeriodicTimerCallback)(void* arg);
static void *periodicTimerArg = NULL;
static char *periodicTimerName = NULL;
#ifdef USING_SPILIT_TOUT_VAL
HwtimerVal_t periodicToutVal;
#endif
#endif

#ifdef USING_ONESHOT_TIMER_SPLIT
static HwtimerDev_t _oneshotTimer;
static struct hrtimerData oneshotTimerData;

static void (*vOneshotTimerCallback)(void* arg);
static void *oneshotTimerArg = NULL;
static char *oneshotTimerName = NULL;
#ifdef USING_SPILIT_TOUT_VAL
HwtimerVal_t oneshotToutVal;
#endif
#endif


#ifdef USING_TIMER_BIND_UNIQUE
static void vDrvHwtimerCbRegister(void *cb, void *arg)
{
    vTimerCallback = cb;
    timerCrtArg = arg;
}

static void vDrvHwtimerPeriodTimeoutValSet(HwtimerVal_t *toutVal)
{
    periodToutVal = *toutVal;
}

static void vDrvHwtimerNameSet(char *name)
{
    timerCrtName = name;
}

void vDrvHwtimerHookInfoRegister(void *cb, void *arg, HwtimerVal_t *toutVal, char *name)
{
    vDrvHwtimerCbRegister(cb, arg);
    vDrvHwtimerPeriodTimeoutValSet(toutVal);
    vDrvHwtimerNameSet(name);
}
#endif

#ifdef USING_PERIODIC_TIMER_SPLIT
static void vDrvHwtimerPeriodicTimerCbRegister(void *cb, void *arg)
{
    vPeriodicTimerCallback = cb;
    periodicTimerArg = arg;
}
#ifdef USING_SPILIT_TOUT_VAL
static void vDrvHwtimerPeriodicTimeoutValSet(HwtimerVal_t *toutVal)
{
    periodicToutVal = *toutVal;
}
#endif
#endif
#ifdef USING_ONESHOT_TIMER_SPLIT
static void vDrvHwtimerOneshotTimerCbRegister(void *cb, void *arg)
{
    vOneshotTimerCallback = cb;
    oneshotTimerArg = arg;
}

#ifdef USING_SPILIT_TOUT_VAL
static void vDrvHwtimerOneshotTimeoutValSet(HwtimerVal_t *toutVal)
{
    oneshotToutVal = *toutVal;
}
#endif
#endif

#ifdef USING_TIMER_BIND_UNIQUE
void vHwtimerDataInit(void)
{
    const esp_timer_create_args_t timerArgs = {
        /* the callback  function will be called when timer expires */
        .callback = (esp_timer_cb_t)vTimerCallback,
        /* argument specified here will be passed to timer callback function */
        .arg = timerCrtArg,
        /* name is optional, but may help identify the timer when debugging */
        .name = timerCrtName
    };

    _timerPriData.crtTimerArgs = timerArgs;
}
#endif

static Err_t lDrvHrtimerEsp32c3Configure(struct HwtimerDevice_st *timer, void *cfg)
{
#if defined(USING_SPILIT_TOUT_VAL) || defined(USING_TIMER_BIND_UNIQUE)
    struct hrtimerCfg *espHwimerCfg = (struct hrtimerCfg *)cfg;
#endif

    EHwtimerMode_t timerMode;

    configASSERT(timer != NULL);

    timerMode = timer->mode;

    if (timerMode == HWTIMER_MODE_PERIOD) {
#if defined(USING_SPILIT_TOUT_VAL)
        vDrvHwtimerPeriodicTimeoutValSet(&espHwimerCfg->timeoutVal);
#elif defined(USING_TIMER_BIND_UNIQUE)
        vDrvHwtimerPeriodTimeoutValSet(&espHwimerCfg->timeoutVal);
#endif
    } else if (timerMode == HWTIMER_MODE_ONESHOT) {
#if defined(USING_SPILIT_TOUT_VAL)
        vDrvHwtimerOneshotTimeoutValSet(&espHwimerCfg->timeoutVal);
#elif defined(USING_TIMER_BIND_UNIQUE)
        vDrvHwtimerPeriodTimeoutValSet(&espHwimerCfg->timeoutVal);
#endif
    }

    return DEV_EOK;
}

static void vDrvHrtimerEsp32c3Init(struct HwtimerDevice_st *timer)
{
    struct hrtimerData *pData = NULL;

    configASSERT(timer != NULL);
    pData = (struct hrtimerData *)timer->parent.user_data;
    configASSERT(pData != NULL);

    ESP_ERROR_CHECK(esp_timer_create(&pData->crtTimerArgs, &pData->timerHandle));
}

static void vDrvHrtimerEsp32c3CreateAno(struct HwtimerDevice_st *timer, void *args)
{
    struct hrtimerData *pData = (struct hrtimerData *)args;

    configASSERT(timer != NULL);
    configASSERT(pData != NULL);

    ESP_ERROR_CHECK(esp_timer_create(&pData->crtTimerArgs, &pData->timerHandle));
}

static Err_t lDrvHrtimerEsp32c3Start(struct HwtimerDevice_st *timer,
                                          uint32_t cnt, EHwtimerMode_t opmode)
{
    struct hrtimerData *pData = NULL;
    EHwtimerMode_t timerMode;
    uint64_t periodUs = (uint64_t)cnt;

    configASSERT(timer != NULL);
    pData = (struct hrtimerData *)timer->parent.user_data;
    configASSERT(pData != NULL);

    timerMode = timer->mode;

    if (timerMode == HWTIMER_MODE_PERIOD) {
        ESP_ERROR_CHECK(esp_timer_start_periodic(pData->timerHandle, periodUs));
    } else if (timerMode == HWTIMER_MODE_ONESHOT) {
        ESP_ERROR_CHECK(esp_timer_start_once(pData->timerHandle, periodUs));
    }

    return DEV_EOK;
}

static Err_t lDrvHrtimerEsp32c3StartAno(struct HwtimerDevice_st *timer, void *args)
{
    struct hrtimerData *pData = (struct hrtimerData *)args;
    EHwtimerMode_t timerMode;
    uint64_t periodUs;

    configASSERT(timer != NULL);
    configASSERT(pData != NULL);

    periodUs = pData->timeoutVal.sec * 1000 * 1000 + pData->timeoutVal.usec;
    timerMode = pData->timerMode;

    if (pData->timerHandle == NULL) {
        LOGE(TAG, "%s: %s timer handle is null", __func__, pData->crtTimerArgs.name);
        return DEV_ENOSYS;
    }

    if (timerMode == HWTIMER_MODE_PERIOD) {
        ESP_ERROR_CHECK(esp_timer_start_periodic(pData->timerHandle, periodUs));
    } else if (timerMode == HWTIMER_MODE_ONESHOT) {
        ESP_ERROR_CHECK(esp_timer_start_once(pData->timerHandle, periodUs));
    }

    return DEV_EOK;
}

static void vDrvHrtimerEsp32c3Stop(struct HwtimerDevice_st *timer)
{
    struct hrtimerData *pData = NULL;
    configASSERT(timer != NULL);
    pData = (struct hrtimerData *)timer->parent.user_data;
    configASSERT(pData != NULL);

    ESP_ERROR_CHECK(esp_timer_stop(pData->timerHandle));
}

static void vDrvHrtimerEsp32c3StopAno(struct HwtimerDevice_st *timer, void *args)
{
    struct hrtimerData *pData = (struct hrtimerData *)args;

    configASSERT(timer != NULL);
    configASSERT(pData != NULL);

    ESP_ERROR_CHECK(esp_timer_stop(pData->timerHandle));
}

static void vDrvHrtimerEsp32c3Delete(struct HwtimerDevice_st *timer)
{
    struct hrtimerData *pData = NULL;

    configASSERT(timer != NULL);
    pData = (struct hrtimerData *)timer->parent.user_data;
    configASSERT(pData != NULL);

    ESP_ERROR_CHECK(esp_timer_delete(pData->timerHandle));
}

static void vDrvHrtimerEsp32c3DeleteAno(struct HwtimerDevice_st *timer, void *args)
{
    struct hrtimerData *pData = (struct hrtimerData *)args;

    configASSERT(timer != NULL);
    configASSERT(pData != NULL);

    ESP_ERROR_CHECK(esp_timer_delete(pData->timerHandle));
}

/* get timer internal counter value */
static uint32_t ulDrvHrtimerEsp32c3CounterGet(struct HwtimerDevice_st *timer)
{
    return 0;
}

static Err_t lDrvHrtimerEsp32c3Ctrl(struct HwtimerDevice_st *timer, uint32_t cmd, void *arg)
{
    configASSERT(timer != NULL);

    switch(cmd) {
    case HWTIMER_CTRL_FREQ_SET:
        break;
    case HWTIMER_CTRL_STOP:
        break;
    case HWTIMER_CTRL_INFO_GET:
        break;
    case HWTIMER_CTRL_MODE_SET:
        break;
    case HWTIMER_CTRL_TOUT_VAL_SET:
#ifdef USING_TIMER_BIND_UNIQUE
        HwtimerVal_t *tVal = (HwtimerVal_t *)arg;
        vDrvHwtimerPeriodTimeoutValSet(tVal);
#endif
        break;
    case HWTIMER_CTRL_INFO_DUMP:
        ESP_ERROR_CHECK(esp_timer_dump(stdout));
        break;
    case HWTIMER_CTRL_GET_TIME:
        int64_t *time = (int64_t *)arg;
        *time = esp_timer_get_time();
        break;
    case HWTIMER_CTRL_CREATE_ANO:
        vDrvHrtimerEsp32c3CreateAno(timer, arg);
        break;
    case HWTIMER_CTRL_START_ANO:
        lDrvHrtimerEsp32c3StartAno(timer, arg);
        break;
    case HWTIMER_CTRL_STOP_ANO:
        vDrvHrtimerEsp32c3StopAno(timer, arg);
        break;
    case HWTIMER_CTRL_DELETE_ANO:
        vDrvHrtimerEsp32c3DeleteAno(timer, arg);
        break;
    case HWTIMER_CTRL_LIGHT_SLEEP:
        uint64_t lightSleepTimeUs = *(uint64_t *)arg;
        ESP_ERROR_CHECK(esp_sleep_enable_timer_wakeup(lightSleepTimeUs));
        esp_light_sleep_start();
        break;
    case HWTIMER_CTRL_DEEP_SLEEP:
        uint64_t deepSleepTimeUs = *(uint64_t *)arg;
        esp_deep_sleep(deepSleepTimeUs);
        break;
    default:
        break;
    }

    return DEV_EOK;
}

static struct HwtimerInfo_st _info =
{
    1000000,            /* the maximum count frequency can be set */
    1000000,            /* the minimum count frequency can be set */
    0xFFFFFFFF,         /* the maximum counter value */
    HWTIMER_CNTMODE_DW, /* Increment or Decreasing count mode */
};

static const struct HwtimerOps_st _ops =
{
    lDrvHrtimerEsp32c3Configure,
    vDrvHrtimerEsp32c3Init,
    lDrvHrtimerEsp32c3Start,
    vDrvHrtimerEsp32c3Stop,
    vDrvHrtimerEsp32c3Delete,
    ulDrvHrtimerEsp32c3CounterGet,
    lDrvHrtimerEsp32c3Ctrl,
};

static int lDrvHrtimerEsp32c3DevInit(void)
{
    static HwtimerDev_t _timeDev;

    _timeDev.info = &_info;
    _timeDev.ops = &_ops;

    lHalHwtimerDeviceRegister(&_timeDev, "hrtimer", NULL);

    return 0;
}
INIT_PREV_EXPORT(lDrvHrtimerEsp32c3DevInit);

#endif /* SWL_USING_HWTIMER */
