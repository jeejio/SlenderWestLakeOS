/*
 * Copyright (c) 2006-2023, JEEJIO Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2023-02-28     lk           First Verison
 *
 */

#include "driver/drv_pm_iomodel.h"

#ifdef CONFIG_PM_ENABLE

#include "string.h"
#include <driver/gpio.h>
#include "freertos/semphr.h"
#include <swldef.h>
#include <device.h>
#include "esp_log.h"
#include "auto_init.h"
#include "hal_pm.h"
#include "esp_log.h"
#include "esp_sleep.h"
#include "hal_hwtimer.h"
#include "esp_pm.h"
#include "esp_private/esp_clk.h"
#include "esp_timer.h"


static const char *DBG_TAG = "drv_pm";

#define MHZ (1000000)

#ifdef AUTO_LIGHT_SLEEP_EN
static bool autoLightSleepEn = false;

#if CONFIG_FREERTOS_USE_TICKLESS_IDLE
static void vAutoLightSleepEnable(void)
{
    int cur_freq_mhz = esp_clk_cpu_freq() / MHZ;
    int xtal_freq = esp_clk_xtal_freq() / MHZ;

    esp_pm_config_esp32c3_t pm_config = {
        .max_freq_mhz = cur_freq_mhz,
        .min_freq_mhz = xtal_freq,
        .light_sleep_enable = true
    };
    ESP_ERROR_CHECK( esp_pm_configure(&pm_config) );
    autoLightSleepEn = true;
}

static void vAutoLightSleepDisable(void)
{
    int cur_freq_mhz = esp_clk_cpu_freq() / MHZ;

    esp_pm_config_esp32c3_t pm_config = {
        .max_freq_mhz = cur_freq_mhz,
        .min_freq_mhz = cur_freq_mhz,
    };
    ESP_ERROR_CHECK( esp_pm_configure(&pm_config) );
    autoLightSleepEn = false;
}
#endif /* CONFIG_FREERTOS_USE_TICKLESS_IDLE */
#endif

/**
 * This function will put xxxx into sleep mode.
 *
 * @param pm pointer to power manage structure
 */
static void vDrvPMSleep(struct PowerManageInfo_st *pm, uint8_t mode)
{
    Hwtimerval_t timeout;
    memset(&timeout, 0, sizeof(Hwtimerval_t));
    ulHalDeviceRead(pm->hwTimerDev, 0, &timeout, sizeof(timeout));
    uint64_t timeoutUs = timeout.sec * 1000000 + timeout.usec;
    timeoutUs = 5000000;
    //LOGI(DBG_TAG, "F:%s L:%d mode = %d, timeoutUs = %lld", __func__, __LINE__, mode, timeoutUs);
    printf("F:%s L:%d mode = %d, timeoutUs = %lld\n", __func__, __LINE__, mode, timeoutUs);
    switch (mode)
    {
        case PM_SLEEP_MODE_NONE:
#ifdef AUTO_LIGHT_SLEEP_EN
            if (autoLightSleepEn)
                vAutoLightSleepDisable();
#endif
            break;

        case PM_SLEEP_MODE_IDLE:
#ifdef AUTO_LIGHT_SLEEP_EN
            if (autoLightSleepEn)
                vAutoLightSleepDisable();
#endif
            break;

        case PM_SLEEP_MODE_LIGHT:
#ifdef AUTO_LIGHT_SLEEP_EN
            //LOGI(DBG_TAG, "F:%s L:%d PM_SLEEP_MODE_LIGHT finished, start auto_light_sleep boot %lldus", __func__, __LINE__, esp_timer_get_time());
            printf("F:%s L:%d PM_SLEEP_MODE_LIGHT finished, start auto_light_sleep boot %lldus\n", __func__, __LINE__, esp_timer_get_time());
            vAutoLightSleepEnable();
            //LOGI(DBG_TAG, "F:%s L:%d PM_SLEEP_MODE_LIGHT finished, since auto_light_sleep boot %lldus", __func__, __LINE__, esp_timer_get_time());
            printf("F:%s L:%d PM_SLEEP_MODE_LIGHT finished, since auto_light_sleep boot %lldus\n", __func__, __LINE__, esp_timer_get_time());
#else
            //LOGI(DBG_TAG, "F:%s L:%d PM_SLEEP_MODE_LIGHT finished, start sleep timer boot %lldus", __func__, __LINE__, esp_timer_get_time());
            printf("F:%s L:%d PM_SLEEP_MODE_LIGHT finished, start sleep timer boot %lldus\n", __func__, __LINE__, esp_timer_get_time());
            esp_sleep_enable_timer_wakeup(timeoutUs);
            esp_light_sleep_start();
            //LOGI(DBG_TAG, "F:%s L:%d PM_SLEEP_MODE_LIGHT finished, since sleep timer boot %lldus", __func__, __LINE__, esp_timer_get_time());
            printf("F:%s L:%d PM_SLEEP_MODE_LIGHT finished, since sleep timer boot %lldus\n", __func__, __LINE__, esp_timer_get_time());
#endif
            break;

        case PM_SLEEP_MODE_DEEP:
            /* Enter STOP 2 mode  */
            //LOGI(DBG_TAG, "F:%s L:%d PM_SLEEP_MODE_DEEP finished, start since sleep timer boot %lldus", __func__, __LINE__, esp_timer_get_time());
            printf("F:%s L:%d PM_SLEEP_MODE_DEEP finished, start since sleep timer boot %lldus\n", __func__, __LINE__, esp_timer_get_time());
            esp_sleep_enable_timer_wakeup(timeoutUs);
            esp_deep_sleep_start();
            //LOGI(DBG_TAG, "F:%s L:%d PM_SLEEP_MODE_DEEP finished, since sleep timer boot %lldus", __func__, __LINE__, esp_timer_get_time());
            printf("F:%s L:%d PM_SLEEP_MODE_DEEP finished, since sleep timer boot %lldus\n", __func__, __LINE__, esp_timer_get_time());
            break;

        case PM_SLEEP_MODE_STANDBY:
            /* Enter STANDBY mode */
            // TODO;
            break;

        case PM_SLEEP_MODE_SHUTDOWN:
            /* Enter SHUTDOWNN mode */
            // TODO;
            break;

        default:
            // LOGE(DBG_TAG, "mode is not support");
            printf("F:%s L:%d mode is not support\n",  __func__, __LINE__);
            break;
    }
}

static uint8_t run_speed[PM_RUN_MODE_MAX][2] =
{
    {240, 0},
    {160, 1},
    {160, 2},
    {80,  3},
};

static void vDrvPMRun(struct PowerManageInfo_st *pm, uint8_t mode)
{
    static uint8_t last_mode = PM_RUN_MODE_NORMAL_SPEED;
    static char *run_str[] = PM_RUN_MODE_NAMES;

    if (mode == last_mode)
    {
        //LOGE(DBG_TAG, "mode is not update, mode = %d", mode);
        printf("mode is not update, mode = %d\n", mode);
        return;
    }

    last_mode = mode;

    esp_pm_config_esp32c3_t cfg = {
        .max_freq_mhz = CONFIG_ESP_DEFAULT_CPU_FREQ_MHZ,
        .min_freq_mhz = esp_clk_xtal_freq() / 1000000,
    };

    switch (mode)
    {
        case PM_RUN_MODE_HIGH_SPEED:
            cfg.max_freq_mhz = 240;
            break;
        case PM_RUN_MODE_NORMAL_SPEED:
            break;
        case PM_RUN_MODE_MEDIUM_SPEED:
            cfg.max_freq_mhz = 160;
            break;
        case PM_RUN_MODE_LOW_SPEED:
            cfg.max_freq_mhz = 80;
            break;
        default:
            break;
    }

    esp_err_t ret = esp_pm_configure(&cfg);

   //LOGI(DBG_TAG, "result %d switch to %s mode, frequency = %d MHz\n", ret, run_str[mode], run_speed[mode][0]);
    printf("result %d switch to %s mode, frequency = %d MHz\n", ret, run_str[mode], run_speed[mode][0]);
}

/**
 * This function start the timer of pm
 *
 * @param pm Pointer to power manage structure
 * @param timeout How many OS Ticks that MCU can sleep
 */
static void vDrvPMTimerStart(struct PowerManageInfo_st *pm, Uint32_t timeout)
{
    //LOGI(DBG_TAG, "%s entry\n", __func__);
    printf("%s entry\n", __func__);
    configASSERT(pm != NULL);
    configASSERT(timeout > 0);

    if (timeout != TICK_MAX)
    {
        lHalDeviceControl(pm->hwTimerDev, HWTIMER_CTRL_START_BY_TICK, (void *)&timeout);
    }
}

/**
 * This function stop the timer of pm
 *
 * @param pm Pointer to power manage structure
 */
static void vDrvPMTimerStop(struct PowerManageInfo_st *pm)
{
    configASSERT(pm != NULL);

    lHalDeviceControl(pm->hwTimerDev, HWTIMER_CTRL_STOP, NULL);
}

/**
 * This function calculate how many OS Ticks that MCU have suspended
 *
 * @param pm Pointer to power manage structure
 *
 * @return OS Ticks
 */
static Tick_t ulDrvPMTimerGetTick(struct PowerManageInfo_st *pm)
{
    Uint32_t timer_tick = 0;

    configASSERT(pm != NULL);

    lHalDeviceControl(pm->hwTimerDev, HWTIMER_CTRL_GET_TICK, &timer_tick);

    return timer_tick;
}

/**
 * This function initialize the power manager
 */
int lDrvPMHWInit(void)
{
    static const struct PmOps_st _ops =
    {
        vDrvPMSleep,
        vDrvPMRun,
        vDrvPMTimerStart,
        vDrvPMTimerStop,
        ulDrvPMTimerGetTick
    };

    Uint8_t timerMask = 0;
    //LOGI(DBG_TAG, "%s enter\n", __func__);
    printf("%s enter\n", __func__);
    /* initialize timer mask */
    timerMask = 1UL << PM_SLEEP_MODE_DEEP;

    /* initialize system pm module */
    vHalSystemPMInit(&_ops, timerMask, NULL);

    //LOGI(DBG_TAG, "%s exit\n", __func__);
    printf("%s exit\n", __func__);

    return 0;
}

INIT_PREV_EXPORT(lDrvPMHWInit);

#endif
