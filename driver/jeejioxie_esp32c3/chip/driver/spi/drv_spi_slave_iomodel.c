#include <driver/spi_common.h>
#include <driver/spi_master.h>
#include <driver/spi_slave.h>
#include <hal_spi.h>
#include <driver/drv_spi_slave_iomodel.h>
#include "esp_log.h"
#include "auto_init.h"



#define TAG "drv_spi_iomodel"

struct esp_spi_cs
{
    Int16_t pin;
};


Err_t esp_spi_bus_attach_device(const char *bus_name, const char *device_name, Uint32_t pin);

static Err_t spi_configure(struct SpiDevice_st *device, struct SpiConfiguration_st *cfg)
{
    struct esp_spi_cs *cs_pin = (struct esp_spi_cs *)device->parent.user_data;
    spi_bus_config_t bus_cfg={
        .mosi_io_num=PIN_NUM_MOSI,
        .miso_io_num=PIN_NUM_MISO,
        .sclk_io_num=PIN_NUM_CLK
    };
    spi_slave_interface_config_t slvcfg={
        .mode=0,
        .spics_io_num=cs_pin->pin,
        .queue_size=3,
        .flags=0,
        .post_setup_cb=0,
        .post_trans_cb=0
    };
    spi_slave_initialize(SLAVE_HOST, &bus_cfg, &slvcfg, SPI_DMA_CH_AUTO);
    LogInfo(TAG, "spi_configure slave");
    return DEV_EOK;
}

static Dev_t spixfer(struct SpiDevice_st *device, struct SpiMessage_st *message)
{
    Err_t ret = DEV_ERROR;
    struct esp_spi_cs *cs = device->parent.user_data;

    spi_slave_transaction_t t={
        .tx_buffer = message->send_buf,
        .rx_buffer = message->recv_buf,
        .length=message->length*8,
    };
    ret = spi_slave_transmit(SLAVE_HOST, &t, portMAX_DELAY);

    return !ret;
}

Err_t esp_spi_bus_attach_device(const char *bus_name, const char *device_name, Uint32_t pin)
{
    Err_t ret;
    struct SpiDevice_st *spi_device;
    struct esp_spi_cs *cs_pin;

    spi_device = (struct SpiDevice_st *)malloc(sizeof(struct SpiDevice_st));
    configASSERT(spi_device != NULL);

    cs_pin = (struct esp_spi_cs *)malloc(sizeof(struct esp_spi_cs));
    configASSERT(cs_pin != NULL);

    cs_pin->pin = pin;
    ret = lHalSpiBusAttachDevice(spi_device, device_name, bus_name, (void *)cs_pin);

    return ret;
}

static struct SpiOps_st esp_spi_ops =
{
    .configure = spi_configure,
    .xfer = spixfer
};

struct SpiBus_st spi2_bus;


int lDrvSpiSlaveInit(void)
{ 
    return lHalSpiBusRegister(&spi2_bus, SPI_BUS_NAME, &esp_spi_ops);
}
INIT_PREV_EXPORT(lDrvSpiSlaveInit);