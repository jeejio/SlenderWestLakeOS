/* FreeRTOS includes. */
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/queue.h"
#include "freertos/semphr.h"

/* Logging includes. */
#include "log.h"

/* Standard includes. */
#include <stdio.h>
#include <stdarg.h>
#include <string.h>

static int vsnprintf_safe( char * s,
                           size_t n,
                           const char * format,
                           va_list arg )
{
    int ret;

    ret = vsnprintf( s, n, format, arg );

    /* Check if the string was truncated and if so, update the return value
     * to reflect the number of characters actually written. */
    if( ret >= n )
    {
        /* Do not include the terminating NULL character to keep the behaviour
         * same as the standard. */
        ret = n - 1;
    }
    else if( ret < 0 )
    {
        /* Encoding error - Return 0 to indicate that nothing was written to the
         * buffer. */
        ret = 0;
    }
    else
    {
        /* Complete string was written to the buffer. */
    }

    return ret;
}

static int snprintf_safe( char * s,
                          size_t n,
                          const char * format,
                          ... )
{
    int ret;
    va_list args;

    va_start( args, format );
    ret = vsnprintf_safe( s, n, format, args );
    va_end( args );

    return ret;
}

#if CONFIG_LOG_OUTPUT_ASYNC
static QueueHandle_t xQueue = NULL;
static void prvLoggingTask( void * pvParameters );
static char * pcPrintString = NULL;

BaseType_t xLoggingTaskInitialize()
{
    BaseType_t xReturn = pdFAIL;

    /* Ensure the logging task has not been created already. */
    if( xQueue == NULL )
    {
        /* Create the queue used to pass pointers to strings to the logging task. */
        xQueue = xQueueCreate( 32, sizeof( char ** ) );

        if( xQueue != NULL )
        {
            if( xTaskCreate( prvLoggingTask, "async_output", 2560, NULL, tskIDLE_PRIORITY + 5, NULL ) == pdPASS )
            {
                xReturn = pdPASS;
            }
            else
            {
                /* Could not create the task, so delete the queue again. */
                vQueueDelete( xQueue );
            }
        }
    }

    return xReturn;
}

static void prvLoggingTask( void * pvParameters )
{
    /* Disable unused parameter warning. */
    ( void ) pvParameters;

    char * pcReceivedString = NULL;
    //UBaseType_t uxHighWaterMark = uxTaskGetStackHighWaterMark( NULL );
    //printf("start StackHighWater: %d \n",uxHighWaterMark);
    for( ; ; )
    {
        /* Block to wait for the next string to print. */
        if( xQueueReceive( xQueue, &pcReceivedString, portMAX_DELAY ) == pdPASS )
        {
            SdkLog( pcReceivedString );

            vPortFree( ( void * ) pcReceivedString );
        }
        //printf("StackHighWater: %d \n",uxTaskGetStackHighWaterMark( NULL ));
    }
}


void vLoggingPrintf(const char* pcFormat, ...)
{
    size_t xLength = 0;
    va_list args;
    va_start(args, pcFormat);

    configASSERT( pcFormat != NULL );
    configASSERT( LOG_OUTPUT_BUFFER_SIZE > 0 );

    /* The queue is created by xLoggingTaskInitialize().  Check
     * xLoggingTaskInitialize() has been called. */
    configASSERT( xQueue );

    /* Allocate a buffer to hold the log message. */
    pcPrintString = pvPortMalloc( LOG_OUTPUT_BUFFER_SIZE );

    if( pcPrintString != NULL )
    {
        const char * pcLevelString = NULL;
        size_t ulFormatLen = 0UL;
    
        xLength += vsnprintf_safe( pcPrintString, LOG_OUTPUT_BUFFER_SIZE, pcFormat, args );

        /* Add newline characters if the message does not end with them.*/
        ulFormatLen = strlen( pcPrintString );

        if( ( ulFormatLen >= 2 ) &&
            ( strncmp( pcPrintString + ulFormatLen - LOG_LF_OFFSET, "\n", 1 ) != 0 ) &&
            ( xLength < LOG_OUTPUT_BUFFER_SIZE ) )
        {
            xLength += snprintf_safe( pcPrintString + xLength, LOG_OUTPUT_BUFFER_SIZE - xLength, "%s", "\r\n" );
        }

        /* The standard says that snprintf writes the terminating NULL
         * character. Just re-write it in case some buggy implementation does
         * not. */
        configASSERT( xLength < LOG_OUTPUT_BUFFER_SIZE );
        pcPrintString[ xLength ] = '\0';

        /* Only send the buffer to the logging task if it is
         * not empty. */
        if( xLength > 0 )
        {
            /* Send the string to the logging task for IO. */
            if( xQueueSend( xQueue, &pcPrintString, 0) != pdPASS )
            {
                /* The buffer was not sent so must be freed again. */
                vPortFree( ( void * ) pcPrintString );
            }
        }
        else
        {
            /* The buffer was not sent, so it must be
             * freed. */
            vPortFree( ( void * ) pcPrintString );
        }
    }
    va_end(args);
}
#else
void vLoggingPrintf(const char* pcFormat, ...)
{
    char pcBuffer[LOG_OUTPUT_BUFFER_SIZE];
    size_t xLength = 0;
    size_t ulFormatLen = 0UL;
    va_list args;
    va_start(args, pcFormat);

    xLength += vsnprintf_safe( pcBuffer, LOG_OUTPUT_BUFFER_SIZE, pcFormat, args );

    ulFormatLen = strlen( pcBuffer );

    if ((ulFormatLen >= 2) &&
        (strncmp(pcBuffer + ulFormatLen - LOG_LF_OFFSET, "\n", 1) != 0))
    {
        xLength += snprintf_safe( pcBuffer + xLength, LOG_OUTPUT_BUFFER_SIZE - xLength, "%s", "\r\n" );
    }

    configASSERT( xLength < LOG_OUTPUT_BUFFER_SIZE );
    pcBuffer[ xLength ] = '\0';

    SdkLog(pcBuffer);
    va_end(args);
}
#endif


#if CONFIG_LOG_TIMESTAMP_TICK
uint32_t xKernelTimestamp(void)
{
    if (xTaskGetSchedulerState() == taskSCHEDULER_NOT_STARTED) {
        return 0;
    }
    return xPortInIsrContext() ? xTaskGetTickCountFromISR() : xTaskGetTickCount();
}
#else
char* xKernelTimestamp(void)
{
    static char ulKernelTime[15];
    if (xTaskGetSchedulerState() == taskSCHEDULER_NOT_STARTED) {
        return 0;
    }
    TickType_t tick_count = xPortInIsrContext() ? xTaskGetTickCountFromISR() : xTaskGetTickCount();
    TickType_t tmp = tick_count * (1000 / configTICK_RATE_HZ);
    TickType_t tick_sec = tmp / 1000;
    TickType_t tick_ms = tmp - (tick_sec * 1000);

    sprintf(ulKernelTime,"%3lu.%03lu", tick_sec, tick_ms);
    return ulKernelTime;
}
#endif

#if CONFIG_LOG_OUTPUT_ASYNC
#include "auto_init.h"
INIT_BOARD_EXPORT(xLoggingTaskInitialize)
#endif
