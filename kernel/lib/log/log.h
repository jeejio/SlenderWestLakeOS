#ifndef LOG_H
#define LOG_H
#include "sdkconfig.h"
#include <stdio.h>
#include <stdint.h>
#include <stdarg.h>
#include <inttypes.h>
#include "freertos/FreeRTOS.h"

#define LOG_NONE     0
#define LOG_ERROR    1
#define LOG_WARN     2
#define LOG_INFO     3
#define LOG_DEBUG    4

#if CONFIG_LOG_COLORS_ENABLE
/* output log front color */
#define FONT_COLOR_BLACK   "30"
#define FONT_COLOR_RED     "31"
#define FONT_COLOR_GREEN   "32"
#define FONT_COLOR_BROWN   "33"
#define FONT_COLOR_BLUE    "34"
#define FONT_COLOR_PURPLE  "35"
#define FONT_COLOR_CYAN    "36"
#define FONT_COLOR(COLOR)  "\033[0;" COLOR "m"
#define FONT_BOLD(COLOR)   "\033[1;" COLOR "m"
#define _RESET_COLOR       "\033[0m"
#define _COLOR_E   FONT_COLOR(FONT_COLOR_RED)
#define _COLOR_W   FONT_COLOR(FONT_COLOR_BROWN)
#define _COLOR_I   FONT_COLOR(FONT_COLOR_GREEN)
#define _COLOR_D   FONT_COLOR(FONT_COLOR_BLUE)
#define LOG_LF_OFFSET 5
#else //CONFIG_LOG_COLORS
#define _COLOR_E
#define _COLOR_W
#define _COLOR_I
#define _COLOR_D
#define _RESET_COLOR
#define LOG_LF_OFFSET 1
#endif //CONFIG_LOG_COLORS

/* Define the uart transfer function port */
#define SdkLog printf

void vLoggingPrintf(const char* pcFormat, ...);

#if CONFIG_LOG_TIMESTAMP_TICK
uint32_t xKernelTimestamp(void);
#define LOG_NORMAL_FORMAT(letter, format)  _COLOR_ ## letter  "[%" PRIu32 "] " "%s: " format _RESET_COLOR
#define LOG_DEBUG_FORMAT(letter, format)  _COLOR_ ## letter  "[%" PRIu32 "] " "%s: " LOG_METADATA_FORMAT format _RESET_COLOR
#else
#define LOG_NORMAL_FORMAT(letter, format)  _COLOR_ ## letter  "[%s] " "%s: " format _RESET_COLOR
#define LOG_DEBUG_FORMAT(letter, format)  _COLOR_ ## letter  "[%s] " "%s: " LOG_METADATA_FORMAT format _RESET_COLOR
char* xKernelTimestamp(void)
#endif

#ifndef LOG_METADATA_FORMAT
    #define LOG_METADATA_FORMAT    "%s:%d "                  /**< @brief Format of metadata prefix in log messages. */
#endif

#ifndef LOG_METADATA_ARGS
    #define LOG_METADATA_ARGS    __FUNCTION__, __LINE__  /**< @brief Arguments into the metadata logging prefix format. */
#endif

#define LOG_OUTPUT_BUFFER_SIZE CONFIG_LOG_OUTPUT_BUFFER_SIZE

/* Metadata information to prepend to every log message. */


#if CONFIG_LOG_LEVEL == LOG_DEBUG
/* All log level messages will logged. */
#define LogError(tag, message, ...)            vLoggingPrintf(LOG_NORMAL_FORMAT(E, message), xKernelTimestamp(), tag , ##__VA_ARGS__)
#define LogWarn(tag, message, ...)             vLoggingPrintf(LOG_NORMAL_FORMAT(W, message), xKernelTimestamp(), tag , ##__VA_ARGS__)
#define LogInfo(tag, message, ...)             vLoggingPrintf(LOG_NORMAL_FORMAT(I, message), xKernelTimestamp(), tag , ##__VA_ARGS__)
#define LogDebug(tag, message, ...)            vLoggingPrintf(LOG_DEBUG_FORMAT(D, message), xKernelTimestamp(), tag , LOG_METADATA_ARGS ##__VA_ARGS__)
#elif CONFIG_LOG_LEVEL == LOG_INFO
/* Only INFO, WARNING and ERROR messages will be logged. */
#define LogError(tag, message, ...)            vLoggingPrintf(LOG_NORMAL_FORMAT(E, message), xKernelTimestamp(), tag , ##__VA_ARGS__)
#define LogWarn(tag, message, ...)             vLoggingPrintf(LOG_NORMAL_FORMAT(W, message), xKernelTimestamp(), tag , ##__VA_ARGS__)
#define LogInfo(tag, message, ...)             vLoggingPrintf(LOG_NORMAL_FORMAT(I, message), xKernelTimestamp(), tag , ##__VA_ARGS__)
#define LogDebug(tag, message, ...)

#elif CONFIG_LOG_LEVEL == LOG_WARN
/* Only WARNING and ERROR messages will be logged.*/
#define LogError(tag, message, ...)            vLoggingPrintf(LOG_NORMAL_FORMAT(E, message), xKernelTimestamp(), tag , ##__VA_ARGS__)
#define LogWarn(tag, message, ...)             vLoggingPrintf(LOG_NORMAL_FORMAT(W, message), xKernelTimestamp(), tag , ##__VA_ARGS__)
#define LogInfo(tag, message, ...)
#define LogDebug(tag, message, ...)

#elif CONFIG_LOG_LEVEL == LOG_ERROR
/* Only ERROR messages will be logged. */
#define LogError(tag, message, ...)            vLoggingPrintf(LOG_NORMAL_FORMAT(E, message), xKernelTimestamp(), tag , ##__VA_ARGS__)
#define LogWarn(tag, message, ...)
#define LogInfo(tag, message, ...)
#define LogDebug(tag, message, ...)

#else /* if LIBRARY_LOG_LEVEL == LOG_ERROR */

#define LogError(tag, message, ...)
#define LogWarn(tag, message, ...)
#define LogInfo(tag, message, ...)
#define LogDebug(tag, message, ...)

#endif /* if LIBRARY_LOG_LEVEL == LOG_ERROR */
#endif