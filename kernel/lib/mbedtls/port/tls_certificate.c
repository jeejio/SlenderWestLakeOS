
/*
 * Copyright (c) 2006-2018 RT-Thread Development Team. All rights reserved.
 * License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may
 * not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

// #include "mbedtls/certs.h"
#include "stdint.h"

const char mbedtls_root_certificate[] =
        "-----BEGIN CERTIFICATE-----\n"
        "MIIDrzCCApegAwIBAgIQCDvgVpBCRrGhdWrJWZHHSjANBgkqhkiG9w0BAQUFADBh\n"
        "MQswCQYDVQQGEwJVUzEVMBMGA1UEChMMRGlnaUNlcnQgSW5jMRkwFwYDVQQLExB3\n"
        "d3cuZGlnaWNlcnQuY29tMSAwHgYDVQQDExdEaWdpQ2VydCBHbG9iYWwgUm9vdCBD\n"
        "QTAeFw0wNjExMTAwMDAwMDBaFw0zMTExMTAwMDAwMDBaMGExCzAJBgNVBAYTAlVT\n"
        "MRUwEwYDVQQKEwxEaWdpQ2VydCBJbmMxGTAXBgNVBAsTEHd3dy5kaWdpY2VydC5j\n"
        "b20xIDAeBgNVBAMTF0RpZ2lDZXJ0IEdsb2JhbCBSb290IENBMIIBIjANBgkqhkiG\n"
        "9w0BAQEFAAOCAQ8AMIIBCgKCAQEA4jvhEXLeqKTTo1eqUKKPC3eQyaKl7hLOllsB\n"
        "CSDMAZOnTjC3U/dDxGkAV53ijSLdhwZAAIEJzs4bg7/fzTtxRuLWZscFs3YnFo97\n"
        "nh6Vfe63SKMI2tavegw5BmV/Sl0fvBf4q77uKNd0f3p4mVmFaG5cIzJLv07A6Fpt\n"
        "43C/dxC//AH2hdmoRBBYMql1GNXRor5H4idq9Joz+EkIYIvUX7Q6hL+hqkpMfT7P\n"
        "T19sdl6gSzeRntwi5m3OFBqOasv+zbMUZBfHWymeMr/y7vrTC0LUq7dBMtoM1O/4\n"
        "gdW7jVg/tRvoSSiicNoxBN33shbyTApOB6jtSj1etX+jkMOvJwIDAQABo2MwYTAO\n"
        "BgNVHQ8BAf8EBAMCAYYwDwYDVR0TAQH/BAUwAwEB/zAdBgNVHQ4EFgQUA95QNVbR\n"
        "TLtm8KPiGxvDl7I90VUwHwYDVR0jBBgwFoAUA95QNVbRTLtm8KPiGxvDl7I90VUw\n"
        "DQYJKoZIhvcNAQEFBQADggEBAMucN6pIExIK+t1EnE9SsPTfrgT1eXkIoyQY/Esr\n"
        "hMAtudXH/vTBH1jLuG2cenTnmCmrEbXjcKChzUyImZOMkXDiqw8cvpOp/2PV5Adg\n"
        "06O/nVsJ8dWO41P0jmP6P6fbtGbfYmbW0W5BjfIttep3Sp+dWOIrWcBAI+0tKIJF\n"
        "PnlUkiaY4IBIqDfv8NZ5YBberOgOzW6sRBc4L0na4UU+Krk2U886UAb3LujEV0ls\n"
        "YSEY1QSteDwsOoBrp+uvFRTp2InBuThs4pFsiv9kuXclVzDAGySj4dzp30d8tbQk\n"
        "CAUw7C29C79Fv1C5qfPrmAESrciIxpg0X40KPMbp1ZWVbd4=\n"
        "-----END CERTIFICATE-----";

//const char mbedtls_root_certificate[] =
//"-----BEGIN CERTIFICATE-----\r\n"
//"MIIEvjCCA6agAwIBAgIQBtjZBNVYQ0b2ii+nVCJ+xDANBgkqhkiG9w0BAQsFADBh\r\n"
//"MQswCQYDVQQGEwJVUzEVMBMGA1UEChMMRGlnaUNlcnQgSW5jMRkwFwYDVQQLExB3\r\n"
//"d3cuZGlnaWNlcnQuY29tMSAwHgYDVQQDExdEaWdpQ2VydCBHbG9iYWwgUm9vdCBD\r\n"
//"QTAeFw0yMTA0MTQwMDAwMDBaFw0zMTA0MTMyMzU5NTlaME8xCzAJBgNVBAYTAlVT\r\n"
//"MRUwEwYDVQQKEwxEaWdpQ2VydCBJbmMxKTAnBgNVBAMTIERpZ2lDZXJ0IFRMUyBS\r\n"
//"U0EgU0hBMjU2IDIwMjAgQ0ExMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKC\r\n"
//"AQEAwUuzZUdwvN1PWNvsnO3DZuUfMRNUrUpmRh8sCuxkB+Uu3Ny5CiDt3+PE0J6a\r\n"
//"qXodgojlEVbbHp9YwlHnLDQNLtKS4VbL8Xlfs7uHyiUDe5pSQWYQYE9XE0nw6Ddn\r\n"
//"g9/n00tnTCJRpt8OmRDtV1F0JuJ9x8piLhMbfyOIJVNvwTRYAIuE//i+p1hJInuW\r\n"
//"raKImxW8oHzf6VGo1bDtN+I2tIJLYrVJmuzHZ9bjPvXj1hJeRPG/cUJ9WIQDgLGB\r\n"
//"Afr5yjK7tI4nhyfFK3TUqNaX3sNk+crOU6JWvHgXjkkDKa77SU+kFbnO8lwZV21r\r\n"
//"eacroicgE7XQPUDTITAHk+qZ9QIDAQABo4IBgjCCAX4wEgYDVR0TAQH/BAgwBgEB\r\n"
//"/wIBADAdBgNVHQ4EFgQUt2ui6qiqhIx56rTaD5iyxZV2ufQwHwYDVR0jBBgwFoAU\r\n"
//"A95QNVbRTLtm8KPiGxvDl7I90VUwDgYDVR0PAQH/BAQDAgGGMB0GA1UdJQQWMBQG\r\n"
//"CCsGAQUFBwMBBggrBgEFBQcDAjB2BggrBgEFBQcBAQRqMGgwJAYIKwYBBQUHMAGG\r\n"
//"GGh0dHA6Ly9vY3NwLmRpZ2ljZXJ0LmNvbTBABggrBgEFBQcwAoY0aHR0cDovL2Nh\r\n"
//"Y2VydHMuZGlnaWNlcnQuY29tL0RpZ2lDZXJ0R2xvYmFsUm9vdENBLmNydDBCBgNV\r\n"
//"HR8EOzA5MDegNaAzhjFodHRwOi8vY3JsMy5kaWdpY2VydC5jb20vRGlnaUNlcnRH\r\n"
//"bG9iYWxSb290Q0EuY3JsMD0GA1UdIAQ2MDQwCwYJYIZIAYb9bAIBMAcGBWeBDAEB\r\n"
//"MAgGBmeBDAECATAIBgZngQwBAgIwCAYGZ4EMAQIDMA0GCSqGSIb3DQEBCwUAA4IB\r\n"
//"AQCAMs5eC91uWg0Kr+HWhMvAjvqFcO3aXbMM9yt1QP6FCvrzMXi3cEsaiVi6gL3z\r\n"
//"ax3pfs8LulicWdSQ0/1s/dCYbbdxglvPbQtaCdB73sRD2Cqk3p5BJl+7j5nL3a7h\r\n"
//"qG+fh/50tx8bIKuxT8b1Z11dmzzp/2n3YWzW2fP9NsarA4h20ksudYbj/NhVfSbC\r\n"
//"EXffPgK2fPOre3qGNm+499iTcc+G33Mw+nur7SpZyEKEOxEXGlLzyQ4UfaJbcme6\r\n"
//"ce1XR2bFuAJKZTRei9AqPCCcUZlM51Ke92sRKw2Sfh3oius2FkOH6ipjv3U/697E\r\n"
//"A7sKPPcw7+uvTPyLNhBzPvOk\r\n"
//"-----END CERTIFICATE-----\r\n";

//const char mbedtls_root_certificate[] = "-----BEGIN CERTIFICATE-----\n"
//"MIIE6zCCAtMCFEbvIXcfbCKVjxZPS5OAMbNeVhHPMA0GCSqGSIb3DQEBCwUAMDIx\n"
//"MDAuBgNVBAoMJ1RMUyBQcm9qZWN0IERvZGd5IENlcnRpZmljYXRlIEF1dGhvcml0\n"
//"eTAeFw0yMzA0MTQwNjMzMDBaFw0zNjEyMjEwNjMzMDBaMDIxMDAuBgNVBAoMJ1RM\n"
//"UyBQcm9qZWN0IERvZGd5IENlcnRpZmljYXRlIEF1dGhvcml0eTCCAiIwDQYJKoZI\n"
//"hvcNAQEBBQADggIPADCCAgoCggIBAJWecZLdsH3JysQqaNoyuNL4VOFsjYTx28wW\n"
//"KwASWaWoSXwmwmu+jnfLVRvD4PnmLrmNYzHnmsCEPh7fViUWynsL8zaOYqLQTcMI\n"
//"rqLVQZHL3Tj6YhiGngm+jwdzFkEW+iZz8gyZlHBgH9TM1b5XCOZJMfJ8/HQZgtS8\n"
//"YpiuJ09aBk8VNOFolxNuj0B8eZW4HwnpzkOcjWZze1QvuN8dDhXpXlrkYVJkXj9C\n"
//"ujHBPR1kT82L47qPP54p9ZbcjpskWFuaCf06bHnIaYJyglAfOJmMK4t1Ji0RNZEL\n"
//"Y5UMY4/CR4IohvJK0dy97Z7wAttoxtVMQz62GIR2sWo2NvZnW3JNK+I6fQGnzJ7g\n"
//"roaR5JkIPs3UIuPFtEFxHZu950W+8u2ph0UrXNkjW2I4ZnRJPtG82RrXddXX8Am0\n"
//"OdNGxY26Hid6hvhX1EbxAe8wDdgqMS1fkVH5hO7sbdO8KVVfG+D90WhseBalK5tk\n"
//"HtvqGVH6DtIksl3i4vwO2EZcMBvHujXzQxGCJyS4Scyr3eK/TdWwv3taHixaRBLh\n"
//"hnDuqyak7AqkO9q1PSotbhnuliSrLn5PT+DfiZRjCRU12OazLMS8aioNXyodqnr+\n"
//"QE43LxIfyT+JGw+bzDquyLd8V4Onokw0E2dTqMh7couBJ46K3R0YDGVlnZNLiKN1\n"
//"9+JUxI1DAgMBAAEwDQYJKoZIhvcNAQELBQADggIBAEoJmMI6E/idbisVHIJuoHTZ\n"
//"eGvKwY4uf9J+jshBqDlQPyTc5ojussp+hxxBinCKN/QP/n7l42ZQ66qril9EqN7b\n"
//"B2GBTe0nQ5tee4LHVVKCPL7V4hRk6my1IqgTm4+n+/KoDsYrb0+QtWwW7kY8/Hx5\n"
//"loiaBOQvchmyoAnTiC09H16t/RwZhPwX4b6sLVxw7LK2XVqLgwBuw1Kfb6gTyTwJ\n"
//"LJLo0LgBv3bB5mLCR46S6RhvAFVygLXXmP5g3e42eIUvtzwYeZSiYGubH9v/uGHW\n"
//"GYLQx/HpTXKhozbcjvr0/fOD/g/7cvwT3I1X7aZijFlDSxqAIDec22kSEYm3qRiP\n"
//"JAziUqFQR/Vryvu0EIMqnHI7pCsFxosSw9HXtju5j6gjR1I1q6htJJsPxF1Ahsr8\n"
//"h5OcV5fd4EBTUnUbQummpiQPzGbNCmIxmWKnkYkBS9ZWGBGLuzy4z+eNtTAGbxMm\n"
//"ncUpiQH5+SMIT9ZWOsOU1M02ZeXVREK68ZqaBJN7bDF3J+v2b4EH+LEle9ZZxPbq\n"
//"aGhO1l4blzR8VRPGYAiyzSyOLZifLGFilSBX/oSOVrabIo19ysNp+tGEf13l0LAV\n"
//"EXCx7RfuhxW25SdC7r4IlKV0OqyKxujihDOaqUlgwssVUpj/KdnKEG4eOo0t8grk\n"
//"C9HoQFmCPbSqBW6EfUuL\n"
//"-----END CERTIFICATE-----\n";


const uint64_t mbedtls_root_certificate_len = sizeof(mbedtls_root_certificate);

