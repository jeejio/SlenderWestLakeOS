/*
 * Copyright (c) 2022, jeejio Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2022-11-29     zhengqian    the first version
*/

#ifndef __DEF_H__
#define __DEF_H__

#define USING_LIBC

#ifdef USING_LIBC
#include <stdint.h>
#include <stddef.h>
#include <stdarg.h>
#include "esp_log.h"

#endif /* USING_LIBC */

#define USING_DEVICE
#define DEV_NAME_MAX 20

#define LogError(tag, pcformat, ...)      ESP_LOGE(tag, pcformat, ##__VA_ARGS__)
#define LogWarn(tag, pcformat, ...)       ESP_LOGW(tag, pcformat, ##__VA_ARGS__)
#define LogInfo(tag, pcformat, ...)       ESP_LOGI(tag, pcformat, ##__VA_ARGS__)
#define LogDebug(tag, pcformat, ...)      ESP_LOGD(tag, pcformat, ##__VA_ARGS__)


#ifdef __cplusplus
extern "C" {
#endif

/**
 * @addtogroup BasicDef
 */

/**@{*/


#define VERSION_CHECK(major, minor, revise)          ((major * 10000) + \
                                                         (minor * 100) + revise)

/* Basic data type definitions */
#ifndef USING_ARCH_DATA_TYPE
#ifdef USING_LIBC
typedef int8_t                          Int8_t;      /**<  8bit integer type */
typedef int16_t                         Int16_t;     /**< 16bit integer type */
typedef int32_t                         Int32_t;     /**< 32bit integer type */
typedef uint8_t                         Uint8_t;     /**<  8bit unsigned integer type */
typedef uint16_t                        Uint16_t;    /**< 16bit unsigned integer type */
typedef uint32_t                        Uint32_t;    /**< 32bit unsigned integer type */
typedef int64_t                         Int64_t;     /**< 64bit integer type */
typedef uint64_t                        Uint64_t;    /**< 64bit unsigned integer type */
typedef size_t                          Size_t;      /**< Type for size number */

#else
typedef signed   char                   Int8_t;      /**<  8bit integer type */
typedef signed   short                  Int16_t;     /**< 16bit integer type */
typedef signed   int                    Int32_t;     /**< 32bit integer type */
typedef unsigned char                   Uint8_t;     /**<  8bit unsigned integer type */
typedef unsigned short                  Uint16_t;    /**< 16bit unsigned integer type */
typedef unsigned int                    Uint32_t;    /**< 32bit unsigned integer type */

#ifdef ARCH_CPU_64BIT
typedef signed long                     Int64_t;     /**< 64bit integer type */
typedef unsigned long                   Uint64_t;    /**< 64bit unsigned integer type */
typedef unsigned long                   Size_t;      /**< Type for size number */
#else
typedef signed long long                Int64_t;     /**< 64bit integer type */
typedef unsigned long long              Uint64_t;    /**< 64bit unsigned integer type */
typedef unsigned int                    Size_t;      /**< Type for size number */
#endif /* ARCH_CPU_64BIT */
#endif /* USING_LIBC */
#endif /* USING_ARCH_DATA_TYPE */

typedef int                             Bool_t;      /**< boolean type */
typedef long                            Base_t;      /**< Nbit CPU related date type */
typedef unsigned long                   Ubase_t;     /**< Nbit unsigned CPU related data type */

typedef Base_t                       Err_t;       /**< Type for error number */
typedef Uint32_t                     Time_t;      /**< Type for time stamp */
typedef Uint32_t                     Tick_t;      /**< Type for tick count */
typedef Base_t                       Flag_t;      /**< Type for flags */
typedef Ubase_t                      Dev_t;       /**< Type for device */
typedef Base_t                       Off_t;       /**< Type for offset */

/* boolean type definitions */
#ifndef TRUE
#define TRUE                             1            /**< boolean true  */
#endif
#ifndef FALSE
#define FALSE                            0            /**< boolean false */
#endif

/**@}*/

#define TICK_MAX                     UINT32_MAX   /**< Maximum number of tick */

/* maximum value of ipc type */
#define SEM_VALUE_MAX                UINT16_MAX   /**< Maximum number of semaphore .value */
#define MUTEX_VALUE_MAX              UINT16_MAX   /**< Maximum number of mutex .value */
#define MUTEX_HOLD_MAX               UINT8_MAX    /**< Maximum number of mutex .hold */
#define MB_ENTRY_MAX                 UINT16_MAX   /**< Maximum number of mailbox .entry */
#define MQ_ENTRY_MAX                 UINT16_MAX   /**< Maximum number of message queue .entry */

#define UNUSED(x)                   ((void)x)

/* Compiler Related Definitions */
#if defined(__ARMCC_VERSION)           /* ARM Compiler */
#define SECTION(x)               __attribute__((section(x)))
#define USED                     __attribute__((used))
#define ALIGN(n)                    __attribute__((aligned(n)))
#define WEAK                     __attribute__((weak))
#define inline                   __inline
/* module compiling */
#ifdef USING_MODULE
#define RTT_API                     __declspec(dllimport)
#else
#define RTT_API                     __declspec(dllexport)
#endif /* USING_MODULE */
#elif defined (__IAR_SYSTEMS_ICC__)     /* for IAR Compiler */
#define SECTION(x)               @ x
#define USED                     __root
#define PRAGMA(x)                   _Pragma(#x)
#define ALIGN(n)                    PRAGMA(data_alignment=n)
#define WEAK                     __weak
#define inline                   __inline
#define RTT_API
#elif defined (__GNUC__)                /* GNU GCC Compiler */
#ifndef USING_LIBC
/* the version of GNU GCC must be greater than 4.x */
typedef __builtin_va_list           GnucVaList_t;
typedef GnucVaList_t              VaList_t;
#define va_start(v,l)               __builtin_va_start(v,l)
#define va_end(v)                   __builtin_va_end(v)
#define va_arg(v,l)                 __builtin_va_arg(v,l)
#endif /* USING_LIBC */
#define SECTION(x)               __attribute__((section(x)))
#define USED                     __attribute__((used))
#define ALIGN(n)                    __attribute__((aligned(n)))
#define WEAK                     __attribute__((weak))
#define inline                   __inline
#define RTT_API
#elif defined (__ADSPBLACKFIN__)        /* for VisualDSP++ Compiler */
#define SECTION(x)               __attribute__((section(x)))
#define USED                     __attribute__((used))
#define ALIGN(n)                    __attribute__((aligned(n)))
#define WEAK                     __attribute__((weak))
#define inline                   __inline
#define RTT_API
#elif defined (_MSC_VER)
#define SECTION(x)
#define USED
#define ALIGN(n)                    __declspec(align(n))
#define WEAK
#define inline                   __inline
#define RTT_API
#elif defined (__TI_COMPILER_VERSION__)
/* The way that TI compiler set section is different from other(at least
    * GCC and MDK) compilers. See ARM Optimizing C/C++ Compiler 5.9.3 for more
    * details. */
#define SECTION(x)
#define USED
#define PRAGMA(x)                   _Pragma(#x)
#define ALIGN(n)
#define WEAK
#define inline                   __inline
#define RTT_API
#elif defined (__TASKING__)
#define SECTION(x)               __attribute__((section(x)))
#define USED                     __attribute__((used, protect))
#define PRAGMA(x)                   _Pragma(#x)
#define ALIGN(n)                    __attribute__((__align(n)))
#define WEAK                     __attribute__((weak))
#define inline                   __inline
#define RTT_API
#else
    #error not supported tool chain
#endif /* __ARMCC_VERSION */


/* memory management option */
#define MM_PAGE_SIZE                 4096
#define MM_PAGE_MASK                 (MM_PAGE_SIZE - 1)
#define MM_PAGE_BITS                 12

/**
 * @addtogroup Error
 */

/**@{*/

/* Device error code definitions */
#define DEV_EOK                          0               /**< There is no error */
#define DEV_ERROR                        1               /**< A generic error happens */
#define DEV_ETIMEOUT                     2               /**< Timed out */
#define DEV_EFULL                        3               /**< The resource is full */
#define DEV_EEMPTY                       4               /**< The resource is empty */
#define DEV_ENOMEM                       5               /**< No memory */
#define DEV_ENOSYS                       6               /**< No system */
#define DEV_EBUSY                        7               /**< Busy */
#define DEV_EIO                          8               /**< IO error */
#define DEV_EINTR                        9               /**< Interrupted system call */
#define DEV_EINVAL                       10              /**< Invalid argument */

/**@}*/

/**
 * @ingroup BasicDef
 *
 * @def ALIGN_DOWN(size, align)
 * Return the down number of aligned at specified width. ALIGN_DOWN(13, 4)
 * would return 12.
 */
#define ALIGN_DOWN(size, align)      ((size) & ~((align) - 1))

/**
 * Double List structure
 */
struct ListNode_st
{
    struct ListNode_st *next;                          /**< point to next node. */
    struct ListNode_st *prev;                          /**< point to prev node. */
};
typedef struct ListNode_st DevList_t;                  /**< Type for lists. */

/**
 * Single List structure
 */
struct SlistNode_st
{
    struct SlistNode_st *next;                         /**< point to next node. */
};
typedef struct SlistNode_st Slist_t;                /**< Type for single list. */

/**
 * The information of the device
 */
struct DeviceInformation_st
{
    DevList_t                 device_list;              /**< device list */
    Size_t                 device_size;              /**< device size */
};

/**
 * The hook function call macro
 */
#ifndef USING_HOOK
    #define __ON_HOOK_ARGS(__hook, argv)
    #define OBJECT_HOOK_CALL(func, argv)
#else
    #define OBJECT_HOOK_CALL(func, argv)         __on_##func argv
    #ifdef HOOK_USING_FUNC_PTR
        #define __ON_HOOK_ARGS(__hook, argv)        do {if ((__hook) != NULL) __hook argv; } while (0)
    #else
        #define __ON_HOOK_ARGS(__hook, argv)
    #endif /* HOOK_USING_FUNC_PTR */
#endif /* USING_HOOK */

#ifndef __oninterrupt_switch_hook
    #define __oninterrupt_switch_hook()         __ON_HOOK_ARGS(jee_interrupt_switch_hook, ())
#endif
#ifndef __onmalloc_hook
    #define __onmalloc_hook(addr, size)         __ON_HOOK_ARGS(jee_malloc_hook, (addr, size))
#endif
#ifndef __onfree_hook
    #define __onfree_hook(rmem)                 __ON_HOOK_ARGS(jee_free_hook, (rmem))
#endif


/**@}*/

/**
 * @addtogroup IPC
 */

/**@{*/

/**
 * IPC flags and control command definitions
 */
#define IPC_FLAG_FIFO                0x00            /**< FIFOed IPC. @ref IPC. */
#define IPC_FLAG_PRIO                0x01            /**< PRIOed IPC. @ref IPC. */

#define IPC_CMD_UNKNOWN              0x00            /**< unknown IPC command */
#define IPC_CMD_RESET                0x01            /**< reset IPC object */

#define WAITING_FOREVER              -1              /**< Block forever until get resource. */
#define WAITING_NO                   0               /**< Non-block. */


#ifdef USING_DEVICE
/**
 * @addtogroup Device
 */

/**@{*/

/**
 * device (I/O) class type
 */
enum DeviceClassType_t
{
    Device_Class_Char = 0,                           /**< character device */
    Device_Class_Block,                              /**< block device */
    Device_Class_NetIf,                              /**< net interface */
    Device_Class_MTD,                                /**< memory device */
    Device_Class_CAN,                                /**< CAN device */
    Device_Class_RTC,                                /**< RTC device */
    Device_Class_Sound,                              /**< Sound device */
    Device_Class_Graphic,                            /**< Graphic device */
    Device_Class_I2CBUS,                             /**< I2C bus device */
    Device_Class_USBDevice,                          /**< USB slave device */
    Device_Class_USBHost,                            /**< USB host bus */
    Device_Class_USBOTG,                             /**< USB OTG bus */
    Device_Class_SPIBUS,                             /**< SPI bus device */
    Device_Class_SPIDevice,                          /**< SPI device */
    Device_Class_SDIO,                               /**< SDIO bus device */
    Device_Class_PM,                                 /**< PM pseudo device */
    Device_Class_Pipe,                               /**< Pipe device */
    Device_Class_Portal,                             /**< Portal device */
    Device_Class_Timer,                              /**< Timer device */
    Device_Class_Miscellaneous,                      /**< Miscellaneous device */
    Device_Class_Sensor,                             /**< Sensor device */
    Device_Class_Touch,                              /**< Touch device */
    Device_Class_PHY,                                /**< PHY device */
    Device_Class_Security,                           /**< Security device */
    Device_Class_WLAN,                               /**< WLAN device */
    Device_Class_Pin,                                /**< Pin device */
    Device_Class_ADC,                                /**< ADC device */
    Device_Class_DAC,                                /**< DAC device */
    Device_Class_WDT,                                /**< WDT device */
    Device_Class_PWM,                                /**< PWM device */
    Device_Class_Unknown                             /**< unknown device */
};

/**
 * device flags definitions
 */
#define DEVICE_FLAG_DEACTIVATE       0x000           /**< device is not not initialized */

#define DEVICE_FLAG_RDONLY           0x001           /**< read only */
#define DEVICE_FLAG_WRONLY           0x002           /**< write only */
#define DEVICE_FLAG_RDWR             0x003           /**< read and write */

#define DEVICE_FLAG_REMOVABLE        0x004           /**< removable device */
#define DEVICE_FLAG_STANDALONE       0x008           /**< standalone device */
#define DEVICE_FLAG_ACTIVATED        0x010           /**< device is activated */
#define DEVICE_FLAG_SUSPENDED        0x020           /**< device is suspended */
#define DEVICE_FLAG_STREAM           0x040           /**< stream mode */

#define DEVICE_FLAG_INT_RX           0x100           /**< INT mode on Rx */
#define DEVICE_FLAG_DMA_RX           0x200           /**< DMA mode on Rx */
#define DEVICE_FLAG_INT_TX           0x400           /**< INT mode on Tx */
#define DEVICE_FLAG_DMA_TX           0x800           /**< DMA mode on Tx */

#define DEVICE_OFLAG_CLOSE           0x000           /**< device is closed */
#define DEVICE_OFLAG_RDONLY          0x001           /**< read only access */
#define DEVICE_OFLAG_WRONLY          0x002           /**< write only access */
#define DEVICE_OFLAG_RDWR            0x003           /**< read and write */
#define DEVICE_OFLAG_OPEN            0x008           /**< device is opened */
#define DEVICE_OFLAG_MASK            0xf0f           /**< mask of open flag */

/**
 * general device commands
 */
#define DEVICE_CTRL_RESUME           0x01            /**< resume device */
#define DEVICE_CTRL_SUSPEND          0x02            /**< suspend device */
#define DEVICE_CTRL_CONFIG           0x03            /**< configure device */
#define DEVICE_CTRL_CLOSE            0x04            /**< close device */

#define DEVICE_CTRL_SET_INT          0x10            /**< set interrupt */
#define DEVICE_CTRL_CLR_INT          0x11            /**< clear interrupt */
#define DEVICE_CTRL_GET_INT          0x12            /**< get interrupt status */

/**
 * device control
 */
#define DEVICE_CTRL_BASE(Type)        (Device_Class_##Type * 0x100)

/**
 * special device commands
 */
#define DEVICE_CTRL_CHAR_STREAM      (DEVICE_CTRL_BASE(Char) + 1)             /**< stream mode on char device */
#define DEVICE_CTRL_BLK_GETGEOME     (DEVICE_CTRL_BASE(Block) + 1)            /**< get geometry information   */
#define DEVICE_CTRL_BLK_SYNC         (DEVICE_CTRL_BASE(Block) + 2)            /**< flush data to block device */
#define DEVICE_CTRL_BLK_ERASE        (DEVICE_CTRL_BASE(Block) + 3)            /**< erase block on block device */
#define DEVICE_CTRL_BLK_AUTOREFRESH  (DEVICE_CTRL_BASE(Block) + 4)            /**< block device : enter/exit auto refresh mode */
#define DEVICE_CTRL_NETIF_GETMAC     (DEVICE_CTRL_BASE(NetIf) + 1)            /**< get mac address */
#define DEVICE_CTRL_MTD_FORMAT       (DEVICE_CTRL_BASE(MTD) + 1)              /**< format a MTD device */

typedef struct Device_st *Device_t;

#ifdef USING_DEVICE_OPS
/**
 * operations set for device object
 */
struct DeviceOps_st
{
    /* common device interface */
    Err_t  (*init)   (Device_t dev);
    Err_t  (*open)   (Device_t dev, Uint16_t oflag);
    Err_t  (*close)  (Device_t dev);
    Size_t (*read)   (Device_t dev, Off_t pos, void *buffer, Size_t size);
    Size_t (*write)  (Device_t dev, Off_t pos, const void *buffer, Size_t size);
    Err_t  (*control)(Device_t dev, int cmd, void *args);
};
#endif /* USING_DEVICE_OPS */

/**
 * WaitQueue structure
 */
struct Wqueue_st
{
    Uint32_t flag;
    DevList_t waiting_list;
};
typedef struct Wqueue_st Wqueue_t;

/**
 * Device structure
 */
struct Device_st
{
     char                      name[DEV_NAME_MAX];       /**< name of device */
     DevList_t                list;                     /**< list node of device */


    enum DeviceClassType_t type;                     /**< device type */
    Uint16_t               flag;                     /**< device flag */
    Uint16_t               open_flag;                /**< device open flag */

    Uint8_t                ref_count;                /**< reference count */
    Uint8_t                device_id;                /**< 0 - 255 */

    /* device call back */
    Err_t (*rx_indicate)(Device_t dev, Size_t size);
    Err_t (*tx_complete)(Device_t dev, void *buffer);

#ifdef USING_DEVICE_OPS
    const struct DeviceOps_st *ops;
#else
    /* common device interface */
    Err_t  (*init)   (Device_t dev);
    Err_t  (*open)   (Device_t dev, Uint16_t oflag);
    Err_t  (*close)  (Device_t dev);
    Size_t (*read)   (Device_t dev, Off_t pos, void *buffer, Size_t size);
    Size_t (*write)  (Device_t dev, Off_t pos, const void *buffer, Size_t size);
    Err_t  (*control)(Device_t dev, int cmd, void *args);
#endif /* USING_DEVICE_OPS */

#ifdef USING_POSIX_DEVIO
    const struct dfs_file_ops *fops;
    struct Wqueue_st wait_queue;
#endif /* USING_POSIX_DEVIO */

    void                     *user_data;                /**< device private data */
};

/**
 * block device geometry structure
 */
struct DeviceBlkGeometry_st
{
    Uint32_t sector_count;                           /**< count of sectors */
    Uint32_t bytes_per_sector;                       /**< number of bytes per sector */
    Uint32_t block_size;                             /**< number of bytes to erase one block */
};

/**
 * sector arrange struct on block device
 */
struct DeviceBlkSectors_st
{
    Uint32_t sector_begin;                           /**< begin sector */
    Uint32_t sector_end;                             /**< end sector   */
};

/**
 * cursor control command
 */
#define DEVICE_CTRL_CURSOR_SET_POSITION  0x10
#define DEVICE_CTRL_CURSOR_SET_TYPE      0x11

/**
 * graphic device control command
 */
#define RTGRAPHIC_CTRL_RECT_UPDATE      (DEVICE_CTRL_BASE(Graphic) + 0)
#define RTGRAPHIC_CTRL_POWERON          (DEVICE_CTRL_BASE(Graphic) + 1)
#define RTGRAPHIC_CTRL_POWEROFF         (DEVICE_CTRL_BASE(Graphic) + 2)
#define RTGRAPHIC_CTRL_GET_INFO         (DEVICE_CTRL_BASE(Graphic) + 3)
#define RTGRAPHIC_CTRL_SET_MODE         (DEVICE_CTRL_BASE(Graphic) + 4)
#define RTGRAPHIC_CTRL_GET_EXT          (DEVICE_CTRL_BASE(Graphic) + 5)
#define RTGRAPHIC_CTRL_SET_BRIGHTNESS   (DEVICE_CTRL_BASE(Graphic) + 6)
#define RTGRAPHIC_CTRL_GET_BRIGHTNESS   (DEVICE_CTRL_BASE(Graphic) + 7)
#define RTGRAPHIC_CTRL_GET_MODE         (DEVICE_CTRL_BASE(Graphic) + 8)
#define RTGRAPHIC_CTRL_GET_STATUS       (DEVICE_CTRL_BASE(Graphic) + 9)
#define RTGRAPHIC_CTRL_PAN_DISPLAY      (DEVICE_CTRL_BASE(Graphic) + 10)
#define RTGRAPHIC_CTRL_WAIT_VSYNC       (DEVICE_CTRL_BASE(Graphic) + 11)

/* graphic device */
enum
{
    RTGRAPHIC_PIXEL_FORMAT_MONO = 0,
    RTGRAPHIC_PIXEL_FORMAT_GRAY4,
    RTGRAPHIC_PIXEL_FORMAT_GRAY16,
    RTGRAPHIC_PIXEL_FORMAT_RGB332,
    RTGRAPHIC_PIXEL_FORMAT_RGB444,
    RTGRAPHIC_PIXEL_FORMAT_RGB565,
    RTGRAPHIC_PIXEL_FORMAT_RGB565P,
    RTGRAPHIC_PIXEL_FORMAT_BGR565 = RTGRAPHIC_PIXEL_FORMAT_RGB565P,
    RTGRAPHIC_PIXEL_FORMAT_RGB666,
    RTGRAPHIC_PIXEL_FORMAT_RGB888,
    RTGRAPHIC_PIXEL_FORMAT_BGR888,
    RTGRAPHIC_PIXEL_FORMAT_ARGB888,
    RTGRAPHIC_PIXEL_FORMAT_ABGR888,
    RTGRAPHIC_PIXEL_FORMAT_RESERVED,
};

/**
 * build a pixel position according to (x, y) Coordinates_st.
 */
#define RTGRAPHIC_PIXEL_POSITION(x, y)  ((x << 16) | y)

/**
 * graphic device information structure
 */
struct DeviceGraphicInfo_st
{
    Uint8_t  pixel_format;                           /**< graphic format */
    Uint8_t  bits_per_pixel;                         /**< bits per pixel */
    Uint16_t pitch;                                  /**< bytes per line */

    Uint16_t width;                                  /**< width of graphic device */
    Uint16_t height;                                 /**< height of graphic device */

    Uint8_t *framebuffer;                            /**< frame buffer */
    Uint32_t smem_len;                               /**< allocated frame buffer size */
};

/**
 * rectangle information structure
 */
struct DeviceRectInfo_st
{
    Uint16_t x;                                      /**< x coordinate */
    Uint16_t y;                                      /**< y coordinate */
    Uint16_t width;                                  /**< width */
    Uint16_t height;                                 /**< height */
};

/**
 * graphic operations
 */
struct DeviceGraphicOps_st
{
    void (*set_pixel) (const char *pixel, int x, int y);
    void (*get_pixel) (char *pixel, int x, int y);

    void (*draw_hline)(const char *pixel, int x1, int x2, int y);
    void (*draw_vline)(const char *pixel, int x, int y1, int y2);

    void (*blit_line) (const char *pixel, int x, int y, Size_t size);
};
#define graphix_ops(device)          ((struct DeviceGraphicOps_st *)(device->user_data))

/**@}*/
#endif /* USING_DEVICE */

#ifdef __cplusplus
}
#endif

#endif /* __DEF_H__ */

