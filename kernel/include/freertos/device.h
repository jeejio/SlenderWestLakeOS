/*
 * Copyright (c) 2022, jeejio Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2022-11-29     zhengqian    the first version
 */

#ifndef __DEVICE_H__
#define __DEVICE_H__

#include "swldef.h"
#include "io_dev_mgt.h"


#ifdef __cplusplus
extern "C" {
#endif



#ifdef USING_DEVICE
/**
 * @addtogroup Device
 */

/**@{*/

/*
 * device (I/O) system interface
 */
Device_t pHalDeviceFind(const char *name);

Err_t lHalDeviceRegister(Device_t dev,
                            const char *name,
                            Uint16_t flags);
Err_t lHalDeviceUnregister(Device_t dev);

#ifdef USING_HEAP
Device_t pHalDeviceCreate(int type, int attach_size);
void vHalDeviceDestroy(Device_t device);
#endif

Err_t
lHalDeviceSetRxIndicate(Device_t dev,
                          Err_t (*rx_ind)(Device_t dev, Size_t size));
Err_t
lHalDeviceSetTxComplete(Device_t dev,
                          Err_t (*tx_done)(Device_t dev, void *buffer));

Err_t  lHalDeviceInit (Device_t dev);
Err_t  lHalDeviceOpen (Device_t dev, Uint16_t oflag);
Err_t  lHalDeviceClose(Device_t dev);
Size_t ulHalDeviceRead (Device_t dev,
                          Off_t    pos,
                          void       *buffer,
                          Size_t   size);
Size_t ulHalDeviceWrite(Device_t dev,
                          Off_t    pos,
                          const void *buffer,
                          Size_t   size);
Err_t  lHalDeviceControl(Device_t dev, int cmd, void *arg);

/**@}*/
#endif


#ifdef __cplusplus
}
#endif

#endif
