

char *const sensor_name_str[] =
{
    "none",
    "acce_",       /* Accelerometer     */
    "gyro_",       /* Gyroscope         */
    "mag_",        /* Magnetometer      */
    "temp_",       /* Temperature       */
    "humi_",       /* Relative Humidity */
    "baro_",       /* Barometer         */
    "li_",         /* Ambient light     */
    "pr_",         /* Proximity         */
    "hr_",         /* Heart Rate        */
    "tvoc_",       /* TVOC Level        */
    "noi_",        /* Noise Loudness    */
    "step_",       /* Step sensor       */
    "forc_",       /* Force sensor      */
    "dust_",       /* Dust sensor       */
    "eco2_",       /* eCO2 sensor       */
    "gnss_",       /* GPS/GNSS sensor   */
    "tof_",        /* TOF sensor        */
    "spo2_",       /* SpO2 sensor       */
    "iaq_",        /* IAQ sensor        */
    "etoh_",       /* EtOH sensor       */
    "bp_",         /* Blood Pressure    */
    "pir_",        /* PIR               */
    "motor_",      /* Motor Sensor      */
    "sg_",         /* Steering Gear     */
    "re_",         /* Rotary Encoder    */
    "wapump_",     /* water pump        */
    "six-axis_",   /* Six-axis sensor   */
    "rfid_",       /* Radio Frequency Identification */
    "hyetometer_", /* Hyetometer sensor */
    "button_",     /* Button       */
    "led_",        /* led array         */
    "gets_",       /* getsture          */
    "scan_",       /* scanner           */
    "joystick_",   /* joystick          */
    "ioexpand_",   /* io expand           */
    "smoke_",      /* Smoke alarm         */
    "co_",         /* carbon monoxide           */
    "lcd_",        /* Liquid Crystal Display     */
};







