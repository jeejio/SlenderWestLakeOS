/*
 * Copyright (c) 2023, Jeejio Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2023-06-01     None         the first version.
 */

#ifndef __HAL_SYSTEM_H__
#define __HAL_SYSTEM_H__

/*
** restart system
*/
int vHalSystemRestart(void);

#endif