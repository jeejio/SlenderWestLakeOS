/*
 * FreeRTOS BLE V2.2.0
 * Copyright (C) 2020 Amazon.com, Inc. or its affiliates.  All Rights Reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * http://aws.amazon.com/freertos
 * http://www.FreeRTOS.org
 */

/**
 * @file hal_ble.h
 * @brief BLE GAP and GATT API.
 */

#ifndef MAOPAO_BLE_H
#define MAOPAO_BLE_H

#include <stddef.h>
#include "hal_ble_manager_adapter.h"
#include "hal_ble_manager.h"
#include "hal_ble_gatt_server.h"
#include "hal_ble_gatt_types.h"
#include "hal_ble_os_ble_config.h"
#include "hal_ble_linear_containers.h"

/**
 * @ingroup ble_datatypes_structs
 * @brief Structure containing all advertisement parameters.
 *
 * @note: The size of the advertisement message is limited, it is up to the application to make sure everything fits in the advertisement message.
 * For example, with BT 5.0 spec, Advertising 2 UUID of 128 bits, will consume all the advertisement message space (32 bytes).
 * @warning: Blocking API should not be called from a callback.
 */
typedef struct
{
    uint32_t appearance;      /**< Appearance. */
    uint32_t minInterval;     /**< Minimum connection interval. Set this to 0, to use BLE stack specific default values. */
    uint32_t maxInterval;     /**< Maximum connection interval. Set this to 0, to use BLE stack specific default values. */
    char * pManufacturerData; /**< Manufacturer data */
    char * pServiceData;      /**< Service data */
    BTUuid_t * pUUID1;        /**< First UUID to advertise. */
    BTUuid_t * pUUID2;        /**< Second UUID to advertise. */
    uint16_t manufacturerLen; /**< Length of manufacturer data. */
    uint16_t serviceDataLen;  /**< Service data length */
    bool includeTxPower;      /**< Include Tx Power in advertisement message. */
    BTGattAdvName_t name;     /**< Specify wether to include short, complete or no name in advertisement message. */
    bool setScanRsp;          /**< Set to true if the user wishes to set up a scan response instead of an advertisement message. */
} BleAdvertisementParams_t;

/**
 * @ingroup ble_datatypes_structs
 * @brief Connection parameters.
 */
typedef struct
{
    uint32_t minInterval; /**< Minimum connection interval. */
    uint32_t maxInterval; /**< Maximum connection interval. */
    uint32_t latency;     /**< Slave latency. */
    uint32_t timeout;     /**< Connection timeout. */
} BleConnectionParam_t;


/**
 * @ingroup ble_datatypes_structs
 * @brief Contains the connection info. Return when requested by MaopaoBleGetConnectionInfoList.
 */
typedef struct
{
    BleLink_t connectionList;                 /**< Link to the list of connections. */
    BleConnectionParam_t connectionParams; /**< Connection parameters. */
    BTBdaddr_t remoteBdAddr;                  /**< Remote device address. */
    BTSecurityLevel_t securityLevel;          /**< Security level of the connection. */
    BleLink_t clientCharDescrListHead;        /**< Head of CCD list. */
    uint16_t connId;                          /**< Connection Id. */
    bool isBonded;                            /**< True if device is bonded. */
} BleConnectionInfoListElement_t;

/**
 * @ingroup ble_datatypes_enums
 * @brief Attribute event type.
 */
typedef enum
{
    eBLERead,                      /**< Read event. */
    eBLEWrite,                     /**< Write event. */
    eBLEWriteNoResponse,           /**< Write event, no response required. */
    eBLEExecWrite,                 /**< Execute Write event. */
    eBLEResponseConfirmation,      /**< Confirmation from remote device. */
    eBLEIndicationConfirmReceived, /**< Received confirm to indication from remote device. */
} BleAttributeEventType_t;

/**
 * @ingroup ble_datatypes_structs
 * @brief Attribute event type.
 */
typedef struct BleAttributeEvent BleAttributeEvent_t;


/**
 * @ingroup ble_datatypes_functionpointers
 * @brief Callback called when a request on a attribute is made by the remote device.
 *
 * @param[in] pAttribute Pointer to the attribute being accessed.
 * @param[in] pEventParam Pointer to the event data.
 */
typedef void (* BleAttributeEventCallback_t)( BleAttributeEvent_t * pEventParam );

/************************************************************/

/**
 * @ingroup ble_datatypes_structs
 * @brief Parameters for read event.
 */
typedef struct
{
    uint16_t connId;            /**< Connection ID. */
    uint32_t transId;           /**< Transaction ID. */
    BTBdaddr_t * pRemoteBdAddr; /**< Remote device address. */
    uint16_t attrHandle;        /**< Param handle. */
    uint16_t offset;            /**< Read offset. */
} BleReadEventParams_t;

/**
 * @ingroup ble_datatypes_structs
 * @brief Parameters for write event.
 */
typedef struct
{
    uint32_t transId;           /**< Transaction ID. */
    BTBdaddr_t * pRemoteBdAddr; /**< Remote device address. */
    uint8_t * pValue;           /**< Data to write. */
    uint32_t length;            /**< Data length. */
    uint16_t connId;            /**< Connection ID. */
    uint16_t attrHandle;        /**< Param handle. */
    uint16_t offset;            /**< Write offset. */
    bool needRsp;               /**< Need to respond. */
    bool isPrep;                /**< Set to true if it is a prepare write. */
} BleWriteEventParams_t;

/**
 * @ingroup ble_datatypes_structs
 * @brief Parameters for write executed event.
 */
typedef struct
{
    uint32_t transId;           /**< Transaction ID. */
    BTBdaddr_t * pRemoteBdAddr; /**< Remote device address. */
    uint16_t connId;            /**< Connection ID. */
    bool execWrite;             /**< Execute (true) or Cancel(false) the Write transaction. */
} BleExecWriteEventParams_t;

/**
 * @ingroup ble_datatypes_structs
 * @brief Parameters for confirmation response event.
 */
typedef struct
{
    BTStatus_t status; /**< Reported status. */
    uint16_t handle;   /**< Param handle. */
} BleRespConfirmEventParams_t;

/**
 * @ingroup ble_datatypes_structs
 * @brief Parameters for indication sent event.
 */
typedef struct
{
    BTAttribute_t * pAttribute; /**< Pointer to attribute being accessed. */
    uint16_t connId;            /**< Connection ID. */
    BTStatus_t status;          /**< Reported status. */
} BleIndicationSentEventParams_t;

/**
 * @ingroup ble_datatypes_structs
 * @brief BLE Attribute event.
 */
struct BleAttributeEvent
{
    union
    {
        BleReadEventParams_t * pParamRead;                     /**< Read event. */
        BleWriteEventParams_t * pParamWrite;                   /**< Write event. */
        BleExecWriteEventParams_t * pParamExecWrite;           /**< Execute write event. */
        BleRespConfirmEventParams_t * pParamRespConfirm;       /**< Response confirm event. */
        BleIndicationSentEventParams_t * pParamIndicationSent; /**< Indication event. */
    };
    BleAttributeEventType_t xEventType;                        /**< Event type (read/write/...). */
};

/**
 * @ingroup ble_datatypes_structs
 * @brief Basic info contained in an attribute.
 * This is common to all attributes.
 */
typedef struct
{
    size_t size;     /**< Size of data field. */
    uint8_t * pData; /**< Attribute data field. */
    BTUuid_t uuid;   /**< Attribute UUID*/
    size_t handle;   /**< Attribute handle. Note, this field is Auto filled(field during BLE_AddService call). */
} BleAttributeData_t;

/**
 * @ingroup ble_datatypes_structs
 * @brief BLE Event response.
 */
typedef struct
{
    BleAttributeData_t * pAttrData; /**< Pointer to common attribute date. */
    size_t attrDataOffset;             /**< Read/Write index. */
    BTStatus_t eventStatus;            /**< Event status. */
    BTRspErrorStatus_t rspErrorStatus; /**< Response error status. */
} BleEventResponse_t;

/**
 * @ingroup ble_datatypes_functionpointers
 * @brief Callback invoked when the MTU for a given connection changes.
 *
 * @param[in] connId Connection ID
 * @param[in] mtu Established MTU size.
 */
typedef void (* lHalBle_MtuChangedCallback_t)( uint16_t connId,
                                              uint16_t mtu );

/**
 * @ingroup ble_datatypes_functionpointers
 * @brief Callback indicating that a remote device has connected or been disconnected.
 * @param[in] status Returns eBTStatusSuccess if operation succeeded.
 * @param[in] connId Connection ID.
 * @param[in] connected Flag set to true if device is connected.
 * @param[in] pRemoteBdAddr Remote device address.
 */
typedef void (* lHalBle_ConnectionCallback_t)( BTStatus_t status,
                                              uint16_t connId,
                                              bool connected,
                                              BTBdaddr_t * pRemoteBdAddr );

/**
 * @ingroup ble_datatypes_functionpointers
 * @brief  Callback indicating the status of start advertisement operation. Invoked on BLE_StartAdv.
 *
 * @param[in] status Returns eBTStatusSuccess if operation succeeded.
 */
typedef void (* lHalBle_StartAdvCallback_t)( BTStatus_t status );

/**
 * @ingroup ble_datatypes_functionpointers
 * @brief  Callback indicating the status of stop advertisement operation. Invoked on BLE_StopAdv.
 *
 * @param[in] status Returns eBTStatusSuccess if operation succeeded.
 */
typedef void (* lHalBle_StopAdvCallback_t)( BTStatus_t status );

/**
 * @ingroup ble_datatypes_functionpointers
 * @brief  Callback invoked on BLE_ConnParameterUpdateRequest from remote device.
 *
 * @param[in] status Returns eBTStatusSuccess if operation succeeded.
 * @param[in] pRemoteBdAddr Address of the Remote device
 * @param[in] pConnectionParam Connection parameters.
 * @param[in] connInterval Established connection interval.
 */
typedef void ( * lHalBle_ConnParameterUpdateRequestCallback_t )( BTStatus_t status,
                                                                const BTBdaddr_t * pRemoteBdAddr,
                                                                BleConnectionParam_t * pConnectionParam,
                                                                uint32_t connInterval );

/**
 * @ingroup ble_datatypes_functionpointers
 * @brief Callback invoked when pairing state is changed.
 *
 * @param[in] status Returns eBTStatusSuccess if operation succeeded.
 * @param[in] pRemoteBdAddr Address of the remote device.
 * @param[in] bondState Bond state value.
 * @param[in] securityLevel Security level (mode 1, level 1, 2 ,3 ,4).
 * @param[in] reason Reason for failing to authenticate.
 */
typedef void (* lHalBle_PairingStateChanged_t)( BTStatus_t status,
                                               BTBdaddr_t * pRemoteBdAddr,
                                               BTBondState_t bondstate,
                                               BTSecurityLevel_t securityLevel,
                                               BTAuthFailureReason_t reason );

/**
 * @ingroup ble_datatypes_functionpointers
 * @brief Callback invoked on pairing request from remote device.
 *
 * Numeric comparison event.
 *
 * @param[in] pRemoteBdAddr Address of the remote device.
 */
typedef void ( * lHalBle_NumericComparisonCallback_t )( BTBdaddr_t * pRemoteBdAddr,
                                                       uint32_t passKey );

/**
 * @ingroup ble_datatypes_enums
 * @brief enum listing all the BLE events (not directly triggered by a function call)
 */
typedef enum
{
    eBLEMtuChanged = 0,                     /**< eBLEMtuChanged Event triggering BLEMtuChangedCallback_t. */
    eBLEConnection,                         /**< eBLEConnection Event  triggering BLEConnectionCallback_t. */
    eBLEPairingStateChanged,                /**< eBLEPairingStateChanged Event triggering BLEPairingStateChanged_t. */
    eBLEConnParameterUpdateRequestCallback, /**< eBLEConnParameterUpdateRequestCallback Event triggering BLEConnParameterUpdateRequestCallback_t.  */
    eBLENumericComparisonCallback,          /**< Numeric comparison event. Need callback from the user. */
    eNbEvents,                              /**< eNbEvents  Number of events  */
} BleEvents_t;

/**
 * @ingroup ble_datatypes_structs
 * @brief BLE events not directly triggered by a function call.
 * Most of them are triggered by a remote device message.
 */
typedef union
{
    lHalBle_MtuChangedCallback_t pMtuChangedCb;                                 /**<  MTU changed event */
    lHalBle_ConnectionCallback_t pConnectionCb;                                 /**<  Connection event. */
    lHalBle_PairingStateChanged_t pGAPPairingStateChangedCb;                    /**<  Pairing state changed event. */
    lHalBle_ConnParameterUpdateRequestCallback_t pConnParameterUpdateRequestCb; /**<  Connection parameter update event. */
    lHalBle_NumericComparisonCallback_t pNumericComparisonCb;                   /**<  Pairing request. */
    void * pvPtr;                                                              /**< Used for generic operations. */
} BleEventsCallbacks_t;


#if ( MAOPAO_BLE_ADD_CUSTOM_SERVICES == 1 )

/**
 * @cond DOXYGEN_IGNORE
 * @brief The implementation of this function needs to be given when MAOPAO_BLE_ADD_CUSTOM_SERVICES is set to
 * 1 and when the user needs to add their own services.
 */
typedef void (* vHalBle_AddCustomServicesCb)(bool value);

/**
 * @brief Callback structure for RPC data .
 */
typedef struct
{
    vHalBle_AddCustomServicesCb _lHalBle_AddCustomServicesCb;
} GattsDemoDataCb;

void vHalBle_AddCustomServicesRegister( vHalBle_AddCustomServicesCb cb);

/** @endcond */
#endif

#if ( MAOPAO_BLE_SET_CUSTOM_ADVERTISEMENT_MSG == 1 )

/**
 * @cond DOXYGEN_IGNORE
 * @brief The implementation of this function needs to be given when MAOPAO_BLE_SET_CUSTOM_ADVERTISEMENT_MSG is
 * set to 1 and when the user needs to set their own advertisement/scan response message.
 *
 * @param[out] pAdvParams: Advertisment structure. Needs to be filled by the user.
 *  @param[out] pScanParams: Scan response structure. Needs to be filled by the user.
 *
 */
    void lHalBle_SetCustomAdvCb( BleAdvertisementParams_t * pAdvParams,
                                BleAdvertisementParams_t * pScanParams );
/** @endcond */
#endif

/**
 * @brief Starting point. Initialize the BLE stack and its services. It is up to the application to decide when BLE should be started.
 *
 * @return Returns eBTStatusSuccess on successful call.
 */
BTStatus_t lHalBle_Init( void );

/**
 * @brief Turns on the BLE radio.
 *
 * @return Returns eBTStatusSuccess on successful call.
 */
BTStatus_t lHalBle_On( void );

/**
 * @brief Turns off the BLE radio.
 *
 * @return Returns eBTStatusSuccess on successful call.
 */
BTStatus_t lHalBle_Off( void );

/**
 * @brief Start advertisements to listen for incoming connections.
 * Triggers lHalBle_StartAdvCallback_t
 * @return Returns eBTStatusSuccess on successful call.
 */
BTStatus_t lHalBle_StartAdv( lHalBle_StartAdvCallback_t pStartAdvCb );

/**
 * @brief Sets an application defined callback invoked when an advertisement duration has ended or
 * advertisement stops due to an error.
 *
 * @param pStopAdvCb The application defined callback to be invoked when advertisement is stopped.
 * @return Returns eBTStatusSuccess on successful call.
 */
BTStatus_t lHalBle_SetStopAdvCallback( lHalBle_StopAdvCallback_t pStopAdvCb );

/**
 * @brief Stop advertisements to listen for incoming connections.
 * Triggers lHalBle_StopAdvCallback_t
 * @return Returns eBTStatusSuccess on successful call.
 */
BTStatus_t lHalBle_StopAdv( lHalBle_StopAdvCallback_t pStopAdvCb );

/**
 * @brief Request an update of the connection parameters.
 *
 * @param[in] pRemoteBdAddr Address of the remote device.
 * @param[in] pConnectionParam New connection parameters.
 * @return Returns eBTStatusSuccess on successful call.
 */
BTStatus_t lHalBle_ConnParameterUpdateRequest( const BTBdaddr_t * pRemoteBdAddr,
                                              BleConnectionParam_t * pConnectionParam );

/**
 * @brief Used to route event to whatever service requests it.
 *
 * For example, one service could require knowledge of connection status,
 * the it would subscribe to connectionCallback event. That API is giving the
 * flexibility of having more than one service listening to the same event.
 * Note: API should not be invoked from within the event callback.
 *
 * @param[in] event The event.
 * @param[in] bleEventsCallbacks Callback returning status of the operation.
 * @return Returns eBTStatusSuccess on successful call.
 */
BTStatus_t lHalBle_RegisterEventCb( BleEvents_t event,
                                   BleEventsCallbacks_t bleEventsCallbacks );

/**
 * @brief Remove a subscription to an event.
 * Note: API should not be invoked from within the event callback.
 *
 * @param[in] event The event.
 * @param[in] bleEventsCallbacks The subscription to remove.
 * @return Returns eBTStatusSuccess on successful call.
 */
BTStatus_t lHalBle_UnRegisterEventCb( BleEvents_t event,
                                     BleEventsCallbacks_t bleEventsCallbacks );

/**
 * @brief Remove a bonded device. Will trigger pBondedCb.
 *
 * @param[in] pRemoteBdAddr Address of the bonded device.
 * @return Returns eBTStatusSuccess on successful call
 */
BTStatus_t lHalBle_RemoveBond( const BTBdaddr_t * pRemoteBdAddr );

/**
 * @brief Create a new service.
 *
 * Allocate memory for a new service.
 *
 * @param[in] pService :        Service to create
 * @param[in] pEventsCallbacks:  Call backs for events on each attributes in the service.
 * @return Returns eBTStatusSuccess on successful call.
 */
BTStatus_t lHalBle_CreateService( BTService_t * pService,
                                 BleAttributeEventCallback_t pEventsCallbacks[] );

/**
 * @brief Delete a local service.
 *
 * @param[in] pService Pointer to the service to delete.
 * @return Returns eBTStatusSuccess on successful call.
 */
BTStatus_t lHalBle_DeleteService( BTService_t * pService );

/**
 * @brief Send value indication to a remote device.
 *
 * @param[in] pResp Pointer to the indication/Notification data.
 * @param[in] connId Connection ID.
 * @param[in] confirm Set to true for an indication, false is a notification.
 * @return Returns eBTStatusSuccess on successful call.
 */
BTStatus_t lHalBle_SendIndication( BleEventResponse_t * pResp,
                                  uint16_t connId,
                                  bool confirm );

/**
 * @brief Send response to the remote device.
 *
 * @param[in] pResp Pointer to the response data.
 * @param[in] connId Connection ID.
 * @param[in] transId Transaction ID.
 * @return Returns eBTStatusSuccess on successful call.
 */
BTStatus_t lHalBle_SendResponse( BleEventResponse_t * pResp,
                                uint16_t connId,
                                uint32_t transId );

/**
 * @brief Send response to the remote device.
 *
 * This function return a pointer to the connection info list.
 * The elements of this list have type:BleConnectionInfoListElement_t.
 * Looked into iot_doubly_linked_list.h for information on how to use the linked list.
 *
 * @param[out] pConnectionInfoList Returns the head of the connection list.
 * @return Returns eBTStatusSuccess on successful call.
 */
BTStatus_t lHalBle_GetConnectionInfoList( BleLink_t ** pConnectionInfoList );

/**
 * @brief Get connection info for a specific connection ID.
 *
 * This function return a pointer to the connection info that matches the connection ID.
 *
 * @param[in] connId Connection ID.
 * @param[out] pConnectionInfo Returns a pointer to the connection info that matches the connection ID.
 * @return Returns eBTStatusSuccess on successful call.
 */
BTStatus_t lHalBle_GetConnectionInfo( uint16_t connId,
                                     BleConnectionInfoListElement_t ** pConnectionInfo );

/**
 * @brief Confirm key for numeric comparison.
 *
 * @param[in] pBdAddr address.
 * @param[in] keyAccepted Set to true when numeric comparison is confirmed by the user.
 * @return Returns eBTStatusSuccess on successful call.
 */
BTStatus_t lHalBle_ConfirmNumericComparisonKeys( BTBdaddr_t * pBdAddr,
                                                bool keyAccepted );


/**
 * @brief Set device name for BLE.
 *
 * API can be used to set the device name before turning on BLE by calling lHalBle_On(),
 * or when BLE is running. When BLE is advertising mode, setting device name should take into effect
 * immediately for the current advertisement. The device name needs to be set only once and
 * is available across BLE turn off-turn on cycles. API should be called only after lHalBle_Init().
 *
 * @param[in] pName Pointer to the device name string.
 * @param[in] length Length of the device name string without null terminator.
 * @return Returns eBTStatusSuccess on successful call or error code otherwise.
 */
BTStatus_t lHalBle_SetDeviceName( const char * pName,
                                 size_t length );


/**
 * @brief Get device MAC for BLE.
 *
 * API can be used to set the device MAC before turning on BLE by calling lHalBle_On(),
 * or when BLE is running. When BLE is advertising mode, get device MAC should take into effect
 * immediately for the current advertisement. The device MAC needs to be Get only once and
 * is available across BLE turn off-turn on cycles. API should be called only after lHalBle_Init().
 *
 * @return Returns BTBdaddr_t   
 *    if all of addrs =0x0 ,is error 
 */

BTBdaddr_t lHalBle_GetDeviceMac(void);

/**
 * @brief  The device actively initiates disconnection to the phone
 *
 * @return Returns eBTStatusSuccess on successful call or error code otherwise.
 */
BTStatus_t lHalBle_Disconnect( void );

#endif /* MAOPAO_BLE_H*/
