
#ifndef __MAOPAO_BLE_ADV_H__
#define __MAOPAO_BLE_ADV_H__

typedef int32_t (*set_BleAdvRespScanData_t)(uint8_t *dest, void *data, uint8_t len);
//要存放到ble广播数据结构中，严格按1字节对齐
#pragma pack(1)
typedef struct BleAdvDataFmt
{
    uint16_t CID;
    uint8_t VID;
    uint8_t FMSK;
    uint32_t PID;
    uint8_t MAC[6];
}BleAdvDataFmt_t;

typedef struct BleScanRespDataFmt
{
    uint8_t name[10];
}BleScanRespDataFmt_t;
#pragma pack()

extern uint8_t Ble_advertData[];
extern uint8_t Ble_scanRespData[];

int32_t lHalBleSetAdvData_CID(uint8_t *dest, void *data, uint8_t len);
int32_t lHalBleSetAdvData_VID(uint8_t *dest, void *data, uint8_t len);
int32_t lHalBleSetAdvData_FMSK(uint8_t *dest, void *data, uint8_t len);
int32_t lHalBleSetAdvData_PID(uint8_t *dest, void *data, uint8_t len);
int32_t lHalBleSetAdvData_MAC(uint8_t *dest, void *data, uint8_t len);
int32_t lHalBleSetScanRespData_name(uint8_t *dest, void *data, uint8_t len);

#endif
