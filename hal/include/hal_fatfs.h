/*
 * Copyright (c) 2023, JEE Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2023-02-28      lk        first version
 */

#ifndef __HAL_FATFS_H__
#define __HAL_FATFS_H__

#include <freertos/FreeRTOS.h>
#include <freertos/swldef.h>



/**
 * This function Save data to a file.
 *
 * @param p input, data
 * @param size input, data size
 * @param name input, file name
 * 
 * @return DEV_EOK on set successfully.
 */
Err_t lHalSaveToFile(void *p, size_t size, char* name);

/**
 * This function Read from a file.
 *
 * @param p input, data
 * @param size input, data size
 * @param name input, file name
 *
 * @return DEV_EOK on set successfully.
 */
Err_t lHalReadFromFile(void *p, size_t size, char* name);

/**
 * This function init fatfs.
 *
 * @return 0 on init successfully.
 */
void vHalFatFSInit(void);

/**
 * This function set int data.
 *
 * @param key input, data index
 * @param value input, int data
 *
 * @return DEV_EOK on set successfully.
 */
Err_t lHalFatFSSetDataInt(char *key, int value);

/**
 * This function set double data.
 *
 * @param key input, data index
 * @param value input, double data
 *
 * @return DEV_EOK on set successfully.
 */
Err_t lHalFatFSSetDataDouble(char *key, double value);

/**
 * This function set string data.
 *
 * @param key input, data index
 * @param value input, string data
 *
 * @return DEV_EOK on set successfully.
 */
Err_t lHalFatFSSetDataString(char *key, char *value);

/**
 * This function get int data.
 *
 * @param key input, data index
 * @param value output, int data
 *
 * @return DEV_EOK on get successfully.
 */
Err_t lHalFatFSGetDataInt(char *key, int *value);

/**
 * This function get double data.
 *
 * @param key input, data index
 * @param value output, double data
 *
 * @return DEV_EOK on get successfully.
 */
Err_t lHalFatFSGetDataDouble(char *key, double *value);

/**
 * This function get string data.
 *
 * @param key input, data index
 * @param value output, string data
 *
 * @return DEV_EOK on get successfully.
 */
Err_t lHalFatFSGetDataString(char *key, char *value);

/**
 * This function delete data.
 *
 * @param key input, data index
 */
Err_t vHalFatFSDeleteData(char *key);

/**
 * This function init fatfs.
 *
 * @param 
 * @param 
 *
 * @return 
 */
Err_t lHalFatFSDataFilesInit(void);

#endif /* __HAL_FATFS_H__ */
