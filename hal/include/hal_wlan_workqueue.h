/*
 * Copyright (c) 2023, jeejio Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2023-02-28       lk         the first version
 */

#ifndef __HAL_WLAN_WORKQUEUE_H__
#define __HAL_WLAN_WORKQUEUE_H__

#include <hal_workqueue.h>

#ifdef __cplusplus
extern "C" {
#endif

#ifndef WLAN_WORKQUEUE_THREAD_NAME
#define WLAN_WORKQUEUE_THREAD_NAME  ("wlan_job")
#endif

#ifndef WLAN_WORKQUEUE_THREAD_SIZE
#define WLAN_WORKQUEUE_THREAD_SIZE  (2048)
#endif

#ifndef WLAN_WORKQUEUE_THREAD_PRIO
#define WLAN_WORKQUEUE_THREAD_PRIO  (20)
#endif

int lHalWlanWorkqueueInit(void);

Err_t lHalWlanWorkqueueDowork(void (*func)(void *parameter), void *parameter);

struct Workqueue_st *pHalWlanGetWorkqueue(void);

#ifdef __cplusplus
}
#endif

#endif /* __HAL_WLAN_WORKQUEUE_H__ */
