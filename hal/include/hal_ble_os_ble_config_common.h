/*
 * FreeRTOS V202203.00
 * Copyright (C) 2020 Amazon.com, Inc. or its affiliates.  All Rights Reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * http://aws.amazon.com/freertos
 * http://www.FreeRTOS.org
 */

/* This file contains default configuration settings for the demos on FreeRTOS. */

#ifndef MAOPAOPAO_CONFIG_COMMON_H_
#define MAOPAOPAO_CONFIG_COMMON_H_

/* FreeRTOS include. */
#include "freertos/FreeRTOS.h"

/* Use platform types on FreeRTOS. */
#include "hal_ble_os_platform_types_freertos.h"
#include "ble_hal_log.h"


/* SDK version. */
#define MAOPAOPAO_SDK_VERSION    "4.0.0"

/* This config file is for the demos; disable any test code. */
#define MAOPAOPAO_BUILD_TESTS    ( 0 )

/* Logging puts function. */
#define MaopaoLogging_Puts( str )    configPRINTF( ( "%s\r\n", str ) )


/* Assert functions. */
#define BleContainers_Assert( expression )     configASSERT( expression )
#define Ble_Assert( expression )        configASSERT( expression )

/* Memory allocation configuration. Note that these functions will not be affected
 * by MAOPAOPAO_STATIC_MEMORY_ONLY. */
#define BleThreads_Malloc    pvPortMalloc
#define BleThreads_Free      vPortFree
#define Ble_Malloc        pvPortMalloc
#define Ble_Free          vPortFree
    
//#define  BLE_MALLOC_PRINT_TEST
#ifdef BLE_MALLOC_PRINT_TEST
/* 将下面的接口 与Ble_Malloc/Ble_Free成对出现，则可以计算每次分配的总量*/
void Ble_Malloc_Print(void *pdu,int len);
void Ble_Free_Print(void *pdu);
#endif

/* Default platform thread stack size and priority. */
#ifndef MAOPAOPAO_THREAD_DEFAULT_STACK_SIZE
    #define MAOPAOPAO_THREAD_DEFAULT_STACK_SIZE    2048
#endif
#ifndef MAOPAOPAO_THREAD_DEFAULT_PRIORITY
    #define MAOPAOPAO_THREAD_DEFAULT_PRIORITY      tskIDLE_PRIORITY
#endif

/* Platform network configuration. */
#ifndef MAOPAOPAO_NETWORK_RECEIVE_TASK_PRIORITY
    #define MAOPAOPAO_NETWORK_RECEIVE_TASK_PRIORITY      ( tskIDLE_PRIORITY + 1 )
#endif
#ifndef MAOPAOPAO_NETWORK_RECEIVE_TASK_STACK_SIZE
    #define MAOPAOPAO_NETWORK_RECEIVE_TASK_STACK_SIZE MAOPAOMAOPAO_THREAD_DEFAULT_STACK_SIZE
#endif

/* Platform and SDK name for AWS IoT MQTT metrics. Only used when
 * AWS_MAOPAOPAO_MQTT_ENABLE_METRICS is 1. */
#define MAOPAOPAO_SDK_NAME             "AmazonFreeRTOS"
#define MAOPAOPAO_PLATFORM_NAME    "Unknown"

/* Cloud endpoint to which the device connects to. */
#define MAOPAOPAO_CLOUD_ENDPOINT        ""

/* Certificate for the device. */
#define MAOPAOPAO_DEVICE_CERTIFICATE    NULL

/**
 * @brief Unique identifier used to recognize a device by the cloud.
 * This could be SHA-256 of the device certificate.
 */
//extern const char * getDeviceIdentifier( void );
#define MAOPAOPAO_DEVICE_IDENTIFIER    "Unknown"

/* Define the data type of metrics connection id as same as Socket_t in aws_secure_socket.h */
#define MaopaoMetricsConnectionId_t           void *


/* iot_logging_setup.h  xuyuhu:add */
/* Define the abbreviated logging macros. */
#define BleLogError(format, ... )          BLE_LOG_LEVEL_LOCAL(ESP_LOG_ERROR,"BLE_E", format, ##__VA_ARGS__ ) 
#define BleLogWarn(format, ... )           BLE_LOG_LEVEL_LOCAL(ESP_LOG_WARN,"BLE_W", format,##__VA_ARGS__ ) 
#define BleLogInfo(format, ... )           BLE_LOG_LEVEL_LOCAL(ESP_LOG_INFO,"BLE_I", format, ##__VA_ARGS__ ) 
#define BleLogDebug(format, ... )          BLE_LOG_LEVEL_LOCAL(ESP_LOG_DEBUG,"BLE_D", format, ##__VA_ARGS__)

#endif /* ifndef MAOPAOPAO_CONFIG_COMMON_H_ */
