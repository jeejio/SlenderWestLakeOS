/*
 * Copyright (c) 2023, Jeejio Technology Co. Ltd
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2023-03-08     Alpha        First Verison
 *
 */
#ifndef __I2C_H__
#define __I2C_H__

#include <freertos/device.h>
#include <freertos/FreeRTOS.h>
#include <freertos/semphr.h>



#ifdef __cplusplus
extern "C" {
#endif


#define I2C_RD               (1u << 0)
#define I2C_STOP             (1u << 1)
#define I2C_ADDR_10BIT       (1u << 2)  /* this is a ten bit chip address */
#define I2C_WR               (1u << 3)
#define I2C_START            (1u << 4)
#define I2C_IGNORE_NACK      (1u << 5)
#define I2C_NO_READ_ACK      (1u << 6)  /* when I2C reading, we do not ACK */
#define I2C_NO_STOP          (1u << 7)
#define I2C_NO_START         (1u << 8)



struct I2cMsg_st
{
    Uint16_t addr;
    Uint16_t flags;
    Uint16_t len;
    Uint8_t  *buf;
};

struct I2cBusDevice_st;

struct I2cBusDeviceOps_st
{
    Size_t (*master_xfer)(struct I2cBusDevice_st *bus,
                             struct I2cMsg_st msgs[],
                             Uint32_t num);
    Size_t (*slave_xfer)(struct I2cBusDevice_st *bus,
                            struct I2cMsg_st msgs[],
                            Uint32_t num);
    Err_t (*i2c_bus_control)(struct I2cBusDevice_st *bus,
                                int cmd ,
                                void *arg);
};

/*for i2c bus driver*/
struct I2cBusDevice_st
{
    struct Device_st parent;
    const struct I2cBusDeviceOps_st *ops;
    Uint16_t  flags;
    SemaphoreHandle_t lock;
    Uint32_t  timeout;
    Uint32_t  retries;
    void *priv;
};

struct I2cClient_st
{
    struct I2cBusDevice_st       *bus;
    Uint16_t                    client_addr;
};

Err_t lHalI2cBusDeviceRegister(struct I2cBusDevice_st *bus,
                                    const char               *bus_name);
struct I2cBusDevice_st *pHalI2cBusDeviceFind(const char *bus_name);
Size_t ulHalI2cTransfer(struct I2cBusDevice_st *bus,
                          struct I2cMsg_st         msgs[],
                          Uint32_t               num);
Err_t lHalI2cControl(struct I2cBusDevice_st *bus,
                        Uint32_t               cmd,
                        Uint32_t               arg);
Size_t ulHalI2cMasterSend(struct I2cBusDevice_st *bus,
                             Uint16_t               addr,
                             Uint16_t               flags,
                             const Uint8_t         *buf,
                             Uint32_t               count);
Size_t ulHalI2cMasterRecv(struct I2cBusDevice_st *bus,
                             Uint16_t               addr,
                             Uint16_t               flags,
                             Uint8_t               *buf,
                             Uint32_t               count);


static inline Err_t lHalI2cBusLock(struct I2cBusDevice_st *bus, Tick_t timeout)
{
    // return mutex_take(&bus->lock, timeout);

    return xSemaphoreTake(bus->lock, timeout);
}
static inline Err_t lHalI2cBusUnlock(struct I2cBusDevice_st *bus)
{
    // return mutex_release(&bus->lock);

    return xSemaphoreGive(bus->lock);
    
}

int lHalI2cCoreInit(void);

#ifdef __cplusplus
}
#endif

#endif
