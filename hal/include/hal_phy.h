/*
 * Copyright (c) 2023, Jeejio Technology Co. Ltd
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2023-03-08     Alpha        First Verison
 *
 */
#include "hal_phy_mdio.h"

#ifndef __PHY_H__
#define __PHY_H__

#ifdef __cplusplus
extern "C"
{
#endif

/* Defines the PHY link speed. This is align with the speed for MAC. */
#define PHY_SPEED_10M   0U     /* PHY 10M speed. */
#define PHY_SPEED_100M  1U     /* PHY 100M speed. */
#define PHY_SPEED_1000M 2U     /* PHY 1000M speed. */

/* Defines the PHY link duplex. */
#define PHY_HALF_DUPLEX 0U     /* PHY half duplex. */
#define PHY_FULL_DUPLEX 1U     /* PHY full duplex. */

/*! @brief Defines the PHY loopback mode. */
#define PHY_LOCAL_LOOP  0U     /* PHY local loopback. */
#define PHY_REMOTE_LOOP 1U     /* PHY remote loopback. */

#define PHY_STATUS_OK      0U
#define PHY_STATUS_FAIL    1U
#define PHY_STATUS_TIMEOUT 2U

typedef struct PhyMsg_st
{
    Uint32_t reg;
    Uint32_t value;
}PhyMsg_t;

typedef struct PhyDevice_st
{
    struct Device_st parent;
    struct MdioBus_st *bus;
    Uint32_t addr;
    struct PhyOps_st *ops;
}Phy_t;

typedef Int32_t PhyStatus_t;

struct PhyOps_st
{
    PhyStatus_t (*init)(void *object, Uint32_t phy_addr, Uint32_t src_clock_hz);
    PhyStatus_t (*read)(Uint32_t reg, Uint32_t *data);
    PhyStatus_t (*write)(Uint32_t reg, Uint32_t data);
    PhyStatus_t (*loopback)(Uint32_t mode, Uint32_t speed, Bool_t enable);
    PhyStatus_t (*get_link_status)(Bool_t *status);
    PhyStatus_t (*get_link_speed_duplex)(Uint32_t *speed, Uint32_t *duplex);
};

Err_t lHalHwPhyRegister(struct PhyDevice_st *phy, const char *name);

#ifdef __cplusplus
}
#endif

#endif /* __PHY_H__*/
