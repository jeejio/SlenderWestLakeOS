/*
 * Copyright (c) 2022-2023, Jeejio Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2023-02-23     None           the first version.
 */
#ifndef __HWTIMER_H__
#define __HWTIMER_H__

#include "freertos/device.h"
#include "freertos/FreeRTOS.h"
#include "hal_config.h"

#ifdef __cplusplus
extern "C" {
#endif

#ifdef SWL_USING_HWTIMER

/* Timer Control Command */
typedef enum
{
    HWTIMER_CTRL_FREQ_SET      = DEVICE_CTRL_BASE(Timer) + 0x01,           /* set the count frequency, 4609 */
    HWTIMER_CTRL_STOP          = DEVICE_CTRL_BASE(Timer) + 0x02,           /* stop timer */
    HWTIMER_CTRL_INFO_GET      = DEVICE_CTRL_BASE(Timer) + 0x03,           /* get a timer feature information */
    HWTIMER_CTRL_MODE_SET      = DEVICE_CTRL_BASE(Timer) + 0x04,           /* Setting the timing mode(oneshot/period) */
    HWTIMER_CTRL_TOUT_VAL_SET  = DEVICE_CTRL_BASE(Timer) + 0x05,           /* init or modify the timeout value */
    HWTIMER_CTRL_START         = DEVICE_CTRL_BASE(Timer) + 0x06,           /* init or modify the timeout value */
    HWTIMER_CTRL_INFO_DUMP     = DEVICE_CTRL_BASE(Timer) + 0x07,           /* dump timer information to the file stream */
    HWTIMER_CTRL_GET_TIME      = DEVICE_CTRL_BASE(Timer) + 0x08,           /* get time in microseconds since boot */
    HWTIMER_CTRL_CREATE_ANO    = DEVICE_CTRL_BASE(Timer) + 0x09,           /* create another timer */
    HWTIMER_CTRL_START_ANO     = DEVICE_CTRL_BASE(Timer) + 0x0A,           /* start another timer */
    HWTIMER_CTRL_STOP_ANO      = DEVICE_CTRL_BASE(Timer) + 0x0B,           /* stop another timer */
    HWTIMER_CTRL_DELETE_ANO    = DEVICE_CTRL_BASE(Timer) + 0x0C,           /* delete another timer */
    HWTIMER_CTRL_LIGHT_SLEEP   = DEVICE_CTRL_BASE(Timer) + 0x0D,           /* timer enter light sleep for a while */
    HWTIMER_CTRL_DEEP_SLEEP    = DEVICE_CTRL_BASE(Timer) + 0x0E,           /* timer enter deep sleep for a while */
} EHwtimerCtrl_t;

/* Timing Mode */
typedef enum
{
    HWTIMER_MODE_ONESHOT = 0x01,
    HWTIMER_MODE_PERIOD
} EHwtimerMode_t;

/* Time Value */
typedef struct HwtimerVal_st
{
    int32_t sec;      /* second */
    int32_t usec;     /* microsecond */
} HwtimerVal_t;

#define HWTIMER_CNTMODE_NONE    0x0
#define HWTIMER_CNTMODE_UP      0x01 /* increment count mode */
#define HWTIMER_CNTMODE_DW      0x02 /* decreasing count mode */

struct HwtimerDevice_st;

struct HwtimerOps_st
{
    Err_t (*connfigure)(struct HwtimerDevice_st *timer, void *cfg);
    void (*init)(struct HwtimerDevice_st *timer);
    Err_t (*start)(struct HwtimerDevice_st *timer, uint32_t cnt, EHwtimerMode_t mode);
    void (*stop)(struct HwtimerDevice_st *timer);
    void (*delete)(struct HwtimerDevice_st *timer);
    uint32_t (*ulCountGet)(struct HwtimerDevice_st *timer);
    Err_t (*control)(struct HwtimerDevice_st *timer, uint32_t cmd, void *args);
};

/* Timer Feature Information */
struct HwtimerInfo_st
{
    int32_t  maxfreq;    /* the maximum count frequency timer support */
    int32_t  minfreq;    /* the minimum count frequency timer support */
    uint32_t maxcnt;     /* counter maximum value */
    uint8_t  cntmode;    /* count mode (inc/dec) */
};

typedef void (*VHwtimerCb_t)(void* arg);

typedef struct HwtimerDevice_st
{
    struct Device_st parent;
    const struct HwtimerOps_st *ops;
    const struct HwtimerInfo_st *info;

    int32_t    freq;                /* counting frequency set by the user */
    int32_t    overflow;            /* timer overflows event occurs times */
    float      periodSec;           /* the timeout value with second unit of each cycle */
    int32_t    cycles;              /* how many times will generate a timeout event after overflow */
    int32_t    reload;              /* reload cycles(using in period mode) */
    EHwtimerMode_t mode;            /* timing mode(oneshot/period) */
} HwtimerDev_t;

Err_t lHalHwtimerDeviceRegister(HwtimerDev_t *timer, const char *name, void *user_data);
void vHalHwtimerDeviceIsr(HwtimerDev_t *timer);

#endif /* SWL_USING_HWTIMER */

#ifdef __cplusplus
}
#endif

#endif
