/*
 * Copyright (c) 2023, JEE Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2023-02-28      lk        first version
 */

#ifndef __HAL_SETTING_H__
#define __HAL_SETTING_H__

#include <freertos/FreeRTOS.h>
#include <freertos/swldef.h>

#define SETTING_DATA_PATH   "/data/setting"
#define SETTING_DATA_FILES   "files"


int lTalDeleteSettingParameterValue(char *para);
int lHalSetSettingParameterValue(char *para, char *value);
void vHalSettingInit(void);



#endif /* __HAL_FATFS_H__ */
