#ifndef HAL_KEYMANAGER_H
#define HAL_KEYMANAGER_H

#include <stdint.h>
#include <freertos/swldef.h>
#ifdef __cplusplus
extern "C"
{
#endif // __cplusplus

#define DEVICE_ID_OFFSET 0
#define DEVICE_ID_SIZE 20
#define PASSWORD_OFFSET 0x24
#define PASSWORD_SIZE 16
#define SN_OFFSET 0x34
#define SN_SIZE 16
#define PRODUCT_CODE_OFFSET 0x44
#define PRODUCT_CODE_SIZE 4
#define TOKEN_OFFSET 0x14
#define TOKEN_SIZE 16

//Login data
#define USER_TOKEN_KEY              "token"
#define NAV_CHANLLENGE_URI_KEY      "navigateChallenge"
#define NAV_MQTT_URI_KEY            "navigateMqtt"

int32_t lHalGetKeyInfoId(char *data);
int32_t lHalGetKeyInfoPw(char *data);
int32_t lHalGetKeyInfoSn(char *data);
int32_t lHalGetKeyInfoProductCode(char *data);
int32_t lHalGetKeyInfoToken(char *data);

#ifdef __cplusplus
}
#endif // __cplusplus

#endif // HAL_KEYMANAGER_H
