/*
 * Copyright (c) 2023, Jeejio Technology Co. Ltd
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2023-03-08     Alpha        First Verison
 *
 */
#ifndef __I2C_DEV_H__
#define __I2C_DEV_H__

#include <freertos/swldef.h>
#include <hal_i2c.h>


#ifdef __cplusplus
extern "C" {
#endif

#define I2C_DEV_CTRL_10BIT        (DEVICE_CTRL_BASE(I2CBUS) + 0x01)
#define I2C_DEV_CTRL_ADDR         (DEVICE_CTRL_BASE(I2CBUS) + 0x02)
#define I2C_DEV_CTRL_TIMEOUT      (DEVICE_CTRL_BASE(I2CBUS) + 0x03)
#define I2C_DEV_CTRL_RW           (DEVICE_CTRL_BASE(I2CBUS) + 0x04)
#define I2C_DEV_CTRL_CLK          (DEVICE_CTRL_BASE(I2CBUS) + 0x05)

struct I2cPrivData_st
{
    struct I2cMsg_st  *msgs;
    Size_t  number;
};

Err_t lHalI2cBusDeviceDeviceInit(struct I2cBusDevice_st *bus, const char *name);

#ifdef __cplusplus
}
#endif

#endif
