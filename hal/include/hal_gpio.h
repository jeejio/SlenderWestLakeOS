/*
 * Copyright (c) 2022, jeejio Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2022-11-30     zhengqian    the first version
 */

#ifndef PIN_H__
#define PIN_H__

#include "freertos/device.h"
#include "freertos/FreeRTOS.h"

#ifdef __cplusplus
extern "C" {
#endif

/* pin device and operations*/
struct DevicePin_st
{
    struct Device_st parent;
    const struct PinOps_st *ops;
};

#define PIN_LOW                 0x00
#define PIN_HIGH                0x01

#define PIN_MODE_OUTPUT         0x00
#define PIN_MODE_INPUT          0x01
#define PIN_MODE_INPUT_PULLUP   0x02
#define PIN_MODE_INPUT_PULLDOWN 0x03
#define PIN_MODE_OUTPUT_OD      0x04

#define PIN_IRQ_MODE_RISING             0x00
#define PIN_IRQ_MODE_FALLING            0x01
#define PIN_IRQ_MODE_RISING_FALLING     0x02
#define PIN_IRQ_MODE_HIGH_LEVEL         0x03
#define PIN_IRQ_MODE_LOW_LEVEL          0x04

#define PIN_IRQ_DISABLE                 0x00
#define PIN_IRQ_ENABLE                  0x01

#define PIN_IRQ_PIN_NONE                -1

struct DevicePinMode_st
{
    Uint16_t pin;
    Uint16_t mode;
};
struct DevicePinStatus_st
{
    Uint16_t pin;
    Uint16_t status;
};
struct PinIrqHdr_st
{
    Int16_t        pin;
    Uint16_t       mode;
    void (*hdr)(void *args);
    void             *args;
};
struct PinOps_st
{
    void (*pin_mode)(struct Device_st *device, Base_t pin, Base_t mode);
    void (*pin_write)(struct Device_st *device, Base_t pin, Base_t value);
    int (*pin_read)(struct Device_st *device, Base_t pin);
    Err_t (*pin_attach_irq)(struct Device_st *device, Int32_t pin,
                      Uint32_t mode, void (*hdr)(void *args), void *args);
    Err_t (*pin_detach_irq)(struct Device_st *device, Int32_t pin);
    Err_t (*pin_irq_enable)(struct Device_st *device, Base_t pin, Uint32_t enabled);
    Base_t (*pin_get)(const char *name);
};

int lHalDevicePinRegister(const char *name, const struct PinOps_st *ops, void *user_data);

void vHalPinMode(Base_t pin, Base_t mode);
void vHalPinWrite(Base_t pin, Base_t value);
int  lHalPinRead(Base_t pin);
Err_t lHalPinAttachIrq(Int32_t pin, Uint32_t mode,
                             void (*hdr)(void *args), void  *args);
Err_t lHalPinDetachIrq(Int32_t pin);
Err_t lHalPinIrqEnable(Base_t pin, Uint32_t enabled);
/* Get pin number by name,such as PA.0,P0.12 */
Base_t lHalPinGet(const char *name);

#ifdef __cplusplus
}
#endif

#endif
