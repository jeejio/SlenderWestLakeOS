/*
 * Copyright (c) 2023, Jeejio Technology Co. Ltd
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2023-03-08     Alpha        First Verison
 *
 */
#ifndef CAN_H_
#define CAN_H_

#include "freertos/device.h"
#include "freertos/FreeRTOS.h"
#include "freertos/timers.h"
#include <freertos/semphr.h>

#ifndef CANMSG_BOX_SZ
#define CANMSG_BOX_SZ    16
#endif
#ifndef CANSND_BOX_NUM
#define CANSND_BOX_NUM   1
#endif

enum CAN_DLC
{
    CAN_MSG_0BYTE = 0,
    CAN_MSG_1BYTE,
    CAN_MSG_2BYTES,
    CAN_MSG_3BYTES,
    CAN_MSG_4BYTES,
    CAN_MSG_5BYTES,
    CAN_MSG_6BYTES,
    CAN_MSG_7BYTES,
    CAN_MSG_8BYTES,
    CAN_MSG_12BYTES,
    CAN_MSG_16BYTES,
    CAN_MSG_20BYTES,
    CAN_MSG_24BYTES,
    CAN_MSG_32BYTES,
    CAN_MSG_48BYTES,
    CAN_MSG_64BYTES,
};

enum CANBAUD
{
    CAN1MBaud   = 1000UL * 1000,/* 1 MBit/sec   */
    CAN800kBaud = 1000UL * 800, /* 800 kBit/sec */
    CAN500kBaud = 1000UL * 500, /* 500 kBit/sec */
    CAN250kBaud = 1000UL * 250, /* 250 kBit/sec */
    CAN125kBaud = 1000UL * 125, /* 125 kBit/sec */
    CAN100kBaud = 1000UL * 100, /* 100 kBit/sec */
    CAN50kBaud  = 1000UL * 50,  /* 50 kBit/sec  */
    CAN20kBaud  = 1000UL * 20,  /* 20 kBit/sec  */
    CAN10kBaud  = 1000UL * 10   /* 10 kBit/sec  */
};

#define CAN_MODE_NORMAL              0
#define CAN_MODE_LISTEN              1
#define CAN_MODE_LOOPBACK            2
#define CAN_MODE_LOOPBACKANLISTEN    3

#define CAN_MODE_PRIV                0x01
#define CAN_MODE_NOPRIV              0x00

struct CanFilterItem_st
{
    Uint32_t id  : 29;
    Uint32_t ide : 1;
    Uint32_t rtr : 1;
    Uint32_t mode : 1;
    Uint32_t mask;
    Int32_t hdr;
#ifdef CAN_USING_HDR
    Err_t (*ind)(Device_t dev, void *args , Int32_t hdr, Size_t size);
    void *args;
#endif /*CAN_USING_HDR*/
};

#ifdef CAN_USING_HDR
#define CAN_FILTER_ITEM_INIT(id,ide,rtr,mode,mask,ind,args) \
     {(id), (ide), (rtr), (mode), (mask), -1, (ind), (args)}
#define CAN_FILTER_STD_INIT(id,ind,args) \
     CAN_FILTER_ITEM_INIT(id,0,0,0,0xFFFFFFFF,ind,args)
#define CAN_FILTER_EXT_INIT(id,ind,args) \
     CAN_FILTER_ITEM_INIT(id,1,0,0,0xFFFFFFFF,ind,args)
#define CAN_STD_RMT_FILTER_INIT(id,ind,args) \
     CAN_FILTER_ITEM_INIT(id,0,1,0,0xFFFFFFFF,ind,args)
#define CAN_EXT_RMT_FILTER_INIT(id,ind,args) \
     CAN_FILTER_ITEM_INIT(id,1,1,0,0xFFFFFFFF,ind,args)
#define CAN_STD_RMT_DATA_FILTER_INIT(id,ind,args) \
     CAN_FILTER_ITEM_INIT(id,0,0,1,0xFFFFFFFF,ind,args)
#define CAN_EXT_RMT_DATA_FILTER_INIT(id,ind,args) \
     CAN_FILTER_ITEM_INIT(id,1,0,1,0xFFFFFFFF,ind,args)
#else

#define CAN_FILTER_ITEM_INIT(id,ide,rtr,mode,mask) \
     {(id), (ide), (rtr), (mode), (mask), -1, }
#define CAN_FILTER_STD_INIT(id) \
     CAN_FILTER_ITEM_INIT(id,0,0,0,0xFFFFFFFF)
#define CAN_FILTER_EXT_INIT(id) \
     CAN_FILTER_ITEM_INIT(id,1,0,0,0xFFFFFFFF)
#define CAN_STD_RMT_FILTER_INIT(id) \
     CAN_FILTER_ITEM_INIT(id,0,1,0,0xFFFFFFFF)
#define CAN_EXT_RMT_FILTER_INIT(id) \
     CAN_FILTER_ITEM_INIT(id,1,1,0,0xFFFFFFFF)
#define CAN_STD_RMT_DATA_FILTER_INIT(id) \
     CAN_FILTER_ITEM_INIT(id,0,0,1,0xFFFFFFFF)
#define CAN_EXT_RMT_DATA_FILTER_INIT(id) \
     CAN_FILTER_ITEM_INIT(id,1,0,1,0xFFFFFFFF)
#endif

struct CanFilterConfig_st
{
    Uint32_t count;
    Uint32_t actived;
    struct CanFilterItem_st *items;
};

struct CanBitTiming_st
{
    Uint16_t prescaler;  /* Pre-scaler */
    Uint16_t num_seg1;   /* Bit Timing Segment 1, in terms of Tq */
    Uint16_t num_seg2;   /* Bit Timing Segment 2, in terms of Tq */
    Uint8_t num_sjw;     /* Synchronization Jump Width, in terms of Tq */
    Uint8_t num_sspoff;  /* Secondary Sample Point Offset, in terms of Tq */
};

/**
 * CAN bit timing configuration list
 * NOTE:
 *  items[0] always for CAN2.0/CANFD Arbitration Phase
 *  items[1] always for CANFD (if it exists)
 */
struct CanBitTimingConfig_st
{
    Uint32_t count;
    struct CanBitTiming_st *items;
};

struct CanConfigure_st
{
    Uint32_t baud_rate;
    Uint32_t msgboxsz;
    Uint32_t sndboxnumber;
    Uint32_t mode      : 8;
    Uint32_t privmode  : 8;
    Uint32_t reserved  : 16;
    Uint32_t ticks;
#ifdef CAN_USING_HDR
    Uint32_t maxhdr;
#endif

#ifdef CAN_USING_CANFD
    Uint32_t baud_rate_fd;       /* CANFD data bit rate*/
    Uint32_t use_bit_timing: 8;  /* Use the bit timing for CAN timing configuration */
    Uint32_t enable_canfd : 8;   /* Enable CAN-FD mode */
    Uint32_t reserved1 : 16;

    /* The below fields take effect only if use_bit_timing is non-zero */
    struct CanBitTiming_st can_timing;    /* CAN bit-timing /CANFD bit-timing for arbitration phase */
    struct CanBitTiming_st canfd_timing;  /* CANFD bit-timing for datat phase */
#endif
};

#define CANDEFAULTCONFIG \
{\
        CAN1MBaud,\
        CANMSG_BOX_SZ,\
        CANSND_BOX_NUM,\
        CAN_MODE_NORMAL,\
};

struct CanOps_st;
#define CAN_CMD_SET_FILTER       0x13
#define CAN_CMD_SET_BAUD         0x14
#define CAN_CMD_SET_MODE         0x15
#define CAN_CMD_SET_PRIV         0x16
#define CAN_CMD_GET_STATUS       0x17
#define CAN_CMD_SET_STATUS_IND   0x18
#define CAN_CMD_SET_BUS_HOOK     0x19
#define CAN_CMD_SET_CANFD        0x1A
#define CAN_CMD_SET_BAUD_FD      0x1B
#define CAN_CMD_SET_BITTIMING    0x1C

#define DEVICE_CAN_INT_ERR       0x1000

enum CanStatusMode_t
{
    NORMAL = 0,
    ERRWARNING = 1,
    ERRPASSIVE = 2,
    BUSOFF = 4,
};
enum CanBusErr_t
{
    CAN_BUS_NO_ERR = 0,
    CAN_BUS_BIT_PAD_ERR = 1,
    CAN_BUS_FORMAT_ERR = 2,
    CAN_BUS_ACK_ERR = 3,
    CAN_BUS_IMPLICIT_BIT_ERR = 4,
    CAN_BUS_EXPLICIT_BIT_ERR = 5,
    CAN_BUS_CRC_ERR = 6,
};

struct CanStatus_st
{
    Uint32_t rcverrcnt;
    Uint32_t snderrcnt;
    Uint32_t errcode;
    Uint32_t rcvpkg;
    Uint32_t dropedrcvpkg;
    Uint32_t sndpkg;
    Uint32_t dropedsndpkg;
    Uint32_t bitpaderrcnt;
    Uint32_t formaterrcnt;
    Uint32_t ackerrcnt;
    Uint32_t biterrcnt;
    Uint32_t crcerrcnt;
    Uint32_t rcvchange;
    Uint32_t sndchange;
    Uint32_t lasterrtype;
};

#ifdef CAN_USING_HDR
struct CanHdr_st
{
    Uint32_t connected;
    Uint32_t msgs;
    struct CanFilterItem_st filter;
    struct ListNode_st list;
};
#endif
struct CanDevice_st;
typedef Err_t (*CanstatusInd_t)(struct CanDevice_st *, void *);
typedef struct CanStatusIndType_st
{
    CanstatusInd_t ind;
    void *args;
} *CanStatusIndType_t;
typedef void (*CanBusHook_t)(struct CanDevice_st *);
struct CanDevice_st
{
    struct Device_st parent;

    const struct CanOps_st *ops;
    struct CanConfigure_st config;
    struct CanStatus_st status;

    Uint32_t timerinitflag;
    TimerHandle_t timer;

    struct CanStatusIndType_st status_indicate;
#ifdef CAN_USING_HDR
    struct CanHdr_st *hdr;
#endif
#ifdef CAN_USING_BUS_HOOK
    CanBusHook_t bus_hook;
#endif /*CAN_USING_BUS_HOOK*/
    SemaphoreHandle_t lock;
    void *can_rx;
    void *can_tx;
};
typedef struct CanDevice_st *Can_t;

#define CAN_STDID 0
#define CAN_EXTID 1
#define CAN_DTR   0
#define CAN_RTR   1

typedef struct CanStatus_st *CanStatus_t;
struct CanMsg_st
{
    Uint32_t id  : 29;
    Uint32_t ide : 1;
    Uint32_t rtr : 1;
    Uint32_t rsv : 1;
    Uint32_t len : 8;
    Uint32_t priv : 8;
    Int32_t hdr : 8;
#ifdef CAN_USING_CANFD
    Uint32_t fd_frame : 1;
    Uint32_t reserved : 7;
#else
    Uint32_t reserved : 8;
#endif
#ifdef CAN_USING_CANFD
    Uint8_t data[64];
#else
    Uint8_t data[8];
#endif
};
typedef struct CanMsg_st *CanMsg_t;

struct CanMsgList_st
{
    struct ListNode_st list;
#ifdef CAN_USING_HDR
    struct ListNode_st hdrlist;
    struct CanHdr_st *owner;
#endif
    struct CanMsg_st data;
};

struct CanRxFifo_st
{
    /* software fifo */
    struct CanMsgList_st *buffer;
    Uint32_t freenumbers;
    struct ListNode_st freelist;
    struct ListNode_st uselist;
};

#define CAN_SND_RESULT_OK        0
#define CAN_SND_RESULT_ERR       1
#define CAN_SND_RESULT_WAIT      2

#define CAN_EVENT_RX_IND         0x01    /* Rx indication */
#define CAN_EVENT_TX_DONE        0x02    /* Tx complete   */
#define CAN_EVENT_TX_FAIL        0x03    /* Tx fail   */
#define CAN_EVENT_RX_TIMEOUT     0x05    /* Rx timeout    */
#define CAN_EVENT_RXOF_IND       0x06    /* Rx overflow */

struct CanSndbxinxList_st
{
    struct ListNode_st list;
    SemaphoreHandle_t completion;
    Uint32_t result;
};

struct CanTxFifo_st
{
    struct CanSndbxinxList_st *buffer;
    SemaphoreHandle_t sem;
    struct ListNode_st freelist;
};

struct CanOps_st
{
    Err_t (*configure)(struct CanDevice_st *can, struct CanConfigure_st *cfg);
    Err_t (*control)(struct CanDevice_st *can, int cmd, void *arg);
    int (*sendmsg)(struct CanDevice_st *can, const void *buf, Uint32_t boxno);
    int (*recvmsg)(struct CanDevice_st *can, void *buf, Uint32_t boxno);
};

Err_t lHalHwCanRegister(struct CanDevice_st    *can,
                            const char              *name,
                            const struct CanOps_st *ops,
                            void                    *data);
void vHalHwCanIsr(struct CanDevice_st *can, int event);
#endif /*_CAN_H*/

