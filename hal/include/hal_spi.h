/*
 * Copyright (c) 2022, jeejio Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2023-1-10      zhengqian    first version
 */



#ifndef __SPI_H__
#define __SPI_H__

#include <stdlib.h>
#include <freertos/device.h>
#include <freertos/semphr.h>


#ifdef __cplusplus
extern "C"{
#endif

/**
 * At CPOL=0 the base value of the clock is zero
 *  - For CPHA=0, data are captured on the clock's rising edge (low->high transition)
 *    and data are propagated on a falling edge (high->low clock transition).
 *  - For CPHA=1, data are captured on the clock's falling edge and data are
 *    propagated on a rising edge.
 * At CPOL=1 the base value of the clock is one (inversion of CPOL=0)
 *  - For CPHA=0, data are captured on clock's falling edge and data are propagated
 *    on a rising edge.
 *  - For CPHA=1, data are captured on clock's rising edge and data are propagated
 *    on a falling edge.
 */
#define SPI_CPHA     (1<<0)                             /* bit[0]:CPHA, clock phase */
#define SPI_CPOL     (1<<1)                             /* bit[1]:CPOL, clock polarity */

#define SPI_LSB      (0<<2)                             /* bit[2]: 0-LSB */
#define SPI_MSB      (1<<2)                             /* bit[2]: 1-MSB */

#define SPI_MASTER   (0<<3)                             /* SPI master device */
#define SPI_SLAVE    (1<<3)                             /* SPI slave device */

#define SPI_CS_HIGH  (1<<4)                             /* Chipselect active high */
#define SPI_NO_CS    (1<<5)                             /* No chipselect */
#define SPI_3WIRE    (1<<6)                             /* SI/SO pin shared */
#define SPI_READY    (1<<7)                             /* Slave pulls low to pause */

#define SPI_MODE_MASK    (SPI_CPHA | SPI_CPOL | SPI_MSB | SPI_SLAVE | SPI_CS_HIGH | SPI_NO_CS | SPI_3WIRE | SPI_READY)

#define SPI_MODE_0       (0 | 0)                        /* CPOL = 0, CPHA = 0 */
#define SPI_MODE_1       (0 | SPI_CPHA)              /* CPOL = 0, CPHA = 1 */
#define SPI_MODE_2       (SPI_CPOL | 0)              /* CPOL = 1, CPHA = 0 */
#define SPI_MODE_3       (SPI_CPOL | SPI_CPHA)    /* CPOL = 1, CPHA = 1 */

#define SPI_BUS_MODE_SPI         (1<<0)
#define SPI_BUS_MODE_QSPI        (1<<1)

/**
 * SPI message structure
 */
struct SpiMessage_st
{
    const void *send_buf;
    void *recv_buf;
    Size_t length;
    struct SpiMessage_st *next;

    unsigned cs_take    : 1;
    unsigned cs_release : 1;
};

/**
 * SPI configuration structure
 */
struct SpiConfiguration_st
{
    Uint8_t mode;
    Uint8_t data_width;
    Uint16_t reserved;

    Uint32_t max_hz;
    void *pConfig;
};

struct SpiOps_st;
struct SpiBus_st
{
    struct Device_st parent;
    Uint8_t mode;
    const struct SpiOps_st *ops;
    SemaphoreHandle_t lock;
    struct SpiDevice_st *owner;
};

/**
 * SPI operators
 */
struct SpiOps_st
{
    Err_t (*configure)(struct SpiDevice_st *device, struct SpiConfiguration_st *configuration);
    Uint32_t (*xfer)(struct SpiDevice_st *device, struct SpiMessage_st *message);
};

/**
 * SPI Virtual BUS, one device must connected to a virtual BUS
 */
struct SpiDevice_st
{
    struct Device_st parent;
    struct SpiBus_st *bus;

    struct SpiConfiguration_st config;
    void   *user_data;
};

struct QspiMessage_st
{
    struct SpiMessage_st parent;

    /* instruction stage */
    struct
    {
        Uint8_t content;
        Uint8_t qspi_lines;
    } instruction;

    /* address and alternate_bytes stage */
    struct
    {
        Uint32_t content;
        Uint8_t size;
        Uint8_t qspi_lines;
    } address, alternate_bytes;

    /* dummy_cycles stage */
    Uint32_t dummy_cycles;

    /* number of lines in qspi data stage, the other configuration items are in parent */
    Uint8_t qspi_data_lines;
};

struct QspiConfiguration_st
{
    struct SpiConfiguration_st parent;
    /* The size of medium */
    Uint32_t medium_size;
    /* double data rate mode */
    Uint8_t ddr_mode;
    /* the data lines max width which QSPI bus supported, such as 1, 2, 4 */
    Uint8_t qspi_dl_width ;
};

struct QspiDevice_st
{
    struct SpiDevice_st parent;

    struct QspiConfiguration_st config;

    void (*enter_qspi_mode)(struct QspiDevice_st *device);

    void (*exit_qspi_mode)(struct QspiDevice_st *device);
};

#define SPI_DEVICE(dev) ((struct SpiDevice_st *)(dev))

/* register a SPI bus */
Err_t lHalSpiBusRegister(struct SpiBus_st       *bus,
                             const char              *name,
                             const struct SpiOps_st *ops);

/* attach a device on SPI bus */
Err_t lHalSpiBusAttachDevice(struct SpiDevice_st *device,
                                  const char           *name,
                                  const char           *bus_name,
                                  void                 *user_data);

/**
 * This function takes SPI bus.
 *
 * @param device the SPI device attached to SPI bus
 *
 * @return DEV_EOK on taken SPI bus successfully. others on taken SPI bus failed.
 */
Err_t lHalSpiTakeBus(struct SpiDevice_st *device);

/**
 * This function releases SPI bus.
 *
 * @param device the SPI device attached to SPI bus
 *
 * @return DEV_EOK on release SPI bus successfully.
 */
Err_t lHalSpiReleaseBus(struct SpiDevice_st *device);

/**
 * This function take SPI device (takes CS of SPI device).
 *
 * @param device the SPI device attached to SPI bus
 *
 * @return DEV_EOK on release SPI bus successfully. others on taken SPI bus failed.
 */
Err_t lHalSpiTake(struct SpiDevice_st *device);

/**
 * This function releases SPI device (releases CS of SPI device).
 *
 * @param device the SPI device attached to SPI bus
 *
 * @return DEV_EOK on release SPI device successfully.
 */
Err_t lHalSpiRelease(struct SpiDevice_st *device);

/* set configuration on SPI device */
Err_t lHalSpiConfigure(struct SpiDevice_st        *device,
                          struct SpiConfiguration_st *cfg);

/* send data then receive data from SPI device */
Err_t lHalSpiSendThenRecv(struct SpiDevice_st *device,
                               const void           *send_buf,
                               Size_t             send_length,
                               void                 *recv_buf,
                               Size_t             recv_length);

Err_t lHalSpiSendThenSend(struct SpiDevice_st *device,
                               const void           *send_buf1,
                               Size_t             send_length1,
                               const void           *send_buf2,
                               Size_t             send_length2);

/**
 * This function transmits data to SPI device.
 *
 * @param device the SPI device attached to SPI bus
 * @param send_buf the buffer to be transmitted to SPI device.
 * @param recv_buf the buffer to save received data from SPI device.
 * @param length the length of transmitted data.
 *
 * @return the actual length of transmitted.
 */
Size_t ulHalSpiTransfer(struct SpiDevice_st *device,
                          const void           *send_buf,
                          void                 *recv_buf,
                          Size_t             length);

/**
 * This function transfers a message list to the SPI device.
 *
 * @param device the SPI device attached to SPI bus
 * @param message the message list to be transmitted to SPI device
 *
 * @return NULL if transmits message list successfully,
 *         SPI message which be transmitted failed.
 */
struct SpiMessage_st *pHalSpiTransferMessage(struct SpiDevice_st  *device,
                                               struct SpiMessage_st *message);

static inline Size_t ulHalSpiRecv(struct SpiDevice_st *device,
                                void                 *recv_buf,
                                Size_t             length)
{
    return ulHalSpiTransfer(device, NULL, recv_buf, length);
}

static inline Size_t ulHalSpiSend(struct SpiDevice_st *device,
                                const void           *send_buf,
                                Size_t             length)
{
    return ulHalSpiTransfer(device, send_buf, NULL, length);
}

static inline Uint8_t ucHalSpiSendrecv8(struct SpiDevice_st *device,
                                      Uint8_t            data)
{
    Uint8_t value = 0;

    lHalSpiSendThenRecv(device, &data, 1, &value, 1);

    return value;
}

static inline Uint16_t usHalSpiSendrecv16(struct SpiDevice_st *device,
                                        Uint16_t           data)
{
    Uint16_t value = 0;

    lHalSpiSendThenRecv(device, &data, 2, &value, 2);

    return value;
}

/**
 * This function appends a message to the SPI message list.
 *
 * @param list the SPI message list header.
 * @param message the message pointer to be appended to the message list.
 */
static inline void vHalSpiMessageAppend(struct SpiMessage_st *list,
                                     struct SpiMessage_st *message)
{
    configASSERT(list != NULL);
    if (message == NULL)
        return; /* not append */

    while (list->next != NULL)
    {
        list = list->next;
    }

    list->next = message;
    message->next = NULL;
}

/**
 * This function can set configuration on QSPI device.
 *
 * @param device the QSPI device attached to QSPI bus.
 * @param cfg the configuration pointer.
 *
 * @return the actual length of transmitted.
 */
Err_t lHalQspiConfigure(struct QspiDevice_st *device, struct QspiConfiguration_st *cfg);

/**
 * This function can register a SPI bus for QSPI mode.
 *
 * @param bus the SPI bus for QSPI mode.
 * @param name The name of the spi bus.
 * @param ops the SPI bus instance to be registered.
 *
 * @return the actual length of transmitted.
 */
Err_t lHalQspiBusRegister(struct SpiBus_st *bus, const char *name, const struct SpiOps_st *ops);

/**
 * This function transmits data to QSPI device.
 *
 * @param device the QSPI device attached to QSPI bus.
 * @param message the message pointer.
 *
 * @return the actual length of transmitted.
 */
Size_t ulHalQspiTransferMessage(struct QspiDevice_st  *device, struct QspiMessage_st *message);

/**
 * This function can send data then receive data from QSPI device
 *
 * @param device the QSPI device attached to QSPI bus.
 * @param send_buf the buffer to be transmitted to QSPI device.
 * @param send_length the number of data to be transmitted.
 * @param recv_buf the buffer to be recivied from QSPI device.
 * @param recv_length the data to be recivied.
 *
 * @return the status of transmit.
 */
Err_t lHalQspiSendThenRecv(struct QspiDevice_st *device, const void *send_buf, Size_t send_length,void *recv_buf, Size_t recv_length);

/**
 * This function can send data to QSPI device
 *
 * @param device the QSPI device attached to QSPI bus.
 * @param send_buf the buffer to be transmitted to QSPI device.
 * @param send_length the number of data to be transmitted.
 *
 * @return the status of transmit.
 */
Err_t lHalQspiSend(struct QspiDevice_st *device, const void *send_buf, Size_t length);

#ifdef __cplusplus
}
#endif

#endif
