/*
 * Copyright (c) 2023, Jeejio Technology Co. Ltd
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2023-03-08     Alpha        First Verison
 *
 */
#ifndef __ADC_H__
#define __ADC_H__
#include <swldef.h>

struct AdcDevice_st;
struct AdcOps_st
{
    Err_t (*enabled)(struct AdcDevice_st *device, Uint32_t channel, Bool_t enabled);
    Err_t (*convert)(struct AdcDevice_st *device, Uint32_t channel, int *value);
    Uint8_t (*get_resolution)(struct AdcDevice_st *device);
    Int16_t (*get_vref) (struct AdcDevice_st *device);
};

struct AdcDevice_st
{
    struct Device_st parent;
    const struct AdcOps_st *ops;
};
typedef struct AdcDevice_st *AdcDevice_t;

typedef enum
{
    ADC_CMD_ENABLE = DEVICE_CTRL_BASE(ADC) + 1,
    ADC_CMD_DISABLE = DEVICE_CTRL_BASE(ADC) + 2,
    ADC_CMD_GET_RESOLUTION = DEVICE_CTRL_BASE(ADC) + 3, /* get the resolution in bits */
    ADC_CMD_GET_VREF = DEVICE_CTRL_BASE(ADC) + 4, /* get reference voltage */
} AdcCmd_t;

Err_t lHalHwAdcRegister(AdcDevice_t adc,const char *name, const struct AdcOps_st *ops, const void *user_data);

Uint32_t uHalAdcRead(AdcDevice_t dev, Uint32_t channel);
Err_t lHalAdcEnable(AdcDevice_t dev, Uint32_t channel);
Err_t lHalAdcDisable(AdcDevice_t dev, Uint32_t channel);
Int16_t sHalAdcVoltage(AdcDevice_t dev, Uint32_t channel);
int lHalHwAdcInit(void);
#endif /* __ADC_H__ */
