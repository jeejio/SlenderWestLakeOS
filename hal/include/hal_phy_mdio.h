/*
 * Copyright (c) 2023, Jeejio Technology Co. Ltd
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2023-03-08     Alpha        First Verison
 *
 */
#ifndef __MDIO_H__
#define __MDIO_H__

#ifdef __cplusplus
extern "C"
{
#endif

struct MdioBusOps_st
{
    Bool_t (*init)(void *bus, Uint32_t src_clock_hz);
    Size_t (*read)(void *bus, Uint32_t addr, Uint32_t reg, void *data, Uint32_t size);
    Size_t (*write)(void *bus, Uint32_t addr, Uint32_t reg, void *data, Uint32_t size);
    Bool_t (*uninit)(void *bus);
};

struct MdioBus_st
{
    void *hw_obj;
    char *name;
    struct MdioBusOps_st *ops;
};

typedef struct MdioBus_st Mdio_t;

#ifdef __cplusplus
}
#endif

#endif
