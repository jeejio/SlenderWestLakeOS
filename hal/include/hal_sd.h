/*
 * Copyright (c) 2022, jeejio Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2023-1-12      zhengqian    the first version
 */

#ifndef __SD_H__
#define __SD_H__

#include <freertos/device.h>
#include <hal_mmcsd_host.h>

#ifdef __cplusplus
extern "C" {
#endif

Err_t lHalMmcsdSendIfCond(struct MmcsdHost_st *host, Uint32_t ocr);
Err_t lHalMmcsdSendAppOpCond(struct MmcsdHost_st *host, Uint32_t ocr, Uint32_t *rocr);

Err_t lHalMmcsdGetCardAddr(struct MmcsdHost_st *host, Uint32_t *rca);
Int32_t lHalMmcsdGetScr(struct MmcsdCard_st *card, Uint32_t *scr);

Int32_t lHalInitSd(struct MmcsdHost_st *host, Uint32_t ocr);

#ifdef __cplusplus
}
#endif

#endif
