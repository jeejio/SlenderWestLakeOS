/*
 * Copyright (c) 2023, JEE Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2023-02-28      lk        first version
 */

#ifndef __HAL_AUDIO_MEMPOOL_H__
#define __HAL_AUDIO_MEMPOOL_H__

#include "swldef.h"

#ifdef __cplusplus
extern "C" {
#endif

#define AUDIO_REPLAY_MP_BLOCK_SIZE 4096
#define AUDIO_REPLAY_MP_BLOCK_COUNT 2

/**
 * Base structure of Memory pool object
 */
struct Mempool_st
{
    void            *start_address;                     /**< memory pool start */
    Size_t        size;                              /**< size of memory pool */

    Size_t        block_size;                        /**< size of memory blocks */
    Uint8_t      *block_list;                        /**< memory blocks list */

    Size_t        block_total_count;                 /**< numbers of memory block */
    Size_t        block_free_count;                  /**< numbers of free memory block */

    DevList_t        suspend_thread;                    /**< threads pended on this resource */
    Size_t        suspend_thread_count;              /**< numbers of thread pended on this resource */
};
typedef struct Mempool_st *Mp_t;

/*
 * memory pool interface
 */
Err_t lHalMemPoolInit(struct Mempool_st *mp,
                    const char        *name,
                    void              *start,
                    Size_t          size,
                    Size_t          block_size);

void *pHalMemPoolAlloc(Mp_t mp, Int32_t time);

void vHalMemPoolFree(void *block);

Mp_t pHalMemPoolCreate(const char *name,
                     Size_t   block_count,
                     Size_t   block_size);

Err_t lHalMemPoolDelete(Mp_t mp);
#ifdef __cplusplus
}
#endif

#endif /* __HAL_AUDIO_MEMPOOL_H__ */
