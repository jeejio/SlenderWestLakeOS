/*
 * FreeRTOS BLE V2.2.0
 * Copyright (C) 2020 Amazon.com, Inc. or its affiliates.  All Rights Reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * http://aws.amazon.com/freertos
 * http://www.FreeRTOS.org
 */

/**
 * @file hal_ble_config_defaults.h
 * @brief BLE config options.
 *
 * Ensures that the config options for BLE are set to sensible
 * default values if the user does not provide one.
 */

#ifndef _MAOPAO_BLE_CONFIG_DEFAULTS_H_
#define _MAOPAO_BLE_CONFIG_DEFAULTS_H_

/**
 * @brief UUID used to uniquely identify a GATT server instance
 *
 * Currently only one server instance is supported.
 *
 */
#define MAOPAO_BLE_SERVER_UUID    { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 }

/**
 *
 * @brief UUID of Jeejio Device Information Service.
 *
 * This 16 bit UUID used by all JEEJIO for devices information.
 * using this UUID.
 *
 *
 */
#define lHalBle_SIG_DEVINFO_SERV_UUID               0x180A  // Device Information


/**
 * @brief Set to true if user wants to send its own advertisement message.
 * If this configuration is set to 1 then user needs to implement lHalBle_SetCustomAdvCb
 * hook in the application.
 */
#ifndef MAOPAO_BLE_SET_CUSTOM_ADVERTISEMENT_MSG
    #define MAOPAO_BLE_SET_CUSTOM_ADVERTISEMENT_MSG    ( 0 )
#endif


/**
 *
 * @brief Define the UUID that is going to be advertised.
 *
 */
#ifndef MAOPAO_BLE_ADVERTISING_UUID
    #define MAOPAO_BLE_ADVERTISING_UUID         lHalBle_SIG_DEVINFO_SERV_UUID
    #define MAOPAO_BLE_ADVERTISING_UUID_SIZE    2
#endif

/**
 * @brief Define the connection interval.
 *
 */
#if ( !defined( MAOPAO_BLE_ADVERTISING_CONN_INTERVAL_MIN ) ) || ( !defined( MAOPAO_BLE_ADVERTISING_CONN_INTERVAL_MAX ) )
    #ifdef MAOPAO_BLE_ADVERTISING_CONN_INTERVAL_MIN
        #error "MAOPAO_BLE_ADVERTISING_CONN_INTERVAL_MAX need to be defined"
    #endif
    #ifdef MAOPAO_BLE_ADVERTISING_CONN_INTERVAL_MAX
        #error "MAOPAO_BLE_ADVERTISING_CONN_INTERVAL_MIN need to be defined"
    #endif
    #define MAOPAO_BLE_ADVERTISING_CONN_INTERVAL_MIN    0x20
    #define MAOPAO_BLE_ADVERTISING_CONN_INTERVAL_MAX    0x40
#endif

/**< The advertising interval (in units of 0.625 ms. This value corresponds to 100 ms). */
#ifndef MAOPAO_BLE_ADVERTISING_INTERVAL
    #define MAOPAO_BLE_ADVERTISING_INTERVAL    160
#endif

/**< Min encryption key size. */
#ifndef MAOPAO_BLE_ENCRYPT_KEY_SIZE_MIN
    #define MAOPAO_BLE_ENCRYPT_KEY_SIZE_MIN    16
#endif


/**
 * @brief Appearance of the device when advertising.
 *
 */
#ifndef MAOPAO_BLE_ADVERTISING_APPEARANCE
    #define MAOPAO_BLE_ADVERTISING_APPEARANCE    0
#endif

/**
 * @brief Supported input/output of the device.
 * This define needs to be of type BTIOtypes_t.
 */
#ifndef MAOPAO_BLE_INPUT_OUTPUT
    #define MAOPAO_BLE_INPUT_OUTPUT    eBTIODisplayYesNo
#endif

/**
 *
 * @brief Device name that is broadcasted in advertising message. It is the truncated complete local name
 *
 */
#ifndef MAOPAO_BLE_DEVICE_SHORT_LOCAL_NAME_SIZE
    #define MAOPAO_BLE_DEVICE_SHORT_LOCAL_NAME_SIZE    4
#endif

/**
 *
 * @brief Device name that can be read from the name characteristic.
 *
 */
#ifndef MAOPAO_BLE_DEVICE_COMPLETE_LOCAL_NAME
    #define MAOPAO_BLE_DEVICE_COMPLETE_LOCAL_NAME    "BLE"
#endif

/**
 * @brief Max length BLE local device name.
 * As per BLE4.2 SPEC, device name length can be between 0 and 248 octet in length.
 *
 */
#ifndef MAOPAO_BLE_DEVICE_LOCAL_NAME_MAX_LENGTH
    #define MAOPAO_BLE_DEVICE_LOCAL_NAME_MAX_LENGTH    ( 248 )
#endif

/**
 * @brief Preferred MTU size for the device for a BLE connection.
 *
 * Upon new BLE connection both peers will negotiate their preferred MTU size. MTU size for the ble connection will be set to minimum
 * of the negotiated values.
 *
 */
#ifndef MAOPAO_BLE_PREFERRED_MTU_SIZE
    #define MAOPAO_BLE_PREFERRED_MTU_SIZE    ( 512 )
#endif

/**
 * @brief The flag to enable bonding for the device.
 *
 * By default bonding will be enabled on all device.
 */
#ifndef MAOPAO_BLE_ENABLE_BONDING
    #define MAOPAO_BLE_ENABLE_BONDING    ( 0 )
#endif

/**
 * @brief The flag to enable secure connection for the device.
 *
 * By default secure connection will be enable on all device.
 */
#ifndef MAOPAO_BLE_ENABLE_SECURE_CONNECTION
    #define MAOPAO_BLE_ENABLE_SECURE_CONNECTION    ( 1 )
#endif

/* Config if set, requires encryption to access services and characteristics */

/**
 * @brief Configuration to force encryption to access all characteristics of services.
 */
#ifndef MAOPAO_BLE_ENCRYPTION_REQUIRED
    #define MAOPAO_BLE_ENCRYPTION_REQUIRED    ( 1 )
#endif

/**
 * @brief Configuration to enable numeric comparison for authentication.
 *
 * If the configuration is set to 0 and #MAOPAO_BLE_ENABLE_SECURE_CONNECTION is set to 1, then
 * device will use just works pairing.
 */
#ifndef MAOPAO_BLE_ENABLE_NUMERIC_COMPARISON
    #define MAOPAO_BLE_ENABLE_NUMERIC_COMPARISON    ( 1 )
#endif

/**
 * @brief Timeout in seconds used for BLE numeric comparison.
 */
#ifndef MAOPAO_BLE_NUMERIC_COMPARISON_TIMEOUT_SEC
    #define MAOPAO_BLE_NUMERIC_COMPARISON_TIMEOUT_SEC    ( 30 )
#endif

/**
 * @brief Maximum number of device this device can be bonded with.
 */
#ifndef MAOPAO_BLE_MAX_BONDED_DEVICES
    #define MAOPAO_BLE_MAX_BONDED_DEVICES    ( 5 )
#endif

#if ( MAOPAO_BLE_ENCRYPTION_REQUIRED == 1 )
    #if ( MAOPAO_BLE_ENABLE_NUMERIC_COMPARISON == 1 )
        #define MAOPAO_BLE_CHAR_READ_PERM     eBTPermReadEncryptedMitm
        #define MAOPAO_BLE_CHAR_WRITE_PERM    eBTPermWriteEncryptedMitm
    #else
        #define MAOPAO_BLE_CHAR_READ_PERM     eBTPermReadEncrypted
        #define MAOPAO_BLE_CHAR_WRITE_PERM    eBTPermWriteEncrypted
    #endif
#else
    #define MAOPAO_BLE_CHAR_READ_PERM         eBTPermRead
    #define MAOPAO_BLE_CHAR_WRITE_PERM        eBTPermWrite
#endif



/**
 * @brief This configuration flag can be used to enable or disable all Jeejio GATT services.
 * Configuration is useful if a custom GATT service is used instead of the default GATT services.
 */
#define MAOPAO_BLE_ENABLE_JEEJIO_GATT_SERVICES    ( 1 )


/**
 * @brief Set to true if user wants to add its own custom GATT services.
 */
#ifndef MAOPAO_BLE_ADD_CUSTOM_SERVICES
    #define MAOPAO_BLE_ADD_CUSTOM_SERVICES    ( 0 )
#endif


/**
 * @brief Flag to enable JEEJIO Device Information Service.
 *
 * Device Information service is used by the JEEJIO mobile SDK to fetch device related information.
 */
#if ( MAOPAO_BLE_ENABLE_JEEJIO_GATT_SERVICES == 1 )
    #define MAOPAO_BLE_ENABLE_DEVICE_INFO_SERVICE        ( 1 )
#else
    #define MAOPAO_BLE_ENABLE_DEVICE_INFO_SERVICE        ( 0 )
#endif


/**
 * @brief Enable WIFI provisioning GATT service.
 *
 * By default WIFI provisioning will be disabled. 
 *
 */
#if ( MAOPAO_BLE_ENABLE_JEEJIO_GATT_SERVICES == 1 )
    #ifndef MAOPAO_BLE_ENABLE_WIFI_PROVISIONING
        #define MAOPAO_BLE_ENABLE_WIFI_PROVISIONING    ( 1 )
    #endif
#else
    #define MAOPAO_BLE_ENABLE_WIFI_PROVISIONING        ( 0 )
#endif


/**
 * @brief Enable RPC GATT service.
 *
 * By default RPC will be disabled. 
 *
 */
#if ( MAOPAO_BLE_ENABLE_JEEJIO_GATT_SERVICES == 1 )
    #ifndef MAOPAO_BLE_ENABLE_RPC_GATTS
        #define MAOPAO_BLE_ENABLE_RPC_GATTS    ( 1 )
    #endif
#else
    #define MAOPAO_BLE_ENABLE_RPC_GATTS        ( 0 )
#endif


#define MAOPAO_BLE_MESG_ENCODER                    ( _MaopaoSerializerCborEncoder )
#define MAOPAO_BLE_MESG_DECODER                    ( _MaopaoSerializerCborDecoder )

/**
 * @brief Default configuration for memory allocation of data transfer service buffers.
 */
#ifndef lHalBle_MallocDataBuffer
    #define lHalBle_MallocDataBuffer    malloc
#endif

#ifndef lHalBle_FreeDataBuffer
    #define lHalBle_FreeDataBuffer    free
#endif


#endif /* _MAOPAO_BLE_CONFIG_DEFAULTS_H_ */
