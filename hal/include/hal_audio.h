/*
 * Copyright (c) 2023, JEE Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2023-02-28      lk        first version
 */

#ifndef __HAL_AUDIO_H__
#define __HAL_AUDIO_H__

#include "swldef.h"
#include "hal_audio_dataqueue.h"
#include "hal_audio_mempool.h"
#include "hal_audio_pipe.h"

/* AUDIO command */
#define _AUDIO_CTL(a) (DEVICE_CTRL_BASE(Sound) + a)

#define AUDIO_CTL_GETCAPS                   _AUDIO_CTL(1)
#define AUDIO_CTL_CONFIGURE                 _AUDIO_CTL(2)
#define AUDIO_CTL_START                     _AUDIO_CTL(3)
#define AUDIO_CTL_STOP                      _AUDIO_CTL(4)
#define AUDIO_CTL_GETBUFFERINFO             _AUDIO_CTL(5)

/* Audio Device Types */
#define AUDIO_TYPE_QUERY                    0x00
#define AUDIO_TYPE_INPUT                    0x01
#define AUDIO_TYPE_OUTPUT                   0x02
#define AUDIO_TYPE_MIXER                    0x04

/* Supported Sampling Rates */
#define AUDIO_SAMP_RATE_8K                  0x0001
#define AUDIO_SAMP_RATE_11K                 0x0002
#define AUDIO_SAMP_RATE_16K                 0x0004
#define AUDIO_SAMP_RATE_22K                 0x0008
#define AUDIO_SAMP_RATE_32K                 0x0010
#define AUDIO_SAMP_RATE_44K                 0x0020
#define AUDIO_SAMP_RATE_48K                 0x0040
#define AUDIO_SAMP_RATE_96K                 0x0080
#define AUDIO_SAMP_RATE_128K                0x0100
#define AUDIO_SAMP_RATE_160K                0x0200
#define AUDIO_SAMP_RATE_172K                0x0400
#define AUDIO_SAMP_RATE_192K                0x0800

/* Supported Bit Rates */
#define AUDIO_BIT_RATE_22K                  0x01
#define AUDIO_BIT_RATE_44K                  0x02
#define AUDIO_BIT_RATE_48K                  0x04
#define AUDIO_BIT_RATE_96K                  0x08
#define AUDIO_BIT_RATE_128K                 0x10
#define AUDIO_BIT_RATE_160K                 0x20
#define AUDIO_BIT_RATE_172K                 0x40
#define AUDIO_BIT_RATE_192K                 0x80

/* Support Dsp(input/output) Units controls */
#define AUDIO_DSP_PARAM                     0           /* get/set all params */
#define AUDIO_DSP_SAMPLERATE                1           /* samplerate */
#define AUDIO_DSP_CHANNELS                  2           /* channels */
#define AUDIO_DSP_SAMPLEBITS                3           /* sample bits width */

/* Supported Mixer Units controls */
#define AUDIO_MIXER_QUERY                   0x0000
#define AUDIO_MIXER_MUTE                    0x0001
#define AUDIO_MIXER_VOLUME                  0x0002
#define AUDIO_MIXER_BASS                    0x0004
#define AUDIO_MIXER_MID                     0x0008
#define AUDIO_MIXER_TREBLE                  0x0010
#define AUDIO_MIXER_EQUALIZER               0x0020
#define AUDIO_MIXER_LINE                    0x0040
#define AUDIO_MIXER_DIGITAL                 0x0080
#define AUDIO_MIXER_MIC                     0x0100
#define AUDIO_MIXER_VITURAL                 0x0200
#define AUDIO_MIXER_EXTEND                  0x8000    /* extend mixer command */

#define AUDIO_VOLUME_MAX                    (100)
#define AUDIO_VOLUME_MIN                    (0)

#define CFG_AUDIO_REPLAY_QUEUE_COUNT        4

enum
{
    AUDIO_STREAM_REPLAY = 0,
    AUDIO_STREAM_RECORD,
    AUDIO_STREAM_LAST = AUDIO_STREAM_RECORD,
};

/* the preferred number and size of audio pipeline buffer for the audio device */
struct AudioBufInfo_st
{
    Uint8_t *buffer;
    Uint16_t block_size;
    Uint16_t block_count;
    Uint32_t total_size;
};
struct AudioBufDesc_st
{
  uint8_t *data_ptr;
  size_t data_size;
};

struct AudioFrame_st
{
    const void *data_ptr;
    size_t   data_size;
};

struct AudioDevice_st;
struct AudioCaps_st;
struct AudioConfigure_st;
struct AudioOps_st
{
    Err_t  (*getcaps)        (struct AudioDevice_st *audio,struct AudioCaps_st *caps);
    Err_t  (*configure)      (struct AudioDevice_st *audio,struct AudioCaps_st *caps);

    Err_t    (*init)         (struct AudioDevice_st *audio);
    Err_t    (*start)        (struct AudioDevice_st *audio,int stream);
    Err_t    (*stop)         (struct AudioDevice_st *audio,int stream);
    size_t   (*transmit)     (struct AudioDevice_st *audio, const void *writeBuf,void *readBuf, size_t size);

    //get page size of codec or private buffer's info
    void    (*buffer_info)      (struct AudioDevice_st *audio,struct AudioBufInfo_st *info );
};

struct AudioConfigure_st
{
    Uint32_t samplerate;
    Uint16_t channels;
    Uint16_t samplebits;
};

struct AudioCaps_st
{
  int main_type;
  int sub_type;

  union
  {
    uint32_t mask;
    int     value;
    struct AudioConfigure_st config;
  }udata;
};

struct AudioReplay_st
{
    struct Mempool_st *mp;
    struct DataQueue_st queue;
    struct AudioBufInfo_st buf_info;
    Uint8_t *write_data;
    Uint16_t write_index;
    Uint16_t read_index;
    Uint32_t pos;
    Uint8_t event;
    Bool_t activated;
};

struct AudioRecord_st
{
    struct AudioPipe_st pipe;
    Bool_t activated;
};

struct AudioDevice_st
{
    struct Device_st           parent;
    struct AudioOps_st        *ops;
    struct AudioReplay_st     *replay;
    struct AudioRecord_st     *record;
};

Err_t lHalAudioRegister(struct AudioDevice_st *audio, const char *name, uint32_t flag, void *data);
void vHalAudioTXComplete(struct AudioDevice_st *audio);
void vHalAudioRXDone(struct AudioDevice_st *audio, uint8_t *pbuf, size_t len);

/* Device Control Commands */
#define CODEC_CMD_RESET             0

#define CODEC_CMD_SET_VOLUME        1
#define CODEC_CMD_GET_VOLUME        2
#define CODEC_CMD_SAMPLERATE        3
#define CODEC_CMD_EQ                4
#define CODEC_CMD_3D                5

#define CODEC_VOLUME_MAX            (63)

#endif /* __HAL_AUDIO_H__ */
