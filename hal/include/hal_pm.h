/*
 * Copyright (c) 2023, jeejio Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2023-02-28     lk           the first version
 */

#ifndef __HAL_PM_H__
#define __HAL_PM_H__

#include <freertos/FreeRTOS.h>

#ifdef CONFIG_PM_ENABLE

#include <stdint.h>
#include <hal_lptimer.h>
#include <swldef.h>

/* All modes used for vHalPMRequest() and vHalPMRelease() */
enum
{
    /* sleep modes */
    PM_SLEEP_MODE_NONE = 0,
    PM_SLEEP_MODE_IDLE,
    PM_SLEEP_MODE_LIGHT,
    PM_SLEEP_MODE_DEEP,
    PM_SLEEP_MODE_STANDBY,
    PM_SLEEP_MODE_SHUTDOWN,
    PM_SLEEP_MODE_MAX,
};

enum
{
    /* run modes*/
    PM_RUN_MODE_HIGH_SPEED = 0,
    PM_RUN_MODE_NORMAL_SPEED,
    PM_RUN_MODE_MEDIUM_SPEED,
    PM_RUN_MODE_LOW_SPEED,
    PM_RUN_MODE_MAX,
};

enum
{
    PM_FREQUENCY_PENDING = 0x01,
};

/* The name of all modes used in the msh command "pm_dump" */
#define PM_SLEEP_MODE_NAMES     \
{                               \
    "None Mode",                \
    "Idle Mode",                \
    "LightSleep Mode",          \
    "DeepSleep Mode",           \
    "Standby Mode",             \
    "Shutdown Mode",            \
}

#define PM_RUN_MODE_NAMES       \
{                               \
    "High Speed",               \
    "Normal Speed",             \
    "Medium Speed",             \
    "Low Mode",                 \
}

#ifndef PM_USING_CUSTOM_CONFIG
/**
 * Modules used for
 * vHalPMModuleRequest(PM_BOARD_ID, PM_SLEEP_MODE_IDLE)
 * vHalPMModuleRelease(PM_BOARD_ID, PM_SLEEP_MODE_IDLE)
 * vHalPMModuleReleaseAll(PM_BOARD_ID, PM_SLEEP_MODE_IDLE)
 */
enum pmModuleId {
    PM_NONE_ID = 0,
    PM_POWER_ID,
    PM_BOARD_ID,
    PM_BSP_ID,
    PM_MAIN_ID,
    PM_PMS_ID,
    PM_PMC_ID,
    PM_TASK_ID,
    PM_SPI_ID,
    PM_I2C_ID,
    PM_UART_ID,
    PM_CAN_ID,
    PM_ETH_ID,
    PM_SENSOR_ID,
    PM_LCD_ID,
    PM_KEY_ID,
    PM_TP_ID,
    PM_MODULE_MAX_ID, /* enum must! */
};

#else

#include <pm_cfg.h>

#endif /* PM_USING_CUSTOM_CONFIG */

#ifndef PM_DEFAULT_SLEEP_MODE
#define PM_DEFAULT_SLEEP_MODE        PM_SLEEP_MODE_NONE
#endif

#ifndef PM_DEFAULT_DEEPSLEEP_MODE
#define PM_DEFAULT_DEEPSLEEP_MODE    PM_SLEEP_MODE_DEEP
#endif

#ifndef PM_DEFAULT_RUN_MODE
#define PM_DEFAULT_RUN_MODE          PM_RUN_MODE_NORMAL_SPEED
#endif

/**
 * device control flag to request or release power
 */

#define PM_DEVICE_CTRL_RELEASE   (DEVICE_CTRL_BASE(PM) + 0x00)
#define PM_DEVICE_CTRL_REQUEST   (DEVICE_CTRL_BASE(PM) + 0x01)

struct PowerManageInfo_st;

/**
 * low power mode operations
 */
struct PmOps_st
{
    void (*sleep)(struct PowerManageInfo_st *pm, uint8_t mode);
    void (*run)(struct PowerManageInfo_st *pm, uint8_t mode);
    void (*timerStart)(struct PowerManageInfo_st *pm, uint32_t timeout);
    void (*timerStop)(struct PowerManageInfo_st *pm);
    Tick_t (*timerGetTick)(struct PowerManageInfo_st *pm);
};

struct PmDeviceOps_st
{
    int (*suspend)(const struct Device_st *device, uint8_t mode);
    void (*resume)(const struct Device_st *device, uint8_t mode);
    int (*frequencyChange)(const struct Device_st *device, uint8_t mode);
};

struct PmDevice_st
{
    const struct Device_st *device;
    const struct PmDeviceOps_st *ops;
};

struct PmModule_st
{
    uint8_t reqStatus;
    bool busyFlag;
    uint32_t timeout;
    uint32_t startTime;
};

/**
 * power management
 */
struct PowerManageInfo_st
{
    struct Device_st parent;

    /* modes */
    uint8_t modes[PM_SLEEP_MODE_MAX];
    uint8_t sleepMode;    /* current sleep mode */
    uint8_t runMode;      /* current running mode */

    /* modules request status*/
    struct PmModule_st moduleStatus[PM_MODULE_MAX_ID];

    /* sleep request table */
    uint32_t sleepStatus[PM_SLEEP_MODE_MAX - 1][(PM_MODULE_MAX_ID + 31) / 32];

    /* the list of device, which has PM feature */
    uint8_t devicePmNumber;
    struct PmDevice_st *devicePm;

    /* if the mode has timer, the corresponding bit is 1*/
    uint8_t timerMask;
    uint8_t flags;

    const struct PmOps_st *ops;

    Device_t hwTimerDev;
};

enum
{
    PM_ENTER_SLEEP = 0,
    PM_EXIT_SLEEP,
};

struct PmNotify_st
{
    void (*notify)(uint8_t event, uint8_t mode, void *data);
    void *data;
};

void vHalPMRequest(uint8_t sleepMode);
void vHalPMRelease(uint8_t sleepMode);
void vHalPMReleaseAll(uint8_t sleepMode);
int lHalPMRunEnter(uint8_t runMode);
void vHalSystemPowerManageExe(void);

void vHalPMDeviceRegister(struct Device_st *device, const struct PmDeviceOps_st *ops);
void vHalPMDeviceUnregister(struct Device_st *device);

void vHalPMNotifySet(void (*notify)(uint8_t event, uint8_t mode, void *data), void *data);
void vHalPMDefaultSet(uint8_t sleepMode);

void vHalSystemPMInit(const struct PmOps_st *ops,
                       uint8_t              timerMask,
                       void                 *user_data);
void vHalPMModuleRequest(uint8_t moduleId, uint8_t sleepMode);
void vHalPMModuleRelease(uint8_t moduleId, uint8_t sleepMode);
void vHalPMModuleReleaseAll(uint8_t moduleId, uint8_t sleepMode);
void vHalPMModuleDelaySleep(uint8_t moduleId, Tick_t timeout);
uint32_t uHalPMModuleGetStatus(void);
uint8_t ucHalPMGetSleepMode(void);
struct PowerManageInfo_st *pHalPMGetHandle(void);

/* sleep : request or release */
void vHalPMSleepRequest(Uint16_t moduleId, uint8_t mode);
void vHalPMSleepRelease(Uint16_t moduleId, uint8_t mode);
void vHalPMSleepNoneRequest(Uint16_t moduleId);
void vHalPMSleepNoneRelease(Uint16_t moduleId);
void vHalPMSleepIdleRequest(Uint16_t moduleId);
void vHalPMSleepIdleRelease(Uint16_t moduleId);
void vHalPMSleepLightRequest(Uint16_t moduleId);
void vHalPMSleepLightRelease(Uint16_t moduleId);

#endif /* CONFIG_PM_ENABLE */
#endif /* __HAL_PM_H__ */
