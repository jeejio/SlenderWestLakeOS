/*
 * Copyright (c) 2022, jeejio Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2023-1-12      zhengqian    the first version
 */

#ifndef __CORE_H__
#define __CORE_H__

#include <freertos/device.h>
#include <hal_mmcsd_host.h>
#include <hal_mmcsd_card.h>
#include <hal_mmcsd_cmd.h>

#ifdef __cplusplus
extern "C" {
#endif

#ifdef MMCSD_DBG
#define mmcsd_dbg(fmt, ...)  printf(fmt, ##__VA_ARGS__)
#else
#define mmcsd_dbg(fmt, ...)
#endif

struct MmcsdData_st {
    Uint32_t  blksize;
    Uint32_t  blks;
    Uint32_t  *buf;
    Int32_t  err;
    Uint32_t  flags;
#define DATA_DIR_WRITE  (1 << 0)
#define DATA_DIR_READ   (1 << 1)
#define DATA_STREAM (1 << 2)

    unsigned int        bytes_xfered;

    struct MmcsdCmd_st *stop;      /* stop command */
    struct MmcsdReq_st *mrq;       /* associated request */

    Uint32_t  timeout_ns;
    Uint32_t  timeout_clks;
};

struct MmcsdCmd_st {
    Uint32_t  cmd_code;
    Uint32_t  arg;
    Uint32_t  resp[4];
    Uint32_t  flags;
/*rsponse types
 *bits:0~3
 */
#define RESP_MASK   (0xF)
#define RESP_NONE   (0)
#define RESP_R1     (1 << 0)
#define RESP_R1B    (2 << 0)
#define RESP_R2     (3 << 0)
#define RESP_R3     (4 << 0)
#define RESP_R4     (5 << 0)
#define RESP_R6     (6 << 0)
#define RESP_R7     (7 << 0)
#define RESP_R5     (8 << 0)    /*SDIO command response type*/
/*command types
 *bits:4~5
 */
#define CMD_MASK    (3 << 4)        /* command type */
#define CMD_AC      (0 << 4)
#define CMD_ADTC    (1 << 4)
#define CMD_BC      (2 << 4)
#define CMD_BCR     (3 << 4)

#define resp_type(cmd)  ((cmd)->flags & RESP_MASK)

/*spi rsponse types
 *bits:6~8
 */
#define RESP_SPI_MASK   (0x7 << 6)
#define RESP_SPI_R1 (1 << 6)
#define RESP_SPI_R1B    (2 << 6)
#define RESP_SPI_R2 (3 << 6)
#define RESP_SPI_R3 (4 << 6)
#define RESP_SPI_R4 (5 << 6)
#define RESP_SPI_R5 (6 << 6)
#define RESP_SPI_R7 (7 << 6)

#define spi_resp_type(cmd)  ((cmd)->flags & RESP_SPI_MASK)
/*
 * These are the command types.
 */
#define cmd_type(cmd)   ((cmd)->flags & CMD_MASK)

    Int32_t  retries;    /* max number of retries */
    Int32_t  err;

    struct MmcsdData_st *data;
    struct MmcsdReq_st *mrq;       /* associated request */
};

struct MmcsdReq_st {
    struct MmcsdData_st  *data;
    struct MmcsdCmd_st   *cmd;
    struct MmcsdCmd_st   *stop;
};

/*the following is response bit*/
#define R1_OUT_OF_RANGE     (1 << 31)   /* er, c */
#define R1_ADDRESS_ERROR    (1 << 30)   /* erx, c */
#define R1_BLOCK_LEN_ERROR  (1 << 29)   /* er, c */
#define R1_ERASE_SEQ_ERROR      (1 << 28)   /* er, c */
#define R1_ERASE_PARAM      (1 << 27)   /* ex, c */
#define R1_WP_VIOLATION     (1 << 26)   /* erx, c */
#define R1_CARD_IS_LOCKED   (1 << 25)   /* sx, a */
#define R1_LOCK_UNLOCK_FAILED   (1 << 24)   /* erx, c */
#define R1_COM_CRC_ERROR    (1 << 23)   /* er, b */
#define R1_ILLEGAL_COMMAND  (1 << 22)   /* er, b */
#define R1_CARD_ECC_FAILED  (1 << 21)   /* ex, c */
#define R1_CC_ERROR     (1 << 20)   /* erx, c */
#define R1_ERROR        (1 << 19)   /* erx, c */
#define R1_UNDERRUN     (1 << 18)   /* ex, c */
#define R1_OVERRUN      (1 << 17)   /* ex, c */
#define R1_CID_CSD_OVERWRITE    (1 << 16)   /* erx, c, CID/CSD overwrite */
#define R1_WP_ERASE_SKIP    (1 << 15)   /* sx, c */
#define R1_CARD_ECC_DISABLED    (1 << 14)   /* sx, a */
#define R1_ERASE_RESET      (1 << 13)   /* sr, c */
#define R1_STATUS(x)            (x & 0xFFFFE000)
#define R1_CURRENT_STATE(x) ((x & 0x00001E00) >> 9) /* sx, b (4 bits) */
#define R1_READY_FOR_DATA   (1 << 8)    /* sx, a */
#define R1_APP_CMD      (1 << 5)    /* sr, c */


#define R1_SPI_IDLE     (1 << 0)
#define R1_SPI_ERASE_RESET  (1 << 1)
#define R1_SPI_ILLEGAL_COMMAND  (1 << 2)
#define R1_SPI_COM_CRC      (1 << 3)
#define R1_SPI_ERASE_SEQ    (1 << 4)
#define R1_SPI_ADDRESS      (1 << 5)
#define R1_SPI_PARAMETER    (1 << 6)
/* R1 bit 7 is always zero */
#define R2_SPI_CARD_LOCKED  (1 << 8)
#define R2_SPI_WP_ERASE_SKIP    (1 << 9)    /* or lock/unlock fail */
#define R2_SPI_LOCK_UNLOCK_FAIL R2_SPI_WP_ERASE_SKIP
#define R2_SPI_ERROR        (1 << 10)
#define R2_SPI_CC_ERROR     (1 << 11)
#define R2_SPI_CARD_ECC_ERROR   (1 << 12)
#define R2_SPI_WP_VIOLATION (1 << 13)
#define R2_SPI_ERASE_PARAM  (1 << 14)
#define R2_SPI_OUT_OF_RANGE (1 << 15)   /* or CSD overwrite */
#define R2_SPI_CSD_OVERWRITE    R2_SPI_OUT_OF_RANGE

#define CARD_BUSY   0x80000000  /* Card Power up status bit */

/* R5 response bits */
#define R5_COM_CRC_ERROR    (1 << 15)
#define R5_ILLEGAL_COMMAND  (1 << 14)
#define R5_ERROR            (1 << 11)
#define R5_FUNCTION_NUMBER  (1 << 9)
#define R5_OUT_OF_RANGE     (1 << 8)
#define R5_STATUS(x)        (x & 0xCB00)
#define R5_IO_CURRENT_STATE(x)  ((x & 0x3000) >> 12)



/**
 * fls - find last (most-significant) bit set
 * @x: the word to search
 *
 * This is defined the same way as ffs.
 * Note fls(0) = 0, fls(1) = 1, fls(0x80000000) = 32.
 */

static inline Uint32_t _uHalFls(Uint32_t val)
{
    Uint32_t  bit = 32;

    if (!val)
        return 0;
    if (!(val & 0xffff0000u))
    {
        val <<= 16;
        bit -= 16;
    }
    if (!(val & 0xff000000u))
    {
        val <<= 8;
        bit -= 8;
    }
    if (!(val & 0xf0000000u))
    {
        val <<= 4;
        bit -= 4;
    }
    if (!(val & 0xc0000000u))
    {
        val <<= 2;
        bit -= 2;
    }
    if (!(val & 0x80000000u))
    {
        bit -= 1;
    }

    return bit;
}

#define MMCSD_HOST_PLUGED       0
#define MMCSD_HOST_UNPLUGED     1

int lHalMmcsdWaitCdChanged(Int32_t timeout);
void vHalMmcsdHostLock(struct MmcsdHost_st *host);
void vHalMmcsdHostUnlock(struct MmcsdHost_st *host);
void vHalMmcsdReqComplete(struct MmcsdHost_st *host);
void vHalMmcsdSendRequest(struct MmcsdHost_st *host, struct MmcsdReq_st *req);
Int32_t lHalMmcsdSendCmd(struct MmcsdHost_st *host, struct MmcsdCmd_st *cmd, int retries);
Int32_t lHalMmcsdGoIdle(struct MmcsdHost_st *host);
Int32_t lHalMmcsdSpiReadOcr(struct MmcsdHost_st *host, Int32_t high_capacity, Uint32_t *ocr);
Int32_t lHalMmcsdAllGetCid(struct MmcsdHost_st *host, Uint32_t *cid);
Int32_t lHalMmcsdGetCid(struct MmcsdHost_st *host, Uint32_t *cid);
Int32_t lHalMmcsdGetCsd(struct MmcsdCard_st *card, Uint32_t *csd);
Int32_t lHalMmcsdSelectCard(struct MmcsdCard_st *card);
Int32_t lHalMmcsdDeselectCards(struct MmcsdCard_st *host);
Int32_t lHalMmcsdSpiUseCrc(struct MmcsdHost_st *host, Int32_t use_crc);
void vHalMmcsdSetChipSelect(struct MmcsdHost_st *host, Int32_t mode);
void vHalMmcsdSetClock(struct MmcsdHost_st *host, Uint32_t clk);
void vHalMmcsdSetBusMode(struct MmcsdHost_st *host, Uint32_t mode);
void vHalMmcsdSetBusWidth(struct MmcsdHost_st *host, Uint32_t width);
void vHalMmcsdSetDataTimeout(struct MmcsdData_st *data, const struct MmcsdCard_st *card);
Uint32_t uHalMmcsdSelectVoltage(struct MmcsdHost_st *host, Uint32_t ocr);
void vHalMmcsdChange(struct MmcsdHost_st *host);
void vHalMmcsdDetect(void *param);
struct MmcsdHost_st *pHalMmcsdAllocHost(void);
void vHalMmcsdFreeHost(struct MmcsdHost_st *host);
int lHalMmcsdCoreInit(void);

Int32_t lHalMmcsdBlkProbe(struct MmcsdCard_st *card);
void vHalMmcsdBlkRemove(struct MmcsdCard_st *card);


#ifdef __cplusplus
}
#endif

#endif
