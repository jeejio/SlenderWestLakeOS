/*
 * Copyright (c) 2023, JEE Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2023-02-28      lk        first version
 */
#ifndef __HAL_AUDIO_DATAQUEUE_H__
#define __HAL_AUDIO_DATAQUEUE_H__

#include "swldef.h"

#define DATAQUEUE_EVENT_UNKNOWN   0x00
#define DATAQUEUE_EVENT_POP       0x01
#define DATAQUEUE_EVENT_PUSH      0x02
#define DATAQUEUE_EVENT_LWM       0x03

struct DataItem_st;
#define DATAQUEUE_SIZE(dq)        ((dq)->put_index - (dq)->get_index)
#define DATAQUEUE_EMPTY(dq)       ((dq)->size - DATAQUEUE_SIZE(dq))
/* data queue implementation */
struct DataQueue_st
{
    Uint16_t size;
    Uint16_t lwm;
    Bool_t   waiting_lwm;

    Uint16_t get_index;
    Uint16_t put_index;

    struct DataItem_st *queue;

    DevList_t suspended_push_list;
    DevList_t suspended_pop_list;

    /* event notify */
    void (*evt_notify)(struct DataQueue_st *queue, Uint32_t event);
};

/**
 * DataQueue for DeviceDriver
 */
Err_t lHalDataQueueInit(struct DataQueue_st *queue,
                            Uint16_t           size,
                            Uint16_t           lwm,
                            void (*evt_notify)(struct DataQueue_st *queue, Uint32_t event));
Err_t lHalDataQueuePush(struct DataQueue_st *queue,
                            const void           *data_ptr,
                            Size_t             data_size,
                            Int32_t            timeout);
Err_t lHalDataQueuePop(struct DataQueue_st *queue,
                           const void          **data_ptr,
                           Size_t            *size,
                           Int32_t            timeout);
Err_t lHalDataQueuePeak(struct DataQueue_st *queue,
                            const void          **data_ptr,
                            Size_t            *size);

#endif /* __HAL_AUDIO_DATAQUEUE_H__ */
