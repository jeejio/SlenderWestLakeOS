/*
 * Copyright (c) 2022, jeejio Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2023-1-10      zhengqian    first version
 */


#ifndef __SPI_BIT_OPS_H__
#define __SPI_BIT_OPS_H__

#include <freertos/device.h>

#ifdef __cplusplus
extern "C" {
#endif

struct SpiBitOps_st
{
    void *data;            /* private data for lowlevel routines */
    void (*const tog_sclk)(void *data);
    void (*const set_sclk)(void *data, Int32_t state);
    void (*const set_mosi)(void *data, Int32_t state);
    void (*const set_miso)(void *data, Int32_t state);
    Int32_t (*const get_sclk)(void *data);
    Int32_t (*const get_mosi)(void *data);
    Int32_t (*const get_miso)(void *data);

    void (*const dir_mosi)(void *data, Int32_t state);
    void (*const dir_miso)(void *data, Int32_t state);

    void (*const udelay)(Uint32_t us);
    Uint32_t delay_us;  /* sclk, mosi and miso line delay */
};

struct SpiBitObj_st
{
    struct SpiBus_st bus;
    struct SpiBitOps_st *ops;
    struct SpiConfiguration_st config;
};

Err_t lHalSpiBitAddBus(struct SpiBitObj_st *obj,
                            const char            *bus_name,
                            struct SpiBitOps_st *ops);

#ifdef __cplusplus
}
#endif

#endif
