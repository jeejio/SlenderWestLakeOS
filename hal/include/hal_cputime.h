/*
 * Copyright (c) 2018-2023, jeejio
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 */
#ifndef CPUTIME_H__
#define CPUTIME_H__

#include <stdint.h>

struct ClockCputimeOps_st
{
    uint32_t (*cputime_getres) (void);
    uint32_t (*cputime_gettime)(void);
};

uint32_t uHalClockCpuGetres(void);
uint32_t uHalClockCpuGettime(void);

uint32_t uHalClockCpuMicrosecond(uint32_t cpu_tick);
uint32_t uHalClockCpuMillisecond(uint32_t cpu_tick);

int lHalClockCpuSetops(const struct ClockCputimeOps_st *ops);

#endif
