/*
 * FreeRTOS V202203.00
 * Copyright (C) 2021 Amazon.com, Inc. or its affiliates.  All Rights Reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * http://aws.amazon.com/freertos
 * http://www.FreeRTOS.org
 */

/**
 * @file maopao_ble_gatts_demo.h
 * @brief Sample demo for a BLE GATT server
 */
#ifndef MAOPAO_BLE_WIFI_PROVISIONING_H_
#define MAOPAO_BLE_WIFI_PROVISIONING_H_

#include "freertos/FreeRTOS.h"
/* The config header is always included first. */
#include "hal_ble_os_config.h"
#include "hal_ble.h"
/**
 * @brief  GATT service, characteristics and descriptor UUIDs used by the sample.
 */
#define MAOPAO_WIFI_PROVISIONING_SERVICE_UUID     0xF120
#define MAOPAO_WIFI_PROVISIONING_CHAR1_UUID       0xF121
#define MAOPAO_WIFI_PROVISIONING_CHAR2_UUID       0xF122

#define MAOPAO_WIFI_PROVISIONING_CHAR_CFG_UUID     ( 0x2902 )

/**
 * @brief Enable Notification and Enable Indication values as defined by GATT spec.
 */
#define MAOPAO_WIFI_PROVISION_ENABLE_NOTIFICATION              ( 0x01 )
#define MAOPAO_WIFI_PROVISION_ENABLE_INDICATION                ( 0x02 )

/**
 * @brief Characteristics used by the GATT sample service.
 */
typedef enum
{
    eWifiProvisionService = 0,              /**< SGatt demo service. */
    eWifiProvisionCharWriteNoRespone,          
    eWifiProvisionCharIndication,             
    eWifiProvisionCharClientConfig,          /**< Client characteristic configuration descriptor used by the GATT client to enable notifications on Counter characteristic */
    eWifiProvisionNbAttributes
} eGattWifiProvisionAttributes_t;

/**
 * @brief Creates and starts FreeRTOS wIFI provisioning service
 *
 * @return true if the service is initialized successfully, false otherwise
 */
BaseType_t cHalBleWifiProvision_Init( void );

/**
 * @brief Cleanup wIFI provisioning service.
 *
 * @return true If the cleanup is successful false otherwise.
 */
BaseType_t cHalBleWifiProvision_Cleanup( void );

typedef void (* vHalBleWifiProvisionReceiveData)(BleWriteEventParams_t * pxWriteParam);

typedef void (* vHalBleWifiProvisionConnetcCb)(bool isConnected);

/**
 * @brief 用于注册一个 vHalBleRpcGattsReceiveDataCb.
 */
BaseType_t cHalWifiProvisioningGattsRegister( vHalBleWifiProvisionReceiveData  cb);
BaseType_t cHalWifiProvisioningConnectRegister( vHalBleWifiProvisionConnetcCb  cb);

void setIndicationSuccess(bool flag);
bool getIndicationSuccess(void);
void setIndicationFail(bool flag);
bool getIndicationFail(void);

typedef struct BleWifiProvision_Info
{
    uint16_t Connect_ID;
    uint32_t MTU_size;
    uint32_t ConnInterval;
    uint32_t Latency;
    uint32_t timeout;
    BTBdaddr_t remoteBdAddr;                  /**< Remote device address. */
    bool Notify_enable;
}BleWifiProvision_Info_t;

BleWifiProvision_Info_t * xHalWifiProvisioningGattsInfo(void);

#endif /* MAOPAO_BLE_WIFI_PROVISIONING_H_ */
