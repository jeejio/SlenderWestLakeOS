/*
 * Copyright (c) 2022, jeejio Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author         Notes
 * 2023-03-1                     first version
 */
#ifndef __HWTIMER_H__
#define __HWTIMER_H__

#include <swldef.h>

#ifdef __cplusplus
extern "C" {
#endif

/* Timer Control Command */
typedef enum
{
    HWTIMER_CTRL_FREQ_SET = DEVICE_CTRL_BASE(Timer) + 0x01,           /* set the count frequency */
    HWTIMER_CTRL_STOP = DEVICE_CTRL_BASE(Timer) + 0x02,               /* stop timer */
    HWTIMER_CTRL_INFO_GET = DEVICE_CTRL_BASE(Timer) + 0x03,           /* get a timer feature information */
    HWTIMER_CTRL_MODE_SET = DEVICE_CTRL_BASE(Timer) + 0x04,            /* Setting the timing mode(oneshot/period) */
    HWTIMER_CTRL_START_BY_TICK = DEVICE_CTRL_BASE(Timer) + 0x05,
    HWTIMER_CTRL_GET_TICK = DEVICE_CTRL_BASE(Timer) + 0x06
} HwtimerCtrl_t;

/* Timing Mode */
typedef enum
{
    HWTIMER_MODE_ONESHOT = 0x01,
    HWTIMER_MODE_PERIOD
} HwtimerMode_t;

/* Time Value */
typedef struct Hwtimerval_st
{
    Int32_t sec;      /* second */
    Int32_t usec;     /* microsecond */
} Hwtimerval_t;

#define HWTIMER_CNTMODE_UP      0x01 /* increment count mode */
#define HWTIMER_CNTMODE_DW      0x00 /* decreasing count mode */

struct HwtimerDevice_st;

struct HwtimerOps_st
{
    void (*init)(struct HwtimerDevice_st *timer, Uint32_t state);
    Err_t (*start)(struct HwtimerDevice_st *timer, Uint32_t cnt, HwtimerMode_t mode);
    void (*stop)(struct HwtimerDevice_st *timer);
    Uint32_t (*count_get)(struct HwtimerDevice_st *timer);
    Err_t (*control)(struct HwtimerDevice_st *timer, Uint32_t cmd, void *args);
};

/* Timer Feature Information */
struct HwtimerInfo_st
{
    Int32_t maxfreq;    /* the maximum count frequency timer support */
    Int32_t minfreq;    /* the minimum count frequency timer support */
    Uint32_t maxcnt;    /* counter maximum value */
    Uint8_t  cntmode;   /* count mode (inc/dec) */
};

typedef struct HwtimerDevice_st
{
    struct Device_st parent;
    const struct HwtimerOps_st *ops;
    const struct HwtimerInfo_st *info;

    Int32_t freq;                /* counting frequency set by the user */
    Int32_t overflow;            /* timer overflows */
    float period_sec;
    Int32_t cycles;              /* how many times will generate a timeout event after overflow */
    Int32_t reload;              /* reload cycles(using in period mode) */
    HwtimerMode_t mode;         /* timing mode(oneshot/period) */
} Hwtimer_t;

Err_t lHalDeviceHwtimerRegister(Hwtimer_t *timer, const char *name, void *user_data);
void vHalDeviceHwtimerIsr(Hwtimer_t *timer);

#ifdef __cplusplus
}
#endif

#endif
