/*
 * Copyright (c) 2023, jeejio Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2023-2-28       lk         the first version
 */

#ifndef __HAL_LPTIMER_H__
#define __HAL_LPTIMER_H__


#include <freertos/FreeRTOS.h>

#ifdef CONFIG_PM_ENABLE

#include <freertos/timers.h>
#include <swldef.h>

struct TmrTimerControl_st                  /* The old naming convention is used to prevent breaking kernel aware debuggers. */
{
    const char * pcTimerName;                   /*<< Text name.  This is not used by the kernel, it is included simply to make debugging easier. */ /*lint !e971 Unqualified char types are allowed for strings and single characters only. */
    ListItem_t xTimerListItem;                  /*<< Standard linked list item as used by all kernel features for event management. */
    TickType_t xTimerPeriodInTicks;             /*<< How quickly and often the timer expires. */
    void * pvTimerID;                           /*<< An ID to identify the timer.  This allows the timer to be identified when the same callback is used for multiple timers. */
    TimerCallbackFunction_t pxCallbackFunction; /*<< The function that will be called when the timer expires. */
    #if ( configUSE_TRACE_FACILITY == 1 )
        UBaseType_t uxTimerNumber;              /*<< An ID assigned by trace tools such as FreeRTOS+Trace */
    #endif
    uint8_t ucStatus;                           /*<< Holds bits to say if the timer was statically allocated or not, and if it is active or not. */
};

struct LPTimer_st
{
    struct TmrTimerControl_st *timer;
    DevList_t list;
};
typedef struct LPTimer_st *LPTimer_t;

void vHalLPTimerInit(LPTimer_t  timer,
                    const char * pcTimerName,
                    Tick_t   time,
                    const UBaseType_t uxAutoReload,
                    void * const pvTimerID,
                    TimerCallbackFunction_t pxCallbackFunction);

Err_t lHalLPTimerDetach(LPTimer_t timer);
Err_t lHalLPTimerStart(LPTimer_t timer);
Err_t lHalLPTimerStop(LPTimer_t timer);

Err_t lHalLPTimerControl(LPTimer_t timer, int cmd, void *arg);

Tick_t ulHalLPTimerNextTimeoutTick(void);

#endif /* CONFIG_PM_ENABLE */

#endif /* __HAL_LPTIMER_H__ */
