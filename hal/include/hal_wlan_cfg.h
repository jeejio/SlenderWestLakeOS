/*
 * Copyright (c) 2023, jeejio Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2023-02-28       lk         the first version
 */

#ifndef __HAL_WLAN_CFG_H__
#define __HAL_WLAN_CFG_H__

#include "freertos/device.h"
#include "freertos/FreeRTOS.h"
#include <hal_wlan_dev.h>

#ifdef __cplusplus
extern "C" {
#endif

#ifndef WLAN_CFG_INFO_MAX
#define WLAN_CFG_INFO_MAX    (3) /* min is 1 */
#endif

#define WLAN_CFG_MAGIC       (0x426f6d62)

struct WlanCfgInfo_st
{
    struct WlanInfo_st info;
    struct WlanKey_st key;
};

typedef int (*WlanWr_t)(void *buff, int len);

struct WlanCfgOps_st
{
    int (*read_cfg)(void *buff, int len);
    int (*get_len)(void);
    int (*write_cfg)(void *buff, int len);
};

void vHalWlanCfgInit(void);

void vHalWlanCfgSetOps(const struct WlanCfgOps_st *ops);

int lHalWlanCfgGetNum(void);

int lHalWlanCfgRead(struct WlanCfgInfo_st *cfg_info, int num);

int lHalWlanCfgReadIndex(struct WlanCfgInfo_st *cfg_info, int index);

Err_t lHalWlanCfgSave(struct WlanCfgInfo_st *cfg_info);

Err_t lHalWlanCfgCacheRefresh(void);

Err_t lHalWlanCfgCacheSave(void);

int lHalWlanCfgDeleteIndex(int index);

void vHalWlanCfgDeleteAll(void);

void vHalWlanCfgDump(void);

#ifdef __cplusplus
}
#endif

#endif /* __HAL_WLAN_CFG_H__ */
