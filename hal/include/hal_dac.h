/*
 * Copyright (c) 2022, jeejio Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2023-3-2                    first version
 */

#ifndef __DAC_H__
#define __DAC_H__
#include <swldef.h>

struct DacDevice_st;
struct DacOps_st
{
    Err_t (*disabled)(struct DacDevice_st *device, Uint32_t channel);
    Err_t (*enabled)(struct DacDevice_st *device, Uint32_t channel);
    Err_t (*convert)(struct DacDevice_st *device, Uint32_t channel, Uint32_t *value);
    Uint8_t (*get_resolution)(struct DacDevice_st *device);
};

struct DacDevice_st
{
    struct Device_st parent;
    const struct DacOps_st *ops;
};
typedef struct DacDevice_st *DacDevice_t;

typedef enum
{
    DAC_CMD_ENABLE = DEVICE_CTRL_BASE(DAC) + 0,
    DAC_CMD_DISABLE = DEVICE_CTRL_BASE(DAC) + 1,
    DAC_CMD_GET_RESOLUTION = DEVICE_CTRL_BASE(DAC) + 2,
} DacCmd_t;

Err_t lHalHwDacRegister(DacDevice_t dac,const char *name, const struct DacOps_st *ops, const void *user_data);

Err_t lHalDacWrite(DacDevice_t dev, Uint32_t channel, Uint32_t value);
Err_t lHalDacEnable(DacDevice_t dev, Uint32_t channel);
Err_t lHalDacDisable(DacDevice_t dev, Uint32_t channel);

#endif
