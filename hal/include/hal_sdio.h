/*
 * Copyright (c) 2022, jeejio Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2023-1-12      zhengqian    the first version
 */

#ifndef __SDIO_H__
#define __SDIO_H__

#include <freertos/device.h>
#include <hal_mmcsd_host.h>
#include <hal_mmcsd_card.h>
#include <hal_sdio_func_ids.h>

#ifdef __cplusplus
extern "C" {
#endif

/*
 * Card Common Control Registers (CCCR)
 */

#define SDIO_REG_CCCR_CCCR_REV      0x00

#define  SDIO_CCCR_REV_1_00 0   /* CCCR/FBR Version 1.00 */
#define  SDIO_CCCR_REV_1_10 1   /* CCCR/FBR Version 1.10 */
#define  SDIO_CCCR_REV_1_20 2   /* CCCR/FBR Version 1.20 */
#define  SDIO_CCCR_REV_3_00 3   /* CCCR/FBR Version 2.00 */

#define  SDIO_SDIO_REV_1_00 0   /* SDIO Spec Version 1.00 */
#define  SDIO_SDIO_REV_1_10 1   /* SDIO Spec Version 1.10 */
#define  SDIO_SDIO_REV_1_20 2   /* SDIO Spec Version 1.20 */
#define  SDIO_SDIO_REV_2_00 3   /* SDIO Spec Version 2.00 */

#define SDIO_REG_CCCR_SD_REV        0x01

#define  SDIO_SD_REV_1_01   0   /* SD Physical Spec Version 1.01 */
#define  SDIO_SD_REV_1_10   1   /* SD Physical Spec Version 1.10 */
#define  SDIO_SD_REV_2_00   2   /* SD Physical Spec Version 2.00 */

#define SDIO_REG_CCCR_IO_EN         0x02
#define SDIO_REG_CCCR_IO_RDY        0x03

#define SDIO_REG_CCCR_INT_EN        0x04    /* Function/Master Interrupt Enable */
#define SDIO_REG_CCCR_INT_PEND      0x05    /* Function Interrupt Pending */

#define SDIO_REG_CCCR_IO_ABORT      0x06    /* function abort/card reset */

#define SDIO_REG_CCCR_BUS_IF        0x07    /* bus interface controls */

#define  SDIO_BUS_WIDTH_1BIT        0x00
#define  SDIO_BUS_WIDTH_4BIT        0x02
#define  SDIO_BUS_ECSI              0x20    /* Enable continuous SPI interrupt */
#define  SDIO_BUS_SCSI              0x40    /* Support continuous SPI interrupt */

#define  SDIO_BUS_ASYNC_INT         0x20

#define  SDIO_BUS_CD_DISABLE        0x80   /* disable pull-up on DAT3 (pin 1) */

#define SDIO_REG_CCCR_CARD_CAPS     0x08

#define  SDIO_CCCR_CAP_SDC  0x01    /* can do CMD52 while data transfer */
#define  SDIO_CCCR_CAP_SMB  0x02    /* can do multi-block xfers (CMD53) */
#define  SDIO_CCCR_CAP_SRW  0x04    /* supports read-wait protocol */
#define  SDIO_CCCR_CAP_SBS  0x08    /* supports suspend/resume */
#define  SDIO_CCCR_CAP_S4MI 0x10    /* interrupt during 4-bit CMD53 */
#define  SDIO_CCCR_CAP_E4MI 0x20    /* enable ints during 4-bit CMD53 */
#define  SDIO_CCCR_CAP_LSC  0x40    /* low speed card */
#define  SDIO_CCCR_CAP_4BLS 0x80    /* 4 bit low speed card */

#define SDIO_REG_CCCR_CIS_PTR       0x09    /* common CIS pointer (3 bytes) */

/* Following 4 regs are valid only if SBS is set */
#define SDIO_REG_CCCR_BUS_SUSPEND   0x0c
#define SDIO_REG_CCCR_FUNC_SEL      0x0d
#define SDIO_REG_CCCR_EXEC_FLAG     0x0e
#define SDIO_REG_CCCR_READY_FLAG    0x0f

#define SDIO_REG_CCCR_FN0_BLKSIZE   0x10 /* 2bytes, 0x10~0x11 */

#define SDIO_REG_CCCR_POWER_CTRL    0x12

#define  SDIO_POWER_SMPC            0x01    /* Supports Master Power Control */
#define  SDIO_POWER_EMPC            0x02    /* Enable Master Power Control */

#define SDIO_REG_CCCR_SPEED         0x13

#define  SDIO_SPEED_SHS             0x01    /* Supports High-Speed mode */
#define  SDIO_SPEED_EHS             0x02    /* Enable High-Speed mode */

/*
 * Function Basic Registers (FBR)
 */

#define SDIO_REG_FBR_BASE(f)    ((f) * 0x100) /* base of function f's FBRs */

#define SDIO_REG_FBR_STD_FUNC_IF    0x00

#define  SDIO_FBR_SUPPORTS_CSA      0x40    /* supports Code Storage Area */
#define  SDIO_FBR_ENABLE_CSA        0x80    /* enable Code Storage Area */

#define SDIO_REG_FBR_STD_IF_EXT     0x01

#define SDIO_REG_FBR_POWER          0x02

#define  SDIO_FBR_POWER_SPS         0x01    /* Supports Power Selection */
#define  SDIO_FBR_POWER_EPS         0x02    /* Enable (low) Power Selection */

#define SDIO_REG_FBR_CIS            0x09    /* CIS pointer (3 bytes) */


#define SDIO_REG_FBR_CSA            0x0C    /* CSA pointer (3 bytes) */

#define SDIO_REG_FBR_CSA_DATA       0x0F

#define SDIO_REG_FBR_BLKSIZE        0x10    /* block size (2 bytes) */

/* SDIO CIS Tuple code */
#define CISTPL_NULL      0x00
#define CISTPL_CHECKSUM  0x10
#define CISTPL_VERS_1    0x15
#define CISTPL_ALTSTR    0x16
#define CISTPL_MANFID    0x20
#define CISTPL_FUNCID    0x21
#define CISTPL_FUNCE     0x22
#define CISTPL_SDIO_STD  0x91
#define CISTPL_SDIO_EXT  0x92
#define CISTPL_END       0xff

/* SDIO device id */
#define SDIO_ANY_FUNC_ID    0xff
#define SDIO_ANY_MAN_ID     0xffff
#define SDIO_ANY_PROD_ID    0xffff

struct SdioDeviceId_st
{
    Uint8_t   func_code;
    Uint16_t  manufacturer;
    Uint16_t  product;
};

struct SdioDriver_st
{
    char *name;
    Int32_t (*probe)(struct MmcsdCard_st *card);
    Int32_t (*remove)(struct MmcsdCard_st *card);
    struct SdioDeviceId_st *id;
};

Int32_t lHalSdioIoSendOpCond(struct MmcsdHost_st *host,
                                Uint32_t           ocr,
                                Uint32_t          *cmd5_resp);
Int32_t lHalSdioIoRwDirect(struct MmcsdCard_st *card,
                             Int32_t            rw,
                             Uint32_t           fn,
                             Uint32_t           reg_addr,
                             Uint8_t           *pdata,
                             Uint8_t            raw);
Int32_t lHalSdioIoRwExtended(struct MmcsdCard_st *card,
                               Int32_t            rw,
                               Uint32_t           fn,
                               Uint32_t           addr,
                               Int32_t            op_code,
                               Uint8_t           *buf,
                               Uint32_t           blocks,
                               Uint32_t           blksize);
Int32_t lHalSdioIoRwExtendedBlock(struct SdioFunction_st *func,
                              Int32_t               rw,
                              Uint32_t              addr,
                              Int32_t               op_code,
                              Uint8_t              *buf,
                              Uint32_t              len);
Uint8_t ucHalSdioIoReadb(struct SdioFunction_st *func,
                         Uint32_t              reg,
                         Int32_t              *err);
Int32_t lHalSdioIoWriteb(struct SdioFunction_st *func,
                          Uint32_t              reg,
                          Uint8_t               data);
Uint16_t usHalSdioIoReadw(struct SdioFunction_st *func,
                          Uint32_t              addr,
                          Int32_t              *err);
Int32_t lHalSdioIoWritew(struct SdioFunction_st *func,
                          Uint16_t              data,
                          Uint32_t              addr);
Uint32_t uHalSdioIoReadl(struct SdioFunction_st *func,
                          Uint32_t              addr,
                          Int32_t              *err);
Int32_t lHalSdioIoWritel(struct SdioFunction_st *func,
                          Uint32_t              data,
                          Uint32_t              addr);
Int32_t lHalSdioIoReadMultiFifoB(struct SdioFunction_st *func,
                                     Uint32_t              addr,
                                     Uint8_t              *buf,
                                     Uint32_t              len);
Int32_t lHalSdioIoWriteMultiFifoB(struct SdioFunction_st *func,
                                      Uint32_t              addr,
                                      Uint8_t              *buf,
                                      Uint32_t              len);
Int32_t lHalSdioIoReadMultiIncrB(struct SdioFunction_st *func,
                                     Uint32_t              addr,
                                     Uint8_t              *buf,
                                     Uint32_t              len);
Int32_t lHalSdioIoWriteMultiIncrB(struct SdioFunction_st *func,
                                      Uint32_t              addr,
                                      Uint8_t              *buf,
                                      Uint32_t              len);
Int32_t lHalInitSdio(struct MmcsdHost_st *host, Uint32_t ocr);
Int32_t lHalSdioAttachIrq(struct SdioFunction_st *func,
                           SdioIrqHandler_t   *handler);
Int32_t lHalSdioDetachIrq(struct SdioFunction_st *func);
void vHalSdioIrqWakeup(struct MmcsdHost_st *host);
Int32_t lHalSdioEnableFunc(struct SdioFunction_st *func);
Int32_t lHalSdioDisableFunc(struct SdioFunction_st *func);
void vHalSdioSetDrvdata(struct SdioFunction_st *func, void *data);
void* pHalSdioGetDrvdata(struct SdioFunction_st *func);
Int32_t lHalSdioSetBlockSize(struct SdioFunction_st *func,
                               Uint32_t              blksize);
Int32_t lHalSdioRegisterDriver(struct SdioDriver_st *driver);
Int32_t lHalSdioUnregisterDriver(struct SdioDriver_st *driver);
void vHalSdioInit(void);

#ifdef __cplusplus
}
#endif

#endif

