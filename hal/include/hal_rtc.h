/*
 * Copyright (c) 2022, jeejio Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2023-1-11      zhengqian    first version
 */

#ifndef __RTC_H__
#define __RTC_H__

#include <freertos/device.h>
#include <sys/time.h>

#ifdef __cplusplus
extern "C" {
#endif

#define DEVICE_CTRL_RTC_GET_TIME     (DEVICE_CTRL_BASE(RTC) + 0x01)              /**< get second time */
#define DEVICE_CTRL_RTC_SET_TIME     (DEVICE_CTRL_BASE(RTC) + 0x02)              /**< set second time */
#define DEVICE_CTRL_RTC_GET_TIMEVAL  (DEVICE_CTRL_BASE(RTC) + 0x03)              /**< get timeval for gettimeofday */
#define DEVICE_CTRL_RTC_SET_TIMEVAL  (DEVICE_CTRL_BASE(RTC) + 0x04)              /**< set timeval for gettimeofday */


struct RtcOps_st
{
    Err_t (*init)(void);
    Err_t (*get_secs)(time_t *sec);
    Err_t (*set_secs)(time_t *sec);
    Err_t (*get_timeval)(struct timeval *tv);
    Err_t (*set_timeval)(struct timeval *tv);
};

typedef struct RtcDevice_st
{
    struct Device_st parent;
    const struct RtcOps_st *ops;
} RtcDev_t;

Err_t lHalHwRtcRegister(RtcDev_t  *rtc,
                            const char    *name,
                            Uint32_t    flag,
                            void          *data);

Err_t lHalSetDate(Uint32_t year, Uint32_t month, Uint32_t day);
Err_t lHalSetTime(Uint32_t hour, Uint32_t minute, Uint32_t second);
Err_t lHalSetTimestamp(time_t timestamp);
Err_t lHalGetTimestamp(time_t *timestamp);

#ifdef __cplusplus
}
#endif

#endif /* __RTC_H__ */
