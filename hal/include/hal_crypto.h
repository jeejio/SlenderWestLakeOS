/*
 * Copyright (c) 2023, Jeejio Technology Co. Ltd
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2023-03-08     Alpha        First Verison
 *
 */
#ifndef __CRYPTO_H__
#define __CRYPTO_H__

#include "freertos/device.h"
#include "freertos/FreeRTOS.h"
#include <hal_hwcrypto.h>
#include <hal_hw_symmetric.h>
#include <hal_hw_rng.h>
#include <hal_hw_hash.h>
#include <hal_hw_crc.h>
#include <hal_hw_gcm.h>
#include <hal_hw_bignum.h>

#endif
