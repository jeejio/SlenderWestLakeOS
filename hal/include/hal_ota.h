/*
 * Copyright (c) 2023, Jeejio Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2023-03-27     None         the first version.
 */

#ifndef __HAL_OTA_H__
#define __HAL_OTA_H__
 #include"stdint.h"
// #include <sys/_stdint.h>
#if 0
char* pcHalOtaGetVersion(void);

int lHalOtaUpgrade(char *url, char *key, char *ivec);

int lHalOtaDownloadPre(void);

int lHalOtaGetState(void);
#endif

void lHalOtaInit(); //   ota初始化

int IHalOtaGetUpdateParSize(void); // 获取ota升级区域大小

int lHalOtaBegin(void) ;// 开始ota升级

int lHalOtaWriteFlash(uint8_t *data, uint16_t len); // 设备写入ota数据

int vHalOtaFinish(void); // ota完成

void vHalOtaRestart(void); // 设备重启

int lHalOtaFwVerify(char *data) ;// 固件合法性校验

#endif