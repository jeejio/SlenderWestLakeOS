/*
 * FreeRTOS Common V1.2.0
 * Copyright (C) 2020 Amazon.com, Inc. or its affiliates.  All Rights Reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * http://aws.amazon.com/freertos
 * http://www.FreeRTOS.org
 */

/**
 * @file iot_ble_linear_containers.h
 * @brief Declares and implements doubly-linked lists and queues.
 */

#ifndef MAOPAO_LINEAR_CONTAINERS_H_
#define MAOPAO_LINEAR_CONTAINERS_H_

/* The config header is always included first. */
#include "hal_ble_os_config.h"

/* Standard includes. */
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

/**
 * @defgroup linear_containers_datatypes_listqueue List and queue
 * @brief Structures that represent a list or queue.
 */

/**
 * @ingroup linear_containers_datatypes_listqueue
 * @brief Link member placed in structs of a list or queue.
 *
 * All elements in a list or queue must contain one of these members. The macro
 * #BleLink_Container can be used to calculate the starting address of the
 * link's container.
 */
typedef struct BleLink
{
    struct BleLink * pPrevious; /**< @brief Pointer to the previous element. */
    struct BleLink * pNext;     /**< @brief Pointer to the next element. */
} BleLink_t;

/**
 * @ingroup linear_containers_datatypes_listqueue
 * @brief Represents a doubly-linked list.
 */
typedef BleLink_t   BleListDouble_t;

/**
 * @ingroup linear_containers_datatypes_listqueue
 * @brief Represents a queue.
 */
typedef BleLink_t   BleDeQueue_t;

/**
 * @constants_page{linear_containers}
 * @constants_brief{linear containers library}
 *
 * @section linear_containers_constants_initializers Linear Containers Initializers
 * @brief Provides default values for initializing the linear containers data types.
 *
 * @snippet this define_linear_containers_initializers
 *
 * All user-facing data types of the linear containers library should be initialized
 * using one of the following.
 *
 * @warning Failure to initialize a linear containers data type with the appropriate
 * initializer may result in a runtime error!
 * @note The initializers may change at any time in future versions, but their
 * names will remain the same.
 */
#define MAOPAO_LINK_INITIALIZER           { 0 }                /**< @brief Initializer for an #BleLink_t. */
#define MAOPAO_LIST_DOUBLE_INITIALIZER    MAOPAO_LINK_INITIALIZER /**< @brief Initializer for an #BleListDouble_t. */
#define MAOPAO_DEQUEUE_INITIALIZER        MAOPAO_LINK_INITIALIZER /**< @brief Initializer for an #BleDeQueue_t. */

/**
 * @def BleContainers_Assert( expression )
 * @brief Assertion macro for the linear containers library.
 *
 * Set @ref MAOPAO_CONTAINERS_ENABLE_ASSERTS to `1` to enable assertions in the linear
 * containers library.
 *
 * @param[in] expression Expression to be evaluated.
 */
#if MAOPAO_CONTAINERS_ENABLE_ASSERTS == 1
    #ifndef BleContainers_Assert
        #include <assert.h>
        #define BleContainers_Assert( expression )    assert( expression )
    #endif
#else
    //#define BleContainers_Assert( expression )
#endif

/**
 * @brief Calculates the starting address of a containing struct.
 *
 * @param[in] type Type of the containing struct.
 * @param[in] pLink Pointer to a link member.
 * @param[in] linkName Name of the #BleLink_t in the containing struct.
 */
#define BleLink_Container( type, pLink, linkName ) \
    ( ( type * ) ( void * ) ( ( ( uint8_t * ) ( pLink ) ) - offsetof( type, linkName ) ) )

/**
 * @brief Iterates through all elements of a linear container.
 *
 * Container elements must not be freed or removed while iterating.
 *
 * @param[in] pStart The first element to iterate from.
 * @param[out] pLink Pointer to a container element.
 */
#define MaopaoContainers_ForEach( pStart, pLink ) \
    for( ( pLink ) = ( pStart )->pNext;        \
         ( pLink ) != ( pStart );              \
         ( pLink ) = ( pLink )->pNext )

/**
 * @brief Check if an #BleLink_t is linked in a list or queue.
 *
 * @param[in] pLink The link to check.
 *
 * @return `true` if `pCurrent` is linked in a list or queue; `false` otherwise.
 */
static inline bool cHalBleLink_IsLinked( const BleLink_t * const pLink )
{
    bool isLinked = false;

    if( pLink != NULL )
    {
        isLinked = ( pLink->pNext != NULL ) && ( pLink->pPrevious != NULL );
    }

    return isLinked;
}

/**
 * @brief Create a new doubly-linked list.
 *
 * This function initializes a new doubly-linked list. It must be called on an
 * uninitialized #BleListDouble_t before calling any other doubly-linked list
 * function. This function must not be called on an already-initialized
 * #BleListDouble_t.
 *
 * This function will not fail. The function @ref linear_containers_function_list_double_removeall
 * may be called to destroy a list.
 *
 * @param[in] pList Pointer to the memory that will hold the new doubly-linked list.
 */
static inline void vHalBleListDouble_Create( BleListDouble_t * const pList )
{
    /* This function must not be called with a NULL parameter. */
    BleContainers_Assert( pList != NULL );

    /* An empty list is a link pointing to itself. */
    pList->pPrevious = pList;
    pList->pNext = pList;
}

/**
 * @brief Return the number of elements contained in an #BleListDouble_t.
 *
 * @param[in] pList The doubly-linked list with the elements to count.
 *
 * @return The number of elements in the doubly-linked list.
 */
static inline size_t vHalBleListDouble_Count( const BleListDouble_t * const pList )
{
    size_t count = 0;

    if( pList != NULL )
    {
        /* Get the list head. */
        const BleLink_t * pCurrent = pList->pNext;

        /* Iterate through the list to count the elements. */
        while( pCurrent != pList )
        {
            count++;
            pCurrent = pCurrent->pNext;
        }
    }

    return count;
}

/**
 * @brief Check if a doubly-linked list is empty.
 *
 * @param[in] pList The doubly-linked list to check.
 *
 * @return `true` if the list is empty; `false` otherwise.
 */
static inline bool cHalBleListDouble_IsEmpty( const BleListDouble_t * const pList )
{
    /* An empty list is NULL link, or a link pointing to itself. */
    return( ( pList == NULL ) || ( pList->pNext == pList ) );
}

/**
 * @brief Return an #BleLink_t representing the first element in a doubly-linked list
 * without removing it.
 *
 * @param[in] pList The list to peek.
 *
 * @return Pointer to an #BleLink_t representing the element at the head of the
 * list; `NULL` if the list is empty. The macro #BleLink_Container may be used to
 * determine the address of the link's container.
 */
static inline BleLink_t * BleListDouble_PeekHead( const BleListDouble_t * const pList )
{
    BleLink_t * pHead = NULL;

    if( pList != NULL )
    {
        if( cHalBleListDouble_IsEmpty( pList ) == false )
        {
            pHead = pList->pNext;
        }
    }

    return pHead;
}

/**
 * @brief Return an #BleLink_t representing the last element in a doubly-linked
 * list without removing it.
 *
 * @param[in] pList The list to peek.
 *
 * @return Pointer to an #BleLink_t representing the element at the tail of the
 * list; `NULL` if the list is empty. The macro #BleLink_Container may be used to
 * determine the address of the link's container.
 */
static inline BleLink_t * xHalBleListDouble_PeekTail( const BleListDouble_t * const pList )
{
    BleLink_t * pTail = NULL;

    if( pList != NULL )
    {
        if( cHalBleListDouble_IsEmpty( pList ) == false )
        {
            pTail = pList->pPrevious;
        }
    }

    return pTail;
}

/**
 * @brief Insert an element at the head of a doubly-linked list.
 *
 * @param[in] pList The doubly-linked list that will hold the new element.
 * @param[in] pLink Pointer to the new element's link member.
 */
static inline void vHalBleListDouble_InsertHead( BleListDouble_t * const pList,
                                             BleLink_t * const pLink )
{
    /* This function must not be called with NULL parameters. */
    BleContainers_Assert( pList != NULL );
    BleContainers_Assert( pLink != NULL );

    /* Save current list head. */
    BleLink_t * pHead = pList->pNext;

    /* Place new element before list head. */
    pLink->pNext = pHead;
    pLink->pPrevious = pList;

    /* Assign new list head. */
    pHead->pPrevious = pLink;
    pList->pNext = pLink;
}

/**
 * @brief Insert an element at the tail of a doubly-linked list.
 *
 * @param[in] pList The double-linked list that will hold the new element.
 * @param[in] pLink Pointer to the new element's link member.
 */
static inline void vHalBleListDouble_InsertTail( BleListDouble_t * const pList,
                                             BleLink_t * const pLink )
{
    /* This function must not be called with NULL parameters. */
    BleContainers_Assert( pList != NULL );
    BleContainers_Assert( pLink != NULL );

    /* Save current list tail. */
    BleLink_t * pTail = pList->pPrevious;

    pLink->pNext = pList;
    pLink->pPrevious = pTail;

    pList->pPrevious = pLink;
    pTail->pNext = pLink;
}

/**
 * @brief Insert an element before another element in a doubly-linked list.
 *
 * @param[in] pElement The new element will be placed before this element.
 * @param[in] pLink Pointer to the new element's link member.
 */
static inline void vHalBleListDouble_InsertBefore( BleLink_t * const pElement,
                                               BleLink_t * const pLink )
{
    vHalBleListDouble_InsertTail( pElement, pLink );
}

/**
 * @brief Insert an element after another element in a doubly-linked list.
 *
 * @param[in] pElement The new element will be placed after this element.
 * @param[in] pLink Pointer to the new element's link member.
 */
static inline void vHalBleListDouble_InsertAfter( BleLink_t * const pElement,
                                              BleLink_t * const pLink )
{
    vHalBleListDouble_InsertHead( pElement, pLink );
}

/**
 * @brief Insert an element in a sorted doubly-linked list.
 *
 * Places an element into a list by sorting it into order. The function
 * `compare` is used to determine where to place the new element.
 *
 * @param[in] pList The list that will hold the new element.
 * @param[in] pLink Pointer to the new element's link member.
 * @param[in] compare Determines the order of the list. Returns a negative
 * value if its first argument is less than its second argument; returns
 * zero if its first argument is equal to its second argument; returns a
 * positive value if its first argument is greater than its second argument.
 * The parameters to this function are #BleLink_t, so the macro #BleLink_Container
 * may be used to determine the address of the link's container.
 */
static inline void vHalBleListDouble_InsertSorted( BleListDouble_t * const pList,
                                               BleLink_t * const pLink,
                                               int32_t ( * compare )( const BleLink_t * const, const BleLink_t * const ) )
{
    /* This function must not be called with NULL parameters. */
    BleContainers_Assert( pList != NULL );
    BleContainers_Assert( pLink != NULL );
    BleContainers_Assert( compare != NULL );

    /* Insert at head for empty list. */
    if( cHalBleListDouble_IsEmpty( pList ) == true )
    {
        vHalBleListDouble_InsertHead( pList, pLink );
    }
    else
    {
        bool inserted = false;
        BleLink_t * pCurrent = pList->pNext;

        /* Iterate through the list to find the correct position. */
        while( pCurrent != pList )
        {
            /* Comparing for '<' preserves the order of insertion. */
            if( compare( pLink, pCurrent ) < 0 )
            {
                vHalBleListDouble_InsertBefore( pCurrent, pLink );
                inserted = true;

                break;
            }

            pCurrent = pCurrent->pNext;
        }

        /* New element is greater than all elements in list. Insert at tail. */
        if( inserted == false )
        {
            vHalBleListDouble_InsertTail( pList, pLink );
        }
    }
}

/**
 * @brief Remove a single element from a doubly-linked list.
 *
 * @param[in] pLink The element to remove.
 */
static inline void vHalBleListDouble_Remove( BleLink_t * const pLink )
{
    /* This function must not be called with a NULL parameter. */
    BleContainers_Assert( pLink != NULL );

    /* This function must be called on a linked element. */
    BleContainers_Assert( cHalBleLink_IsLinked( pLink ) == true );

    pLink->pPrevious->pNext = pLink->pNext;
    pLink->pNext->pPrevious = pLink->pPrevious;
    pLink->pPrevious = NULL;
    pLink->pNext = NULL;
}

/**
 * @brief Remove the element at the head of a doubly-linked list.
 *
 * @param[in] pList The doubly-linked list that holds the element to remove.
 *
 * @return Pointer to an #BleLink_t representing the removed list head; `NULL`
 * if the list is empty. The macro #BleLink_Container may be used to determine
 * the address of the link's container.
 */
static inline BleLink_t * xHalBleListDouble_RemoveHead( BleListDouble_t * const pList )
{
    BleLink_t * pHead = NULL;

    if( cHalBleListDouble_IsEmpty( pList ) == false )
    {
        pHead = pList->pNext;
        vHalBleListDouble_Remove( pHead );
    }

    return pHead;
}

/**
 * @brief Remove the element at the tail of a doubly-linked list.
 *
 * @param[in] pList The doubly-linked list that holds the element to remove.
 *
 * @return Pointer to an #BleLink_t representing the removed list tail; `NULL`
 * if the list is empty. The macro #BleLink_Container may be used to determine
 * the address of the link's container.
 */
static inline BleLink_t * xHalBleListDouble_RemoveTail( BleListDouble_t * const pList )
{
    BleLink_t * pTail = NULL;

    if( cHalBleListDouble_IsEmpty( pList ) == false )
    {
        pTail = pList->pPrevious;
        vHalBleListDouble_Remove( pTail );
    }

    return pTail;
}

/**
 * @brief Remove all elements in a doubly-linked list.
 *
 * @param[in] pList The list to empty.
 * @param[in] freeElement A function to free memory used by each removed list
 * element. Optional; pass `NULL` to ignore.
 * @param[in] linkOffset Offset in bytes of a link member in its container, used
 * to calculate the pointer to pass to `freeElement`. This value should be calculated
 * with the C `offsetof` macro. This parameter is ignored if `freeElement` is `NULL`
 * or its value is `0`.
 */
static inline void vHalBleListDouble_RemoveAll( BleListDouble_t * const pList,
                                            void ( * freeElement )( void * ),
                                            size_t linkOffset )
{
    /* This function must not be called with a NULL pList parameter. */
    BleContainers_Assert( pList != NULL );

    /* Get the list head. */
    BleLink_t * pCurrent = pList->pNext;

    /* Iterate through the list and remove all elements. */
    while( pCurrent != pList )
    {
        /* Save a pointer to the next list element. */
        BleLink_t * pNext = pCurrent->pNext;

        /* Remove and free the current list element. */
        vHalBleListDouble_Remove( pCurrent );

        if( freeElement != NULL )
        {
            freeElement( ( ( uint8_t * ) pCurrent ) - linkOffset );
        }

        /* Move the iterating pointer to the next list element. */
        pCurrent = pNext;
    }
}

/**
 * @brief Search a doubly-linked list for the first matching element.
 *
 * If a match is found, the matching element is <b>not</b> removed from the list.
 * See @ref linear_containers_function_list_double_removefirstmatch for the function
 * that searches and removes.
 *
 * @param[in] pList The doubly-linked list to search.
 * @param[in] pStartPoint An element in `pList`. Only elements between this one and
 * the list tail are checked. Pass `NULL` to search from the beginning of the list.
 * @param[in] isMatch Function to determine if an element matches. Pass `NULL` to
 * search using the address `pMatch`, i.e. `element == pMatch`.
 * @param[in] pMatch If `isMatch` is `NULL`, each element in the list is compared
 * to this address to find a match. Otherwise, it is passed as the second argument
 * to `isMatch`.
 *
 * @return Pointer to an #BleLink_t representing the first matched element; `NULL`
 * if no match is found. The macro #BleLink_Container may be used to determine the
 * address of the link's container.
 */
static inline BleLink_t * xHalBleListDouble_FindFirstMatch( const BleListDouble_t * const pList,
                                                        const BleLink_t * const pStartPoint,
                                                        bool ( * isMatch )( const BleLink_t * const, void * ),
                                                        void * pMatch )
{
    /* The const must be cast away to match this function's return value. Nevertheless,
     * this function will respect the const-ness of pStartPoint. */
    BleLink_t * pCurrent = ( BleLink_t * ) pStartPoint;

    /* This function must not be called with a NULL pList parameter. */
    BleContainers_Assert( pList != NULL );

    /* Search starting from list head if no start point is given. */
    if( pStartPoint == NULL )
    {
        pCurrent = pList->pNext;
    }

    /* Iterate through the list to search for matches. */
    while( pCurrent != pList )
    {
        /* Call isMatch if provided. Otherwise, compare pointers. */
        if( isMatch != NULL )
        {
            if( isMatch( pCurrent, pMatch ) == true )
            {
                return pCurrent;
            }
        }
        else
        {
            if( pCurrent == pMatch )
            {
                return pCurrent;
            }
        }

        pCurrent = pCurrent->pNext;
    }

    /* No match found, return NULL. */
    return NULL;
}

/**
 * @brief Search a doubly-linked list for the first matching element and remove
 * it.
 *
 * An #BleLink_t may be passed as `pList` to start searching after the head of a
 * doubly-linked list.
 *
 * @param[in] pList The doubly-linked list to search.
 * @param[in] pStartPoint An element in `pList`. Only elements between this one and
 * the list tail are checked. Pass `NULL` to search from the beginning of the list.
 * @param[in] isMatch Function to determine if an element matches. Pass `NULL` to
 * search using the address `pMatch`, i.e. `element == pMatch`.
 * @param[in] pMatch If `isMatch` is `NULL`, each element in the list is compared
 * to this address to find a match. Otherwise, it is passed as the second argument
 * to `isMatch`.
 *
 * @return Pointer to an #BleLink_t representing the matched and removed element;
 * `NULL` if no match is found. The macro #BleLink_Container may be used to determine
 * the address of the link's container.
 */
static inline BleLink_t * xHalBleListDouble_RemoveFirstMatch( BleListDouble_t * const pList,
                                                          const BleLink_t * const pStartPoint,
                                                          bool ( * isMatch )( const BleLink_t *, void * ),
                                                          void * pMatch )
{
    BleLink_t * pMatchedElement = xHalBleListDouble_FindFirstMatch( pList,
                                                                pStartPoint,
                                                                isMatch,
                                                                pMatch );

    if( pMatchedElement != NULL )
    {
        vHalBleListDouble_Remove( pMatchedElement );
    }

    return pMatchedElement;
}

/**
 * @brief Remove all matching elements from a doubly-linked list.
 *
 * @param[in] pList The doubly-linked list to search.
 * @param[in] isMatch Function to determine if an element matches. Pass `NULL` to
 * search using the address `pMatch`, i.e. `element == pMatch`.
 * @param[in] pMatch If `isMatch` is `NULL`, each element in the list is compared
 * to this address to find a match. Otherwise, it is passed as the second argument
 * to `isMatch`.
 * @param[in] freeElement A function to free memory used by each removed list
 * element. Optional; pass `NULL` to ignore.
 * @param[in] linkOffset Offset in bytes of a link member in its container, used
 * to calculate the pointer to pass to `freeElement`. This value should be calculated
 * with the C `offsetof` macro. This parameter is ignored if `freeElement` is `NULL`
 * or its value is `0`.
 */
static inline void vHalBleListDouble_RemoveAllMatches( BleListDouble_t * const pList,
                                                   bool ( * isMatch )( const BleLink_t *, void * ),
                                                   void * pMatch,
                                                   void ( * freeElement )( void * ),
                                                   size_t linkOffset )
{
    BleLink_t * pMatchedElement = NULL, * pNextElement = NULL;

    /* Search the list for all matching elements. */
    do
    {
        pMatchedElement = xHalBleListDouble_FindFirstMatch( pList,
                                                        pMatchedElement,
                                                        isMatch,
                                                        pMatch );

        if( pMatchedElement != NULL )
        {
            /* Save pointer to next element. */
            pNextElement = pMatchedElement->pNext;

            /* Match found; remove and free. */
            vHalBleListDouble_Remove( pMatchedElement );

            if( freeElement != NULL )
            {
                freeElement( ( ( uint8_t * ) pMatchedElement ) - linkOffset );
            }

            /* Continue search from next element. */
            pMatchedElement = pNextElement;
        }
    } while( pMatchedElement != NULL );
}

/**
 * @brief Create a new queue.
 *
 * This function initializes a new double-ended queue. It must be called on an uninitialized
 * #BleDeQueue_t before calling any other queue function. This function must not be
 * called on an already-initialized #BleDeQueue_t.
 *
 * This function will not fail.
 *
 * @param[in] pQueue Pointer to the memory that will hold the new queue.
 */
static inline void vHalBleDeQueue_Create( BleDeQueue_t * const pQueue )
{
    vHalBleListDouble_Create( pQueue );
}

/**
 * @brief Return the number of elements contained in an #BleDeQueue_t.
 *
 * @param[in] pQueue The queue with the elements to count.
 *
 * @return The number of items elements in the queue.
 */
static inline size_t lHalBleDeQueue_Count( const BleDeQueue_t * const pQueue )
{
    return vHalBleListDouble_Count( pQueue );
}

/**
 * @brief Check if a queue is empty.
 *
 * @param[in] pQueue The queue to check.
 *
 * @return `true` if the queue is empty; `false` otherwise.
 *
 */
static inline bool cHalBleDeQueue_IsEmpty( const BleDeQueue_t * const pQueue )
{
    return cHalBleListDouble_IsEmpty( pQueue );
}

/**
 * @brief Return an #BleLink_t representing the element at the front of the queue
 * without removing it.
 *
 * @param[in] pQueue The queue to peek.
 *
 * @return Pointer to an #BleLink_t representing the element at the head of the
 * queue; `NULL` if the queue is empty. The macro #BleLink_Container may be used
 * to determine the address of the link's container.
 */
static inline BleLink_t * xHalBleDeQueue_PeekHead( const BleDeQueue_t * const pQueue )
{
    return BleListDouble_PeekHead( pQueue );
}

/**
 * @brief Return an #BleLink_t representing the element at the back of the queue
 * without removing it.
 *
 * @param[in] pQueue The queue to peek.
 *
 * @return Pointer to an #BleLink_t representing the element at the head of the
 * queue; `NULL` if the queue is empty. The macro #BleLink_Container may be used
 * to determine the address of the link's container.
 */
static inline BleLink_t * xHalBleDeQueue_PeekTail( const BleDeQueue_t * const pQueue )
{
    return xHalBleListDouble_PeekTail( pQueue );
}

/**
 * @brief Add an element at the head of the queue.
 *
 * @param[in] pQueue The queue that will hold the new element.
 * @param[in] pLink Pointer to the new element's link member.
 */
static inline void xHalBleDeQueue_EnqueueHead( BleDeQueue_t * const pQueue,
                                           BleLink_t * const pLink )
{
    vHalBleListDouble_InsertHead( pQueue, pLink );
}

/**
 * @brief Remove an element at the head of the queue.
 *
 * @param[in] pQueue The queue that holds the element to remove.
 *
 * @return Pointer to an #BleLink_t representing the removed queue element; `NULL`
 * if the queue is empty. The macro #BleLink_Container may be used to determine
 * the address of the link's container.
 */
static inline BleLink_t * xHalBleDeQueue_DequeueHead( BleDeQueue_t * const pQueue )
{
    return xHalBleListDouble_RemoveHead( pQueue );
}

/**
 * @brief Add an element at the tail of the queue.
 *
 * @param[in] pQueue The queue that will hold the new element.
 * @param[in] pLink Pointer to the new element's link member.
 */
static inline void vHalBleDeQueue_EnqueueTail( BleDeQueue_t * const pQueue,
                                           BleLink_t * const pLink )
{
    vHalBleListDouble_InsertTail( pQueue, pLink );
}

/**
 * @brief Remove an element at the tail of the queue.
 *
 * @param[in] pQueue The queue that holds the element to remove.
 *
 * @return Pointer to an #BleLink_t representing the removed queue element; `NULL`
 * if the queue is empty. The macro #BleLink_Container may be used to determine
 * the address of the link's container.
 */
static inline BleLink_t * xHalBleDeQueue_DequeueTail( BleDeQueue_t * const pQueue )
{
    return xHalBleListDouble_RemoveTail( pQueue );
}

/**
 * @brief Remove a single element from a queue.
 *
 * @param[in] pLink The element to remove.
 */
static inline void vHalBleDeQueue_Remove( BleLink_t * const pLink )
{
    vHalBleListDouble_Remove( pLink );
}

/**
 * @brief Remove all elements in a queue.
 *
 * @param[in] pQueue The queue to empty.
 * @param[in] freeElement A function to free memory used by each removed queue
 * element. Optional; pass `NULL` to ignore.
 * @param[in] linkOffset Offset in bytes of a link member in its container, used
 * to calculate the pointer to pass to `freeElement`. This value should be calculated
 * with the C `offsetof` macro. This parameter is ignored if `freeElement` is `NULL`
 * or its value is `0`.
 */
static inline void vHalBleDeQueue_RemoveAll( BleDeQueue_t * const pQueue,
                                         void ( * freeElement )( void * ),
                                         size_t linkOffset )
{
    vHalBleListDouble_RemoveAll( pQueue, freeElement, linkOffset );
}

/**
 * @brief Remove all matching elements from a queue.
 *
 * @param[in] pQueue The queue to search.
 * @param[in] isMatch Function to determine if an element matches. Pass `NULL` to
 * search using the address `pMatch`, i.e. `element == pMatch`.
 * @param[in] pMatch If `isMatch` is `NULL`, each element in the queue is compared
 * to this address to find a match. Otherwise, it is passed as the second argument
 * to `isMatch`.
 * @param[in] freeElement A function to free memory used by each removed queue
 * element. Optional; pass `NULL` to ignore.
 * @param[in] linkOffset Offset in bytes of a link member in its container, used
 * to calculate the pointer to pass to `freeElement`. This value should be calculated
 * with the C `offsetof` macro. This parameter is ignored if `freeElement` is `NULL`
 * or its value is `0`.
 */
static inline void vHalBleDeQueue_RemoveAllMatches( BleDeQueue_t * const pQueue,
                                                bool ( * isMatch )( const BleLink_t *, void * ),
                                                void * pMatch,
                                                void ( * freeElement )( void * ),
                                                size_t linkOffset )
{
    vHalBleListDouble_RemoveAllMatches( pQueue, isMatch, pMatch, freeElement, linkOffset );
}

#endif /* MAOPAO_LINEAR_CONTAINERS_H_ */
