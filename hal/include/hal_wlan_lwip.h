/*
 * Copyright (c) 2023, jeejio Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2023-02-28       lk         the first version
 */

#ifndef __HAL_WLAN_LWIP_H__
#define __HAL_WLAN_LWIP_H__

#include "freertos/device.h"
#include "freertos/FreeRTOS.h"
#include "timers.h"
#include "hal_wlan_prot.h"
#include "hal_workqueue.h"
#include "ethernetif_port.h"

#ifdef __cplusplus
extern "C" {
#endif

struct LwipProtDes_st
{
    struct WlanProt_st prot;
    struct eth_device eth;
    Int8_t connected_flag;
    TimerHandle_t timer;
    struct Work_st work;
};

int lHalWlanLwipInit(void);

#ifdef __cplusplus
}
#endif

#endif /* __HAL_WLAN_LWIP_H__ */
