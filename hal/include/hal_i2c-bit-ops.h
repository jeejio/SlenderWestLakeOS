/*
 * Copyright (c) 2023, Jeejio Technology Co. Ltd
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2023-03-08     Alpha        First Verison
 *
 */
#include <freertos/swldef.h>

#ifndef __I2C_BIT_OPS_H__
#define __I2C_BIT_OPS_H__

#ifdef __cplusplus
extern "C" {
#endif

struct I2cBitOps_st
{
    void *data;            /* private data for lowlevel routines */
    void (*set_sda)(void *data, Int32_t state);
    void (*set_scl)(void *data, Int32_t state);
    Int32_t (*get_sda)(void *data);
    Int32_t (*get_scl)(void *data);

    void (*udelay)(Uint32_t us);

    Uint32_t delay_us;  /* scl and sda line delay */
    Uint32_t timeout;   /* in tick */
};

Err_t lHalI2cBitAddBus(struct I2cBusDevice_st *bus,
                            const char               *bus_name);

#ifdef __cplusplus
}
#endif

#endif
