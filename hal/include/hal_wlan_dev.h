/*
 * Copyright (c) 2023, jeejio Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2023-02-28       lk         the first version
 */

#ifndef __HAL_WLAN_DEVICE_H__
#define __HAL_WLAN_DEVICE_H__

#include "freertos/FreeRTOS.h"
#include <freertos/semphr.h>
#include <swldef.h>
#include "hal_config.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef enum
{
    WLAN_MODE_NONE,
    WLAN_MODE_STATION,
    WLAN_MODE_AP,
    WLAN_MODE_MAX
} WlanMode_t;

typedef enum
{
    WLAN_CMD_MODE = 0x10,
    WLAN_CMD_SCAN,              /* trigger scanning (list cells) */
    WLAN_CMD_JOIN,
    WLAN_CMD_SOFTAP,            /* start soft-AP */
    WLAN_CMD_DISCONNECT,
    WLAN_CMD_AP_STOP,           /* stop soft-AP */
    WLAN_CMD_AP_DEAUTH,
    WLAN_CMD_SCAN_STOP,
    WLAN_CMD_GET_RSSI,          /* get sensitivity (dBm) */
    WLAN_CMD_SET_POWERSAVE,
    WLAN_CMD_GET_POWERSAVE,
    WLAN_CMD_CFG_PROMISC,       /* start/stop minitor */
    WLAN_CMD_CFG_FILTER,        /* start/stop frame filter */
    WLAN_CMD_CFG_MGNT_FILTER,   /* start/stop management frame filter */
    WLAN_CMD_SET_CHANNEL,
    WLAN_CMD_GET_CHANNEL,
    WLAN_CMD_SET_COUNTRY,
    WLAN_CMD_GET_COUNTRY,
    WLAN_CMD_SET_MAC,
    WLAN_CMD_GET_MAC,
    WLAN_CMD_GET_FAST_CONNECT_INFO,
    WLAN_CMD_FAST_CONNECT,
#ifdef WLAN_TRANSFER
    WLAN_CMD_INPUT,
    WLAN_CMD_OUTPUT,
#endif
} WlanCmd_t;

typedef enum
{
    WLAN_DEV_EVT_INIT_DONE = 0,
    WLAN_DEV_EVT_CONNECT,
    WLAN_DEV_EVT_CONNECT_FAIL,
    WLAN_DEV_EVT_DISCONNECT,
    WLAN_DEV_EVT_GOT_IP,
    WLAN_DEV_EVT_AP_START,
    WLAN_DEV_EVT_AP_STOP,
    WLAN_DEV_EVT_AP_ASSOCIATED,
    WLAN_DEV_EVT_AP_DISASSOCIATED,
    WLAN_DEV_EVT_AP_ASSOCIATE_FAILED,
    WLAN_DEV_EVT_SCAN_REPORT,
    WLAN_DEV_EVT_SCAN_DONE,
    WLAN_DEV_EVT_MAX,
} WlanDevEvent_t;

#define SHARED_ENABLED  0x00008000
#define WPA_SECURITY    0x00200000
#define WPA2_SECURITY   0x00400000
#define WPS_ENABLED     0x10000000
#define WEP_ENABLED     0x0001
#define TKIP_ENABLED    0x0002
#define AES_ENABLED     0x0004
#define WSEC_SWFLAG     0x0008

#define WLAN_FLAG_STA_ONLY    (0x1 << 0)
#define WLAN_FLAG_AP_ONLY     (0x1 << 1)

#ifndef WLAN_SSID_MAX_LENGTH
#define WLAN_SSID_MAX_LENGTH  (32)   /* SSID MAX LEN */
#endif

#ifndef WLAN_BSSID_MAX_LENGTH
#define WLAN_BSSID_MAX_LENGTH (6)    /* BSSID MAX LEN (default is 6) */
#endif

#ifndef WLAN_PASSWORD_MAX_LENGTH
#define WLAN_PASSWORD_MAX_LENGTH   (32)   /* PASSWORD MAX LEN*/
#endif

#ifndef WLAN_DEV_EVENT_NUM
#define WLAN_DEV_EVENT_NUM  (2)   /* EVENT GROUP MAX NUM */
#endif

/**
 * Enumeration of Wi-Fi security modes
 */
typedef enum
{
    SECURITY_OPEN           = 0,                                                /* Open security                           */
    SECURITY_WEP_PSK        = WEP_ENABLED,                                      /* WEP Security with open authentication   */
    SECURITY_WEP_SHARED     = (WEP_ENABLED | SHARED_ENABLED),                   /* WEP Security with shared authentication */
    SECURITY_WPA_TKIP_PSK   = (WPA_SECURITY  | TKIP_ENABLED),                   /* WPA Security with TKIP                  */
    SECURITY_WPA_AES_PSK    = (WPA_SECURITY  | AES_ENABLED),                    /* WPA Security with AES                   */
    SECURITY_WPA2_AES_PSK   = (WPA2_SECURITY | AES_ENABLED),                    /* WPA2 Security with AES                  */
    SECURITY_WPA2_TKIP_PSK  = (WPA2_SECURITY | TKIP_ENABLED),                   /* WPA2 Security with TKIP                 */
    SECURITY_WPA2_MIXED_PSK = (WPA2_SECURITY | AES_ENABLED | TKIP_ENABLED),     /* WPA2 Security with AES & TKIP           */
    SECURITY_WPS_OPEN       = WPS_ENABLED,                                      /* WPS with open security                  */
    SECURITY_WPS_SECURE     = (WPS_ENABLED | AES_ENABLED),                      /* WPS with AES security                   */
    SECURITY_UNKNOWN        = -1,                                               /* May be returned by scan function if security is unknown.
                                                                                    Do not pass this to the join function! */
} WlanSecurity_t;

typedef enum
{
    BAND_802_11_5GHZ  =  0,             /* Denotes 5GHz radio band   */
    BAND_802_11_2_4GHZ =  1,            /* Denotes 2.4GHz radio band */
    BAND_802_11_UNKNOWN = 0x7fffffff,   /* unknown */
} Band80211_t;

typedef enum
{
    COUNTRY_AFGHANISTAN,
    COUNTRY_ALBANIA,
    COUNTRY_ALGERIA,
    COUNTRY_AMERICAN_SAMOA,
    COUNTRY_ANGOLA,
    COUNTRY_ANGUILLA,
    COUNTRY_ANTIGUA_AND_BARBUDA,
    COUNTRY_ARGENTINA,
    COUNTRY_ARMENIA,
    COUNTRY_ARUBA,
    COUNTRY_AUSTRALIA,
    COUNTRY_AUSTRIA,
    COUNTRY_AZERBAIJAN,
    COUNTRY_BAHAMAS,
    COUNTRY_BAHRAIN,
    COUNTRY_BAKER_ISLAND,
    COUNTRY_BANGLADESH,
    COUNTRY_BARBADOS,
    COUNTRY_BELARUS,
    COUNTRY_BELGIUM,
    COUNTRY_BELIZE,
    COUNTRY_BENIN,
    COUNTRY_BERMUDA,
    COUNTRY_BHUTAN,
    COUNTRY_BOLIVIA,
    COUNTRY_BOSNIA_AND_HERZEGOVINA,
    COUNTRY_BOTSWANA,
    COUNTRY_BRAZIL,
    COUNTRY_BRITISH_INDIAN_OCEAN_TERRITORY,
    COUNTRY_BRUNEI_DARUSSALAM,
    COUNTRY_BULGARIA,
    COUNTRY_BURKINA_FASO,
    COUNTRY_BURUNDI,
    COUNTRY_CAMBODIA,
    COUNTRY_CAMEROON,
    COUNTRY_CANADA,
    COUNTRY_CAPE_VERDE,
    COUNTRY_CAYMAN_ISLANDS,
    COUNTRY_CENTRAL_AFRICAN_REPUBLIC,
    COUNTRY_CHAD,
    COUNTRY_CHILE,
    COUNTRY_CHINA,
    COUNTRY_CHRISTMAS_ISLAND,
    COUNTRY_COLOMBIA,
    COUNTRY_COMOROS,
    COUNTRY_CONGO,
    COUNTRY_CONGO_THE_DEMOCRATIC_REPUBLIC_OF_THE,
    COUNTRY_COSTA_RICA,
    COUNTRY_COTE_DIVOIRE,
    COUNTRY_CROATIA,
    COUNTRY_CUBA,
    COUNTRY_CYPRUS,
    COUNTRY_CZECH_REPUBLIC,
    COUNTRY_DENMARK,
    COUNTRY_DJIBOUTI,
    COUNTRY_DOMINICA,
    COUNTRY_DOMINICAN_REPUBLIC,
    COUNTRY_DOWN_UNDER,
    COUNTRY_ECUADOR,
    COUNTRY_EGYPT,
    COUNTRY_EL_SALVADOR,
    COUNTRY_EQUATORIAL_GUINEA,
    COUNTRY_ERITREA,
    COUNTRY_ESTONIA,
    COUNTRY_ETHIOPIA,
    COUNTRY_FALKLAND_ISLANDS_MALVINAS,
    COUNTRY_FAROE_ISLANDS,
    COUNTRY_FIJI,
    COUNTRY_FINLAND,
    COUNTRY_FRANCE,
    COUNTRY_FRENCH_GUINA,
    COUNTRY_FRENCH_POLYNESIA,
    COUNTRY_FRENCH_SOUTHERN_TERRITORIES,
    COUNTRY_GABON,
    COUNTRY_GAMBIA,
    COUNTRY_GEORGIA,
    COUNTRY_GERMANY,
    COUNTRY_GHANA,
    COUNTRY_GIBRALTAR,
    COUNTRY_GREECE,
    COUNTRY_GRENADA,
    COUNTRY_GUADELOUPE,
    COUNTRY_GUAM,
    COUNTRY_GUATEMALA,
    COUNTRY_GUERNSEY,
    COUNTRY_GUINEA,
    COUNTRY_GUINEA_BISSAU,
    COUNTRY_GUYANA,
    COUNTRY_HAITI,
    COUNTRY_HOLY_SEE_VATICAN_CITY_STATE,
    COUNTRY_HONDURAS,
    COUNTRY_HONG_KONG,
    COUNTRY_HUNGARY,
    COUNTRY_ICELAND,
    COUNTRY_INDIA,
    COUNTRY_INDONESIA,
    COUNTRY_IRAN_ISLAMIC_REPUBLIC_OF,
    COUNTRY_IRAQ,
    COUNTRY_IRELAND,
    COUNTRY_ISRAEL,
    COUNTRY_ITALY,
    COUNTRY_JAMAICA,
    COUNTRY_JAPAN,
    COUNTRY_JERSEY,
    COUNTRY_JORDAN,
    COUNTRY_KAZAKHSTAN,
    COUNTRY_KENYA,
    COUNTRY_KIRIBATI,
    COUNTRY_KOREA_REPUBLIC_OF,
    COUNTRY_KOSOVO,
    COUNTRY_KUWAIT,
    COUNTRY_KYRGYZSTAN,
    COUNTRY_LAO_PEOPLES_DEMOCRATIC_REPUBIC,
    COUNTRY_LATVIA,
    COUNTRY_LEBANON,
    COUNTRY_LESOTHO,
    COUNTRY_LIBERIA,
    COUNTRY_LIBYAN_ARAB_JAMAHIRIYA,
    COUNTRY_LIECHTENSTEIN,
    COUNTRY_LITHUANIA,
    COUNTRY_LUXEMBOURG,
    COUNTRY_MACAO,
    COUNTRY_MACEDONIA_FORMER_YUGOSLAV_REPUBLIC_OF,
    COUNTRY_MADAGASCAR,
    COUNTRY_MALAWI,
    COUNTRY_MALAYSIA,
    COUNTRY_MALDIVES,
    COUNTRY_MALI,
    COUNTRY_MALTA,
    COUNTRY_MAN_ISLE_OF,
    COUNTRY_MARTINIQUE,
    COUNTRY_MAURITANIA,
    COUNTRY_MAURITIUS,
    COUNTRY_MAYOTTE,
    COUNTRY_MEXICO,
    COUNTRY_MICRONESIA_FEDERATED_STATES_OF,
    COUNTRY_MOLDOVA_REPUBLIC_OF,
    COUNTRY_MONACO,
    COUNTRY_MONGOLIA,
    COUNTRY_MONTENEGRO,
    COUNTRY_MONTSERRAT,
    COUNTRY_MOROCCO,
    COUNTRY_MOZAMBIQUE,
    COUNTRY_MYANMAR,
    COUNTRY_NAMIBIA,
    COUNTRY_NAURU,
    COUNTRY_NEPAL,
    COUNTRY_NETHERLANDS,
    COUNTRY_NETHERLANDS_ANTILLES,
    COUNTRY_NEW_CALEDONIA,
    COUNTRY_NEW_ZEALAND,
    COUNTRY_NICARAGUA,
    COUNTRY_NIGER,
    COUNTRY_NIGERIA,
    COUNTRY_NORFOLK_ISLAND,
    COUNTRY_NORTHERN_MARIANA_ISLANDS,
    COUNTRY_NORWAY,
    COUNTRY_OMAN,
    COUNTRY_PAKISTAN,
    COUNTRY_PALAU,
    COUNTRY_PANAMA,
    COUNTRY_PAPUA_NEW_GUINEA,
    COUNTRY_PARAGUAY,
    COUNTRY_PERU,
    COUNTRY_PHILIPPINES,
    COUNTRY_POLAND,
    COUNTRY_PORTUGAL,
    COUNTRY_PUETO_RICO,
    COUNTRY_QATAR,
    COUNTRY_REUNION,
    COUNTRY_ROMANIA,
    COUNTRY_RUSSIAN_FEDERATION,
    COUNTRY_RWANDA,
    COUNTRY_SAINT_KITTS_AND_NEVIS,
    COUNTRY_SAINT_LUCIA,
    COUNTRY_SAINT_PIERRE_AND_MIQUELON,
    COUNTRY_SAINT_VINCENT_AND_THE_GRENADINES,
    COUNTRY_SAMOA,
    COUNTRY_SANIT_MARTIN_SINT_MARTEEN,
    COUNTRY_SAO_TOME_AND_PRINCIPE,
    COUNTRY_SAUDI_ARABIA,
    COUNTRY_SENEGAL,
    COUNTRY_SERBIA,
    COUNTRY_SEYCHELLES,
    COUNTRY_SIERRA_LEONE,
    COUNTRY_SINGAPORE,
    COUNTRY_SLOVAKIA,
    COUNTRY_SLOVENIA,
    COUNTRY_SOLOMON_ISLANDS,
    COUNTRY_SOMALIA,
    COUNTRY_SOUTH_AFRICA,
    COUNTRY_SPAIN,
    COUNTRY_SRI_LANKA,
    COUNTRY_SURINAME,
    COUNTRY_SWAZILAND,
    COUNTRY_SWEDEN,
    COUNTRY_SWITZERLAND,
    COUNTRY_SYRIAN_ARAB_REPUBLIC,
    COUNTRY_TAIWAN_PROVINCE_OF_CHINA,
    COUNTRY_TAJIKISTAN,
    COUNTRY_TANZANIA_UNITED_REPUBLIC_OF,
    COUNTRY_THAILAND,
    COUNTRY_TOGO,
    COUNTRY_TONGA,
    COUNTRY_TRINIDAD_AND_TOBAGO,
    COUNTRY_TUNISIA,
    COUNTRY_TURKEY,
    COUNTRY_TURKMENISTAN,
    COUNTRY_TURKS_AND_CAICOS_ISLANDS,
    COUNTRY_TUVALU,
    COUNTRY_UGANDA,
    COUNTRY_UKRAINE,
    COUNTRY_UNITED_ARAB_EMIRATES,
    COUNTRY_UNITED_KINGDOM,
    COUNTRY_UNITED_STATES,
    COUNTRY_UNITED_STATES_REV4,
    COUNTRY_UNITED_STATES_NO_DFS,
    COUNTRY_UNITED_STATES_MINOR_OUTLYING_ISLANDS,
    COUNTRY_URUGUAY,
    COUNTRY_UZBEKISTAN,
    COUNTRY_VANUATU,
    COUNTRY_VENEZUELA,
    COUNTRY_VIET_NAM,
    COUNTRY_VIRGIN_ISLANDS_BRITISH,
    COUNTRY_VIRGIN_ISLANDS_US,
    COUNTRY_WALLIS_AND_FUTUNA,
    COUNTRY_WEST_BANK,
    COUNTRY_WESTERN_SAHARA,
    COUNTRY_WORLD_WIDE_XX,
    COUNTRY_YEMEN,
    COUNTRY_ZAMBIA,
    COUNTRY_ZIMBABWE,
    COUNTRY_UNKNOWN
} CountryCode_t;

struct WlanDevice_st;
struct WlanBuff_st;

typedef void (*WlanDevEventHandler_t)(struct WlanDevice_st *device, WlanDevEvent_t event, struct WlanBuff_st *buff, void *parameter);

typedef void (*WlanPormiscCallback_t)(struct WlanDevice_st *device, void *data, int len);

typedef void (*WlanMgntFilterCallback_t)(struct WlanDevice_st *device, void *data, int len);

struct WlanSsid_st
{
    Uint8_t len;
    Uint8_t val[WLAN_SSID_MAX_LENGTH + 1];
};
typedef struct WlanSsid_st WlanSsid_t;

struct WlanKey_st
{
    Uint8_t len;
    Uint8_t val[WLAN_PASSWORD_MAX_LENGTH + 1];
};
typedef struct WlanKey_st WlanKey_t;

#define INVALID_INFO(_info)       do {    \
                                        memset((_info), 0, sizeof(struct WlanInfo_st)); \
                                        (_info)->band = BAND_802_11_UNKNOWN; \
                                        (_info)->security = SECURITY_UNKNOWN; \
                                        (_info)->channel = -1; \
                                    } while(0)

#define SSID_SET(_info, _ssid)    do {    \
                                        strncpy((char *)(_info)->ssid.val, (_ssid), WLAN_SSID_MAX_LENGTH); \
                                        (_info)->ssid.len = strlen((char *)(_info)->ssid.val); \
                                    } while(0)

struct WlanInfo_st
{
    /* security type */
    WlanSecurity_t security;
    /* 2.4G/5G */
    Band80211_t band;
    /* maximal data rate */
    Uint32_t datarate;
    /* radio channel */
    Int16_t channel;
    /* signal strength */
    Int8_t  rssi;
    /* ssid */
    WlanSsid_t ssid;
    /* hwaddr */
    Uint8_t bssid[WLAN_BSSID_MAX_LENGTH];
    Uint8_t hidden;
};

struct WlanBuff_st
{
    void *data;
    Int32_t len;
};

struct FilterPattern_st
{
    Uint16_t offset;     /* Offset in bytes to start filtering (referenced to the start of the ethernet packet) */
    Uint16_t mask_size;  /* Size of the mask in bytes */
    Uint8_t *mask;       /* Pattern mask bytes to be ANDed with the pattern eg. "\xff00" (must be in network byte order) */
    Uint8_t *pattern;    /* Pattern bytes used to filter eg. "\x0800"  (must be in network byte order) */
};

typedef enum
{
    POSITIVE_MATCHING  = 0, /* Receive the data matching with this pattern and discard the other data  */
    NEGATIVE_MATCHING  = 1  /* Discard the data matching with this pattern and receive the other data */
} FilterRule_t;

struct WlanTransInputFuncArgs_st {
    void *wlanif;
    void *buffer;
    size_t len;
    void* l2_buff;
};

struct WlanTransOutputFuncArgs_st {
    void *wlanif;
    void *pbuf;
};

struct WlanFilter_st
{
    struct FilterPattern_st patt;
    FilterRule_t rule;
    Uint8_t enable;
};

struct WlanDevEventDesc_st
{
    WlanDevEventHandler_t handler;
    void *parameter;
};

struct WlanDevice_st
{
    struct Device_st device;
    WlanMode_t mode;
    SemaphoreHandle_t lock;
    struct WlanDevEventDesc_st handler_table[WLAN_DEV_EVT_MAX][WLAN_DEV_EVENT_NUM];
    WlanPormiscCallback_t pormisc_callback;
    WlanMgntFilterCallback_t mgnt_filter_callback;
    const struct WlanDevOps_st *ops;
    Uint32_t flags;
    struct netdev *netdev;
    void *prot;
    void *user_data;
#ifdef WLAN_TRANSFER
    void *pWlanTrans;
#endif

};

struct StaInfo_st
{
    WlanSsid_t ssid;
    WlanKey_t key;
    Uint8_t bssid[6];
    Uint16_t channel;
    WlanSecurity_t security;
};

struct ApInfo_st
{
    WlanSsid_t ssid;
    WlanKey_t key;
    Bool_t hidden;
    Uint16_t channel;
    WlanSecurity_t security;
};

struct ScanInfo_st
{
    WlanSsid_t ssid;
    Uint8_t bssid[6];
    Int16_t channel_min;
    Int16_t channel_max;
    Bool_t passive;
};

struct WlanDevOps_st
{
    Err_t (*wlan_init)(struct WlanDevice_st *wlan);
    Err_t (*wlan_mode)(struct WlanDevice_st *wlan, WlanMode_t mode);
    Err_t (*wlan_scan)(struct WlanDevice_st *wlan, struct ScanInfo_st *scan_info);
    Err_t (*wlan_join)(struct WlanDevice_st *wlan, struct StaInfo_st *sta_info);
    Err_t (*wlan_softap)(struct WlanDevice_st *wlan, struct ApInfo_st *ap_info);
    Err_t (*wlan_disconnect)(struct WlanDevice_st *wlan);
    Err_t (*wlan_ap_stop)(struct WlanDevice_st *wlan);
    Err_t (*wlan_ap_deauth)(struct WlanDevice_st *wlan, Uint8_t mac[]);
    Err_t (*wlan_scan_stop)(struct WlanDevice_st *wlan);
    int (*wlan_get_rssi)(struct WlanDevice_st *wlan);
    Err_t (*wlan_set_powersave)(struct WlanDevice_st *wlan, int level);
    int (*wlan_get_powersave)(struct WlanDevice_st *wlan);
    Err_t (*wlan_cfg_promisc)(struct WlanDevice_st *wlan, Bool_t start);
    Err_t (*wlan_cfg_filter)(struct WlanDevice_st *wlan, struct WlanFilter_st *filter);
    Err_t (*wlan_cfg_mgnt_filter)(struct WlanDevice_st *wlan, Bool_t start);
    Err_t (*wlan_set_channel)(struct WlanDevice_st *wlan, int channel);
    int (*wlan_get_channel)(struct WlanDevice_st *wlan);
    Err_t (*wlan_set_country)(struct WlanDevice_st *wlan, CountryCode_t country_code);
    CountryCode_t (*wlan_get_country)(struct WlanDevice_st *wlan);
    Err_t (*wlan_set_mac)(struct WlanDevice_st *wlan, Uint8_t mac[]);
    Err_t (*wlan_get_mac)(struct WlanDevice_st *wlan, Uint8_t mac[]);
    int (*wlan_recv)(struct WlanDevice_st *wlan, void *buff, int len);
    int (*wlan_send)(struct WlanDevice_st *wlan, void *buff, int len);
    int (*wlan_send_raw_frame)(struct WlanDevice_st *wlan, void *buff, int len);
    int (*wlan_get_fast_info)(void *data);
    Err_t (*wlan_fast_connect)(void *data,Int32_t len);
    Err_t (*wlan_drv_input)(struct WlanDevice_st *wlan, void*args);
    Err_t (*wlan_drv_output)(struct WlanDevice_st *wlan, void* args);
};

/*
 * wlan device init
 */
Err_t lHalWlanDevInit(struct WlanDevice_st *device, WlanMode_t mode);

/*
 * wlan device station interface
 */
Err_t lHalWlanDevConnect(struct WlanDevice_st *device, struct WlanInfo_st *info, const char *password, int password_len);
Err_t lHalWlanDevFastConnect(struct WlanDevice_st *device, struct WlanInfo_st *info, const char *password, int password_len);
Err_t lHalWlanDevDisconnect(struct WlanDevice_st *device);
int lHalWlanDevGetRssi(struct WlanDevice_st *device);

/*
 * wlan device ap interface
 */
Err_t lHalWlanDevApStart(struct WlanDevice_st *device, struct WlanInfo_st *info, const char *password, int password_len);
Err_t lHalWlanDevApStop(struct WlanDevice_st *device);
Err_t lHalWlanDevApDeauth(struct WlanDevice_st *device, Uint8_t mac[6]);

/*
 * wlan device scan interface
 */
Err_t lHalWlanDevScan(struct WlanDevice_st *device, struct WlanInfo_st *info);
Err_t lHalWlanDevScanStop(struct WlanDevice_st *device);

/*
 * wlan device mac interface
 */
Err_t lHalWlanDevGetMac(struct WlanDevice_st *device, Uint8_t mac[6]);
Err_t lHalWlanDevSetMac(struct WlanDevice_st *device, Uint8_t mac[6]);

/*
 * wlan device powersave interface
 */
Err_t lHalWlanDevSetPowersave(struct WlanDevice_st *device, int level);
int lHalWlanDevGetPowersave(struct WlanDevice_st *device);

/*
 * wlan device event interface
 */
Err_t lHalWlanDevRegisterEventHandler(struct WlanDevice_st *device, WlanDevEvent_t event, WlanDevEventHandler_t handler, void *parameter);
Err_t lHalWlanDevUnregisterEventHandler(struct WlanDevice_st *device, WlanDevEvent_t event, WlanDevEventHandler_t handler);
void vHalWlanDevIndicateEventHandle(struct WlanDevice_st *device, WlanDevEvent_t event, struct WlanBuff_st *buff);

/*
 * wlan device promisc interface
 */
Err_t lHalWlanDevEnterPromisc(struct WlanDevice_st *device);
Err_t lHalWlanDevExitPromisc(struct WlanDevice_st *device);
Err_t lHalWlanDevSetPromiscCallback(struct WlanDevice_st *device, WlanPormiscCallback_t callback);
void vHalWlanDevPromiscHandler(struct WlanDevice_st *device, void *data, int len);

/*
 * wlan device filter interface
 */
Err_t lHalWlanDevCfgFilter(struct WlanDevice_st *device, struct WlanFilter_st *filter);

/*
 * wlan device channel interface
 */
Err_t lHalWlanDevSetChannel(struct WlanDevice_st *device, int channel);
int lHalWlanDevGetChannel(struct WlanDevice_st *device);

/*
 * wlan device country interface
 */
Err_t lHalWlanDevSetCountry(struct WlanDevice_st *device, CountryCode_t country_code);
CountryCode_t xHalWlanDevGetCountry(struct WlanDevice_st *device);

/*
 * wlan device datat transfer interface
 */
Err_t lHalWlanDevReportData(struct WlanDevice_st *device, void *buff, int len);

/*
 * wlan device register interface
 */
Err_t lHalWlanDevRegister(struct WlanDevice_st *wlan, const char *name,
    const struct WlanDevOps_st *ops, Uint32_t flag, void *user_data);

#ifdef __cplusplus
}
#endif

#endif /* __HAL_WLAN_DEVICE_H__ */
