/*
 * FreeRTOS V202203.00
 * Copyright (C) 2021 Amazon.com, Inc. or its affiliates.  All Rights Reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * http://aws.amazon.com/freertos
 * http://www.FreeRTOS.org
 */

/**
 * @file jee_ble_rpc_over_gatts.H
 * @brief Jeejio Rpc_over_gatts
 */
#ifndef MAOPAO_BLE_RPC_OVER_GATT_H_
#define MAOPAO_BLE_RPC_OVER_GATT_H_

#include "freertos/FreeRTOS.h"
/* The config header is always included first. */
#include "hal_ble_os_config.h"

/**
 * @brief  GATT service, characteristics and descriptor UUIDs used by the sample.
 */
#define MAOPAO_RPC_BLE_SERVICE_UUID                        0xF110
#define MAOPAO_RPC_BLE_DATA_UUID                           0xF111             /* This att is used to transmit Json-RPC data */
#define MAOPAO_RPC_BLE_CONTROL_WRITE_NO_RESPOSE_UUID       0xF112             /* This att is used for sending Json-RPC data length before app transmit Json-RPC data to device*/
#define MAOPAO_RPC_BLE_CONTROL_NOTIFY_UUID                 0xF113             /* This att is used for notify or read remaining length When device transmit Json-RPC data to APP*/

#define MAOPAO_RPC_CLIENT_CHAR_CFG_UUID                  ( 0x2902 )

/**
 * @brief Enable Notification and Enable Indication values as defined by GATT spec.
 */
#define MAOPAO_RPC_ENABLE_NOTIFICATION              ( 0x01 )
#define MAOPAO_RPC_ENABLE_INDICATION                ( 0x02 )


#define MAOPAO_RPC_GATTS_WRITE_MAX_LEN   (1024)     /*写入一包数据帧最大限制在1024bytes*/
#define MAOPAO_RPC_GATTS_READ_MAX_LEN    (1024)     /*读取一包数据帧最大限制在1024bytes*/

#define BLE_ERROR_DELAY_TIME  500   /* 单位：ms  ，说明：当产生错误的时候，延迟/超时多久进行上报 */

#if 0
#define BLE_ERROR_TRANSPORT_TIMEOUT 1103
#define BLE_ERROR_LENGTH_MISMATCHED 1154
#define BLE_ERROR_INVALID_PARAMETER  1153  /*无效参数*/

#define BLE_ERROR_INVALID_PARAMETER_MSG "invalid parameter"
#define BLE_ERROR_TRANSPORT_TIMEOUT_MSG "transport timeout"
#define BLE_ERROR_LENGTH_MISMATCHED_MSG "length mismatched"
#endif

/**
 * @brief Characteristics used by the GATT sample service.
 */
typedef enum
{
    eRpcGattService = 0,      /**< RPC GATT service. */
    eRpcGattCharData,
    eRpcGattCharWriteNoRespond,
    eRpcGattCharNotify,
    eRpcGattClientCharConfig,
    eRpcGattNbAttributes
} eRpcGattAttributes_t;


/**
 * @brief Rpc Gatts Status
 * 
 *  所有状态都可直接跳转到eRpcGattsStatus_WAIT_WriteData，
 *  1.eRpcGattsStatus_WriteData 禁止与 eRpcGattsStatus_ReadData 之间跳转。
 *  2.eRpcGattsStatus_IDLE 跳转 eRpcGattsStatus_WAIT_WriteData      ：表示收到期望数据长度
 *  3.eRpcGattsStatus_WriteData 跳转 eRpcGattsStatus_WAIT_WriteData ：表示APP重新write数据帧，之前的数据帧丢弃。
 *  4.eRpcGattsStatus_ReadData 跳转 eRpcGattsStatus_WAIT_WriteData  ：表示暂停等待读取的动作，等待读取的数据将被释放。再回到 1。
 *  5.eRpcGattsStatus_WAIT_WriteData 跳转 eRpcGattsStatus_WAIT_WriteData  ：表示重置期望数据长度。
 */
typedef enum
{
    eRpcGattsStatus_IDLE = 0,        /**< 空闲状态： 空闲状态或者发生错误，比如：期望数据长度小于实际收到的数据帧长度等等*/
    eRpcGattsStatus_WAIT_WriteData,  /**< 收到期望长度，等待接收数据 */
    eRpcGattsStatus_WriteData,       /**< 开始接收APP下发的数据. */
    eRpcGattsStatus_ReadData,        /**< 开始等待APP读取数据. */
} eRpcGattsStatus_t;

/**
 * @brief Parameters for RPC gatts.
 */
typedef struct
{
    uint32_t expected_flen;             /* 期望即将写入的数据长度 */ 
    bool   receiving_frame_malloc;      /* 0 ：receiving_frame 为空指针  ，1：receiving_frame已分配内存 */                
    uint8_t *receiving_frame;           /* 缓存 APP 已经下发的数据帧，*/       
    uint32_t receive_frame_len;         /* 记录 APP 当前包下发的数据长度，*/      
    uint32_t receive_offset;            /* 记录 APP 之前下发的数据产长度，当 （receive_offset + receive_frame_len）== expected_flen 证明数据帧已经按预期下发完成*/  
    bool    sending_frame_malloc;       /* 0 sending_frame 为空指针  ，1：sending_frame已分配内存 */                    
    uint8_t *sending_frame;             /* 缓存 即将被APP读取的数据*/     
    uint32_t send_total_len;            /* 记录 等待读取的数据的总长度*/
    uint32_t send_offset;               /* 记录 已经读取的数据的偏移位置，当send_offset==send_total_len 证明此包已经被完整的读取*/
    eRpcGattsStatus_t  current_status;  /* 记录 当前 的状态*/
} BleRpcGattsParams_t;

/**
 * @brief Creates and starts Maopao RPC gatt service
 *
 * @return true if the service is initialized successfully, false otherwise
 */
BaseType_t cHalBleRpcGatts_Init( void );

/**
 * @brief Cleanup Maopao RPC gatt service.
 *
 * @return true If the cleanup is successful false otherwise.
 */
BaseType_t cHalBleRpcGatts_Cleanup( void );


/**
 * @brief device will notity remainLength of json-rpc data.
 *
 * @param[in] remainLength  
 *
 * @return true If the notify is successful false otherwise.
 */
BaseType_t cHalRpcGattsIndicationRemainLength(uint32_t remainLength);

/**
 * @ingroup When the data receives a complete packet of data frames, this callback function will be called
 *
 * @param[in] frame    Data frame
 * @param[in] frame_len  Data frame len
 */
typedef void (* vHalBleRpcGattsReceiveDataCb)(uint8_t * frame,
                                            uint32_t frame_len );

/**
 * @ingroup When the data send a complete packet of data frames, this callback function will be called
 *
 * @param[in] frame    Data frame
 * @param[in] frame_len  Data frame len
 */
typedef void (* vHalBleRpcGattsSendDataCb)( uint8_t * frame,
                                            uint32_t frame_len );

/**
 * @ingroup send error to app
 *
 * @param[in] error    error code
 * @param[in] msg      error msg
 */
typedef void (* vHalBleRpcGattsReportErrorCb)(int error,
                                             char* msg );

/**
 * @brief Callback structure for RPC data .
 */
typedef struct
{
    vHalBleRpcGattsReceiveDataCb _MaopaoRpcGattsReceiveDataCb;
    vHalBleRpcGattsSendDataCb   _MaopaoRpcGattsSendDataCb;
    vHalBleRpcGattsReportErrorCb _MaopaoRpcGattsReportErrorCb;
} RpcGattsDataCb;

/**
 * @brief 用于注册一个 vHalBleRpcGattsReceiveDataCb.
 */
BaseType_t cHalRpcGattsRxFrameRegister( vHalBleRpcGattsReceiveDataCb _MaopaoRpcGattsReceiveData);


/**
 * @brief 用于注册一个 vHalBleRpcGattsSendDataCb.
 */
BaseType_t cHalRpcGattsTxFrameRegister( vHalBleRpcGattsSendDataCb _MaopaoRpcGattsSendData);

/**
 * @brief 接收RPC-send 的数据帧,此时设备将等待APP read数据
 */
BaseType_t cHalRpcGattsWaitReadStart(char *frame,uint32_t frame_len);

/**
 * @brief 用于注册一个 vHalBleRpcGattsReportErrorCb.
 */
BaseType_t cHalRpcGattsReportErrorRegister( vHalBleRpcGattsReportErrorCb _MaopaoRpcGattsReportErrorCb);

#endif /* MAOPAO_BLE_RPC_OVER_GATT_H_ */
