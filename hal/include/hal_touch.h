/*
 * Copyright (c) 2022-2023, Jeejio Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2023-02-22     None         the first version.
 */

#ifndef __HAL_TOUCH_H__
#define __HAL_TOUCH_H__

#include "hal_gpio.h"
#include "esp_log.h"

#ifdef __cplusplus
extern "C" {
#endif

/* Touch vendor types */
#define TOUCH_VENDOR_UNKNOWN          (0)  /* unknown */
#define TOUCH_VENDOR_GT               (1)  /* GTxx series */
#define TOUCH_VENDOR_FT               (2)  /* FTxx series */

/* Touch ic type*/
#define  TOUCH_TYPE_NONE              (0)  /* touch ic none */
#define  TOUCH_TYPE_CAPACITANCE       (1)  /* capacitance ic */
#define  TOUCH_TYPE_RESISTANCE        (2)  /* resistance ic */

/* Touch control cmd types */
#define  TOUCH_CTRL_GET_ID            (DEVICE_CTRL_BASE(Touch) + 0)   /* Get device id */
#define  TOUCH_CTRL_GET_INFO          (DEVICE_CTRL_BASE(Touch) + 1)   /* Get touch info */
#define  TOUCH_CTRL_SET_MODE          (DEVICE_CTRL_BASE(Touch) + 2)   /* Set touch's work mode. ex. TOUCH_MODE_POLLING,TOUCH_MODE_INT */
#define  TOUCH_CTRL_SET_X_RANGE       (DEVICE_CTRL_BASE(Touch) + 3)   /* Set x coordinate range */
#define  TOUCH_CTRL_SET_Y_RANGE       (DEVICE_CTRL_BASE(Touch) + 4)   /* Set y coordinate range */
#define  TOUCH_CTRL_SET_X_TO_Y        (DEVICE_CTRL_BASE(Touch) + 5)   /* Set X Y coordinate exchange */
#define  TOUCH_CTRL_DISABLE_INT       (DEVICE_CTRL_BASE(Touch) + 6)   /* Disable interrupt */
#define  TOUCH_CTRL_ENABLE_INT        (DEVICE_CTRL_BASE(Touch) + 7)   /* Enable interrupt */
#define  TOUCH_CTRL_POWER_ON          (DEVICE_CTRL_BASE(Touch) + 8)   /* Touch Power On */
#define  TOUCH_CTRL_POWER_OFF         (DEVICE_CTRL_BASE(Touch) + 9)   /* Touch Power Off */
#define  TOUCH_CTRL_GET_STATUS        (DEVICE_CTRL_BASE(Touch) + 10)  /* Get Touch Power Status */

/* Touch event */
#define TOUCH_EVENT_NONE              (0)   /* Touch none */
#define TOUCH_EVENT_UP                (1)   /* Touch up event */
#define TOUCH_EVENT_DOWN              (2)   /* Touch down event */
#define TOUCH_EVENT_MOVE              (3)   /* Touch move event */

struct TouchInfo_st
{
    Uint8_t     type;                       /* The touch type */
    Uint8_t     vendor;                     /* Vendor of touchs */
    Uint8_t     point_num;                  /* Support point num */
    Int32_t     range_x;                    /* X coordinate range */
    Int32_t     range_y;                    /* Y coordinate range */
};

struct TouchConfig_st
{
#ifdef TOUCH_PIN_IRQ
    struct DevicePinMode_st  irq_pin;       /* Interrupt pin, The purpose of this pin is to notification read data */
#endif
    char                        *dev_name;     /* The name of the communication device */
    void                        *user_data;
};

typedef struct TouchDevice_st *TouchDev_t;
struct TouchDevice_st
{
    struct Device_st      parent;        /* The standard device */
    struct TouchInfo_st       info;          /* The touch info data */
    struct TouchConfig_st     config;        /* The touch config data */

    const struct TouchOps_st  *ops;           /* The touch ops */
    Err_t (*irq_handle)(TouchDev_t touch);  /* Called when an interrupt is generated, registered by the driver */
};

struct TouchData_st
{
    Uint8_t          event;                 /* The touch event of the data */
    Uint8_t          track_id;              /* Track id of point */
    Uint8_t          width;                 /* Point of width */
    Uint16_t         x_coordinate;          /* Point of x coordinate */
    Uint16_t         y_coordinate;          /* Point of y coordinate */
    Tick_t           timestamp;             /* The timestamp when the data was received */
};

struct TouchOps_st
{
    Size_t (*szTouchReadpoint)(struct TouchDevice_st *touch, void *buf, Size_t touch_num);
    Err_t (*lHalTouchControl)(struct TouchDevice_st *touch, int cmd, void *arg);
};

int lHalHwTouchRegister(TouchDev_t    touch,
                     const char    *name,
                     Uint32_t  flag,
                     void          *data);

/* if you doesn't use pin device. you must call this function in your touch irq callback */
void vHalHwTouchIsr(TouchDev_t touch);

#ifdef __cplusplus
}
#endif

#endif /* __TOUCH_H__ */
