/*
 * Copyright (c) 2022, jeejio Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2023-1-12      zhengqian    the first version
 */

#ifndef __MMCSD_CARD_H__
#define __MMCSD_CARD_H__

#include <freertos/device.h>
#include <hal_mmcsd_host.h>

#ifdef __cplusplus
extern "C" {
#endif

#define SD_SCR_BUS_WIDTH_1  (1 << 0)
#define SD_SCR_BUS_WIDTH_4  (1 << 2)

struct MmcsdCid_st {
    Uint8_t  mid;       /* ManufacturerID */
    Uint8_t  prv;       /* Product Revision */
    Uint16_t oid;       /* OEM/Application ID */
    Uint32_t psn;       /* Product Serial Number */
    Uint8_t  pnm[5];    /* Product Name */
    Uint8_t  reserved1;/* reserved */
    Uint16_t mdt;       /* Manufacturing Date */
    Uint8_t  crc;       /* CID CRC */
    Uint8_t  reserved2;/* not used, always 1 */
};

struct MmcsdCsd_st {
    Uint8_t      csd_structure;  /* CSD register version */
    Uint8_t      taac;
    Uint8_t      nsac;
    Uint8_t      tran_speed; /* max data transfer rate */
    Uint16_t     card_cmd_class; /* card command classes */
    Uint8_t      rd_blk_len; /* max read data block length */
    Uint8_t      rd_blk_part;
    Uint8_t      wr_blk_misalign;
    Uint8_t      rd_blk_misalign;
    Uint8_t      dsr_imp;    /* DSR implemented */
    Uint8_t      c_size_mult;    /* CSD 1.0 , device size multiplier */
    Uint32_t     c_size;     /* device size */
    Uint8_t      r2w_factor;
    Uint8_t      wr_blk_len; /* max wtire data block length */
    Uint8_t      wr_blk_partial;
    Uint8_t      csd_crc;

};

struct SdScr_st {
    Uint8_t      sd_version;
    Uint8_t      sd_bus_widths;
};

struct SdioCccr_st {
    Uint8_t      sdio_version;
    Uint8_t      sd_version;
    Uint8_t      direct_cmd:1,     /*  Card Supports Direct Commands during data transfer
                                                   only SD mode, not used for SPI mode */
                multi_block:1,    /*  Card Supports Multi-Block */
                read_wait:1,      /*  Card Supports Read Wait
                                       only SD mode, not used for SPI mode */
                suspend_resume:1, /*  Card supports Suspend/Resume
                                       only SD mode, not used for SPI mode */
                s4mi:1,            /* generate interrupts during a 4-bit
                                      multi-block data transfer */
                e4mi:1,            /*  Enable the multi-block IRQ during
                                       4-bit transfer for the SDIO card */
                low_speed:1,      /*  Card  is  a  Low-Speed  card */
                low_speed_4:1;    /*  4-bit support for Low-Speed cards */

    Uint8_t      bus_width:1,     /* Support SDIO bus width, 1:4bit, 0:1bit */
                cd_disable:1,    /*  Connect[0]/Disconnect[1] the 10K-90K ohm pull-up
                                     resistor on CD/DAT[3] (pin 1) of the card */
                power_ctrl:1,    /* Support Master Power Control */
                high_speed:1;    /* Support High-Speed  */


};

struct SdioCis_st {
    Uint16_t     manufacturer;
    Uint16_t     product;
    Uint16_t     func0_blk_size;
    Uint32_t     max_tran_speed;
};

/*
 * SDIO function CIS tuple (unknown to the core)
 */
struct SdioFunctionTuple_st {
    struct SdioFunctionTuple_st *next;
    Uint8_t code;
    Uint8_t size;
    Uint8_t *data;
};

struct SdioFunction_st;
typedef void (SdioIrqHandler_t)(struct SdioFunction_st *);

/*
 * SDIO function devices
 */
struct SdioFunction_st {
    struct MmcsdCard_st        *card;      /* the card this device belongs to */
    SdioIrqHandler_t   *irq_handler;   /* IRQ callback */
    Uint8_t      num;        /* function number */

    Uint8_t      func_code;   /*  Standard SDIO Function interface code  */
    Uint16_t     manufacturer;       /* manufacturer id */
    Uint16_t     product;        /* product id */

    Uint32_t     max_blk_size;   /* maximum block size */
    Uint32_t     cur_blk_size;   /* current block size */

    Uint32_t     enable_timeout_val; /* max enable timeout in msec */

    struct SdioFunctionTuple_st *tuples;

    void            *priv;
};

#define SDIO_MAX_FUNCTIONS      7



struct MmcsdCard_st {
    struct MmcsdHost_st *host;
    Uint32_t rca;        /* card addr */
    Uint32_t resp_cid[4];    /* card CID register */
    Uint32_t resp_csd[4];    /* card CSD register */
    Uint32_t resp_scr[2];    /* card SCR register */

    Uint16_t tacc_clks;  /* data access time by ns */
    Uint32_t tacc_ns;    /* data access time by clk cycles */
    Uint32_t max_data_rate;  /* max data transfer rate */
    Uint32_t card_capacity;  /* card capacity, unit:KB */
    Uint32_t card_blksize;   /* card block size */
    Uint32_t erase_size; /* erase size in sectors */
    Uint16_t card_type;
#define CARD_TYPE_MMC                   0 /* MMC card */
#define CARD_TYPE_SD                    1 /* SD card */
#define CARD_TYPE_SDIO                  2 /* SDIO card */
#define CARD_TYPE_SDIO_COMBO            3 /* SD combo (IO+mem) card */

    Uint16_t flags;
#define CARD_FLAG_HIGHSPEED  (1 << 0)   /* SDIO bus speed 50MHz */
#define CARD_FLAG_SDHC       (1 << 1)   /* SDHC card */
#define CARD_FLAG_SDXC       (1 << 2)   /* SDXC card */

    struct SdScr_st    scr;
    struct MmcsdCsd_st csd;
    Uint32_t     hs_max_data_rate;  /* max data transfer rate in high speed mode */

    Uint8_t      sdio_function_num;  /* totol number of SDIO functions */
    struct SdioCccr_st    cccr;  /* common card info */
    struct SdioCis_st     cis;  /* common tuple info */
    struct SdioFunction_st *sdio_function[SDIO_MAX_FUNCTIONS + 1]; /* SDIO functions (devices) */
    DevList_t blk_devices;  /* for block device list */
};

#ifdef __cplusplus
}
#endif

#endif
