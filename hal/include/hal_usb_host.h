/*
 * Copyright (c) 2023, Jeejio Technology Co. Ltd
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2023-03-08     Alpha        First Verison
 *
 */
#ifndef __HAL_USB_HOST_H__
#define __HAL_USB_HOST_H__

#ifdef __cplusplus
extern "C" {
#endif

#include <swldef.h>
#include <freertos/FreeRTOS.h>
#include <freertos/queue.h>
#include <io_dev_mgt.h>
#include <string.h>
#include "hal_usb_common.h"

#define USB_MAX_DEVICE                  0x20
#define USB_MAX_INTERFACE               0x08
#define USB_HUB_PORT_NUM                0x04
#define SIZEOF_USB_REQUEST              0x08

#define DEV_STATUS_IDLE                 0x00
#define DEV_STATUS_BUSY                 0x01
#define DEV_STATUS_ERROR                0x02

#define UPIPE_STATUS_OK                 0x00
#define UPIPE_STATUS_STALL              0x01
#define UPIPE_STATUS_ERROR              0x02

#define USBH_PID_SETUP                  0x00
#define USBH_PID_DATA                   0x01

struct Uhcd_st;
struct Uhintf_st;
struct Uhub_st;
struct Upipe_st;

struct UclassDriver_st
{
    DevList_t list;
    int class_code;
    int subclass_code;

    Err_t (*enable)(void* arg);
    Err_t (*disable)(void* arg);

    void* user_data;
};
typedef struct UclassDriver_st* Ucd_t;

struct Uprotocal_st
{
    DevList_t list;
    int pro_id;

    Err_t (*init)(void* arg);
    Err_t (*callback)(void* arg);
};
typedef struct Uprotocal_st* Uprotocal_t;

struct Uinstance_st
{
    struct Device_st parent;

    struct UdeviceDescriptor_st dev_desc;
    UcfgDesc_t cfg_desc;
    struct Uhcd_st *hcd;

    struct Upipe_st * pipe_ep0_out;
    struct Upipe_st * pipe_ep0_in;
    DevList_t pipe;

    Uint8_t status;
    Uint8_t type;
    Uint8_t index;
    Uint8_t address;
    Uint8_t speed;
    Uint8_t max_packet_size;
    Uint8_t port;

    struct Uhub_st* parent_hub;
    struct Uhintf_st* intf[USB_MAX_INTERFACE];
};
typedef struct Uinstance_st* Uinst_t;

struct Uhintf_st
{
    struct Uinstance_st* device;
    UintfDesc_t intf_desc;

    Ucd_t drv;
    void *user_data;
};

struct Upipe_st
{
    DevList_t list;
    Uint8_t pipe_index;
    Uint32_t status;
    struct UendpointDescriptor_st ep;
    Uinst_t inst;
    FuncCallback_t callback;
    void* user_data;
};
typedef struct Upipe_st* Upipe_t;

struct Uhub_st
{
    struct UhubDescriptor_st hub_desc;
    Uint8_t num_ports;
    Uint32_t port_status[USB_HUB_PORT_NUM];
    struct Uinstance_st* child[USB_HUB_PORT_NUM];

    Bool_t is_roothub;

    Uint8_t buffer[8];
    struct Uinstance_st* self;
    struct Uhcd_st *hcd;
};
typedef struct Uhub_st* Uhub_t;

struct UhcdOps_st
{
    Err_t    (*reset_port)   (Uint8_t port);
    int         (*pipe_xfer)    (Upipe_t pipe, Uint8_t token, void* buffer, int nbytes, int timeout);
    Err_t    (*open_pipe)    (Upipe_t pipe);
    Err_t    (*close_pipe)   (Upipe_t pipe);
};
typedef struct UhcdOps_st* UhcdOps_t;
struct Uhcd_st
{
    struct Device_st parent;
    UhcdOps_t ops;
    Uint8_t num_ports;
    Uhub_t roothub;
    QueueHandle_t usb_mq;
};
typedef struct Uhcd_st* Uhcd_t;

enum UhostMsgType_t
{
    USB_MSG_CONNECT_CHANGE,
    USB_MSG_CALLBACK,
};
typedef enum UhostMsgType_t UhostMsgType_t;

struct UhostMsg_st
{
    UhostMsgType_t type;
    union
    {
        struct Uhub_st* hub;
        struct
        {
            FuncCallback_t function;
            void *context;
        }cb;
    }content;
};
typedef struct UhostMsg_st* UhostMsg_t;

/* usb host system interface */
Err_t lHalUsbHostInit(const char *name);
void vHalUsbhHubInit(struct Uhcd_st *hcd);

/* usb host core interface */
struct Uinstance_st* xHalUsbhAllocInstance(Uhcd_t Uhcd_st);
Err_t lHalUsbhAttatchInstance(struct Uinstance_st* device);
Err_t lHalUsbhDetachInstance(struct Uinstance_st* device);
Err_t lHalUsbhGetDescriptor(struct Uinstance_st* device, Uint8_t type, void* buffer, int nbytes);
Err_t lHalUsbhSetConfigure(struct Uinstance_st* device, int config);
Err_t lHalUsbhSetAddress(struct Uinstance_st* device);
Err_t lHalUsbhSetInterface(struct Uinstance_st* device, int intf);
Err_t lHalUsbhClearFeature(struct Uinstance_st* device, int endpoint, int feature);
Err_t lHalUsbhGetInterfaceDescriptor(UcfgDesc_t cfg_desc, int num, UintfDesc_t* intf_desc);
Err_t lHalUsbhGetEndpointDescriptor(UintfDesc_t intf_desc, int num, UepDesc_t* ep_desc);

/* usb class driver interface */
Err_t lHalUsbhClassDriverInit(void);
Err_t lHalUsbhClassDriverRegister(Ucd_t drv);
Err_t lHalUsbhClassDriverUnregister(Ucd_t drv);
Err_t lHalUsbhClassDriverEnable(Ucd_t drv, void* args);
Err_t lHalUsbhClassDriverDisable(Ucd_t drv, void* args);
Ucd_t xHalUsbhClassDriverFind(int class_code, int subclass_code);

/* usb class driver implement */
Ucd_t xHalUsbhClassDriverHub(void);
Ucd_t xHalUsbhClassDriverStorage(void);



/* usb hub interface */
Err_t lHalUsbhHubGetDescriptor(struct Uinstance_st* device, Uint8_t *buffer,
    Size_t size);
Err_t lHalUsbhHubGetStatus(struct Uinstance_st* device, Uint32_t* buffer);
Err_t lHalUsbhHubGetPortStatus(Uhub_t Uhub_st, Uint16_t port,
    Uint32_t* buffer);
Err_t lHalUsbhHubClearPortFeature(Uhub_t Uhub_st, Uint16_t port,
    Uint16_t feature);
Err_t lHalUsbhHubSetPortFeature(Uhub_t Uhub_st, Uint16_t port,
    Uint16_t feature);
Err_t lHalUsbhHubResetPort(Uhub_t Uhub_st, Uint16_t port);
Err_t lHalUsbhEventSignal(Uhcd_t Uhcd_st, struct UhostMsg_st* msg);


void vHalUsbhRootHubConnectHandler(struct Uhcd_st *hcd, Uint8_t port, Bool_t isHS);
void vHalUsbhRootHubDisconnectHandler(struct Uhcd_st *hcd, Uint8_t port);

/* usb host controller driver interface */
static inline Err_t lHalUsbInstanceAddPipe(Uinst_t inst, Upipe_t pipe)
{
    configASSERT(inst != NULL);
    configASSERT(pipe != NULL);
    vHalListInsertBefore(&inst->pipe, &pipe->list);
    return DEV_EOK;
}
static inline Upipe_t xHalUsbInstanceFindPipe(Uinst_t inst,Uint8_t ep_address)
{
    DevList_t * l;
    for(l = inst->pipe.next;l != &inst->pipe;l = l->next)
    {
        if(list_entry(l,struct Upipe_st,list)->ep.bEndpointAddress == ep_address)
        {
            return list_entry(l,struct Upipe_st,list);
        }
    }
    return NULL;
}
static inline Err_t lHalUsbHcdAllocPipe(Uhcd_t hcd, Upipe_t* pipe, Uinst_t inst, UepDesc_t ep)
{
    *pipe = (Upipe_t)malloc(sizeof(struct Upipe_st));
    if(*pipe == NULL)
    {
        return DEV_ERROR;
    }
    memset(*pipe,0,sizeof(struct Upipe_st));
    (*pipe)->inst = inst;
    memcpy(&(*pipe)->ep,ep,sizeof(struct UendpointDescriptor_st));
    return hcd->ops->open_pipe(*pipe);
}
static inline void vHalUsbPipeAddCallback(Upipe_t pipe, FuncCallback_t callback)
{
    pipe->callback = callback;
}

static inline Err_t lHalUsbHcdFreePipe(Uhcd_t hcd, Upipe_t pipe)
{
    configASSERT(pipe != NULL);
    hcd->ops->close_pipe(pipe);
    free(pipe);
    return DEV_EOK;
}

int lHalUsbHcdPipeXfer(Uhcd_t hcd, Upipe_t pipe, void* buffer, int nbytes, int timeout);
static inline int lHalUsbHcdSetupXfer(Uhcd_t hcd, Upipe_t pipe, Ureq_t setup, int timeout)
{
    return hcd->ops->pipe_xfer(pipe, USBH_PID_SETUP, (void *)setup, 8, timeout);
}

#ifdef __cplusplus
}
#endif

#endif
