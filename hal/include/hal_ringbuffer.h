/*
 * Copyright (c) 2022, Jeejio Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2022-12-03     Monk         First version.
 */
#ifndef HAL_RINGBUFFER_H__
#define HAL_RINGBUFFER_H__

#include "swldef.h"
#include "freertos/FreeRTOS.h"

#ifdef __cplusplus
extern "C" {
#endif

#define USING_HEAP
#define ALIGN_SIZE 4

/* ring buffer */
struct Ringbuffer_st
{
    Uint8_t *buffer_ptr;
    /* use the msb of the {read,write}_index as mirror bit. You can see this as
     * if the buffer adds a virtual mirror and the pointers point either to the
     * normal or to the mirrored buffer. If the write_index has the same value
     * with the read_index, but in a different mirror, the buffer is full.
     * While if the write_index and the read_index are the same and within the
     * same mirror, the buffer is empty. The ASCII art of the ringbuffer is:
     *
     *          mirror = 0                    mirror = 1
     * +---+---+---+---+---+---+---+|+~~~+~~~+~~~+~~~+~~~+~~~+~~~+
     * | 0 | 1 | 2 | 3 | 4 | 5 | 6 ||| 0 | 1 | 2 | 3 | 4 | 5 | 6 | Full
     * +---+---+---+---+---+---+---+|+~~~+~~~+~~~+~~~+~~~+~~~+~~~+
     *  read_idx-^                   write_idx-^
     *
     * +---+---+---+---+---+---+---+|+~~~+~~~+~~~+~~~+~~~+~~~+~~~+
     * | 0 | 1 | 2 | 3 | 4 | 5 | 6 ||| 0 | 1 | 2 | 3 | 4 | 5 | 6 | Empty
     * +---+---+---+---+---+---+---+|+~~~+~~~+~~~+~~~+~~~+~~~+~~~+
     * read_idx-^ ^-write_idx
     *
     * The tradeoff is we could only use 32KiB of buffer for 16 bit of index.
     * But it should be enough for most of the cases.
     *
     * Ref: http://en.wikipedia.org/wiki/Circular_buffer#Mirroring */
    Uint16_t read_mirror : 1;
    Uint16_t read_index : 15;
    Uint16_t write_mirror : 1;
    Uint16_t write_index : 15;
    /* as we use msb of index as mirror bit, the size should be signed and
     * could only be positive. */
    Int16_t buffer_size;
};

enum RingbufferState_t
{
    RINGBUFFER_EMPTY,
    RINGBUFFER_FULL,
    /* half full is neither full nor empty */
    RINGBUFFER_HALFFULL,
};

/**
 * RingBuffer for DeviceDriver
 */
void vHalRingbufferInit(struct Ringbuffer_st *rb, Uint8_t *pool, Int16_t size);
void vHalRingbufferReset(struct Ringbuffer_st *rb);
Size_t ulHalRingbufferPut(struct Ringbuffer_st *rb, const Uint8_t *ptr, Uint16_t length);
Size_t ulHalRingbufferPutForce(struct Ringbuffer_st *rb, const Uint8_t *ptr, Uint16_t length);
Size_t ulHalRingbufferPutchar(struct Ringbuffer_st *rb, const Uint8_t ch);
Size_t ulHalRingbufferPutcharForce(struct Ringbuffer_st *rb, const Uint8_t ch);
Size_t ulHalRingbufferGet(struct Ringbuffer_st *rb, Uint8_t *ptr, Uint16_t length);
Size_t ulHalRingbufferPeek(struct Ringbuffer_st *rb, Uint8_t **ptr);
Size_t ulHalRingbufferGetchar(struct Ringbuffer_st *rb, Uint8_t *ch);
Size_t ulHalRingbufferDataLen(struct Ringbuffer_st *rb);

#ifdef USING_HEAP
struct Ringbuffer_st* pHalRingbufferCreate(Uint16_t length);
void vHalRingbufferDestroy(struct Ringbuffer_st *rb);
#endif

/**
 * @brief Get the buffer size of the ring buffer object.
 *
 * @param rb        A pointer to the ring buffer object.
 *
 * @return  Buffer size.
 */
static inline Uint16_t usHalRingbufferGetSize(struct Ringbuffer_st *rb)
{
    configASSERT(rb != NULL);
    return rb->buffer_size;
}

/** return the size of empty space in rb */
#define ringbuffer_space_len(rb) ((rb)->buffer_size - ulHalRingbufferDataLen(rb))


#ifdef __cplusplus
}
#endif

#endif
