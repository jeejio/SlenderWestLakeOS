/*
 * Copyright (c) 2023, jeejio Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2023-02-28       lk         the first version
 */

#ifndef __HAL_WORKQUEUE_H__
#define __HAL_WORKQUEUE_H__

#include "freertos/FreeRTOS.h"
#include <swldef.h>
#include "freertos/semphr.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/timers.h"
#include <hal_config.h>

#ifdef __cplusplus
extern "C" {
#endif

enum
{
    WORK_STATE_PENDING    = 0x0001,     /* Work item pending state */
    WORK_STATE_SUBMITTING = 0x0002,     /* Work item submitting state */
};

/**
 * work type definitions
 */
enum
{
    WORK_TYPE_DELAYED     = 0x0001,
};

/* workqueue implementation */
struct Workqueue_st
{
    DevList_t      work_list;
    DevList_t      delayed_list;
    struct Work_st *work_current; /* current work */

    SemaphoreHandle_t sem;
    TaskHandle_t    work_thread;
};

struct Work_st
{
    DevList_t list;

    void (*work_func)(struct Work_st *work, void *work_data);
    void *work_data;
    Uint16_t flags;
    Uint16_t type;
    TimerHandle_t timer;
    struct Workqueue_st *workqueue;
};

/**
 * WorkQueue for DeviceDriver
 */
void vHalWorkInit(struct Work_st *work, void (*work_func)(struct Work_st *work, void *work_data), void *work_data);
struct Workqueue_st *pHalWorkqueueCreate(const char *name, Uint16_t stack_size, Uint8_t priority);
Err_t lHalWorkqueueDestroy(struct Workqueue_st *queue);
Err_t lHalWorkqueueDowork(struct Workqueue_st *queue, struct Work_st *work);
Err_t lHalWorkqueueSubmitWork(struct Workqueue_st *queue, struct Work_st *work, Tick_t ticks);
Err_t lHalWorkqueueCancelWork(struct Workqueue_st *queue, struct Work_st *work);
Err_t lHalWorkqueueCancelWorkSync(struct Workqueue_st *queue, struct Work_st *work);
Err_t lHalWorkqueueCancelAllWork(struct Workqueue_st *queue);
Err_t lHalWorkqueueUrgentWork(struct Workqueue_st *queue, struct Work_st *work);

#ifdef USING_SYSTEM_WORKQUEUE
Err_t lHalWorkSubmit(struct Work_st *work, Tick_t ticks);
Err_t lHalWorkUrgent(struct Work_st *work);
Err_t lHalWorkCancel(struct Work_st *work);
#endif /* USING_SYSTEM_WORKQUEUE */

#ifdef __cplusplus
}
#endif

#endif /* __HAL_WORKQUEUE_H__ */
