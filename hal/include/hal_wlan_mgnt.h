/*
 * Copyright (c) 2023, jeejio Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2023-02-28       lk         the first version
 */

#ifndef __HAL_WLAN_MGNT_H__
#define __HAL_WLAN_MGNT_H__

#include <hal_wlan_dev.h>

#ifdef __cplusplus
extern "C" {
#endif

#ifndef WLAN_SCAN_WAIT_MS
#define WLAN_SCAN_WAIT_MS       (10 * 1000)
#endif

#ifndef WLAN_SCAN_CACHE_NUM
#define WLAN_SCAN_CACHE_NUM     (50)
#endif

#ifndef WLAN_CONNECT_WAIT_MS
#define WLAN_CONNECT_WAIT_MS    (10 * 1000)
#endif

#ifndef WLAN_START_AP_WAIT_MS
#define WLAN_START_AP_WAIT_MS    (10 * 1000)
#endif

#ifndef WLAN_EBOX_NUM
#define WLAN_EBOX_NUM           (10)
#endif

#ifndef WLAN_SCAN_RETRY_CNT
#define WLAN_SCAN_RETRY_CNT      (3)
#endif

#ifndef AUTO_CONNECTION_PERIOD_MS
#define AUTO_CONNECTION_PERIOD_MS (2000)
#endif

/*state fot station*/
#define WLAN_STATE_CONNECT     (1UL << 0)
#define WLAN_STATE_CONNECTING  (1UL << 1)
#define WLAN_STATE_READY       (1UL << 2)
#define WLAN_STATE_POWERSAVE   (1UL << 3)
#define WLAN_STATE_GOTIP       (1UL << 4)

/*flags fot station*/
#define WLAN_STATE_AUTOEN      (1UL << 0)

/*state fot ap*/
#define WLAN_STATE_ACTIVE      (1UL << 0)

typedef enum
{
    WLAN_EVT_READY = 0,              /* connect and prot is ok, You can send data*/
    WLAN_EVT_SCAN_DONE,              /* Scan a info */
    WLAN_EVT_SCAN_REPORT,            /* Scan end */
    WLAN_EVT_STA_CONNECTED,          /* connect success */
    WLAN_EVT_STA_CONNECTED_FAIL,     /* connection failed */
    WLAN_EVT_STA_DISCONNECTED,       /* disconnect */
    WLAN_EVT_STA_GOT_IP,             /* got ip */
    WLAN_EVT_AP_START,               /* AP start */
    WLAN_EVT_AP_STOP,                /* AP stop */
    WLAN_EVT_AP_ASSOCIATED,          /* sta associated */
    WLAN_EVT_AP_DISASSOCIATED,       /* sta disassociated */
    WLAN_EVT_MAX
} WlanEvent_t;

typedef void (*WlanEventHandler_t)(int event, struct WlanBuff_st *buff, void *parameter);

struct WlanScanResult_st
{
    Int32_t num;
    struct WlanInfo_st *info;
};

/*
 * wifi init interface
 */
int lHalWlanInit(void);
Err_t lHalWlanSetMode(const char *dev_name, WlanMode_t mode);
WlanMode_t xHalWlanGetMode(const char *dev_name);

/*
 * wifi station mode interface
 */
Err_t lHalWlanConnect(const char *ssid, const char *password);
Err_t lHalWlanConnectAdv(struct WlanInfo_st *info, const char *password);
Err_t lHalWlanDisconnect(void);
Bool_t lHalWlanIsConnected(void);
Bool_t lHalWlanIsGotIP(void);
void lHalWlanIsClrConnectGotIP(void);
Bool_t lHalWlanIsReady(void);
Err_t lHalWlanSetMac(Uint8_t *mac);
Err_t lHalWlanGetMac(Uint8_t *mac);
Err_t lHalWlanGetInfo(struct WlanInfo_st *info);
int lHalWlanGetRssi(void);

/*
 * wifi ap mode interface
 */
Err_t lHalWlanStartAp(const char *ssid, const char *password);
Err_t lHalWlanStartApAdv(struct WlanInfo_st *info, const char *password);
Bool_t lHalWlanApIsActive(void);
Err_t lHalWlanApStop(void);
Err_t lHalWlanApGetInfo(struct WlanInfo_st *info);
int lHalWlanApGetStaNum(void);
int lHalWlanApGetStaInfo(struct WlanInfo_st *info, int num);
Err_t lHalWlanApDeauthSta(Uint8_t *mac);
Err_t lHalWlanApSetCountry(CountryCode_t country_code);
CountryCode_t xHalWlanApGetCountry(void);

/*
 * wifi scan interface
 */
Err_t lHalWlanScan(void);
struct WlanScanResult_st *rt_wlan_scan_sync(void);
Err_t lHalWlanScanWithInfo(struct WlanInfo_st *info);


/*
 * wifi auto connect interface
 */
void vHalWlanConfigAutoreconnect(Bool_t enable);
Bool_t lHalWlanGetAutoreconnectMode(void);

/*
 * wifi power management interface
 */
Err_t lHalWlanSetPowersave(int level);
int lHalWlanGetPowersave(void);

/*
 * wifi event management interface
 */
Err_t lHalWlanRegisterEventHandler(WlanEvent_t event, WlanEventHandler_t handler, void *parameter);
Err_t lHalWlanUnregisterEventHandler(WlanEvent_t event);

/*
 * wifi management lock interface
 */
void vHalWlanMgntLock(void);
void vHalWlanMgntUnlock(void);

void vHalWlanGModeSetSta(void);
void vHalWlanGModeSetAp(void);
int lHalWlanGModeGet(void);

#ifdef __cplusplus
}
#endif

#endif /* __HAL_WLAN_MGNT_H__ */
