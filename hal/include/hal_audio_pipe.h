/*
 * Copyright (c) 2023, JEE Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2023-02-28      lk        first version
 */
#ifndef __HAL_AUDIO_PIPE_H__
#define __HAL_AUDIO_PIPE_H__

/**
 * Pipe Device
 */
#include "swldef.h"
#include "hal_ringbuffer.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"

#define AUDIO_RECORD_PIPE_SIZE 2048

/* portal device */
struct AudioPortalDevice_st
{
    struct Device_st parent;
    struct Device_st *write_dev;
    struct Device_st *read_dev;
};

enum AudioPipeFlag_t
{
    /* both read and write won't block */
    AUDIO_PIPE_FLAG_NONBLOCK_RDWR = 0x00,
    /* read would block */
    AUDIO_PIPE_FLAG_BLOCK_RD = 0x01,
    /* write would block */
    AUDIO_PIPE_FLAG_BLOCK_WR = 0x02,
    /* write to this pipe will discard some data when the pipe is full.
     * When this flag is set, AUDIO_PIPE_FLAG_BLOCK_WR will be ignored since write
     * operation will always be success. */
    AUDIO_PIPE_FLAG_FORCE_WR = 0x04,
};

struct AudioPipe_st
{
    struct Device_st parent;

    /* ring buffer in pipe device */
    struct Ringbuffer_st ringbuffer;

    int32_t flag;

    /* suspended list */
    DevList_t suspended_read_list;
    DevList_t suspended_write_list;

    struct AudioPortalDevice_st *write_portal;
    struct AudioPortalDevice_st *read_portal;

    TaskHandle_t    task;
};

#define PIPE_CTRL_GET_SPACE          0x14            /**< get the remaining size of a pipe device */

Err_t lHalAudioPipeInit(struct AudioPipe_st *pipe,
                      const char *name,
                      int32_t flag,
                      uint8_t *buf,
                      size_t size);
Err_t lHalAudioPipeDetach(struct AudioPipe_st *pipe);

#endif /* __HAL_AUDIO_PIPE_H__ */
