/*
 * Copyright (c) 2022, jeejio Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2023-2-2       zhengqian    first version
 */

#ifndef __PWM_H_INCLUDE__
#define __PWM_H_INCLUDE__

#include "freertos/device.h"
#include "freertos/FreeRTOS.h"


#define PWM_CMD_ENABLE      (DEVICE_CTRL_BASE(PWM) + 0)
#define PWM_CMD_DISABLE     (DEVICE_CTRL_BASE(PWM) + 1)
#define PWM_CMD_SET         (DEVICE_CTRL_BASE(PWM) + 2)
#define PWM_CMD_GET         (DEVICE_CTRL_BASE(PWM) + 3)
#define PWMN_CMD_ENABLE     (DEVICE_CTRL_BASE(PWM) + 4)
#define PWMN_CMD_DISABLE    (DEVICE_CTRL_BASE(PWM) + 5)
#define PWM_CMD_SET_PERIOD  (DEVICE_CTRL_BASE(PWM) + 6)
#define PWM_CMD_SET_PULSE   (DEVICE_CTRL_BASE(PWM) + 7)
#define PWM_CMD_INIT        (DEVICE_CTRL_BASE(PWM) + 8)
#define PWM_CMD_SET_DUTY    (DEVICE_CTRL_BASE(PWM) + 9)
#define PWM_CMD_UPDATE_DUTY (DEVICE_CTRL_BASE(PWM) + 10)


struct PwmConfiguration_st
{
    Uint32_t channel; /* 1-n or 0-n, which depends on specific MCU requirements */
    Uint32_t period;  /* unit:ns 1ns~4.29s:1Ghz~0.23hz */
    Uint32_t pulse;   /* unit:ns (pulse<=period) */

    Uint32_t timer_num;
    Uint32_t speed_mode;
    Uint32_t pin_num;
    Uint32_t duty;
    Uint8_t bit_resolution; /*duty_resolution:1bit 2bit 3bit 4bit eg... */

    /*
     * TRUE  : The channel of pwm is complememtary.
     * FALSE : The channel of pwm is nomal.
    */
    Bool_t  complementary;
};

struct DevicePwm_st;
struct PwmOps_st
{
    Err_t (*control)(struct DevicePwm_st *device, int cmd, void *arg);
};

struct DevicePwm_st
{
    struct Device_st parent;
    const struct PwmOps_st *ops;
};

Err_t lHalDevicePwmRegister(struct DevicePwm_st *device, const char *name, const struct PwmOps_st *ops, const void *user_data);

Err_t lHalPwmEnable(struct DevicePwm_st *device, int channel);
Err_t lHalPwmDisable(struct DevicePwm_st *device, int channel);
Err_t lHalPwmSet(struct DevicePwm_st *device, int channel, Uint32_t period, Uint32_t pulse);
Err_t lHalPwmSetPeriod(struct DevicePwm_st *device, int channel, Uint32_t period);
Err_t lHalPwmSetPulse(struct DevicePwm_st *device, int channel, Uint32_t pulse);

#endif /* __PWM_H_INCLUDE__ */
