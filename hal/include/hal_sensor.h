/*
 * Copyright (c) 2022, jeejio Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2022-11-30     zhengqian    first version
 */

#ifndef __SENSOR_H__
#define __SENSOR_H__

#include "freertos/device.h"
#include "hal_gpio.h"

#ifdef __cplusplus
extern "C" {
#endif

#define  PIN_NONE                   0xFFFF    /* RT PIN NONE */
#define  DEVICE_FLAG_FIFO_RX        0x200     /* Flag to use when the sensor is open by fifo mode */

#define  SENSOR_MODULE_MAX          (3)       /* The maximum number of members of a sensor module */

/* Sensor types */

#define SENSOR_CLASS_NONE           (0)
#define SENSOR_CLASS_ACCE           (1)  /* Accelerometer     */
#define SENSOR_CLASS_GYRO           (2)  /* Gyroscope         */
#define SENSOR_CLASS_MAG            (3)  /* Magnetometer      */
#define SENSOR_CLASS_TEMP           (4)  /* Temperature       */
#define SENSOR_CLASS_HUMI           (5)  /* Relative Humidity */
#define SENSOR_CLASS_BARO           (6)  /* Barometer         */
#define SENSOR_CLASS_LIGHT          (7)  /* Ambient light     */
#define SENSOR_CLASS_PROXIMITY      (8)  /* Proximity         */
#define SENSOR_CLASS_HR             (9)  /* Heart Rate        */
#define SENSOR_CLASS_TVOC           (10) /* TVOC Level        */
#define SENSOR_CLASS_NOISE          (11) /* Noise Loudness    */
#define SENSOR_CLASS_STEP           (12) /* Step sensor       */
#define SENSOR_CLASS_FORCE          (13) /* Force sensor      */
#define SENSOR_CLASS_DUST           (14) /* Dust sensor       */
#define SENSOR_CLASS_ECO2           (15) /* eCO2 sensor       */
#define SENSOR_CLASS_GNSS           (16) /* GPS/GNSS sensor   */
#define SENSOR_CLASS_TOF            (17) /* TOF sensor        */
#define SENSOR_CLASS_SPO2           (18) /* SpO2 sensor       */
#define SENSOR_CLASS_IAQ            (19) /* IAQ sensor.       */
#define SENSOR_CLASS_ETOH           (20) /* EtOH sensor.      */
#define SENSOR_CLASS_BP             (21) /* Blood Pressure    */
#define SENSOR_CLASS_PIR            (22) /* PIR Sensor        */
#define SENSOR_CLASS_MOTOR          (23) /* Motor Sensor      */
#define SENSOR_CLASS_SG             (24) /* Steering Gear     */
#define SENSOR_CLASS_RE             (25) /* Rotary Encoder    */
#define SENSOR_CLASS_WAPUMP         (26) /* water pump        */
#define SENSOR_CLASS_SIX_AXIS       (27) /* Six-axis sensor   */
#define SENSOR_CLASS_RFID           (28) /* Radio Frequency Identification */
#define SENSOR_CLASS_HYETOMETER     (29) /* Hyetometer sensor */
#define SENSOR_CLASS_BUTTON_PANEL   (30) /* Button panel      */
#define SENSOR_CLASS_LEDARRAY       (31) /* led array         */
#define SENSOR_CLASS_GETSTURE       (32) /* led array         */
#define SENSOR_CLASS_SCANNER        (33) /* code scanner      */
#define SENSOR_CLASS_JOYSTICK       (34) /* joystick          */
#define SENSOR_CLASS_IOEXPAND       (35) /* io expand         */
#define SENSOR_CLASS_SMOKE          (36) /* smoke             */
#define SENSOR_CLASS_CO             (37) /* co                */
#define SENSOR_CLASS_DISPLAY        (38) /* display           */
#define SENSOR_CLASS_GAS            (39) /* combustible gas   */
#define SENSOR_CLASS_SMART_PLUG     (40) /* smart plug        */
#define SENSOR_CLASS_DOOR           (41) /* Door sensor       */
#define SENSOR_CLASS_BUTTON         (42) /* Button            */
/* Sensor vendor types */

#define SENSOR_VENDOR_UNKNOWN       (0)
#define SENSOR_VENDOR_STM           (1)  /* STMicroelectronics */
#define SENSOR_VENDOR_BOSCH         (2)  /* Bosch */
#define SENSOR_VENDOR_INVENSENSE    (3)  /* Invensense */
#define SENSOR_VENDOR_SEMTECH       (4)  /* Semtech */
#define SENSOR_VENDOR_GOERTEK       (5)  /* Goertek */
#define SENSOR_VENDOR_MIRAMEMS      (6)  /* MiraMEMS */
#define SENSOR_VENDOR_DALLAS        (7)  /* Dallas */
#define SENSOR_VENDOR_ASAIR         (8)  /* Aosong */
#define SENSOR_VENDOR_SHARP         (9)  /* Sharp */
#define SENSOR_VENDOR_SENSIRION     (10) /* Sensirion */
#define SENSOR_VENDOR_TI            (11) /* Texas Instruments */
#define SENSOR_VENDOR_PLANTOWER     (12) /* Plantower */
#define SENSOR_VENDOR_AMS           (13) /* ams AG */
#define SENSOR_VENDOR_MAXIM         (14) /* Maxim Integrated */
#define SENSOR_VENDOR_MELEXIS       (15) /* Melexis */

/* Sensor unit types */

#define  SENSOR_UNIT_NONE           (0)
#define  SENSOR_UNIT_MG             (1)  /* Accelerometer           unit: mG         */
#define  SENSOR_UNIT_MDPS           (2)  /* Gyroscope               unit: mdps       */
#define  SENSOR_UNIT_MGAUSS         (3)  /* Magnetometer            unit: mGauss     */
#define  SENSOR_UNIT_LUX            (4)  /* Ambient light           unit: lux        */
#define  SENSOR_UNIT_CM             (5)  /* Distance                unit: cm         */
#define  SENSOR_UNIT_PA             (6)  /* Barometer               unit: pa         */
#define  SENSOR_UNIT_PERMILLAGE     (7)  /* Relative Humidity       unit: permillage */
#define  SENSOR_UNIT_DCELSIUS       (8)  /* Temperature             unit: dCelsius   */
#define  SENSOR_UNIT_HZ             (9)  /* Frequency               unit: HZ         */
#define  SENSOR_UNIT_ONE            (10) /* Dimensionless quantity  unit: 1          */
#define  SENSOR_UNIT_BPM            (11) /* Heart rate              unit: bpm        */
#define  SENSOR_UNIT_MM             (12) /* Distance                unit: mm         */
#define  SENSOR_UNIT_MN             (13) /* Force                   unit: mN         */
#define  SENSOR_UNIT_PPM            (14) /* Concentration           unit: ppm        */
#define  SENSOR_UNIT_PPB            (15) /* Concentration           unit: ppb        */
#define  SENSOR_UNIT_DMS            (16) /* Coordinates             unit: DMS        */
#define  SENSOR_UNIT_DD             (17) /* Coordinates             unit: DD         */
#define  SENSOR_UNIT_MGM3           (18) /* Concentration           unit: mg/m3      */
#define  SENSOR_UNIT_MMHG           (19) /* Blood Pressure          unit: mmHg       */
/* Sensor communication interface types */

#define  SENSOR_INTF_I2C            (1 << 0)
#define  SENSOR_INTF_SPI            (1 << 1)
#define  SENSOR_INTF_UART           (1 << 2)
#define  SENSOR_INTF_ONEWIRE        (1 << 3)

/* Sensor power mode types */

#define  SENSOR_POWER_NONE          (0)
#define  SENSOR_POWER_DOWN          (1)  /* power down mode   */
#define  SENSOR_POWER_NORMAL        (2)  /* normal-power mode */
#define  SENSOR_POWER_LOW           (3)  /* low-power mode    */
#define  SENSOR_POWER_HIGH          (4)  /* high-power mode   */

/* Sensor work mode types */

#define  SENSOR_MODE_NONE           (0)
#define  SENSOR_MODE_POLLING        (1)  /* One shot only read a data */
#define  SENSOR_MODE_INT            (2)  /* TODO: One shot interrupt only read a data */
#define  SENSOR_MODE_FIFO           (3)  /* TODO: One shot interrupt read all fifo data */

/* Sensor control cmd types */

#define  SENSOR_CTRL_GET_ID         (DEVICE_CTRL_BASE(Sensor) + 0)  /* Get device id */
#define  SENSOR_CTRL_GET_INFO       (DEVICE_CTRL_BASE(Sensor) + 1)  /* Get sensor info */
#define  SENSOR_CTRL_SET_RANGE      (DEVICE_CTRL_BASE(Sensor) + 2)  /* Set the measure range of sensor. unit is info of sensor */
#define  SENSOR_CTRL_SET_ODR        (DEVICE_CTRL_BASE(Sensor) + 3)  /* Set output date rate. unit is HZ */
#define  SENSOR_CTRL_SET_MODE       (DEVICE_CTRL_BASE(Sensor) + 4)  /* Set sensor's work mode. ex. SENSOR_MODE_POLLING,SENSOR_MODE_INT */
#define  SENSOR_CTRL_SET_POWER      (DEVICE_CTRL_BASE(Sensor) + 5)  /* Set power mode. args type of sensor power mode. ex. SENSOR_POWER_DOWN,SENSOR_POWER_NORMAL */
#define  SENSOR_CTRL_SELF_TEST      (DEVICE_CTRL_BASE(Sensor) + 6)  /* Take a self test */

#define  SENSOR_CTRL_USER_CMD_START 0x100  /* User commands should be greater than 0x100 */

struct SensorInfo_st
{
    Uint8_t     type;                    /* The sensor type */
    const char  *vendor;                 /* Vendor of sensors */
    const char  *model;                  /* model name of sensor */
    Uint8_t     unit;                    /* unit of measurement */
    Uint8_t     intf_type;               /* Communication interface type */
    Int32_t     range_max;               /* maximum range of this sensor's value. unit is 'unit' */
    Int32_t     range_min;               /* minimum range of this sensor's value. unit is 'unit' */
    Uint32_t    period_min;              /* Minimum measurement period,unit:ms. zero = not a constant rate */
    Uint8_t     fifo_max;
};

struct SensorIntf_st
{
    char                       *dev_name;   /* The name of the communication device */
    Uint8_t                 type;       /* Communication interface type */
    void                       *user_data;  /* Private data for the sensor. ex. i2c addr,spi cs,control I/O */
};

struct SensorConfig_st
{
    struct SensorIntf_st        intf;      /* sensor interface config */
    struct DevicePinMode_st    irq_pin;   /* Interrupt pin, The purpose of this pin is to notification read data */
    Uint8_t                   mode;      /* sensor work mode */
    Uint8_t                   power;     /* sensor power mode */
    Uint16_t                  odr;       /* sensor out data rate */
    Int32_t                   range;     /* sensor range of measurement */
};

typedef struct SensorDevice_st *Sensor_t;

struct SensorDevice_st
{
    struct Device_st             parent;    /* The standard device */

    struct SensorInfo_st        info;      /* The sensor info data */
    struct SensorConfig_st      config;    /* The sensor config data */

    void                         *data_buf;  /* The buf of the data received */
    Size_t                    data_len;  /* The size of the data received */

    const struct SensorOps_st  *ops;       /* The sensor ops */

    struct SensorModule_st     *module;    /* The sensor module */

    Err_t (*irq_handle)(Sensor_t sensor);             /* Called when an interrupt is generated, registered by the driver */
};

struct SensorModule_st
{
    Sensor_t                  sen[SENSOR_MODULE_MAX]; /* The module contains a list of sensors */
    Uint8_t                   sen_num;                   /* Number of sensors contained in the module */
};

/* 3-axis Data Type */
struct Sensor3Axis_st
{
    Int32_t x;
    Int32_t y;
    Int32_t z;
};

/* Blood Pressure Data Type */
struct SensorBp_st
{
    Int32_t sbp; /* SBP : systolic pressure */
    Int32_t dbp; /* DBP : diastolic pressure */
};

struct Coordinates_st
{
    double longitude;
    double latitude;
};

struct SensorData_st
{
    Uint32_t         timestamp;          /* The timestamp when the data was received */
    Uint8_t          type;               /* The sensor type of the data */
    union
    {
        struct Sensor3Axis_st acce;          /* Accelerometer.       unit: mG          */
        struct Sensor3Axis_st gyro;          /* Gyroscope.           unit: mdps        */
        struct Sensor3Axis_st mag;           /* Magnetometer.        unit: mGauss      */
        struct Coordinates_st   coord;         /* Coordinates          unit: degrees     */
        Int32_t           temp;          /* Temperature.         unit: dCelsius    */
        Int32_t           humi;          /* Relative humidity.   unit: permillage  */
        Int32_t           baro;          /* Pressure.            unit: pascal (Pa) */
        Int32_t           light;         /* Light.               unit: lux         */
        Int32_t           proximity;     /* Distance.            unit: centimeters */
        Int32_t           hr;            /* Heart rate.          unit: bpm         */
        Int32_t           tvoc;          /* TVOC.                unit: permillage  */
        Int32_t           noise;         /* Noise Loudness.      unit: HZ          */
        Uint32_t          step;          /* Step sensor.         unit: 1           */
        Int32_t           force;         /* Force sensor.        unit: mN          */
        Uint32_t          dust;          /* Dust sensor.         unit: ug/m3       */
        Uint32_t          eco2;          /* eCO2 sensor.         unit: ppm         */
        Uint32_t          spo2;          /* SpO2 sensor.         unit: permillage  */
        Uint32_t          iaq;           /* IAQ sensor.          unit: 1 */
        Uint32_t          etoh;          /* EtOH sensor.         unit: ppm */
        struct SensorBp_st     bp;            /* BloodPressure.       unit: mmHg        */
    } data;
};

struct SensorOps_st
{
    Size_t (*fetch_data)(struct SensorDevice_st *sensor, void *buf, Size_t len);
    Err_t (*control)(struct SensorDevice_st *sensor, int cmd, void *arg);
};

int lHalHwSensorRegister(Sensor_t      sensor,
                          const char        *name,
                          Uint32_t       flag,
                          void              *data);

#ifdef __cplusplus
}
#endif

#endif /* __SENSOR_H__ */
