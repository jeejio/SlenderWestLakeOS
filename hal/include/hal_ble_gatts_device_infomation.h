/*
 * FreeRTOS V202203.00
 * Copyright (C) 2021 Amazon.com, Inc. or its affiliates.  All Rights Reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * http://aws.amazon.com/freertos
 * http://www.FreeRTOS.org
 */

/**
 * @file jee_ble_device_infomation.h
 * @brief ble device infomation
 */
#ifndef MAOPAO_BLE_DEVICE_INFOMATION_H_
#define MAOPAO_BLE_DEVICE_INFOMATION_H_

#include "freertos/FreeRTOS.h"
/* The config header is always included first. */
#include "hal_ble_os_config.h"
#include <stdbool.h>

/**
 * @brief  GATT service, characteristics and descriptor UUIDs used by the sample.
 */

#define SIG_SYSTEM_ID_UUID                  0x2A23  // System ID
#define SIG_MODEL_NUMBER_UUID               0x2A24  // Model Number String
#define SIG_SERIAL_NUMBER_UUID              0x2A25  // Serial Number String
#define SIG_FIRMWARE_REV_UUID               0x2A26  // Firmware Revision String
#define SIG_HARDWARE_REV_UUID               0x2A27  // Hardware Revision String
#define SIG_SOFTWARE_REV_UUID               0x2A28  // Software Revision String
#define SIG_MANUFACTURER_NAME_UUID          0x2A29  // Manufacturer Name String
#define SIG_IEEE_11073_CERT_DATA_UUID       0x2A2A  // IEEE 11073-20601 Regulatory Certification Data List
#define SIG_PNP_ID_UUID                     0x2A50  // PnP ID

#define SIG_CLIENT_CHAR_CFG_UUID          ( 0x2902 )

/**
 * @brief Enable Notification and Enable Indication values as defined by GATT spec.
 */
#define DEVICE_INFO_ENABLE_NOTIFICATION              ( 0x01 )
#define DEVICE_INFO_ENABLE_INDICATION                ( 0x02 )

// IEEE 11073 authoritative body values
#define DEVINFO_11073_BODY_EMPTY          0
#define DEVINFO_11073_BODY_IEEE           1
#define DEVINFO_11073_BODY_CONTINUA       2
#define DEVINFO_11073_BODY_EXP            254

// System ID length
#define DEVINFO_SYSTEM_ID_LEN             6

  // PnP ID length
#define DEVINFO_PNP_ID_LEN                7

/**
 * @brief Characteristics used by the GATT sample service.
 */
typedef enum
{
    eDeviceInfoService = 0,                      /**< Device Information */
    eDeviceInfoCharSystemID,                     /**< System ID */
    eDeviceInfoCharModelNumberString,            /**< Model Number String */
    eDeviceInfoCharSerialNumberString,           /**< Serial Number String */
    eDeviceInfoCharFirmwareRevisionString,       /**< Firmware Revision String */
    eDeviceInfoCharHardwareRevisionString,       /**< Hardware Revision String */
    eDeviceInfoCharSoftwareRevisionString,       /**< Software Revision String */
    eDeviceInfoCharManufacturerNameString,       /**< Manufacturer Name String */
    eDeviceInfoCharIEEEDataList,                 /**< IEEE 11073-20601 Regulatory Certification Data List */
    eDeviceInfoCharPnPID,                        /**< PnP ID */
    eDeviceInfoNbAttributes
} eDeviceInfoAttributes_t;


/**
 * @brief Creates and starts FreeRTOS device information service
 *
 * @return true if the service is initialized successfully, false otherwise
 */
BaseType_t cHalBleDeviceInfo_Init( void );


/**
 * @brief Cleanup device information service.
 *
 * @return true If the cleanup is successful false otherwise.
 */
BaseType_t cHalDeviceInfo_Cleanup( void );


#endif /* MAOPAO_BLE_DEVICE_INFOMATION_H_ */
