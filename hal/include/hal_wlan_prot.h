/*
 * Copyright (c) 2023, jeejio Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2023-02-28       lk         the first version
 */

#ifndef __HAL_WLAN_PROT_H__
#define __HAL_WLAN_PROT_H__

#include <swldef.h>
#include <hal_wlan_dev.h>

#ifdef __cplusplus
extern "C" {
#endif

#ifndef WLAN_PROT_NAME_LEN
#define WLAN_PROT_NAME_LEN  (8)
#endif

#ifndef WLAN_PROT_MAX
#define WLAN_PROT_MAX       (1)
#endif

#define LWAN_ID_PREFIX      (0x5054)

typedef enum
{
    WLAN_PROT_EVT_INIT_DONE = 0,
    WLAN_PROT_EVT_CONNECT,
    WLAN_PROT_EVT_DISCONNECT,
    WLAN_PROT_EVT_AP_START,
    WLAN_PROT_EVT_AP_STOP,
    WLAN_PROT_EVT_AP_ASSOCIATED,
    WLAN_PROT_EVT_AP_DISASSOCIATED,
    WLAN_PROT_EVT_MAX,
} WlanProtEvent_t;

struct WlanProt_st;
struct WlanProtOps_st
{
    Err_t (*prot_recv)(struct WlanDevice_st *wlan, void *buff, int len);
    struct WlanProt_st *(*dev_reg_callback)(struct WlanProt_st *prot, struct WlanDevice_st *wlan);
    void (*dev_unreg_callback)(struct WlanProt_st *prot, struct WlanDevice_st *wlan);
};

struct WlanProt_st
{
    char name[WLAN_PROT_NAME_LEN];
    Uint32_t id;
    const struct WlanProtOps_st *ops;
};

typedef void (*WlanProtEventHandler_t)(struct WlanProt_st *port, struct WlanDevice_st *wlan, int event);

Err_t lHalWlanProtAttach(const char *dev_name, const char *prot_name);

Err_t lHalWlanProtAttachDev(struct WlanDevice_st *wlan, const char *prot_name);

Err_t lHalWlanProtDetach(const char *dev_name);

Err_t lHalWlanProtDetachDev(struct WlanDevice_st *wlan);

Err_t lHalWlanProtRegisetr(struct WlanProt_st *prot);

Err_t lHalWlanProtTransferDev(struct WlanDevice_st *wlan, void *buff, int len);

Err_t lHalWlanDevTransferProt(struct WlanDevice_st *wlan, void *buff, int len);

Err_t lHalWlanProtEventRegister(struct WlanProt_st *prot, WlanProtEvent_t event, WlanProtEventHandler_t handler);

Err_t lHalWlanProtEventUnregister(struct WlanProt_st *prot, WlanProtEvent_t event);

int lHalWlanProtReady(struct WlanDevice_st *wlan, struct WlanBuff_st *buff);

void vHalWlanProtDump(void);

#ifdef __cplusplus
}
#endif

#endif /* __HAL_WLAN_PROT_H__ */
