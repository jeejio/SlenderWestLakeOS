#ifndef HAL_NETCONFIG_H
#define HAL_NETCONFIG_H

#include <stdint.h>
#include <freertos/swldef.h>

#ifdef __cplusplus
extern "C"
{
#endif // __cplusplus

#define __NET_CONF(LABEL) NETCONFIG_ ## LABEL

#define NETCONFIG_EMPTY_ITEM 0xFF    // Empty config item with 0xFF length flag
#define NETCONFIG_END_FLAG 0    // Ending file with 0x00 flag
#define NETCONFIG_BYTES_OF_LENGTH_ITEM 1   // Every length item of config is 1 byte long 


#define __NET_CONF_IDX(LABEL) NETCONFIG_ ## LABEL ##_INDEX

#define NETCONFIG_ENABLE_INDEX                     0
#define NETCONFIG_AUTHORIZATION_ADDRESS_INDEX      1
#define NETCONFIG_MQTT_SERVER_IP_INDEX             2
#define NETCONFIG_MQTT_SERVER_PORT_INDEX           3
#define NETCONFIG_MQTT_USERNAME_INDEX              4
#define NETCONFIG_MQTT_PASSWORD_INDEX              5 
#define NETCONFIG_SSID_INDEX                       6
#define NETCONFIG_PSK_INDEX                        7
#define NETCONFIG_CLIENT_ID_INDEX                  8
#define NETCONFIG_SUBSCRIPTION_TOPIC_PREFIX_INDEX  9
#define NETCONFIG_PUBLISHMENT_TOPIC_PREFIX_INDEX   10
// end index must be last index otherwise abnormal operation may occur
#define NETCONFIG_END_INDEX                        11

int32_t lHalGetEnableFlag(uint8_t* out_data);
int32_t lHalGetAuthorizationAddress(char* out_data, int32_t out_data_len);
int32_t lHalGetMQTTServerIP(char* out_data, int32_t out_data_len);
int32_t lHalGetMQTTServerPort(int16_t* port);
int32_t lHalGetMQTTUsername(char* out_data, int32_t out_data_len);
int32_t lHalGetMQTTPassword(char* out_data, int32_t out_data_len);
int32_t lHalGetSSID(char* out_data, int32_t out_data_len);
int32_t lHalGetPSK(char* out_data, int32_t out_data_len);
int32_t lHalGetClientID(char* out_data, int32_t out_data_len);
int32_t lHalGetSubscriptionTopicPrefix(char* out_data, int32_t out_data_len);
int32_t lHalGetPublishmentTopicPrefix(char* out_data, int32_t out_data_len);

int32_t lHalSetEnableFlag(uint8_t flag);

#ifdef __cplusplus
}
#endif // __cplusplus
#endif