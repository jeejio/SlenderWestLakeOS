#ifndef __CONFIG_H__
#define __CONFIG_H__

/* Automatically generated file; DO NOT EDIT. */
/* Function Configuration */

/*********************************************************************
 *************************** Device Drivers **************************
 *********************************************************************/
/* hwtimer */
#define SWL_USING_HWTIMER

/* wifi */
#define USING_WIFI
#define WLAN_DEVICE_STA_NAME "wlan0"
#define WLAN_DEVICE_AP_NAME "wlan1"
#define WLAN_PASSWORD_MAX_LENGTH 32
#define WLAN_MANAGE_ENABLE
#define WLAN_SCAN_WAIT_MS 10000
#define WLAN_CONNECT_WAIT_MS 10000
#define WLAN_SCAN_SORT
// #define WLAN_AUTO_CONNECT_ENABLE
#define AUTO_CONNECTION_PERIOD_MS 2000
// #define WLAN_CFG_ENABLE
#define WLAN_CFG_INFO_MAX 3
#define WLAN_PROT_ENABLE
// #define WLAN_PROT_NAME_LEN 8
// #define WLAN_PROT_MAX 2
#define WLAN_DEFAULT_PROT "lwip"
#define WLAN_PROT_LWIP_ENABLE
#define WLAN_PROT_LWIP_NAME "lwip"
#define WLAN_WORK_THREAD_ENABLE
#define WLAN_WORKQUEUE_THREAD_NAME "wlan_job"
#define WLAN_WORKQUEUE_THREAD_SIZE 2048
#define WLAN_WORKQUEUE_THREAD_PRIO 22
// #define USING_SYSTEM_WORKQUEUE

#define SYSTEM_WORKQUEUE_STACKSIZE 2048
#define SYSTEM_WORKQUEUE_PRIORITY 23

#define WLAN_TRANSFER

#endif
