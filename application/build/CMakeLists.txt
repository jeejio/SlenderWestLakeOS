idf_component_register(SRCS "app_main.c"
                    INCLUDE_DIRS "${jeejio_path}/hal/include"
                    REQUIRES maopao tal)

target_compile_options(${COMPONENT_LIB} PRIVATE "-Wno-format")
