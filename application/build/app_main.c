#include <auto_init.h>
#include "maopaoChannel.h"
#include "maopaoServer.h"
#include "tal_ota.h"
#define VERSION "v0.0.1"

void app_main(void) {
    printf("Build version:%s, build time:%s %s\n", VERSION, __DATE__, __TIME__);

    /*System component initialization, do not remove*/
    vHalComponentsInit();

    /*Maopao service channel initialization, channel could select mqtt/uart/ble*/
    maopaoServerInit(maopaoChannelMqttInit());

    /*add timer service, etc*/
    //maopaoServerAddServices(tal_timer_serviceGroup);
    maopaoServerAddServices(tal_ota_serviceGroup);

    /*Run maopao service*/
    maopaoServerRun();

    /*User defined logic can be added below*/
}