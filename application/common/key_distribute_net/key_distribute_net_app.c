#include <string.h>

#include "auto_init.h"

#include "hal_gpio.h"
#include "ble_wifi_provisioning_object.h"
#include "flexible_button.h"

#include "hal_netconfig.h"

#define INDEPENDENT 0X00
#define TEST 0X01

uint8_t osMode = 0xff;

#define DISTRIBUTE_KEY (10)
#define ENUM_TO_STR(e) (#e)

typedef enum
{
    USER_BUTTON_0 = 0,
    USER_BUTTON_MAX
} user_button_t;

static char *enum_event_string[] = {
    ENUM_TO_STR(FLEX_BTN_PRESS_DOWN),
    ENUM_TO_STR(FLEX_BTN_PRESS_CLICK),
    ENUM_TO_STR(FLEX_BTN_PRESS_DOUBLE_CLICK),
    ENUM_TO_STR(FLEX_BTN_PRESS_REPEAT_CLICK),
    ENUM_TO_STR(FLEX_BTN_PRESS_SHORT_START),
    ENUM_TO_STR(FLEX_BTN_PRESS_SHORT_UP),
    ENUM_TO_STR(FLEX_BTN_PRESS_LONG_START),
    ENUM_TO_STR(FLEX_BTN_PRESS_LONG_UP),
    ENUM_TO_STR(FLEX_BTN_PRESS_LONG_HOLD),
    ENUM_TO_STR(FLEX_BTN_PRESS_LONG_HOLD_UP),
    ENUM_TO_STR(FLEX_BTN_PRESS_MAX),
    ENUM_TO_STR(FLEX_BTN_PRESS_NONE),
};

static char *enum_btn_id_string[] = {
    ENUM_TO_STR(USER_BUTTON_0),
    ENUM_TO_STR(USER_BUTTON_MAX),
};

static flex_button_t keyGroup[USER_BUTTON_MAX];

static uint8_t u8DistributeKeyRead(void *arg)
{
    return lHalPinRead(DISTRIBUTE_KEY) ? 1 : 0;
}

static void vDistributeKeyEvtCb(void *arg)
{
    flex_button_t *btn = (flex_button_t *)arg;
    LOGI("SINGLE SWITCH","id: [%d - %s]  event: [%d - %30s]  repeat: %d\n",
           btn->id, enum_btn_id_string[btn->id],
           btn->event, enum_event_string[btn->event],
           btn->click_cnt);
    switch (btn->id)
    {
    case USER_BUTTON_0:
    {
        switch (btn->event)
        {
        case FLEX_BTN_PRESS_CLICK:
        {
            LOGI("SINGLE SWITCH","key click!\n");
        }
        break;
        case FLEX_BTN_PRESS_DOUBLE_CLICK:
        {
            LOGI("SINGLE SWITCH","double click!\n");
        }
        break;
        case FLEX_BTN_PRESS_LONG_HOLD:
        {
            LOGI("SINGLE SWITCH","key long hold!\n");
#if 0            
            // lConfigDistributeNetStatus(DISTRIBUTE_START);
            // lSave_DistributeNetStatus(DISTRIBUTE_START);
            // LOGI("SINGLE SWITCH","System restart!\n");
            // vHalSystemRestart();
#else
            if((lHalGetEnableFlag(&osMode) != DEV_EOK))      
            {
                LOGE("SINGLE SWITCH", "Read osMode error, [osMode]:%02X", osMode);
                return;
            }
            else
            {
                LOGI("SINGLE SWITCH", "[osMode]:%02X",osMode);
            }

            if(!(osMode == INDEPENDENT || osMode == TEST))  //manhattan
            {
                lEnterDistributefNetMode();
            }
#endif            
        }
        break;

        default:
            break;
        }
    }
    break;
    }
}

void vDistributeKeyTask(void *arg)
{
    while (1)
    {
        flex_button_scan();
        vTaskDelay(pdMS_TO_TICKS(20));
    }
}

int vDistributKeyInit(void)
{
    uint8_t i;
    LOGI(__func__,"Distribute key init\n");
    vHalPinMode(DISTRIBUTE_KEY, PIN_MODE_INPUT_PULLUP);

    memset(keyGroup, 0, sizeof(keyGroup));
    for (i = 0; i < USER_BUTTON_MAX; i++)
    {
        keyGroup[i].id = i;
        keyGroup[i].usr_button_read = u8DistributeKeyRead;
        keyGroup[i].cb = vDistributeKeyEvtCb;
        keyGroup[i].pressed_logic_level = 0;
        keyGroup[i].max_multiple_clicks_interval = FLEX_MS_TO_SCAN_CNT(500); // 单击间隔500ms
        keyGroup[i].short_press_start_tick = FLEX_MS_TO_SCAN_CNT(1500);
        keyGroup[i].long_press_start_tick = FLEX_MS_TO_SCAN_CNT(3000);
        keyGroup[i].long_hold_start_tick = FLEX_MS_TO_SCAN_CNT(4000);
        flex_button_register(&keyGroup[i]);
    }
    xTaskCreate(vDistributeKeyTask, "vDistributeKeyTask", 4096, NULL, 5, NULL);

    return 0;
}

INIT_APP_EXPORT(vDistributKeyInit);