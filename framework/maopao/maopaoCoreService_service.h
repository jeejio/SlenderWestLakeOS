//Slender West Lake automatically generated code, do not edit

#ifndef MAOPAOCORESERVICE_SERVICE_H
#define MAOPAOCORESERVICE_SERVICE_H

#include "maopaoServer.h"
#include "maopaoError.h"
#include "mson.h"


void __maopaoServer__lMaopaoRpcList__service__(maopao_requestInfo_st *_requestInfo, SemaphoreHandle_t _mux);

void __maopaoServer__getTalInfo__service__(maopao_requestInfo_st *_requestInfo, SemaphoreHandle_t _mux);

#endif  //MAOPAOCORESERVICE_SERVICE_H
