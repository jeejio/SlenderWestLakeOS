/*
 * Copyright (c) 2023, jeejio Development Team
 *
 * Change Logs:
 * Date           Author         Notes
 * 2023-04-23     quziming      the first version
 */
#ifndef SLENDERWESTLAKEOS_MAOPAOERROR_H
#define SLENDERWESTLAKEOS_MAOPAOERROR_H
#include "stdbool.h"

#if 0
#define MAOPAO_SUCCESS 200
#define MAOPAO_ERROR_FIND_METHOD_FAILED 404
#define MAOPAO_ERROR_PARAM_ERROR 500


#define MAOPAO_ERROR_OUT_OF_MEMORY 1012
#define MAOPAO_ERROR_TRANSPORT_TIMEOUT 1103
#define MAOPAO_ERROR_RPC_ANALYSE_FAILED 1150
#define MAOPAO_ERROR_LENGTH_MISMATCHED 1154
#define MAOPAO_ERROR_TAL_OPERATION_FAILED 1501
#define MAOPAO_ERROR_TIMEOUT 1502

#define MAOPAO_ERROR_READONLEY_PERMISSION 1329

#define MAOPAO_SUCCESS_MSG "success"
#define MAOPAO_ERROR_FIND_METHOD_FAILED_MSG "find method failed"

#define MAOPAO_ERROR_OUT_OF_MEMORY_MSG "out of memory"
#define MAOPAO_ERROR_TRANSPORT_TIMEOUT_MSG "transport timeout"
#define MAOPAO_ERROR_RPC_ANALYSE_FAILED_MSG "rpc analyse failed"
#define MAOPAO_ERROR_LENGTH_MISMATCHED_MSG "length mismatched"
#define MAOPAO_ERROR_TAL_OPERATION_FAILED_MSG "tal operation failed"
#define MAOPAO_ERROR_TIMEOUT_MSG "timeout"
#define MAOPAO_ERROR_READONLEY_PERMISSION_MSG "readonly permission"
#else
/***************************common start********************************/
#define MAOPAO_ERROR_TAL_OPERATION_FAILED 1501		//TODO:等tal代码生成工具完善后删除
#define MAOPAO_ERROR_TAL_OPERATION_FAILED_MSG "tal operation failed"	//TODO:等tal代码生成工具完善后删除

#define MAOPAO_SUCCESS 							(200)
#define MAOPAO_ERROR_OUT_OF_MEMORY 				(1012)
#define MAOPAO_ERROR_TRANSPORT_TIMEOUT 			(1103)
#define MAOPAO_ERROR_LENGTH_MISMATCHED 			(1154)
#define MAOPAO_ERROR_DRIVER_FAILED				(1205)
#define MAOPAO_ERROR_DEVICE_OPEN_FAILED			(1206)
#define MAOPAO_ERROR_DEVICE_NOT_FIND			(1219)
#define MAOPAO_ERROR_READONLY_PERMISSION 		(1329)
#define MAOPAO_ERROR_RPC_FIND_METHOD_FAILED 	(-32601)
#define MAOPAO_ERROR_RPC_ANALYSE_FAILED 		(-32700)
#define MAOPAO_ERROR_RPC_TIMEOUT 				(-32099)
#define MAOPAO_ERROR_RPC_INVALID_PARAMS			(-32602)

#define MAOPAO_SUCCESS_MSG "success"
#define MAOPAO_ERROR_OUT_OF_MEMORY_MSG "out of memory"
#define MAOPAO_ERROR_TRANSPORT_TIMEOUT_MSG "transport timeout"
#define MAOPAO_ERROR_LENGTH_MISMATCHED_MSG "length mismatched"
#define MAOPAO_ERROR_DEVICE_OPEN_FAILED_MSG "device opening failed"
#define MAOPAO_ERROR_DRIVER_FAILED_MSG "i/o error"
#define MAOPAO_ERROR_DEVICE_NOT_FIND_MSG "unable to find device"
#define MAOPAO_ERROR_RPC_FIND_METHOD_FAILED_MSG "find rpc method failed"
#define MAOPAO_ERROR_READONLY_PERMISSION_MSG "Readonly permission"
#define MAOPAO_ERROR_RPC_ANALYSE_FAILED_MSG "rpc analyse failed"
#define MAOPAO_ERROR_RPC_TIMEOUT_MSG "rpc timeout"
#define MAOPAO_ERROR_RPC_INVALID_PARAMS_MSG "invalid parameter"
/***************************common end********************************/

/***************************timer start********************************/
#define TAL_TIMER_ERROR_TASK_COUNT_OVERMUCH 			 		 (100001)
#define TAL_TIMER_ERROR_TASK_DURATION_OVERMUCH			 		 (100002)
#define TAL_TIMER_ERROR_TASK_DURATION_TOO_SHORT			 		 (100003)
#define TAL_TIMER_ERROR_TASK_TIME_OVERMUCH				 		 (100004)
#define TAL_TIMER_ERROR_TASK_TIME_TOO_LITTLE			 		 (100005)
#define TAL_TIMER_ERROR_TASK_STOP_STATE_FAULT			 		 (100006)
#define TAL_TIMER_ERROR_TASK_RESUME_STATE_FAULT			 		 (100007)
#define TAL_TIMER_ERROR_TASK_STOP_OVER_TIME				         (100008)
#define TAL_TIMER_ERROR_TASK_RESET_FAULT				 		 (100009)
#define TAL_TIMER_ERROR_TASK_ID_NOT_FOUND				 		 (100010)
#define TAL_TIMER_ERROR_TASK_TIME_CALIBRATION_UNSUCCESSFUL		 (100011)
#define TAL_TIMER_ERROR_TASK_COUNTDOWN_FAILED			 		 (100012)
#define TAL_TIMER_ERROR_TASK_TIME_FAILED 				 	     (100013)
#define TAL_TIMER_ERROR_TASK_DURATION_NOT_EXIST			 		 (100014)
#define TAL_TIMER_ERROR_TASK_TIMESTAMP_NOT_EXIST			     (100015)
#define TAL_TIMER_ERROR_TASK_METHOD_NOT_EXIST			 		 (100016)
#define TAL_TIMER_ERROR_TASK_INVALID_METHOD				 		 (100017)
#define TAL_TIMER_ERROR_TASK_PARAMETER_NOT_FOUND			 	 (100018)
#define TAL_TIMER_ERROR_TASK_INVALID_PARAMETER 			 	 	 (100019)

#define TAL_TIMER_ERROR_TASK_COUNT_OVERMUCH_MSG "task count overmuch"
#define TAL_TIMER_ERROR_TASK_DURATION_OVERMUCH_MSG "task duration overmuch"
#define TAL_TIMER_ERROR_TASK_DURATION_TOO_SHORT_MSG "task duration too short"
#define TAL_TIMER_ERROR_TASK_TIME_OVERMUCH_MSG "task time overmuch"
#define TAL_TIMER_ERROR_TASK_TIME_TOO_LITTLE_MSG "task time too little"
#define TAL_TIMER_ERROR_TASK_STOP_STATE_FAULT_MSG "task stop state fault"
#define TAL_TIMER_ERROR_TASK_RESUME_STATE_FAULT_MSG "task resume state fault"
#define TAL_TIMER_ERROR_TASK_STOP_OVER_TIME_MSG "task stop over time"
#define TAL_TIMER_ERROR_TASK_RESET_FAULT_MSG "task reset fault"
#define TAL_TIMER_ERROR_TASK_ID_NOT_FOUND_MSG "task id not found"
#define TAL_TIMER_ERROR_TASK_TIME_CALIBRATION_UNSUCCESSFUL_MSG "time calibration unsuccessful"
#define TAL_TIMER_ERROR_TASK_COUNTDOWN_FAILED_MSG "countdown failed"
#define TAL_TIMER_ERROR_TASK_TIME_FAILED_MSG "time failed"
#define TAL_TIMER_ERROR_TASK_DURATION_NOT_EXIST_MSG "duration not exist"
#define TAL_TIMER_ERROR_TASK_TIMESTAMP_NOT_EXIST_MSG "timestamp not exist"
#define TAL_TIMER_ERROR_TASK_METHOD_NOT_EXIST_MSG "method not exist"
#define TAL_TIMER_ERROR_TASK_INVALID_METHOD_MSG "invalid method"
#define TAL_TIMER_ERROR_TASK_PARAMETER_NOT_FOUND_MSG "parameter not found"
#define TAL_TIMER_ERROR_TASK_INVALID_PARAMETER_MSG "invalid parameter"
/***************************timer end********************************/
#endif

typedef struct{
    int code;
    char *message;
}MAOPAO_ERRER_CODE;

extern MAOPAO_ERRER_CODE errlist[];

extern bool flag_find;
extern bool flag_open;
extern int checkresult; 

extern int lDeviceCheck();
#endif //SLENDERWESTLAKEOS_MAOPAOERROR_H
