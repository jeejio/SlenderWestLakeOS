/*
 * Copyright (c) 2023, Jeejio Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2023-03-27     None         the first version.
 *
 * maopaoServer internal declarations for rpc, users should never change it.
 *
 */

#ifndef __MAOPAOSERVER_H__
#define __MAOPAOSERVER_H__

#include <stdio.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/semphr.h"
#include "maopaoChannel.h"
#include "maopaoList.h"
#include "maopao-json/mson.h"
#include "maopaoReport.h"

#ifdef __cplusplus
extern "C" {
#endif

#define MAOPAO_TASK_STACK_SIZE_DEFAULT    4096
#define MAOPAO_TASK_PRIO_DEFAULT          10    // 1-25
#define MAOPAO_TASK_HANDLE_TIMEOUT        (5 * 1000 / portTICK_PERIOD_MS)
#define MAOPAO_EVENT_QUEUE_LENGTH         20
#define MAOPAO_INFO_QUEUE              6
#define MAOPAO_SUCCESS 200

#define MAOPAO_LOCAL  "maopao_local"

/////////////////////////////////////////////////////////////////////////////////////////// maopao_Server 数据结构
typedef struct maopao_CfgInfo     maopao_cfg_st;
typedef struct maopao_RequestInfo maopao_requestInfo_st;

typedef void (* maopao_handler_cb)(maopao_requestInfo_st *ri, SemaphoreHandle_t mux);


#define MAOPAO_SERVICE(_cb) __maopaoServer__##_cb##__service__

typedef struct TalInfo_st {
    char *manufacturer;
    char *category;
    char *version;
    char *developer;
} TalInfo_st, *PTalInfo_st;

// 实现rpc服务的核心结构体，综合管理配置信息、通道信息、service信息、events信息等
typedef struct maopao_Server {
    SLIST_HEAD(handlers, maopao_handler_st) handlers;   // 多个handlers通过链表进行动态管理
    QueueHandle_t      eventQueue;                      // event notify queue
    unsigned int       eventMask;                       // event mask
    maopao_channel_st  *channel;                        // channel info
    maopao_cfg_st      *cfg;                            // config info
    int isRunning;                                      // server is running
} maopao_server_st;

typedef struct {
    char *method;
    char *methodId;
    maopao_handler_cb handlerCb;
    char *inParam;
    char *outParam;
}maopao_service_handler_st;

// 处理rpc请求的handler
typedef struct {
    maopao_service_handler_st service;
    SemaphoreHandle_t mux;
    SLIST_ENTRY(maopao_handler_st) next;
} maopao_handler_st;

// 配置信息
typedef struct maopao_CfgInfo {
    const char *id;
    int max_queue_length;
    int default_channel_idle_close_timeout;
} maopao_cfg_st;

// 上位机发送rpc请求消息数据结构
typedef struct maopao_RequestInfo {
    int  timeout;
    char *method;
    char *methodId;
    char *params;
    char *id;
    char *from;
    char *to;
} maopao_requestInfo_st;

typedef struct maopao_EventInfo {
    char *params;
    char *method;
    char *methodId;
    uint32_t event;
} maopao_eventInfo_st;

/////////////////////////////////////////////////////////////////////////////////////////// maopao_Server 处理函数
bool maopaoServerInit(maopao_channel_st *ch);

void maopaoServerAddDefaultServiceGroup(maopao_service_handler_st serviceGroup[]);

void maopaoServerAddServices(maopao_service_handler_st services[]);

void maopaoServerAddService(maopao_service_handler_st service);

void MaopaoServerMsgTask(void *pvPara);

void MaopaoServerRequestHandle(maopao_requestInfo_st *requestInfo);

maopao_handler_st *maopaoServicesGetHandler(maopao_requestInfo_st *requestInfo);

void maopaoServerRun(void);

void maopaoServerRunOnService(void *pvParam);

void maopaoServerAddEvents(uint32_t *maopaoEvents);

int maopaoServerCallRemoteWithEvent(maopao_eventInfo_st *eventInfo);

//上报错误消息
int maopaoReportError(int code, char *msg);

int maopaoServerReportOnline(char *pvParams);

int maopaoServerCallRemoteWithMethod(char *method, char *methodId,char *pvParams,maopao_report_cb maopaoReport_cb);

void maopaoServerRunOnEvent(void *pvParameters);

void maopaoServerSendMsgToEventQueue(maopao_eventInfo_st *eventInfo);

uint32_t maopaoServerReadChannelBuf(maopao_channel_st *pchannel, char *pBuf);

maopao_server_st *maopaoServerGetGlobal(void);

// int maopaoServerAnalyse(const char *maopaoMsg, maopao_frame_st *frame);

void destroyRequestInfo(maopao_requestInfo_st *requestInfo);

void maopaoServerSendResponse(maopao_requestInfo_st *ri, char *result);

void maopaoServerSendError(maopao_requestInfo_st *ri, int code, char *msg);

void maopaoServerDeserializeMsg(char *pSrc, char *pDes);

void maopaoServerCallcb(maopao_requestInfo_st *requestInfo, maopao_handler_st *phandler);

maopao_cfg_st *maopaoServerSysConfigGet(void);

void LocalRpcCall(char *method, char *params, char *from); // for local rpc usage

void vMaopaoDeviceBindUser(char *token);

#ifdef __cplusplus
}
#endif

#endif /* __MAOPAOSERVER_H__ */
