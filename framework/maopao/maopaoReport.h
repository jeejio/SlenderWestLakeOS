#ifndef __MAOPAOREPORT_H__
#define __MAOPAOREPORT_H__

#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <time.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/semphr.h"
#include "maopaoList.h"
#include "hal_wlan_mgnt.h"

#ifdef __cplusplus
extern "C" {
#endif

// #define CONCURRENCY

typedef void(*maopao_report_cb)();

extern time_t TIMEOUT;
extern int MAX_LENGTH;
extern int CUR_LENGTH;

typedef struct maopao_report_handler{
    char id[64];
    maopao_report_cb reportCb;
    time_t timeout;
}maopao_report_handler_st;


typedef struct maopao_report_node{
    maopao_report_handler_st report;
    SLIST_ENTRY(maopao_report_node) next;
    // struct maopao_handler *next;
} maopao_report_node_st;


typedef struct maopao_report{
    SLIST_HEAD(head, maopao_report_node) head;
    // struct maopao_handler *head;
    SemaphoreHandle_t xSemaphore;
} maopao_report_st;

typedef struct maopao_response_Info {
    char *result;
    char *id;
    char *code;
    char *message;
} maopao_responseInfo_st;

bool maopaoReportInit();

extern maopao_report_st *getglobalMaopaoReport();

void maopaoReportAddReport(maopao_report_handler_st report);

void destroyResponseInfo(maopao_responseInfo_st *responseInfo);

extern void modifyTimeoutValue(time_t value);

extern void MaopaoReportMsgTask(void *pvPara);

#ifdef __cplusplus
}
#endif

#endif /* __MAOPAOREPORT_H__ */