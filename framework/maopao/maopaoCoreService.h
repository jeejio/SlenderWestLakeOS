//Slender West Lake automatically generated code

#ifndef MAOPAOCORESERVICE_H
#define MAOPAOCORESERVICE_H

#include <stdbool.h>
#include "maopaoServer.h"
#include "maopaoError.h"

extern maopao_service_handler_st maopaoCoreService_serviceGroup[];

/**
 * @brief lMaopaoRpcList
 * @param[out] rpcList
 * @return errorCode
 */
int lMaopaoRpcList(jsonStr *rpcList);

/**
 * @brief _getTalInfo
 * @param[out] talInfo
 * @return errorCode
 */
int getTalInfo(jsonStr talInfo);

#endif  //MAOPAOCORESERVICE_H
