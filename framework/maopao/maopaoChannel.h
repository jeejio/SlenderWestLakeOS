/*
 * Copyright (c) 2023, Jeejio Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2023-03-27     None         the first version.
 *
 * maopaoServer internal declarations for rpc, users should never change it.
 *
 */

#ifndef __MAOPAOCHANNEL_H__
#define __MAOPAOCHANNEL_H__

#include <stdint.h>

#define RECV_BUF_SIZE 1024
#define BLE_BUF_SIZE 1024

#define    PRINT_MACRO_HELPER(x)  #x
#define    PRINT_MACRO(x)         #x"="PRINT_MACRO_HELPER(x)

#ifndef JENKINS_AUTO_PACKAGE
    /***change env , modify macro ,only enable one mode *****/
    #define DEV 0
    #define QA 0
    #define RC 0
    #define RELEASE 1
    /********/

    #pragma message("MARC_ENV "PRINT_MACRO(DEV))
    #pragma message("MARC_ENV "PRINT_MACRO(QA))
    #pragma message("MARC_ENV "PRINT_MACRO(RC))
    #pragma message("MARC_ENV "PRINT_MACRO(RELEASE))

    #if(DEV + QA + RC + RELEASE) != 1
    #error "ENV macro error!!!"
    #endif
#endif

typedef struct channel_buffer
{
    uint8_t *buf;
    uint16_t len;
    uint16_t write;
    uint16_t read;
    


} channel_buffer_st;

typedef struct maopao_channel
{
    void (*connect)(struct maopao_channel *ch);
    uint32_t (*send)(struct maopao_channel *ch, char *frame);
    uint32_t (*receive)(struct maopao_channel *ch, char *frame);
    char *(*getId)(struct maopao_channel *ch); // 用于获取主动上报的id，由函数实现内分配空间，由发送处释放
    void (*close)(struct maopao_channel *ch);
    uint32_t counter;
    channel_buffer_st rx_buf;
} maopao_channel_st;

maopao_channel_st *maopaoChannelUartInit(void);
void maopaoChannelUartDestroy(maopao_channel_st *ch);

maopao_channel_st *maopaoChannelMqttInit(void);
void maopaoChannelMqttDestroy(maopao_channel_st *ch);

void maopaoChannelMqttConnectNetworkCallback();

void maopaoChannelMqttDisconnectNetworkCallback();

maopao_channel_st* maopaoChannelGattsInit(void);

void maopaoChannelGattsDestory(maopao_channel_st *ch);
#endif