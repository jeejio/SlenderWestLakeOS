#ifndef _MD5H
#define _MD5H

#include "string.h"

typedef struct
{
    unsigned int count[2];
    unsigned int state[4];
    unsigned char buffer[64];
} MD5CTX_t;

#define F(x, y, z) ((x & y) | (~x & z))
#define G(x, y, z) ((x & z) | (y & ~z))
#define H(x, y, z) (x ^ y ^ z)
#define I(x, y, z) (y ^ (x | ~z))
#define ROTATE_LEFT(x, n) ((x << n) | (x >> (32 - n)))
#define FF(a, b, c, d, x, s, ac)  \
    {                             \
        a += F(b, c, d) + x + ac; \
        a = ROTATE_LEFT(a, s);    \
        a += b;                   \
    }
#define GG(a, b, c, d, x, s, ac)  \
    {                             \
        a += G(b, c, d) + x + ac; \
        a = ROTATE_LEFT(a, s);    \
        a += b;                   \
    }
#define HH(a, b, c, d, x, s, ac)  \
    {                             \
        a += H(b, c, d) + x + ac; \
        a = ROTATE_LEFT(a, s);    \
        a += b;                   \
    }
#define II(a, b, c, d, x, s, ac)  \
    {                             \
        a += I(b, c, d) + x + ac; \
        a = ROTATE_LEFT(a, s);    \
        a += b;                   \
    }
void vMaopaoMD5Init(MD5CTX_t *context);
void vMaopaoMD5Update(MD5CTX_t *context, unsigned char *input, unsigned int inputlen);
void vMaopaoMD5Final(MD5CTX_t *context, unsigned char digest[16]);
void vMaopaoMD5Transform(unsigned int state[4], unsigned char block[64]);
void vMaopaoMD5Encode(unsigned char *output, unsigned int *input, unsigned int len);
void vMaopaoMD5Decode(unsigned int *output, unsigned char *input, unsigned int len);
void vMaopaoMd5(unsigned char *src, unsigned int len, unsigned char *result);
#endif