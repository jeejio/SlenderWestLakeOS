#include "freertos/FreeRTOSConfig.h"
#include "hal_ble_os_ble_config_common.h"
#include "hal_ble_os_ble_config.h"
// #include "hal_ble_gatt_server_demo.h"
#include "hal_ble.h"
#include "freertos/task.h"
#include "freertos/semphr.h"
#include "cJSON.h"
#include <time.h>

typedef struct BleWifiProvisionValue_st {
    uint16_t header;
    uint8_t flag;
    uint8_t method;
    uint16_t length;
    char *payload;
    uint8_t Checksum;
}BleWifiProvisionValue_t;

extern BleWifiProvisionValue_t value;
extern char * MaopaoBle_Pack;
extern int size;
extern BaseType_t flag_timeout;
extern BaseType_t flag_timeout1;
extern BaseType_t flag_net_mode_ble;

typedef struct BleWifiProvisionNode{
    char *data;
    struct BleWifiProvisionNode *next;
}BleWifiProvisionNode,*Q_BleWifiProvisionNode;

typedef struct queues{
    Q_BleWifiProvisionNode front;
    Q_BleWifiProvisionNode rear;
}queues,*Queues;

extern Q_BleWifiProvisionNode xBleWifiProvisionInit_Node();
extern Queues xBleWifiProvisionInit_Queues();
extern int lBleWifiProvisionQueue_Push(Queues queues,char *e);
extern int lBleWifiProvisionQueue_Pop(Queues q,char **e);
void vJeeChoice_Distribution_network_mode(bool ble);
int lJeeEnter_distribution_network_mode();
void vJeeMethodHandle();
extern void __vRecvTimeOut(void *pvParams);
extern int lBleWifiProvisionGet_checksum();
extern void vBleWifiProvisionParse_json();
extern void vBleWifiProvisionReceiveData(BleWriteEventParams_t * pxWriteParam);

void vBleGattsWifiProvisioningTransport_init(void);
