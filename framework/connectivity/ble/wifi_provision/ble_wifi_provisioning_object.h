#ifndef __BLE_WIFI_PRIOVISIONING_OBJECT_H__
#define __BLE_WIFI_PRIOVISIONING_OBJECT_H__

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "hal_ble_adv.h"

#define TLV_FMT_TYPE_STR 0
#define TLV_FMT_TYPE_INT 1
#define TLV_FMT_TYPE_DOUBLE 2
#define TLV_FMT_TYPE_BOOL 3

typedef struct tlv_fmt
{
    uint8_t type;
    uint8_t length;
    uint8_t *value; 
}tlv_fmt_t;

typedef struct jeeLogInDataFmt
{
    tlv_fmt_t *ssid;
    tlv_fmt_t *password;
    tlv_fmt_t *token;
    tlv_fmt_t *navChallenge;
    tlv_fmt_t *navMqtt;
}logInDataFmt_t;

typedef int32_t (*connect_wifi_task_t)(void *logIn);

typedef enum distribute_net_status
{
    DISTRIBUTE_NONE = 0,
    DISTRIBUTE_START,
    DISTRIBUTE_REMOTE_CONNECTED,
    DISTRIBUTE_RECEIVED_INFO,
    DISTRIBUTE_WIFI_CONNECTING,
    DISTRIBUTE_NET_OK,
}distribute_net_status_t;

typedef struct wifi_provision
{
    //广播、扫描回应相关
    uint8_t *piotBleAdvRawData;
    uint8_t *pJeeAdvScanRespData;

    set_BleAdvRespScanData_t setAdvData_CID;
    set_BleAdvRespScanData_t setAdvData_VID;
    set_BleAdvRespScanData_t setAdvData_FMSK;
    set_BleAdvRespScanData_t setAdvData_PID;
    set_BleAdvRespScanData_t setAdvData_MAC;
    set_BleAdvRespScanData_t setScanRespData_name;

    //接收相关
    logInDataFmt_t *logInData;               //用户联网、登录相关数据
    BaseType_t connect_wifi_task_handle;        //联网处理任务句柄
    connect_wifi_task_t wlanConnectHandler;      //联网处理任务
}wifi_provision_t;

typedef struct jee_ble_net_data {
    char ssid[64];
    char password[32];
    char navigateChallenge[128];
    char navigateMqtt[128];
    char token[128];
}BleLoginData_t;

extern BleLoginData_t login_data;
extern wifi_provision_t *g_pWifiProvision;

wifi_provision_t *xGet_WifiProvision(void);

int32_t lSave_DistributeNetStatus(int32_t stage);
int32_t lRead_DistributeNetStatus(void);
int32_t lConfigDistributeNetStatus(distribute_net_status_t status);     //transport, provision使用
distribute_net_status_t xGetDistributeNetStatus(void);                  //transport, provision使用

void fast_wlan_connect_task_ex(void);

extern int vDistributNetInit(void);
int32_t lEnterDistributefNetMode(void);

typedef enum BleWifiStage
{
    NULL_STAGE = 0,
    BLE_STAGE,
    WIFI_STAGE,
    MQTT_STAGE,
}BleWifiStage_t;
void BleWifiProvisionDebugLog(bool isLast,int stage);

int BleWifiProvisionDebugLogGetProgress(void);
void BleWifiProvisionDebugLogSetProgress(int Progress);

#endif