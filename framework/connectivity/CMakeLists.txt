idf_component_register(SRCS

    INCLUDE_DIRS "."
    "${jeejio_path}/framework/connectivity/ble/wifi_provision"
    REQUIRES "driver" "external" "maopao"
    PRIV_REQUIRES kernel hal 
)
jee_auto_init_list_register()

set(LIBS libconnectivity)
add_library(${LIBS} STATIC IMPORTED)
set_property(TARGET ${LIBS} PROPERTY IMPORTED_LOCATION ${CMAKE_CURRENT_SOURCE_DIR}/${LIBS}.a)
target_link_libraries(${COMPONENT_LIB} INTERFACE ${LIBS})
set_property(TARGET ${LIBS} APPEND PROPERTY INTERFACE_LINK_LIBRARIES ${COMPONENT_LIB})
