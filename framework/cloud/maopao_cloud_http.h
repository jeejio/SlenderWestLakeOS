
#ifndef __CLOUD_HTTP_H__
#define __CLOUD_HTTP_H__
#include "webclient.h"
#include "string.h"
#include "cJSON.h"
#define DEV  (0)

#define TOKEN_LENGTH (512)

typedef enum {
	CHANLLENGE = 1,
	VERIFICATION,
    GET_UUID,
}HTTP_PATH_ID;

typedef struct httpContext_t
{
    uint8_t isChallenge;
    uint8_t isVerification;
    uint8_t isGetUuid;
    uint8_t httpPathId;

} httpContext;


int lMaopaoCloudinit(void);
static int lMaopaoWebclientPostComm(const char *uri, const void *post_data, unsigned int data_len);
int lMaopaoWebclientPostJson(const char *uri, const char *post_data, size_t data_len, char **response, size_t *resp_len);


#endif