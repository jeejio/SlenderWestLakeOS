
#ifndef __MAOPAO_OTA_H__
#define __MAOPAO_OTA_H__

typedef enum
{
    maopaoOtaStateNull=0,
    maopaoOtaStateFail,
    maopaoOtaStateSucc,
}maopaoOtaState;


int lMaopaoOtaStart(char *url, char *version, char *checksum, char *key, char *iv, int size, int encrypted);
#endif