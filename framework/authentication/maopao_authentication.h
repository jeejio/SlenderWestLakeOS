/*
 * Copyright (c) 2023, jeejio Development Team
 *
 * Change Logs:
 * Date           Author         Notes
 * 2023-04-12     quziming      the first version
 */
#ifndef SLENDERWESTLAKEOS_MAOPAO_AUTHENTICATION_H
#define SLENDERWESTLAKEOS_MAOPAO_AUTHENTICATION_H

char *pGetMqttToken();

int lAuthentication();

#endif //SLENDERWESTLAKEOS_MAOPAO_AUTHENTICATION_H
