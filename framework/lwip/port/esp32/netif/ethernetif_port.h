/*
 * Copyright (c) 2023, Jeejio Technology Co. Ltd
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2023-03-08     Alpha        First Verison
 *
 */
#ifndef __NETIF_ETHERNETIF_H__
#define __NETIF_ETHERNETIF_H__

#ifdef __cplusplus
extern "C" {
#endif

#include "lwip/netif.h"
#include "freertos/swldef.h"

#define NIOCTL_GADDR        0x01
#ifndef LWIP_ETH_MTU
#define ETHERNET_MTU        1500
#else
#define ETHERNET_MTU        LWIP_ETH_MTU
#endif

/* eth flag with auto_linkup or phy_linkup */
#define ETHIF_LINK_AUTOUP   0x0000
#define ETHIF_LINK_PHYUP    0x0100

struct eth_device
{
    /* inherit from device */
    struct Device_st parent;

    /* network interface for lwip */
    struct netif *netif;

    Uint16_t flags;
    Uint8_t  link_changed;
    Uint8_t  link_status;
    Uint8_t  rx_notice;

    /* eth device interface */
    struct pbuf* (*eth_rx)(Device_t dev);
    Err_t (*eth_tx)(Device_t dev, struct pbuf* p);
};

int eth_system_device_init(void);
void eth_device_deinit(struct eth_device *dev);
Err_t eth_device_ready(struct eth_device* dev);
Err_t eth_device_init(struct eth_device * dev, const char *name);
Err_t eth_device_init_with_flag(struct eth_device *dev, const char *name, Uint16_t flag);
Err_t eth_device_linkchange(struct eth_device* dev, Bool_t up);

#ifdef __cplusplus
}
#endif

#endif /* __NETIF_ETHERNETIF_H__ */
