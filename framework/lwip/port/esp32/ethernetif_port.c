/*
 * Copyright (c) 2023, Jeejio Technology Co. Ltd
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2023-03-08     Alpha        First Verison
 *
 */

/*
 * Copyright (c) 2001-2004 Swedish Institute of Computer Science.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
 * SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
 * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY
 * OF SUCH DAMAGE.
 *
 * This file is part of the lwIP TCP/IP stack.
 *
 * Author: Adam Dunkels <adam@sics.se>
 */

#include <string.h>
#include <stdio.h>

#include <lwip/init.h>
#include <lwip/opt.h>
#include <lwip/debug.h>
#include <lwip/def.h>
#include <lwip/mem.h>
#include <lwip/pbuf.h>
#include <lwip/sys.h>
#include <lwip/netif.h>
#include <lwip/stats.h>
#include <lwip/tcpip.h>
#include <lwip/dhcp.h>
#include <lwip/netifapi.h>
#include <lwip/inet.h>
#include <netif/etharp.h>
#include <ethernetif_port.h>

// #include <ipc/completion.h>
#include "freertos/semphr.h"
#include <freertos/swldef.h>
#include <freertos/device.h>
#include <lwip/netifapi.h>


#if LWIP_IPV6
#include "lwip/ethip6.h"
#endif /* LWIP_IPV6 */

#define netifapi_netif_set_link_up(n)      netifapi_netif_common(n, netif_set_link_up, NULL)
#define netifapi_netif_set_link_down(n)    netifapi_netif_common(n, netif_set_link_down, NULL)

#ifndef LWIP_ETHTHREAD_PRIORITY
#define ETHERNETIF_THREAD_PREORITY   0x90
#else
#define ETHERNETIF_THREAD_PREORITY   LWIP_ETHTHREAD_PRIORITY
#endif

#ifndef LWIP_NO_TX_THREAD
/**
 * Tx message structure for Ethernet interface
 */
struct eth_tx_msg
{
    struct netif    *netif;
    struct pbuf     *buf;
    // struct completion ack;
    SemaphoreHandle_t ack;

};

// static struct mailbox eth_tx_thread_mb;
// static SemaphoreHandle_t eth_tx_thread_mb;

// static struct thread eth_tx_thread;
static TaskHandle_t eth_tx_handle;
#ifndef LWIP_ETHTHREAD_MBOX_SIZE
// static char eth_tx_thread_mb_pool[32 * sizeof(Ubase_t)];
// static char eth_tx_thread_stack[512];
#else
// static char eth_tx_thread_mb_pool[LWIP_ETHTHREAD_MBOX_SIZE * sizeof(Ubase_t)];
// static char eth_tx_thread_stack[RT_LWIP_ETHTHREAD_STACKSIZE];
#endif
#endif

#ifndef LWIP_NO_RX_THREAD
// static struct mailbox eth_rx_thread_mb;
static TaskHandle_t eth_rx_handle;
// static struct thread eth_rx_thread;
#ifndef LWIP_ETHTHREAD_MBOX_SIZE
// static char eth_rx_thread_mb_pool[48 * sizeof(Ubase_t)];
// static char eth_rx_thread_stack[1024];
#else
// static char eth_rx_thread_mb_pool[LWIP_ETHTHREAD_MBOX_SIZE * sizeof(Ubase_t)];
// static char eth_rx_thread_stack[RT_LWIP_ETHTHREAD_STACKSIZE];
#endif
#endif




#ifdef USING_NETDEV

#include "lwip/ip.h"
#include "lwip/init.h"
#include "lwip/netdb.h"
#include <netdev.h>

static int lwip_netdev_set_up(struct netdev *netif)
{
    netif_set_up((struct netif *)netif->user_data);
    return ERR_OK;
}

static int lwip_netdev_set_down(struct netdev *netif)
{
    netif_set_down((struct netif *)netif->user_data);
    return ERR_OK;
}

#ifndef ip_2_ip4
#define ip_2_ip4(ipaddr) (ipaddr)
#endif
static int lwip_netdev_set_addr_info(struct netdev *netif, ip_addr_t *ip_addr, ip_addr_t *netmask, ip_addr_t *gw)
{
    if (ip_addr && netmask && gw)
    {
        netif_set_addr((struct netif *)netif->user_data, ip_2_ip4(ip_addr), ip_2_ip4(netmask), ip_2_ip4(gw));
    }
    else
    {
        if (ip_addr)
        {
            netif_set_ipaddr((struct netif *)netif->user_data, ip_2_ip4(ip_addr));
        }

        if (netmask)
        {
            netif_set_netmask((struct netif *)netif->user_data, ip_2_ip4(netmask));
        }

        if (gw)
        {
            netif_set_gw((struct netif *)netif->user_data, ip_2_ip4(gw));
        }
    }

    return ERR_OK;
}

#ifdef LWIP_DNS
static int lwip_netdev_set_dns_server(struct netdev *netif, uint8_t dns_num, ip_addr_t *dns_server)
{
#if LWIP_VERSION_MAJOR == 1U /* v1.x */
    extern void dns_setserver(u8_t numdns, ip_addr_t *dnsserver);
#else /* >=2.x */
    extern void dns_setserver(uint8_t dns_num, const ip_addr_t *dns_server);
#endif /* LWIP_VERSION_MAJOR == 1U */

    dns_setserver(dns_num, dns_server);
    return ERR_OK;
}
#endif /* LWIP_DNS */

#ifdef LWIP_DHCP
static int lwip_netdev_set_dhcp(struct netdev *netif, Bool_t is_enabled)
{
    netdev_low_level_set_dhcp_status(netif, is_enabled);

    if(TRUE == is_enabled)
    {
        dhcp_start((struct netif *)netif->user_data);
    }
    else
    {
        dhcp_stop((struct netif *)netif->user_data);
    }

    return ERR_OK;
}
#endif /* LWIP_DHCP */

#ifdef USING_FINSH
#ifdef LWIP_USING_PING
extern int lwip_ping_recv(int s, int *ttl);
extern err_t lwip_ping_send(int s, ip_addr_t *addr, int size);

int lwip_netdev_ping(struct netdev *netif, const char *host, size_t data_len,
                        uint32_t timeout, struct netdev_ping_resp *ping_resp)
{
    int s, ttl, recv_len, result = 0;
    int elapsed_time;
    tick_t recv_start_tick;
#if LWIP_VERSION_MAJOR == 1U /* v1.x */
    int recv_timeout = timeout * 1000UL / TICK_PER_SECOND;
#else /* >= v2.x */
    struct timeval recv_timeout = { timeout / TICK_PER_SECOND, timeout % TICK_PER_SECOND };
#endif
    ip_addr_t target_addr;
    struct addrinfo hint, *res = NULL;
    struct sockaddr_in *h = NULL;
    struct in_addr ina;
    struct sockaddr_in local;

    configASSERT(netif);
    configASSERT(host);
    configASSERT(ping_resp);

    memset(&hint, 0x00, sizeof(hint));
    /* convert URL to IP */
    if (lwip_getaddrinfo(host, NULL, &hint, &res) != 0)
    {
        return -DEV_ERROR;
    }
    SMEMCPY(&h, &res->ai_addr, sizeof(struct sockaddr_in *));
    SMEMCPY(&ina, &h->sin_addr, sizeof(ina));
    lwip_freeaddrinfo(res);
    if (inet_aton(inet_ntoa(ina), &target_addr) == 0)
    {
        return -DEV_ERROR;
    }
    SMEMCPY(&(ping_resp->ip_addr), &target_addr, sizeof(ip_addr_t));

    /* new a socket */
    if ((s = lwip_socket(AF_INET, SOCK_RAW, IP_PROTO_ICMP)) < 0)
    {
        return -DEV_ERROR;
    }

    local.sin_len = sizeof(local);
    local.sin_family = AF_INET;
    local.sin_port = 0;
    local.sin_addr.s_addr = (netif->ip_addr.addr);
    lwip_bind(s, (struct sockaddr *)&local, sizeof(struct sockaddr_in));

    lwip_setsockopt(s, SOL_SOCKET, SO_RCVTIMEO, &recv_timeout, sizeof(recv_timeout));

    if (lwip_ping_send(s, &target_addr, data_len) == ERR_OK)
    {
        recv_start_tick = tick_get();
        if ((recv_len = lwip_ping_recv(s, &ttl)) >= 0)
        {
            elapsed_time = (tick_get() - recv_start_tick) * 1000UL / TICK_PER_SECOND;
            ping_resp->data_len = recv_len;
            ping_resp->ttl = ttl;
            ping_resp->ticks = elapsed_time;
        }
        else
        {
            result = -DEV_ETIMEOUT;
            goto __exit;
        }
    }
    else
    {
        result = -DEV_ETIMEOUT;
        goto __exit;
    }

__exit:
    lwip_close(s);

    return result;
}
#endif /* LWIP_USING_PING */

#if defined (LWIP_TCP) || defined (LWIP_UDP)
void lwip_netdev_netstat(struct netdev *netif)
{
    extern void list_tcps(void);
    extern void list_udps(void);

#ifdef LWIP_TCP
    list_tcps();
#endif
#ifdef LWIP_UDP
    list_udps();
#endif
}
#endif /* LWIP_TCP || LWIP_UDP */
#endif /* USING_FINSH */

static int lwip_netdev_set_default(struct netdev *netif)
{
    netif_set_default((struct netif *)netif->user_data);
    return ERR_OK;
}

const struct netdev_ops lwip_netdev_ops =
{
    lwip_netdev_set_up,
    lwip_netdev_set_down,

    lwip_netdev_set_addr_info,
#ifdef LWIP_DNS
    lwip_netdev_set_dns_server,
#else
    NULL,
#endif /* LWIP_DNS */

#ifdef LWIP_DHCP
    lwip_netdev_set_dhcp,
#else
    NULL,
#endif /* LWIP_DHCP */

#ifdef USING_FINSH
#ifdef LWIP_USING_PING
    lwip_netdev_ping,
#else
    NULL,
#endif /* LWIP_USING_PING */

#if defined (LWIP_TCP) || defined (LWIP_UDP)
    lwip_netdev_netstat,
#endif /* LWIP_TCP || LWIP_UDP */
#endif /* USING_FINSH */

    lwip_netdev_set_default,
};

/* synchronize lwIP network interface device and network interface device flags */
static int netdev_flags_sync(struct netif *lwip_netif)
{
    struct netdev *netdev = NULL;

    configASSERT(lwip_netif);

    netdev = netdev_get_by_name(lwip_netif->name);
    if (netdev == NULL)
    {
        return -ERR_IF;
    }

    netdev->mtu = lwip_netif->mtu;

    /* the macro definition is different from lwip-1.4.1 and lwip-2.x.x about 'flags'. */
    if(lwip_netif->flags & NETIF_FLAG_BROADCAST)
    {
        netdev->flags |= NETDEV_FLAG_BROADCAST;
    }
    if(lwip_netif->flags & NETIF_FLAG_ETHARP)
    {
        netdev->flags |= NETDEV_FLAG_ETHARP;
    }
    if(lwip_netif->flags & NETIF_FLAG_IGMP)
    {
        netdev->flags |= NETDEV_FLAG_IGMP;
    }
#if LWIP_VERSION_MAJOR >= 2U /* >= v2.x */
    if(lwip_netif->flags & NETIF_FLAG_MLD6)
    {
        netdev->flags |= NETDEV_FLAG_MLD6;
    }
#endif /* LWIP_VERSION_MAJOR >= 2U */

#if LWIP_DHCP
    netdev_low_level_set_dhcp_status(netdev, TRUE);
#else
    netdev_low_level_set_dhcp_status(netdev, FALSE);
#endif

    return ERR_OK;
}

static int netdev_add(struct netif *lwip_netif)
{
#define LWIP_NETIF_NAME_LEN 2
    int result = 0;
    struct netdev *netdev = NULL;
    char name[LWIP_NETIF_NAME_LEN + 1] = {0};

    configASSERT(lwip_netif);

    netdev = (struct netdev *)calloc(1, sizeof(struct netdev));
    if (netdev == NULL)
    {
        return -ERR_IF;
    }

#ifdef SAL_USING_LWIP
    extern int sal_lwip_netdev_set_pf_info(struct netdev *netdev);
    /* set the lwIP network interface device protocol family information */
    sal_lwip_netdev_set_pf_info(netdev);
#endif /* SAL_USING_LWIP */

    strncpy(name, lwip_netif->name, LWIP_NETIF_NAME_LEN);
    result = netdev_register(netdev, name, (void *)lwip_netif);

    /* Update netdev info after registered */
    netdev_flags_sync(lwip_netif);
    netdev->ops = &lwip_netdev_ops;
    netdev->hwaddr_len =  lwip_netif->hwaddr_len;
    SMEMCPY(netdev->hwaddr, lwip_netif->hwaddr, lwip_netif->hwaddr_len);
    netdev->ip_addr = lwip_netif->ip_addr;
    netdev->gw = lwip_netif->gw;
    netdev->netmask = lwip_netif->netmask;

    return result;
}

static void netdev_del(struct netif *lwip_netif)
{
    char name[LWIP_NETIF_NAME_LEN + 1];
    struct netdev *netdev;

    configASSERT(lwip_netif);

    strncpy(name, lwip_netif->name, LWIP_NETIF_NAME_LEN);
    netdev = netdev_get_by_name(name);
    netdev_unregister(netdev);
    free(netdev);
}
#endif /* USING_NETDEV */

static err_t ethernetif_linkoutput(struct netif *netif, struct pbuf *p)
{
#ifndef LWIP_NO_TX_THREAD
    struct eth_tx_msg msg;
    BaseType_t err = pdFALSE;

    configASSERT(netif != NULL);

    /* send a message to eth tx thread */
    msg.netif = netif;
    msg.buf   = p;

    // completion_init(&msg.ack);
    msg.ack = xSemaphoreCreateBinary();
    // if (mb_send(&eth_tx_thread_mb, (Ubase_t) &msg) == DEV_EOK)
    err = xTaskNotify((TaskHandle_t )eth_tx_handle, (Ubase_t)&msg, (eNotifyAction)eSetValueWithOverwrite);
    if (err == DEV_EOK)
    {
        /* waiting for ack */
        // completion_wait(&msg.ack, WAITING_FOREVER);

        xSemaphoreTake(msg.ack, portMAX_DELAY);

    }
#else
    struct eth_device* enetif;

    configASSERT(netif != NULL);
    enetif = (struct eth_device*)netif->state;

    if (enetif->eth_tx(&(enetif->parent), p) != DEV_EOK)
    {
        return ERR_IF;
    }
#endif
    return ERR_OK;
}

static err_t eth_netif_device_init(struct netif *netif)
{
    struct eth_device *ethif;

    ethif = (struct eth_device*)netif->state;
    if (ethif != NULL)
    {
        Device_t device;

#ifdef USING_NETDEV
        /* network interface device register */
        netdev_add(netif);
#endif /* USING_NETDEV */

        /* get device object */
        device = (Device_t) ethif;
        if (lHalDeviceOpen(device, DEVICE_FLAG_RDWR) != DEV_EOK)
        {
            return ERR_IF;
        }

        /* copy device flags to netif flags */
        netif->flags = (ethif->flags & 0xff);
        netif->mtu = ETHERNET_MTU;

        /* set output */
        netif->output       = etharp_output;

#if LWIP_IPV6
        netif->output_ip6 = ethip6_output;
        netif->ip6_autoconfig_enabled = 1;
        netif_create_ip6_linklocal_address(netif, 1);

#if LWIP_IPV6_MLD
        netif->flags |= NETIF_FLAG_MLD6;

        /*
        * For hardware/netifs that implement MAC filtering.
        * All-nodes link-local is handled by default, so we must let the hardware know
        * to allow multicast packets in.
        * Should set mld_mac_filter previously. */
        if (netif->mld_mac_filter != NULL)
        {
            ip6_addr_t ip6_allnodes_ll;
            ip6_addr_set_allnodes_linklocal(&ip6_allnodes_ll);
            netif->mld_mac_filter(netif, &ip6_allnodes_ll, NETIF_ADD_MAC_FILTER);
        }
#endif /* LWIP_IPV6_MLD */

#endif /* LWIP_IPV6 */

        /* set default netif */
        if (netif_default == NULL)
            netif_set_default(ethif->netif);

        /* set interface up */
        netif_set_up(ethif->netif);

#if LWIP_DHCP
        /* if this interface uses DHCP, start the DHCP client */
        dhcp_start(ethif->netif);
#endif

        if (ethif->flags & ETHIF_LINK_PHYUP)
        {
            /* set link_up for this netif */
            netif_set_link_up(ethif->netif);
        }

#ifdef USING_NETDEV
        /* network interface device flags synchronize */
        netdev_flags_sync(netif);
#endif /* USING_NETDEV */

        return ERR_OK;
    }

    return ERR_IF;
}

/* Keep old drivers compatible */
Err_t eth_device_init_with_flag(struct eth_device *dev, const char *name, Uint16_t flags)
{
    struct netif* netif;
#if LWIP_NETIF_HOSTNAME
#define LWIP_HOSTNAME_LEN 16
    char *hostname = NULL;
    netif = (struct netif*) calloc (1, sizeof(struct netif) + LWIP_HOSTNAME_LEN);
#else
    netif = (struct netif*) calloc (1, sizeof(struct netif));
#endif
    if (netif == NULL)
    {
        printf("malloc netif failed\n");
        return -DEV_ERROR;
    }

    /* set netif */
    dev->netif = netif;
    dev->flags = flags;
    /* link changed status of device */
    dev->link_changed = 0x00;
    /* avoid send the same mail to mailbox */
    dev->rx_notice = 0x00;
    dev->parent.type = Device_Class_NetIf;
    /* register to device manager */
    lHalDeviceRegister(&(dev->parent), name, DEVICE_FLAG_RDWR);

    /* set name */
    netif->name[0] = name[0];
    netif->name[1] = name[1];

    /* set hw address to 6 */
    netif->hwaddr_len   = 6;
    /* maximum transfer unit */
    netif->mtu          = ETHERNET_MTU;

    /* set linkoutput */
    netif->linkoutput   = ethernetif_linkoutput;

    /* get hardware MAC address */
    lHalDeviceControl(&(dev->parent), NIOCTL_GADDR, netif->hwaddr);

#if LWIP_NETIF_HOSTNAME
    /* Initialize interface hostname */
    hostname = (char *)netif + sizeof(struct netif);
    sprintf(hostname, "rtthread_%02x%02x", name[0], name[1]);
    netif->hostname = hostname;
#endif /* LWIP_NETIF_HOSTNAME */

    /* if tcp thread has been started up, we add this netif to the system */
    if (xTaskGetHandle("tcpip") != NULL)
    {
#if LWIP_VERSION_MAJOR == 1U /* v1.x */
        struct ip_addr ipaddr, netmask, gw;
#else /* >= v2.x */
        ip4_addr_t ipaddr, netmask, gw;
#endif /* LWIP_VERSION_MAJOR == 1U */

#if !LWIP_DHCP
        ipaddr.addr = inet_addr(RT_LWIP_IPADDR);
        gw.addr = inet_addr(RT_LWIP_GWADDR);
        netmask.addr = inet_addr(RT_LWIP_MSKADDR);
#else
        IP4_ADDR(&ipaddr, 0, 0, 0, 0);
        IP4_ADDR(&gw, 0, 0, 0, 0);
        IP4_ADDR(&netmask, 0, 0, 0, 0);
#endif
        netifapi_netif_add(netif, &ipaddr, &netmask, &gw, dev, eth_netif_device_init, tcpip_input);
    }

    return DEV_EOK;
}

Err_t eth_device_init(struct eth_device * dev, const char *name)
{
    Uint16_t flags = NETIF_FLAG_BROADCAST | NETIF_FLAG_ETHARP;

#if LWIP_IGMP
    /* IGMP support */
    flags |= NETIF_FLAG_IGMP;
#endif

    return eth_device_init_with_flag(dev, name, flags);
}

void eth_device_deinit(struct eth_device *dev)
{
    struct netif* netif = dev->netif;

#if LWIP_DHCP
    dhcp_stop(netif);
    dhcp_cleanup(netif);
#endif
    netif_set_down(netif);
    netif_remove(netif);
#ifdef USING_NETDEV
    netdev_del(netif);
#endif
    lHalDeviceClose(&(dev->parent));
    lHalDeviceUnregister(&(dev->parent));
    free(netif);
}

#ifndef LWIP_NO_RX_THREAD
Err_t eth_device_ready(struct eth_device* dev)
{
    BaseType_t err = pdFALSE;
    if (dev->netif)
    {
        if(dev->rx_notice == FALSE)
        {
            dev->rx_notice = TRUE;
            // return mb_send(&eth_rx_thread_mb, (Ubase_t)dev);
            err = xTaskNotify((TaskHandle_t )eth_rx_handle, (Ubase_t)dev, (eNotifyAction)eSetValueWithOverwrite);
            return err;
        }
        else
            return DEV_EOK;
        /* post message to Ethernet thread */
    }
    else
        return -DEV_ERROR; /* netif is not initialized yet, just return. */
}

Err_t eth_device_linkchange(struct eth_device* dev, Bool_t up)
{
    BaseType_t err = pdFALSE;

    configASSERT(dev != NULL);

    portDISABLE_INTERRUPTS();
    dev->link_changed = 0x01;
    if (up == TRUE)
        dev->link_status = 0x01;
    else
        dev->link_status = 0x00;
    portENABLE_INTERRUPTS();

    /* post message to ethernet thread */
    // return mb_send(&eth_rx_thread_mb, (Ubase_t)dev);
    err = xTaskNotify((TaskHandle_t )eth_rx_handle, (Ubase_t)dev, (eNotifyAction)eSetValueWithOverwrite);

    return err;
}
#else
/* NOTE: please not use it in interrupt when no RxThread exist */
Err_t eth_device_linkchange(struct eth_device* dev, Bool_t up)
{
    if (up == TRUE)
        netifapi_netif_set_link_up(dev->netif);
    else
        netifapi_netif_set_link_down(dev->netif);

    return DEV_EOK;
}
#endif

#ifndef LWIP_NO_TX_THREAD
/* Ethernet Tx Thread */
static void eth_tx_thread_entry(void* parameter)
{
    struct eth_tx_msg* msg;
    BaseType_t err;
    while (1)
    {
        // if (rt_mb_recv(&eth_tx_thread_mb, (Ubase_t *)&msg, WAITING_FOREVER) == DEV_EOK)
        err = xTaskNotifyWait((uint32_t )0x00, (uint32_t)0xffffffff, (Ubase_t *)&msg, (TickType_t)portMAX_DELAY);
        if (err == DEV_EOK)
        {
            struct eth_device* enetif;

            configASSERT(msg->netif != NULL);
            configASSERT(msg->buf   != NULL);

            enetif = (struct eth_device*)msg->netif->state;
            if (enetif != NULL)
            {
                /* call driver's interface */
                if (enetif->eth_tx(&(enetif->parent), msg->buf) != DEV_EOK)
                {
                    /* transmit eth packet failed */
                }
            }

            /* send ACK */
            // completion_done(&msg->ack);
            xSemaphoreGive(msg->ack); 

        }
    }
}
#endif

#ifndef LWIP_NO_RX_THREAD
/* Ethernet Rx Thread */
static void eth_rx_thread_entry(void* parameter)
{
    BaseType_t err;
    struct eth_device* device;

    while (1)
    {
        err = xTaskNotifyWait((uint32_t )0x00, (uint32_t)0xffffffff, (Ubase_t *)&device, (TickType_t)portMAX_DELAY);
        // if (rt_mb_recv(&eth_rx_thread_mb, (Ubase_t *)&device, WAITING_FOREVER) == DEV_EOK)
        if (err == DEV_EOK)
        {
            struct pbuf *p;

            /* check link status */
            if (device->link_changed)
            {
                int status;

                portDISABLE_INTERRUPTS();
                status = device->link_status;
                device->link_changed = 0x00;
                portENABLE_INTERRUPTS();

                if (status)
                    netifapi_netif_set_link_up(device->netif);
                else
                    netifapi_netif_set_link_down(device->netif);
            }

            portDISABLE_INTERRUPTS();
            /* 'rx_notice' will be modify in the interrupt or here */
            device->rx_notice = FALSE;
            portENABLE_INTERRUPTS();

            /* receive all of buffer */
            while (1)
            {
                if(device->eth_rx == NULL) break;

                p = device->eth_rx(&(device->parent));
                if (p != NULL)
                {
                    /* notify to upper layer */
                    if( device->netif->input(p, device->netif) != ERR_OK )
                    {
                        LWIP_DEBUGF(NETIF_DEBUG, ("ethernetif_input: Input error\n"));
                        pbuf_free(p);
                        p = NULL;
                    }
                }
                else break;
            }
        }
        else
        {
            LWIP_ASSERT("Should not happen!\n",0);
        }
    }
}
#endif

/* this function does not need,
 * use eth_system_device_init_private()
 * call by lwip_system_init().
 */
int eth_system_device_init(void)
{
    return 0;
}

int eth_system_device_init_private(void)
{
    Err_t result = DEV_EOK;

    /* initialize Rx thread. */
#ifndef LWIP_NO_RX_THREAD
    /* initialize mailbox and create Ethernet Rx thread */
    // result = mb_init(&eth_rx_thread_mb, "erxmb",
    //                     &eth_rx_thread_mb_pool[0], sizeof(eth_rx_thread_mb_pool)/sizeof(Ubase_t),
    //                     IPC_FLAG_FIFO);
    // configASSERT(result == DEV_EOK);

    // result = thread_init(&eth_rx_thread, "erx", eth_rx_thread_entry, NULL,
    //                         &eth_rx_thread_stack[0], sizeof(eth_rx_thread_stack),
    //                         ETHERNETIF_THREAD_PREORITY, 16);
    result = xTaskCreate(eth_rx_thread_entry, "erx", 4096, NULL, 5, &eth_rx_handle);

    // configASSERT(result == DEV_EOK);
    // result = thread_startup(&eth_rx_thread);
    // configASSERT(result == DEV_EOK);




#endif

    /* initialize Tx thread */
#ifndef LWIP_NO_TX_THREAD
    /* initialize mailbox and create Ethernet Tx thread */
    // result = mb_init(&eth_tx_thread_mb, "etxmb",
    //                     &eth_tx_thread_mb_pool[0], sizeof(eth_tx_thread_mb_pool)/sizeof(Ubase_t),
    //                     IPC_FLAG_FIFO);
    // configASSERT(result == DEV_EOK);

    // result = thread_init(&eth_tx_thread, "etx", eth_tx_thread_entry, NULL,
    //                         &eth_tx_thread_stack[0], sizeof(eth_tx_thread_stack),
    //                         ETHERNETIF_THREAD_PREORITY, 16);
    result = xTaskCreate(eth_tx_thread_entry, "etx", 4096, NULL, 4, &eth_tx_handle);

    // configASSERT(result == DEV_EOK);

    // result = thread_startup(&eth_tx_thread);
    // configASSERT(result == DEV_EOK);
    
#endif

    vTaskStartScheduler();


    return (int)result;
}

void set_if(char* netif_name, char* ip_addr, char* gw_addr, char* nm_addr)
{
#if LWIP_VERSION_MAJOR == 1U /* v1.x */
    struct ip_addr *ip;
    struct ip_addr addr;
#else /* >= v2.x */
    ip4_addr_t *ip;
    ip4_addr_t addr;
#endif /* LWIP_VERSION_MAJOR == 1U */

    struct netif * netif = netif_list;

    if(strlen(netif_name) > sizeof(netif->name))
    {
        printf("network interface name too long!\r\n");
        return;
    }

    while(netif != NULL)
    {
        if(strncmp(netif_name, netif->name, sizeof(netif->name)) == 0)
            break;

        netif = netif->next;
        if( netif == NULL )
        {
            printf("network interface: %s not found!\r\n", netif_name);
            return;
        }
    }

#if LWIP_VERSION_MAJOR == 1U /* v1.x */
    ip = (struct ip_addr *)&addr;
#else /* >= v2.x */
    ip = (ip4_addr_t *)&addr;
#endif /* LWIP_VERSION_MAJOR == 1U */


    /* set ip address */
    if ((ip_addr != NULL) && inet_aton(ip_addr, &addr))
    {
        netif_set_ipaddr(netif, ip);
    }

    /* set gateway address */
    if ((gw_addr != NULL) && inet_aton(gw_addr, &addr))
    {
        netif_set_gw(netif, ip);
    }

    /* set netmask address */
    if ((nm_addr != NULL) && inet_aton(nm_addr, &addr))
    {
        netif_set_netmask(netif, ip);
    }
}

#ifdef USING_FINSH
#include <finsh.h>
FINSH_FUNCTION_EXPORT(set_if, set network interface address);

#if LWIP_DNS
#include <lwip/dns.h>
void set_dns(uint8_t dns_num, char* dns_server)
{
    ip_addr_t addr;

    if ((dns_server != NULL) && ipaddr_aton(dns_server, &addr))
    {
        dns_setserver(dns_num, &addr);
    }
}
FINSH_FUNCTION_EXPORT(set_dns, set DNS server address);
#endif

void list_if(void)
{
    Ubase_t index;
    struct netif * netif;

    taskENTER_CRITICAL();

    netif = netif_list;

    while( netif != NULL )
    {
        printf("network interface: %c%c%s\n",
                   netif->name[0],
                   netif->name[1],
                   (netif == netif_default)?" (Default)":"");
        printf("MTU: %d\n", netif->mtu);
        printf("MAC: ");
        for (index = 0; index < netif->hwaddr_len; index ++)
            printf("%02x ", netif->hwaddr[index]);
        printf("\nFLAGS:");
        if (netif->flags & NETIF_FLAG_UP) printf(" UP");
        else printf(" DOWN");
        if (netif->flags & NETIF_FLAG_LINK_UP) printf(" LINK_UP");
        else printf(" LINK_DOWN");
        if (netif->flags & NETIF_FLAG_ETHARP) printf(" ETHARP");
        if (netif->flags & NETIF_FLAG_BROADCAST) printf(" BROADCAST");
        if (netif->flags & NETIF_FLAG_IGMP) printf(" IGMP");
        printf("\n");
        printf("ip address: %s\n", ipaddr_ntoa(&(netif->ip_addr)));
        printf("gw address: %s\n", ipaddr_ntoa(&(netif->gw)));
        printf("net mask  : %s\n", ipaddr_ntoa(&(netif->netmask)));
#if LWIP_IPV6
        {
            ip6_addr_t *addr;
            int addr_state;
            int i;

            addr = (ip6_addr_t *)&netif->ip6_addr[0];
            addr_state = netif->ip6_addr_state[0];

            printf("\nipv6 link-local: %s state:%02X %s\n", ip6addr_ntoa(addr),
            addr_state, ip6_addr_isvalid(addr_state)?"VALID":"INVALID");

            for(i=1; i<LWIP_IPV6_NUM_ADDRESSES; i++)
            {
                addr = (ip6_addr_t *)&netif->ip6_addr[i];
                addr_state = netif->ip6_addr_state[i];

                printf("ipv6[%d] address: %s state:%02X %s\n", i, ip6addr_ntoa(addr),
                addr_state, ip6_addr_isvalid(addr_state)?"VALID":"INVALID");
            }

        }
        printf("\r\n");
#endif /* LWIP_IPV6 */
        netif = netif->next;
    }

#if LWIP_DNS
    {
#if LWIP_VERSION_MAJOR == 1U /* v1.x */
        struct ip_addr ip_addr;

        for(index=0; index<DNS_MAX_SERVERS; index++)
        {
            ip_addr = dns_getserver(index);
            printf("dns server #%d: %s\n", index, ipaddr_ntoa(&(ip_addr)));
        }
#else /* >= v2.x */
        const ip_addr_t *ip_addr;

        for(index=0; index<DNS_MAX_SERVERS; index++)
        {
            ip_addr = dns_getserver(index);
            printf("dns server #%d: %s\n", index, inet_ntoa(ip_addr));
        }
#endif /* LWIP_VERSION_MAJOR == 1U */

    }
#endif /**< #if LWIP_DNS */

    taskEXIT_CRITICAL();
}
FINSH_FUNCTION_EXPORT(list_if, list network interface information);

#if LWIP_TCP
#include <lwip/tcp.h>
#if LWIP_VERSION_MAJOR == 1U /* v1.x */
#include <lwip/tcp_impl.h>
#else /* >= v2.x */
#include <lwip/priv/tcp_priv.h>
#endif /* LWIP_VERSION_MAJOR == 1U */

void list_tcps(void)
{
    Uint32_t num = 0;
    struct tcp_pcb *pcb;
    char local_ip_str[16];
    char remote_ip_str[16];

    extern struct tcp_pcb *tcp_active_pcbs;
    extern union tcp_listen_pcbs_t tcp_listen_pcbs;
    extern struct tcp_pcb *tcp_tw_pcbs;

    taskENTER_CRITICAL();
    printf("Active PCB states:\n");
    for(pcb = tcp_active_pcbs; pcb != NULL; pcb = pcb->next)
    {
        strcpy(local_ip_str, ipaddr_ntoa(&(pcb->local_ip)));
        strcpy(remote_ip_str, ipaddr_ntoa(&(pcb->remote_ip)));

        printf("#%d %s:%d <==> %s:%d snd_nxt 0x%08X rcv_nxt 0x%08X ",
                   num++,
                   local_ip_str,
                   pcb->local_port,
                   remote_ip_str,
                   pcb->remote_port,
                   pcb->snd_nxt,
                   pcb->rcv_nxt);
        printf("state: %s\n", tcp_debug_state_str(pcb->state));
    }

    printf("Listen PCB states:\n");
    num = 0;
    for(pcb = (struct tcp_pcb *)tcp_listen_pcbs.pcbs; pcb != NULL; pcb = pcb->next)
    {
        printf("#%d local port %d ", num++, pcb->local_port);
        printf("state: %s\n", tcp_debug_state_str(pcb->state));
    }

    printf("TIME-WAIT PCB states:\n");
    num = 0;
    for(pcb = tcp_tw_pcbs; pcb != NULL; pcb = pcb->next)
    {
        strcpy(local_ip_str, ipaddr_ntoa(&(pcb->local_ip)));
        strcpy(remote_ip_str, ipaddr_ntoa(&(pcb->remote_ip)));

        printf("#%d %s:%d <==> %s:%d snd_nxt 0x%08X rcv_nxt 0x%08X ",
                   num++,
                   local_ip_str,
                   pcb->local_port,
                   remote_ip_str,
                   pcb->remote_port,
                   pcb->snd_nxt,
                   pcb->rcv_nxt);
        printf("state: %s\n", tcp_debug_state_str(pcb->state));
    }
    taskEXIT_CRITICAL();
}
FINSH_FUNCTION_EXPORT(list_tcps, list all of tcp connections);
#endif /* LWIP_TCP */

#if LWIP_UDP
#include "lwip/udp.h"
void list_udps(void)
{
    struct udp_pcb *pcb;
    Uint32_t num = 0;
    char local_ip_str[16];
    char remote_ip_str[16];

    taskENTER_CRITICAL();
    printf("Active UDP PCB states:\n");
    for (pcb = udp_pcbs; pcb != NULL; pcb = pcb->next)
    {
        strcpy(local_ip_str, ipaddr_ntoa(&(pcb->local_ip)));
        strcpy(remote_ip_str, ipaddr_ntoa(&(pcb->remote_ip)));

        printf("#%d %d %s:%d <==> %s:%d \n",
                   num, (int)pcb->flags,
                   local_ip_str,
                   pcb->local_port,
                   remote_ip_str,
                   pcb->remote_port);

        num++;
    }
    taskEXIT_CRITICAL();
}
FINSH_FUNCTION_EXPORT(list_udps, list all of udp connections);
#endif /* LWIP_UDP */

#endif
